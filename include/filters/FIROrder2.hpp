#include <utility>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <vector>

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <kdl/frames.hpp>

#ifndef FIRORDER2_H
#define FIRORDER2_H

template <unsigned int FILTER_SIZE>
class FIROrder2
{
  public:
    FIROrder2(double cutoff, double quality, double sample_time)
    {
        this->data_size = FILTER_SIZE;
        this->Q = quality;
        this->updateParameters(sample_time, quality, cutoff);
        this->data.resize(data_size);
        this->output.resize(data_size);
        this->valid = false;

        for (int i = 0; i < this->data_size; i++)
        {
            this->data[i].resize(4);
        }
    }

    ~FIROrder2()
    {
    }

    void updateParameters(double sample_time, double Q, double cutoff)
    {
        this->Q = Q;
        this->Fs = 1.0 / sample_time;
        this->Fc = cutoff;
        this->W = tan(M_PI * this->Fc / this->Fs);
        this->N = 1.0 / (pow(this->W, 2) + this->W / this->Q + 1);
        this->B0 = this->N * pow(this->W, 2);
        this->B1 = 2 * this->B0;
        this->B2 = this->B0;
        this->A1 = 2 * this->N * (pow(this->W, 2) - 1);
        this->A2 = this->N * (pow(this->W, 2) - this->W / this->Q + 1);
        printf("SETTED %f, %f, %f\n", this->Fs, this->Fc, this->Q);
    }

    void setInput(int i, double X)
    {

        double Acc =
            X * this->B0 + this->data[i][0] * this->B1 + this->data[i][1] * this->B2 - this->data[i][2] * this->A1 - this->data[i][3] * this->A2;
        this->data[i][3] = this->data[i][2];
        this->data[i][2] = Acc;
        this->data[i][1] = this->data[i][0];
        this->data[i][0] = X;
        this->output[i] = Acc;
        // printf("Setting %f -> %f\n", X, Acc);
    }

    void setInput(std::vector<double> &input)
    {
        for (int i = 0; i < this->data_size; i++)
        {
            this->setInput(i, input[i]);
        }
    }

    void setInput(KDL::Frame &t)
    {
        std::vector<double> v;
        FIROrder2::tfToVector(t, v);
        this->setInput(v);
    }

    void setInitialData(std::vector<double> &Xs)
    {
        for (int i = 0; i < this->data_size; i++)
        {
            this->data[i][0] = Xs[i];
            this->data[i][1] = Xs[i];
            this->data[i][2] = Xs[i];
            this->data[i][3] = Xs[i];
            this->output[i] = Xs[i];
            // printf("Set initial %i = %f\n", i, Xs[i]);
        }
        this->valid = true;
    }

    void setInitialData(KDL::Frame &t)
    {
        printf("Reset!!");
        std::vector<double> v;
        FIROrder2::tfToVector(t, v);
        this->setInitialData(v);
    }

    void getOutput(KDL::Frame &t)
    {
        FIROrder2::vectorToTf(this->output, t);
    }

    bool isValid()
    {
        return this->valid;
    }

    static void tfToVector(KDL::Frame &t, std::vector<double> &v)
    {
        v.clear();
        v.resize(FILTER_SIZE);
        v[0] = t.p.x();
        v[1] = t.p.y();
        v[2] = t.p.z();
        double qx, qy, qz, qw;
        t.M.GetQuaternion(qx, qy, qz, qw);
        v[3] = qx;
        v[4] = qy;
        v[5] = qz;
        v[6] = qw;
    }

    static void vectorToTf(std::vector<double> &v, KDL::Frame &t)
    {
        t.p = KDL::Vector(v[0], v[1], v[2]);
        t.M = KDL::Rotation::Quaternion(v[3], v[4], v[5], v[6]);
    }

    double Fc;
    double Fs;
    double Q;
    double W;
    double N;
    double B0;
    double B1;
    double B2;
    double A1;
    double A2;
    bool valid;

    std::vector<double> output;
    std::vector<std::vector<double>> data;

  private:
    int data_size;
};

#endif /* FIRORDER2_H */
