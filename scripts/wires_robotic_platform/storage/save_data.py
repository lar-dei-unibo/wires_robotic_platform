#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

import rospy

from std_msgs.msg import *


import random
import numpy
from numpy import *
import numpy as np
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.storage.mongo import MessageStorage
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState
import PyKDL
from PyKDL import Frame, Vector, Rotation
import threading

from std_msgs.msg import String


import tf
import cv2
import time


from rospkg import RosPack

from scipy.io import loadmat, savemat 

class SaveData():

    def __init__(self, file_name):
        self.file_name = file_name
        self.file_handler = open("{}.txt".format(file_name), "a")

    def saveList(self, data=[]):
        self.file_handler = open("{}.txt".format(self.file_name), "a")
        if len(data) == 0:
            pass
        else:
            fmt_str = ""
            for i in data:
                fmt_str += "%.18e "
            try:
                np.savetxt(self.file_handler,
                           [data],
                           fmt=fmt_str,
                           delimiter=' ')
            except Exception as e:
                print(e)
        self.file_handler.close()
        
class SaveAsMat():

    def __init__(self, file_name):
        if not file_name.endswith(".mat"):
            self.file_name = "{}.mat".format(file_name)
        else:
            self.file_name = file_name
        self.data_dict = {}
        savemat(self.file_name,self.data_dict)  # write
 
    def saveCell(self,dataname, newdata):
        try:
            if dataname in self.data_dict.keys():
                tmp = self.data_dict[dataname]
                tmp.append(newdata)
                self.data_dict[dataname] = tmp
            else:
                self.data_dict[dataname] = [newdata]

            savemat(self.file_name,self.data_dict) 
        except Exception as e:
            print e

