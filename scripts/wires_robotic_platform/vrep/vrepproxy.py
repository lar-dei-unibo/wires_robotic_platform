#!/usr/bin/env python
# license removed for brevity
from wires_robotic_platform.vrep import vrep
from wires_robotic_platform.utils.logger import Logger
import numpy as np
import math
from PyKDL import Frame, Vector, Rotation
import tf


class VrepProxy(object):
    HOST_IP = '127.0.0.1'
    HOST_PORT = 19997
    CONNECTION_HANDSHAKE = True
    AUTO_RECONNECTION = False
    CONNECTION_TIMEOUT_MS = 5000
    THREAD_CYCLE_MS = 5
    handle = None

    def __init__(self):
        vrep.simxFinish(-1)
        self.clientID = vrep.simxStart(VrepProxy.HOST_IP, VrepProxy.HOST_PORT, VrepProxy.CONNECTION_HANDSHAKE,
                                       not VrepProxy.AUTO_RECONNECTION, VrepProxy.CONNECTION_TIMEOUT_MS, VrepProxy.THREAD_CYCLE_MS)

    def isConnected(self):
        return self.clientID != -1

    def closeConnection(self):
        vrep.simxFinish(self.clientID)

    def startSimulation(self):
        vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)

    def stopSimulation(self):
        vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_blocking)

    def _parseResult(self, raw_result):
        result = {"code": -1, "data": []}
        if isinstance(raw_result, int):
            result["code"] = raw_result
        elif len(raw_result) > 1:
            result["code"] = raw_result[0]
            for i in range(1, len(raw_result)):
                result["data"].append(raw_result[i])
        return result

    def setSynchronousMode(self, status):
        self.callApi("simxSynchronous",
                     enable=status)

    def triggerSynch(self):
        self.callApi("simxSynchronousTrigger")

    def callApi(self, api_name="", **kwargs):
        """ Calls a generic Vrep Remote Api trapping errors"""
        if len(api_name) > 0:
            try:
                method = getattr(vrep, api_name)
            except AttributeError as err:
                method = None
                Logger.error(
                    "Api Call error for: {}: {}".format(api_name, str(err)))
            if method != None:
                kwargs["clientID"] = self.getClientID()
                results = self._parseResult(method(**kwargs))
                if results.get("code") != vrep.simx_return_ok:
                    Logger.error("({}) Return code error for {}".format(
                        results.get("code"), api_name))
                return results
            else:
                Logger.error("Api Call error for: {}".format(api_name))
        return None

    @staticmethod
    def connect():
        return VrepProxy.getInstance()

    @staticmethod
    def getInstance():
        if VrepProxy.handle == None:
            VrepProxy.handle = VrepProxy()
        return VrepProxy.handle

    @staticmethod
    def getClientID():
        return VrepProxy.getInstance().clientID


class VrepObject(object):
    """ Vrep Generic Object wrapper """

    def __init__(self, name):
        self.name = name
        rcode, self.handle = vrep.simxGetObjectHandle(
            VrepProxy.getClientID(), name, vrep.simx_opmode_blocking)
        self.valid = self.handle != -1 and rcode == vrep.simx_return_ok

    def getFrame(self, relative_to=None):
        """ Gets the current PyKDL.Frame of the object """
        relative_code = -1
        if relative_to != None:
            if isinstance(relative_to, int):
                relative_code = relative_to

        result_pos = VrepProxy.getInstance().callApi("simxGetObjectPosition",
                                                     objectHandle=self.handle,
                                                     relativeToObjectHandle=relative_code,
                                                     operationMode=vrep.simx_opmode_blocking)
        result_or = VrepProxy.getInstance().callApi("simxGetObjectOrientation",
                                                    objectHandle=self.handle,
                                                    relativeToObjectHandle=relative_code,
                                                    operationMode=vrep.simx_opmode_blocking)

        pos = np.array(result_pos.get("data")[0])
        euler = np.array(result_or.get("data")[0])

        frame = Frame()
        frame.p = Vector(pos[0], pos[1], pos[2])
        frame.M.DoRotX(euler[0])
        frame.M.DoRotY(euler[1])
        frame.M.DoRotZ(euler[2])

        return frame

    def setFrame(self, frame, relative_to=None):
        """ Sets the current PyKDL.Frame of the object """
        relative_code = -1
        if relative_to != None:
            if isinstance(relative_to, int):
                relative_code = relative_to

        pos = [frame.p.x(), frame.p.y(), frame.p.z()]
        euler = list(reversed(frame.M.GetEulerZYX()))

        VrepProxy.getInstance().callApi("simxSetObjectPosition",
                                        objectHandle=self.handle,
                                        relativeToObjectHandle=relative_code,
                                        position=pos,
                                        operationMode=vrep.simx_opmode_blocking)

        VrepProxy.getInstance().callApi("simxCallScriptFunction",
                                        scriptDescription="PythonServer",
                                        options=vrep.sim_scripttype_childscript,
                                        functionName="set_object_quaternion",
                                        inputInts=[self.handle, relative_code],
                                        inputFloats=frame.M.GetQuaternion(),
                                        inputStrings=[],
                                        inputBuffer=bytearray(),
                                        operationMode=vrep.simx_opmode_blocking)

        print euler


class VrepShape(VrepObject):
    """ Vrep Shape Wrapper """

    def __init__(self, name):
        super(VrepShape, self).__init__(name)


class VrepJoint(VrepObject):
    """ Vrep Joint Wrapper """
    FAKE_VELOCITY_MAX = 10

    def __init__(self, name):
        super(VrepJoint, self).__init__(name)

    def getPosition(self):

        result_pos = VrepProxy.getInstance().callApi("simxGetJointPosition",
                                                     jointHandle=self.handle,
                                                     operationMode=vrep.simx_opmode_blocking)
        return result_pos.get("data")[0]

    def setForce(self, f):
        v = f * VrepJoint.FAKE_VELOCITY_MAX

        VrepProxy.getInstance().callApi("simxSetJointForce",
                                        jointHandle=self.handle,
                                        force=math.fabs(f),
                                        operationMode=vrep.simx_opmode_blocking)
        self.setVelocity(v)

    def setVelocity(self, v):
        VrepProxy.getInstance().callApi("simxSetJointTargetVelocity",
                                        jointHandle=self.handle,
                                        targetVelocity=v,
                                        operationMode=vrep.simx_opmode_blocking)
