#!/usr/bin/env python
# -*- encoding: utf-8 -*-


import rospy
from wires_robotic_platform.utils.logger import Logger
from std_msgs.msg import Header, Float64, Float32,  Float64MultiArray
import math
import sys
import time
import numpy as np
from keras.models import load_model
from keras.utils import plot_model
import tensorflow as tf


class TactileRNN(object):

    def __init__(self, model_h5_file_path, seq_len, bias_window_size=10, tactile_num_cells=16):
        self.data = RNNDataSequence(seq_len)
        self.pred_history = np.zeros(bias_window_size)
        self.bias_window_size = bias_window_size
        self.init_ct = bias_window_size
        self.model = None
        self.reg_name = model_h5_file_path.split("/")[-1]
        try:
            self.model = load_model(model_h5_file_path)
            self.model.predict(np.zeros((1, seq_len, tactile_num_cells)))  # void prediction to create the graph (multithread problem in tensorflow with GPU)
            # self.model._make_predict_function() # https://github.com/fchollet/keras/issues/2397, https://github.com/fchollet/keras/issues/6124
            # self.graph = tf.get_default_graph()
            print("Regressors found and loaded!\n" + model_h5_file_path)
        except Exception as e:
            Logger.error("None regressor found!\n " + model_h5_file_path)
            print e

    def update(self, msg):
        self.data.tactile_update(msg)
        if not self.isReady():
            self.getPrediction()  # for initialization purpose

    def getPrediction(self):
        pred = self.model.predict(self.data.getWindow())
        pred = pred.item()  # np.ravel(pred, order='F') # flatten the prediction in a single-element-array
        self._update_history(pred)
        return pred

    def _update_history(self, last_pred):
        tmp = np.append(self.pred_history, [last_pred], axis=0)  # add the new prediction in the queue
        self.pred_history = np.delete(tmp, 0, 0)  # remove the oldes prediction from the queue
        if not self.isReady():
            print "{} initialization {}".format(self.reg_name, self.init_ct)
            self.init_ct -= 1

    def reset(self):
        self.init_ct = self.bias_window_size

    def isReady(self):
        return self.init_ct <= 0

    def getMean(self):
        mean = sum(self.pred_history) / len(self.pred_history)
        return mean


class RNNDataSequence(object):

    def __init__(self, seq_len):
        self.window = np.zeros((seq_len, 16))
        self.new_sample = None

    def tactile_update(self, msg):
        self.new_sample = np.ravel(msg.data).reshape((1, 16))
        self._window_update(self.new_sample)

    def _window_update(self, new_sample):
        tmp_window = np.append(self.window, new_sample, axis=0)
        self.window = np.delete(tmp_window, 0, 0)

    def getWindow(self):
        w = np.reshape(self.window, (1, self.window.shape[0], self.window.shape[1]))  # vector -> tensor
        return w
