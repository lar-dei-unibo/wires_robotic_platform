import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import math
import PyKDL
import tf
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.robots.controllers as controllers
from wires_robotic_platform.utils.logger import Logger

from wires_robotic_platform.srv import IKService, IKServiceResponse


""" GLOBAL PARAMETERS """

ROBOT_MARKET_LIBRARY = {
    "comau_smart_six": {
        "robot_name": "comau_smart_six",
        "joint_names": [
            'base_to_link1',
            'link1_to_link2',
            'link2_to_link3',
            'link3_to_link4',
            'link4_to_link5',
            'link5_to_link6',
        ],
        "link_names": [
            'base_link',
            'link1',
            'link2',
            'link3',
            'link4',
            'link5',
            'link6'
        ],
        "tools": {
            "default": [0, 0, 0, 0, 0, 0, 1]
        },
        "parameters": {
            "joints_tollerance": 0.0005,
            "repeatability": 0.00006
        },
        "auto_ik_service": True
    },
    "grasshopper": {
        "robot_name": "grasshopper",
        "joint_names": [
            'base_to_link1',
            'link1_to_link2',
            'link2_to_centerpoint',
            'centerpoint_to_finger_r',
            'centerpoint_to_finger_l',
            'base_to_driver'
        ],
        "link_names": [
            'base_link',
            'link1',
            'link2',
            'centerpoint',
            'finger_r',
            'finger_l',
            'driver'
        ],
        "tools": {
            "default": [0, 0, 0, 0, 0, 0, 1]
        },
        "parameters": {
            "joints_tollerance": 0.02,  # da modificare
            "repeatability": 0.00005  # da modificare
        },
        "auto_ik_service": True
    },
    "schunk_pg70": {
        "robot_name": "schunk_pg70",
        "joint_names": [
            'pg70_finger1_joint',
            'pg70_finger2_joint'
        ],
        "link_names": [
            'schunk_pg70_base',
            'schunk_pg70_finger1',
            'schunk_pg70_finger2'
        ],
        "tools": {
            "default": [0, 0, 0, 0, 0, 0, 1]
        },
        "joint_topics": {
            "joint_command_topic": "/schunk_pg70/joint_command",
            "joint_state_topic": "/schunk_pg70/joint_states"
        },
        "parameters": {
            "joints_tollerance": 0.0005,
            "repeatability": 0.00005
        },
        "auto_ik_service": True
    },
    "bogie": {
        "robot_name": "bogie",
        "joint_names": [
            'bogie_joint_0',
            'bogie_joint_1'
        ],
        "link_names": [
            'base_link',
            'platform_link'
        ],
        "tools": {
            "default": [-0.224, 0.09, 0.02, 0, 0, 0, 1]  # Dreidel Cable end
        },
        "parameters": {
            "joints_tollerance": 0.02,
            "repeatability": 0.00005
        },
        "auto_ik_service": True
    },

    "bonmetc60": {
        "robot_name": "bonmetc60",
        "joint_names": [
            'joint_1',
            'joint_2',
            'joint_3',
            'joint_4',
            'joint_5',
            'joint_6',
        ],
        "link_names": [
            'base_link',
            'link1',
            'link2',
            'link3',
            'link4',
            'link5',
            'link6'
        ],
        "tools": {
            "default": [0, 0, 0, 0, 0, 0, 1]
        },
        "parameters": {
            "joints_tollerance": 0.008,  # dove li prendo??????
            "repeatability": 0.00006  # dove li prendo??????
        },
        "auto_ik_service": True
    }
}

ROBOT_MARKET_SHAPE_LIBRARIES = {

    "comau_smart_six": [
        {"name": "table", "q": [-0.10445055803861009, -0.3132592774834806, -
                                2.3185108174307256, 0.029787635543396494, 1.1210464903876165, 0.14982902680893762]},
        {"name": "test1", "q": [0, 0, -1.57, 0, 0, 0]},
        {"name": "test2", "q": [0.6, 0.6, -1, 0.6, 0.6, 0.6]},
        {"name": "calibration", "q": [0, 0, -1.57, 0, 0, 0]},
        {"name": "hook", "q": [0.1, -0.1, -2.00, 0.4, .5, .4]},
        {"name": "hook#NW", "q": [-0.785, 0, -2.00, 0.4, .5, .4]},
        {"name": "hook#W", "q": [-1.57, 0, -2.00, 0.4, .5, .4]},
        {"name": "debug1", "q": [
            1.57, 0, -1.57, 0, 0, 0
        ]
        },
        {"name": "debug2", "q": [
            1.7730035781860352, 0.33083900809288025, -2.1676669120788574, -
            4.04744605475571e-05, 0.6469197273254395, 1.208625078201294
        ]
        },
    ],
    "grasshopper": [
        {"name": "testup", "q": [0, 0, 0, 0]},
        {"name": "testdown", "q": [-0.08, 0.0, 0, 0]},
        {"name": "align", "q": [-0.060, 0.045, 0.003, 0.003]},
        {"name": "plug", "q": [-0.060, 0.075, 0.003, 0.003]},
    ],
    "schunk_pg70": [
        {"name": "open", "q": [0.01, 0.01]},
        {"name": "close", "q": [0.001, 0.001]}
    ],
    "bogie": [
        {"name": "testpt1", "q": [-0.2, 0.2]},
        {"name": "testpt2", "q": [+0.2, -0.2]},
    ]
}


class RobotMarket(object):

    @staticmethod
    def createRobotByName(name, tf_listener=None):
        cfg = ROBOT_MARKET_LIBRARY.get(name)
        if cfg != None:
            robot = controllers.Robot(configuration=cfg,
                                      tf_listener=tf_listener)

            shapes = ROBOT_MARKET_SHAPE_LIBRARIES.get(name)
            if shapes:
                for s in shapes:
                    robot_shape = controllers.RobotShape(s["name"], s["q"])
                    robot.addRobotShape(robot_shape)
            return robot
        else:
            Logger.error("No Robot with name '{}'".format(name))
            return None
