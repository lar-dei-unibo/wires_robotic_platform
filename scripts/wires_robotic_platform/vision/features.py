# -*- encoding: utf-8 -*-
""" Image Processing """

from __future__ import division, print_function
import math
import PyKDL
import rospy
import tf
import cv2
import aruco
import numpy as np
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.vision.visionutils as visionutils

from wires_robotic_platform.srv import PolyDetectionService
from cv_bridge import CvBridge, CvBridgeError

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class FeatureMatching(object):
    ELSD = 1
    HOUGH = 2

    def __init__(self):
        self.esld_service = None
        self.cvbridge = CvBridge()
        self.circles_detected = []

    def setELSDService(self, node):
        self.esld_service = node.getServiceProxy("/elsd_detector_service/detection_service", PolyDetectionService)

    def detectCircle(self, image, min_radius, max_radius, method=None, show_it=False):
        self.circles_detected = []

        if method is None:
            method = FeatureMatching.ELSD
        try:
            if method == FeatureMatching.ELSD:
                self._circleELSD(image, min_radius, max_radius)
            elif method == FeatureMatching.HOUGH:
                self._circleHough(image, min_radius, max_radius)

            if show_it:
                cv2.namedWindow("circle_detection", cv2.WINDOW_NORMAL)
                cv2.imshow("circle_detection", image)

        except Exception as e:
            Logger.error(e)

        return self.circles_detected

    def _circleELSD(self, image, min_radius, max_radius):
        detected = []
        if self.esld_service:
            image_msg = self.cvbridge.cv2_to_imgmsg(image, "8UC1")
            res = self.esld_service(colored=False, image=image_msg, circle_max_radius=max_radius)
            for c in res.circles:
                center = (int(c.x), int(c.y))
                radius = int(c.z)
                if radius > min_radius:
                    cv2.circle(image, center, radius, (0, 0, 255), thickness=3)
                    detected.append(center)
        else:
            Logger.error("ELSD Proxy NOT Found")

        self.circles_detected = detected

    def _circleHough(self, image, min_radius, max_radius):
        circles = cv2.HoughCircles(image, cv2.HOUGH_GRADIENT, 1, 20, param1=50, param2=30, minRadius=min_radius, maxRadius=max_radius)
        circles = np.uint16(np.around(circles))
        detected = []
        for i in circles[0, :]:
            center = (i[0], i[1])
            # draw the outer circle
            cv2.circle(image, center, i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv2.circle(image, center, 2, (0, 0, 255), 3)
            detected.append(center)
        self.circles_detected = detected
