"""Dataset management."""
import cv2
import rospy
import numpy as np
import message_filters
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
import cv_bridge
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.cameras import Camera, CameraRGB, CameraRGBD, FrameRGB, FrameRGBD
from wires_robotic_platform.vision.ar import VirtualObject
import wires_robotic_platform.utils.transformations as transformations
import os
from os import listdir
from os.path import isfile, join
import collections
from shutil import copyfile
import pickle

############################################################
############################################################
############################################################
############################################################


class DatasetLoader(object):

    def __init__(self, base_path, dataset_name):

        self.base_path = base_path
        self.dataset_name = dataset_name
        self.ground_truth_file = os.path.join(
            self.base_path, self.dataset_name + "_gt.txt")
        self.ground_truth = self.loadGroundTruth(self.ground_truth_file)
        self.full_path = os.path.join(self.base_path, self.dataset_name)
        self.files_list = [f for f in listdir(
            self.full_path) if isfile(join(self.full_path, f))]

        self.map = {}

        for f in self.files_list:
            i = int(f.split("_")[0])
            name = f.split("_")[1]
            tp = name.split(".")[0]
            if i in self.map:
                self.map[i][tp] = f
            else:
                self.map[i] = {
                    tp: f
                }

        self.map = collections.OrderedDict(sorted(self.map.items()))

    def loadGroundTruth(self, gt_file):
        data = np.genfromtxt(gt_file, dtype=None)

        gt = {}
        for r in data:
            t = tuple(r)
            gt[t[0]] = {
                "size": np.array(t[1:4]),
                "pose": transformations.NumpyVectorToKDL(np.array(t[4:11])),
                "color": np.array(t[11:14])
            }
        gt = collections.OrderedDict(sorted(gt.items()))
        return gt

    def getGroundTruthByClassName(self, class_name):
        if class_name in self.ground_truth:
            return self.ground_truth[class_name]
        return None

    def getElement(self, index):
        if index < self.size():
            return self.map[index]
        else:
            return None

    def getPath(self, index, field):
        el = self.getElement(index)
        if field in el:
            file = el[field]
            return os.path.join(self.full_path, file)
        return ''

    def size(self):
        return len(self.map)

    def generateManifest(self, input_fields, save=True):
        rows = []
        for i, values in self.map.iteritems():
            row = "{} ".format(i)
            for field in input_fields:
                if field in values:
                    row += self.dataset_name + '/' + values[field] + ' '
            rows.append(row.strip())

        if save:
            manifest_path = os.path.join(
                self.base_path, self.dataset_name + "_manifest.txt")
            ff = open(manifest_path, 'w')
            for r in rows:
                ff.write(r + os.linesep)

        return rows


############################################################
############################################################
############################################################
############################################################


class DatasetStorage(object):

    def __init__(self, dataset_path, dataset_name, sample_time=1, depth_frames=False):
        self.dataset_path = dataset_path
        self.dataset_name = dataset_name
        self.full_path = os.path.join(self.dataset_path, self.dataset_name)

        if not os.path.exists(self.full_path):
            os.makedirs(self.full_path)

        self.depth_frames = depth_frames
        self.sample_time = sample_time
        self.last_time = 0
        self.frames = []
        self.data = []
        self.times = []

    def size(self):
        return len(self.frames)

    def pushFrame(self, frame, data):
        time = frame.time.to_sec()
        dt = time - self.last_time
        if dt >= self.sample_time:
            self.frames.append(frame)
            self.data.append(data)
            self.times.append(time)
            self.last_time = time

    def save(self):

        for i in range(0, len(self.frames)):
            frame = self.frames[i]
            data = self.data[i]
            times = self.times[i]

            rgb_image_name = "{}_rgb.png".format(i)
            cv2.imwrite(os.path.join(self.full_path,
                                     rgb_image_name), frame.rgb_image)

            for key, value in data.iteritems():
                data_name = "{}_{}.txt".format(i, key)
                np.savetxt(os.path.join(self.full_path, data_name), value)


############################################################
############################################################
############################################################
############################################################


class YoloDataset(DatasetLoader):

    def __init__(self, base_path, dataset_name, output_path, camera, class_map):
        super(YoloDataset, self).__init__(base_path, dataset_name)
        self.output_path = output_path
        self.camera = camera
        self.class_map = class_map
        print('ok!')

    def asList(self, grow_ratio=1.0, min_perc_size=0.05, center_coordinates=False):
        item_list = []
        for index, data in self.map.iteritems():

            rgb_file = self.getPath(index, 'rgb')
            camerapose_file = self.getPath(index, 'camerapose')
            camera_pose = transformations.NumpyVectorToKDL(
                np.loadtxt(camerapose_file))
            gts = []

            for name, data in self.ground_truth.iteritems():
                if 'fake' in name:
                    continue
                vobj = VirtualObject(frame=data["pose"], size=data["size"])
                img_points = vobj.getImagePoints(
                    camera_frame=camera_pose,
                    camera=self.camera
                )
                img_frame = vobj.getImageFrame(
                    points=img_points,
                    camera=self.camera,
                    only_top_face=False
                )
                img_frame = VirtualObject.enlargeFrame(
                    img_frame, grow_ratio, center_coordinates=center_coordinates)
                obj_class = self.class_map[name]
                gt_data = np.array([
                    int(obj_class),
                    img_frame[0],
                    img_frame[1],
                    img_frame[2],
                    img_frame[3]
                ])
                gts.append(gt_data)
                bad_frame = img_frame[2] < min_perc_size or img_frame[3] < min_perc_size

            item_list.append((rgb_file, gts))
        return item_list

    def test(self, min_perc_size=0.05):

        for index, data in self.map.iteritems():
            rgb_file = self.getPath(index, 'rgb')
            camerapose_file = self.getPath(index, 'camerapose')
            camera_pose = transformations.NumpyVectorToKDL(
                np.loadtxt(camerapose_file))
            image = cv2.imread(rgb_file)

            for name, data in self.ground_truth.iteritems():

                vobj = VirtualObject(frame=data["pose"], size=data["size"])
                img_points = vobj.getImagePoints(
                    camera_frame=camera_pose,
                    camera=self.camera
                )
                img_frame = vobj.getImageFrame(
                    points=img_points,
                    camera=self.camera,
                    only_top_face=False
                )
                img_frame = VirtualObject.enlargeFrame(img_frame, 1.2)

                # if bad_frame and not bad:
                #     print("BAD COMPONENTE ", name)
                #     bad = True
                #VirtualObject.drawBox(img_points, image, data["color"])
                VirtualObject.drawFrame(
                    img_frame, image, color=data["color"], camera=self.camera)

            cv2.imshow("ciao", image)
            cv2.waitKey(10)

    @staticmethod
    def writeList(element_list, output_path, test_percentage):
        if not os.path.exists(output_path):
            os.makedirs(output_path)

        images_path = os.path.join(output_path, "images")
        labels_path = os.path.join(output_path, "labels")

        if not os.path.exists(images_path):
            os.makedirs(images_path)

        if not os.path.exists(labels_path):
            os.makedirs(labels_path)

        trains = []
        tests = []
        test_index = len(element_list) * test_percentage

        for index in range(0, len(element_list)):
            fileprefix = "img_{}".format(index)

            img_file = element_list[index][0]
            dest_file_img = os.path.join(images_path, fileprefix + ".png")
            copyfile(img_file, dest_file_img)

            dest_file_label = os.path.join(labels_path, fileprefix + ".txt")
            matrix = np.array(element_list[index][1])
            np.savetxt(dest_file_label, matrix, fmt="%d %f %f %f %f")

            if index <= test_index:
                tests.append(dest_file_img)
            else:
                trains.append(dest_file_img)

        trains_file_path = os.path.join(output_path, "train.txt")
        tests_file_path = os.path.join(output_path, "test.txt")

        trains_file = open(trains_file_path, 'w')
        tests_file = open(tests_file_path, 'w')

        for r in trains:
            trains_file.write("{}\n".format(r))
        for r in tests:
            tests_file.write("{}\n".format(r))
