import cv2
import numpy as np

def cropCircleArea(input_image,circle_center,circle_radius):
    mask = np.zeros(input_image.shape, dtype=np.uint8)
    circle_center = circle_center.astype(int)
    cv2.circle(
        mask, 
        tuple(circle_center), 
        circle_radius, 
        (255, 255, 255), -1, 8, 0)

    # Apply mask (using bitwise & operator)
    result_array = input_image & mask
    # Crop/center result (assuming max_loc is of the form (x, y))
    result_array = result_array[circle_center[1] - circle_radius:circle_center[1] + circle_radius,circle_center[0] - circle_radius:circle_center[0] + circle_radius, :]
    return result_array