#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import PyKDL
import numpy as np
import math
import wires_robotic_platform.utils.visualization as visualization
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.vision.markers import ARMarker
from wires_robotic_platform.utils.sci import ClusterBuilder2D
import wires_robotic_platform.vision.cv as cv
import cv2


class Terminal(object):
    EDGE_DETECTION_TH1 = 100
    EDGE_DETECTION_TH2 = 200

    def __init__(self, features, image, terminal_area, magnitude_outlier_ratio=0.8, occupancy_outlier_ratio=0.3, label=0):
        self.features = features
        self.image = image
        self.terminal_area = terminal_area
        self.label = label

        self.centroid = np.mean(features, axis=0)

        distances = []
        for p in features:
            dist = np.linalg.norm(self.centroid - p)
            distances.append(dist)

        self.covariance = np.cov(features.T, aweights=distances)
        covx = self.covariance[:, 0]
        covy = self.covariance[:, 1]
        self.covx = covx / np.linalg.norm(covx)
        self.covy = covx / np.linalg.norm(covy)

        self.zone1 = self.centroid + self.covx * self.terminal_area * 2
        self.zone2 = self.centroid - self.covx * self.terminal_area * 2

        #⬢⬢⬢⬢⬢➤ Crops
        self.crop_center = cv.cropCircleArea(
            self.image, self.centroid, self.terminal_area)
        self.crop_zone1 = cv.cropCircleArea(
            self.image, self.zone1, self.terminal_area)
        self.crop_zone2 = cv.cropCircleArea(
            self.image, self.zone2, self.terminal_area)

        self.edges_zone1 = cv2.Canny(
            self.crop_zone1, Terminal.EDGE_DETECTION_TH1, Terminal.EDGE_DETECTION_TH2)
        self.edges_zone2 = cv2.Canny(
            self.crop_zone2, Terminal.EDGE_DETECTION_TH1, Terminal.EDGE_DETECTION_TH2)

        cv2.circle(
            self.edges_zone1,
            (int(terminal_area), int(terminal_area)),
            terminal_area, (0, 0, 0), 2
        )
        cv2.circle(
            self.edges_zone2,
            (int(terminal_area), int(terminal_area)),
            terminal_area, (0, 0, 0), 2
        )

        self.magnitude_zone1 = cv2.countNonZero(self.edges_zone1)
        self.magnitude_zone2 = cv2.countNonZero(self.edges_zone2)

        self.magnitude_zone1 = self.magnitude_zone1 if self.magnitude_zone1 > 0 else 1
        self.magnitude_zone2 = self.magnitude_zone2 if self.magnitude_zone2 > 0 else 1

        if self.magnitude_zone1 < self.magnitude_zone2:
            self.outlier_ratio = float(
                self.magnitude_zone1) / float(self.magnitude_zone2)
            self.minimum_zone = self.zone1
        else:
            self.outlier_ratio = float(
                self.magnitude_zone2) / float(self.magnitude_zone1)
            self.minimum_zone = self.zone2

        #⬢⬢⬢⬢⬢➤ CHECK ration between zones
        self.valid = self.outlier_ratio < magnitude_outlier_ratio

        #⬢⬢⬢⬢⬢➤ Check overall occupancy ratio
        self.occupancy_ratio_zone1 = float(self.magnitude_zone1) / float(
            self.edges_zone1.shape[0] * self.edges_zone1.shape[1])
        self.occupancy_ratio_zone2 = float(self.magnitude_zone2) / float(
            self.edges_zone1.shape[0] * self.edges_zone1.shape[1])
        if self.occupancy_ratio_zone1 < occupancy_outlier_ratio and self.occupancy_ratio_zone2 < occupancy_outlier_ratio:
            self.valid = False

    def __str__(self):
        return "Terminal [{}] {},{},{},{},{},{},{}".format(
            'VALID' if self.isValid() else 'INVALID',
            self.label,
            self.magnitude_zone1,
            self.magnitude_zone2,
            self.outlier_ratio,
            self.occupancy_ratio_zone1,
            self.occupancy_ratio_zone2,
            self.edges_zone1.shape[0]
        )

    def isValid(self):
        return self.valid

    def get2DReferenceFrame(self):
        rf = np.array([0, 0, 0, 0]).reshape(2, 2)
        direction = self.minimum_zone - self.centroid
        direction = direction / np.linalg.norm(direction)
        rf = np.array([
            [direction[0], direction[1]],
            [direction[1], -direction[0]],
        ])
        return rf

    def getControlPoint(self):
        rf = self.get2DReferenceFrame()
        xaxis = rf[:, 0]
        px = self.centroid + xaxis * self.terminal_area
        return px

    def buildCropsImage(self):
        center_crop = cv2.cvtColor(self.crop_center, cv2.COLOR_BGR2GRAY)
        return np.hstack((self.edges_zone1, center_crop, self.edges_zone2))

    def drawPoints(self, image):
        color = visualization.Color.pickFromPalette(self.label)
        for pt in self.features:
            cv2.circle(
                image,
                tuple(pt.astype(int)),
                2, color.toOpenCV(), -1
            )

    def drawZones(self, image, color=(255, 0, 0)):
        valid_color = (0, 255, 0)
        if not self.isValid():
            valid_color = (0, 0, 255)
        cv2.circle(image, tuple(self.centroid.astype(int)),
                   self.terminal_area, valid_color)
        cv2.circle(image, tuple(self.zone1.astype(int)),
                   self.terminal_area, (255, 255, 255))
        cv2.circle(image, tuple(self.zone2.astype(int)),
                   self.terminal_area, (255, 255, 255))

    def drawReferenceFrame(self, image):
        rf = self.get2DReferenceFrame()
        xaxis = rf[:, 0]
        yaxis = rf[:, 1]
        px = self.centroid + xaxis * self.terminal_area
        py = self.centroid + yaxis * self.terminal_area
        try:
            cv2.arrowedLine(
                image,
                tuple(self.centroid.astype(int)),
                tuple(px.astype(int)),
                (0, 0, 255),
                2,
                tipLength=0.5
            )
            cv2.arrowedLine(
                image,
                tuple(self.centroid.astype(int)),
                tuple(py.astype(int)),
                (0, 255, 0),
                2,
                tipLength=0.5
            )
        except:
            pass

    @staticmethod
    def compare(t1, t2):
        if t1.isValid() and not t2.isValid():
            return -1
        if t2.isValid() and not t1.isValid():
            return 1
        if t1.centroid[0] == t2.centroid[0]:
            if t1.centroid[1] > t2.centroid[1]:
                return -1
            return 1
        if t1.centroid[0] > t2.centroid[0]:
            return -1
        return 1


#############################################################################
#############################################################################
#############################################################################
#############################################################################


class TerminalDetector(object):

    def __init__(self, detector_2d, camera):
        self.detector_2d = detector_2d
        self.camera = camera
        self.tray = None
        self.kp_points = []
        self.mask = None

    def buildTray(self, marker, relative_transform, size):
        self.tray = TerminalTray(
            marker,
            relative_transform=relative_transform,
            size=size
        )
        return self.tray

    def getTray(self):
        return self.tray

    def buildMask(self, marker_hide_factor=1.2):
        self.mask = self.tray.buildMask(
            self.camera,
            marker_hide_factor=marker_hide_factor
        )
        return self.mask

    def extractRawFeatures(self, image):

        kp = self.detector_2d.detect(image, self.mask)
        kp_points = np.zeros((len(kp), 2), dtype=np.float32)
        for i in range(0, len(kp)):
            kp_points[i, 0] = kp[i].pt[0]
            kp_points[i, 1] = kp[i].pt[1]
        return kp_points

    def maskImage(self, image):
        return image.copy() & self.mask

    def buildClusters(self, points, cluster_th):
        if len(points) <= 0:
            return {}
        clusters = ClusterBuilder2D.clusterPoints(
            points,
            distance_th=cluster_th
        )
        return clusters

    def TEMP_printList(self, list):
        print("LIST")
        for t in list:
            print(t)

    def detectTerminals(self, image, marker_hide_factor=1.2, cluster_distance_th=30, search_area=35, outlier_ratio=0.8, occupancy_outlier_ratio=0.3, min_inilers=3):
        self.buildMask(marker_hide_factor=marker_hide_factor)
        self.kp_points = self.extractRawFeatures(image)
        clusters = self.buildClusters(
            self.kp_points,
            cluster_th=cluster_distance_th
        )
        terminals = []
        for label, cluster in clusters.iteritems():
            if len(cluster) < min_inilers:
                continue
            terminal = Terminal(
                cluster,
                self.maskImage(image),
                terminal_area=search_area,
                magnitude_outlier_ratio=outlier_ratio,
                occupancy_outlier_ratio=occupancy_outlier_ratio,
                label=label
            )
            terminals.append(terminal)
        # print("##########")
        # self.TEMP_printList(terminals)
        terminals = sorted(terminals, cmp=Terminal.compare)
        for i in range(0, len(terminals)):
            terminals[i].label = i
        # self.TEMP_printList(terminals)
        # print("@@@@@@@@@@")
        return terminals

    def estimate3DReferenceFrameOfTerminal(self, terminal):
        p0 = terminal.centroid
        p1 = terminal.getControlPoint()
        ray0 = self.tray.createRay(p0, self.camera)
        ray1 = self.tray.createRay(p1, self.camera)
        p3d_0 = self.tray.rayIntersection(ray0)
        p3d_1 = self.tray.rayIntersection(ray1)

        x_axis = p3d_1 - p3d_0
        x_axis = x_axis / np.linalg.norm(x_axis)

        z_axis = self.tray.getPlaneCoefficients()[0:3]

        rx = transformations.NumpyVectorToKDLVector(x_axis)
        rz = transformations.NumpyVectorToKDLVector(z_axis)
        ry = rz * rx

        frame = PyKDL.Frame()
        frame.M[0, 0] = rx.x()
        frame.M[1, 0] = rx.y()
        frame.M[2, 0] = rx.z()

        frame.M[0, 1] = ry.x()
        frame.M[1, 1] = ry.y()
        frame.M[2, 1] = ry.z()

        frame.M[0, 2] = rz.x()
        frame.M[1, 2] = rz.y()
        frame.M[2, 2] = rz.z()

        frame.p = PyKDL.Vector(
            p3d_0[0], p3d_0[1], p3d_0[2]
        )

        return frame

    def estimateGlobal3DReferenceFrameOfTerminal(self, terminal, camera_pose, global_plane_coefficients):
        p0 = terminal.centroid
        p1 = terminal.getControlPoint()
        ray0 = self.tray.createRay(p0, self.camera)
        ray1 = self.tray.createRay(p1, self.camera)
        p3d_0 = self.tray.rayIntersection(ray0)
        p3d_1 = self.tray.rayIntersection(ray1)

        x_axis = p3d_1 - p3d_0
        x_axis = x_axis / np.linalg.norm(x_axis)

        z_axis = self.tray.getPlaneCoefficients()[0:3]

        rx = transformations.NumpyVectorToKDLVector(x_axis)
        rz = transformations.NumpyVectorToKDLVector(z_axis)
        ry = rz * rx

        frame = PyKDL.Frame()
        frame.M[0, 0] = rx.x()
        frame.M[1, 0] = rx.y()
        frame.M[2, 0] = rx.z()

        frame.M[0, 1] = ry.x()
        frame.M[1, 1] = ry.y()
        frame.M[2, 1] = ry.z()

        frame.M[0, 2] = rz.x()
        frame.M[1, 2] = rz.y()
        frame.M[2, 2] = rz.z()

        frame.p = PyKDL.Vector(
            p3d_0[0], p3d_0[1], p3d_0[2]
        )

        return frame


#############################################################################
#############################################################################
#############################################################################
#############################################################################
class TerminalTray(PyKDL.Frame):

    def __init__(self, reference_ar_marker, relative_transform=PyKDL.Frame(), size=(0.1, 0.1)):
        super(TerminalTray, self).__init__()
        self.ar_marker = reference_ar_marker
        self.M = reference_ar_marker.M
        self.p = reference_ar_marker.p
        self.relative_transform = relative_transform
        self.size = size

    def getPlaneCoefficients(self):
        return transformations.planeCoefficientsFromFrame(self)

    def createVisualObject(self, parent_frame):
        plane = visualization.createCube(
            frame_id=parent_frame,
            transform=self.relative_transform,
            size=[self.size[0], self.size[1], 0.001],
            name="tray"
        )
        return plane

    def createRay(self, point_2d, camera):
        point_2d = np.array([
            point_2d[0],
            point_2d[1],
            1.0
        ]).reshape(3, 1)
        ray = np.matmul(camera.camera_matrix_inv, point_2d)
        ray = ray / np.linalg.norm(ray)
        return ray.reshape(3)

    def rayIntersection(self, ray):
        plane_coefficients = self.getPlaneCoefficients()
        t = -(plane_coefficients[3]) / (
            plane_coefficients[0] * ray[0] +
            plane_coefficients[1] * ray[1] +
            plane_coefficients[2] * ray[2]
        )
        x = ray[0] * t
        y = ray[1] * t
        z = ray[2] * t
        inters = np.array([x, y, z])
        return inters.reshape(3)

    def point2DIntersection(self, point_2d, camera):
        ray = self.createRay(point_2d, camera)
        return self.rayIntersection(ray)

    def getImagePoints(self, camera):
        t1 = self.relative_transform * PyKDL.Frame(
            PyKDL.Vector(-self.size[0] * 0.5, self.size[1] * 0.5, 0)
        )
        t2 = self.relative_transform * PyKDL.Frame(
            PyKDL.Vector(self.size[0] * 0.5, self.size[1] * 0.5, 0)
        )
        t3 = self.relative_transform * PyKDL.Frame(
            PyKDL.Vector(self.size[0] * 0.5, -self.size[1] * 0.5, 0)
        )
        t4 = self.relative_transform * PyKDL.Frame(
            PyKDL.Vector(-self.size[0] * 0.5, -self.size[1] * 0.5, 0)
        )
        t1 = self * t1
        t2 = self * t2
        t3 = self * t3
        t4 = self * t4
        t1 = transformations.KDLVectorToNumpyArray(t1.p)
        t2 = transformations.KDLVectorToNumpyArray(t2.p)
        t3 = transformations.KDLVectorToNumpyArray(t3.p)
        t4 = transformations.KDLVectorToNumpyArray(t4.p)

        p1 = np.matmul(camera.camera_matrix, t1)
        p1 = p1 / p1[2]
        p2 = np.matmul(camera.camera_matrix, t2)
        p2 = p2 / p2[2]
        p3 = np.matmul(camera.camera_matrix, t3)
        p3 = p3 / p3[2]
        p4 = np.matmul(camera.camera_matrix, t4)
        p4 = p4 / p4[2]

        tray_points = (p1, p2, p3, p4)
        pts = np.zeros((4, 2), dtype=int)
        for i in range(0, len(tray_points)):
            p = tray_points[i]
            pts[i, 0] = p[0]
            pts[i, 1] = p[1]
        return pts

    def buildMask(self, camera, marker_hide_factor=1.2):
        tray_points = self.getImagePoints(camera)
        mask = np.zeros((camera.height, camera.width, 1), dtype=np.uint8)
        cv2.fillPoly(mask, [tray_points], (255, 255, 255))
        cv2.circle(
            mask,
            (int(self.ar_marker.center[0]), int(self.ar_marker.center[1])),
            int(self.ar_marker.radius * marker_hide_factor),
            (0, 0, 0),
            -1
        )
        return mask
