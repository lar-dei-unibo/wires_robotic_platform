#!/usr/bin/env python
# -*- coding: utf-8 -*- 


import numpy as np
import rospy

from wires_robotic_platform.msg import RobotFollow
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.transformations import FrameVectorFromKDL, FrameVectorToKDL, ListToKDLVector, KDLVectorToList
from wires_robotic_platform.utils.devices import ForceSensor, Joystick
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.robots.controllers import RobotStatus
from wires_robotic_platform.proxy.target_follower_proxy import TargetFollowerProxy
from wires_robotic_platform.proxy.alarm_proxy import AlarmProxy
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.sensors.tactile_data_processing.tactile_rnn import TactileRNN

from std_msgs.msg import String, Bool, Float64MultiArray
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
import threading
import PyKDL
import time
import copy
import math
import tf
 

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇   SpringTactileController  ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
 

class SpringTactileController(object):

    STANDARD_INPUT = {
    }

    DEFAULT_PARAMETERS = {
        "pushing_gain": 0.01,
        "pushing_threshold": 0.05
    }

    def __init__(self, robot_name="", params_dict={}):
        self.robot_name = robot_name

        # Controller Parameters
        if params_dict == {} or params_dict == None:
            params_dict = self.DEFAULT_PARAMETERS

        self.params = params_dict
        self.pushing_gain = self._param("pushing_gain", params_dict)
        self.pushing_threshold = self._param("pushing_threshold", params_dict)
        self.initial_pushing_val = None
        self.last_tactile_msg = Float64MultiArray()
        self.axis = PyKDL.Vector(1, 0, 0)
 

    def _param(self, name, param_dict):
        if name in param_dict.keys():
            par = param_dict[name]
            self.params[name] = par
            return par
        else:
            return self.params[name]

    def _getPushValue(self):
        tactile_grid = self.last_tactile_msg.data
        value = tactile_grid[0] - tactile_grid[3] + \
            tactile_grid[4] - tactile_grid[7] + \
            tactile_grid[8] - tactile_grid[11] + \
            tactile_grid[12] - tactile_grid[15]
        return value

    def setParameters(self, params_dict):
        self.pushing_gain = self._param("pushing_gain", params_dict)
        self.pushing_threshold = self._param("pushing_threshold", params_dict)
        pass

    def getParameters(self):
        return self.params

    def reset(self):
        self.initial_pushing_val = self._getPushValue()
        pass

    def start(self, data):
        self.initial_pushing_val = self._getPushValue()
        pass
 

    def output(self,data):
        if "tactile" in data.keys():
            self.last_msg = data["tactile"]

        Tr = PyKDL.Frame()
        current_tf = data["pose"]

        if self.initial_pushing_val:   
            push_val = self._getPushValue()
            delta_push = push_val - self.initial_pushing_val
            if math.fabs(delta_push) > self.pushing_threshold:
                Tr = PyKDL.Frame(self.axis * (self.pushing_gain * delta_push)) 
          
        print "push_val={}".format(push_val)

        target_tf = current_tf * Tr

        return target_tf


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 
# ▇▇▇▇▇▇▇▇▇▇    DampedForwardTactileController   ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ 


class DampedForwardTactileController(object):

    STANDARD_INPUT = {
    }

    DEFAULT_PARAMETERS = {
        "velocity": 0.0005,
        "damp_tactile_threshold": 0.02,
        "damp_magnitude": 0.995
    }

    def __init__(self, robot_name="", params_dict={}):
        self.robot_name = robot_name

        # Controller Parameters
        if params_dict == {} or params_dict == None:
            params_dict = self.DEFAULT_PARAMETERS

        self.params = params_dict

        self.damp_tactile_threshold = self._param("damp_tactile_threshold",
                                                  params_dict)
        self.damp_magnitude = self._param("damp_magnitude",
                                          params_dict)
        self.initial_velocity = self._param("velocity",
                                            params_dict)
        self.initial_pushing_val = None
        self.velocity = 0
        self.last_tactile_msg = Float64MultiArray()
        self.axis = PyKDL.Vector(1, 0, 0)

    def _param(self, name, param_dict):
        if name in param_dict.keys():
            par = param_dict[name]
            self.params[name] = par
            return par
        else:
            return self.params[name]

    def _getPushValue(self):
        tactile_grid = self.last_tactile_msg.data
        value = tactile_grid[0] - tactile_grid[3] + \
            tactile_grid[4] - tactile_grid[7] + \
            tactile_grid[8] - tactile_grid[11] + \
            tactile_grid[12] - tactile_grid[15]
        return value

    def _updateVelocity(self):
        push_val = self._getPushValue()
        delta_push = push_val - self.initial_pushing_val
        damp_coefficient = self.damp_magnitude
        if math.fabs(delta_push) > self.damp_tactile_threshold:
            self.velocity = self.velocity * damp_coefficient

    def setParameters(self, params_dict):
        try:
            self.damp_tactile_threshold = self._param("damp_tactile_threshold",
                                                      params_dict)
            self.damp_magnitude = self._param("damp_magnitude",
                                              params_dict)
            self.initial_velocity = self._param("velocity",
                                                params_dict)
            print(self.initial_velocity)

        except Exception as e:
            Logger.error(e)

    def getParameters(self):
        return self.params

    def reset(self):
        self.initial_pushing_val = self._getPushValue()

    def start(self, data):
        self.initial_pushing_val = self._getPushValue()
        self.velocity = self.initial_velocity

    def update(self, msg, feedback_source='tactile'):
        if feedback_source=='tactile':
            self.last_tactile_msg = msg
            if self.initial_pushing_val is not None:
                self._updateVelocity()

    def output(self,data):
        Tr = PyKDL.Frame()
        current_tf = data["pose"]

        Tr = PyKDL.Frame(self.axis * self.velocity) 
        print(self.velocity)

        target_tf = current_tf * Tr

        return target_tf

 