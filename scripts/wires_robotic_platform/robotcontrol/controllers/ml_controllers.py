#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy as np
import rospy

from wires_robotic_platform.msg import RobotFollow
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.devices import ForceSensor, Joystick
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.robots.controllers import RobotStatus
from wires_robotic_platform.proxy.target_follower_proxy import TargetFollowerProxy
from wires_robotic_platform.proxy.alarm_proxy import AlarmProxy
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.sensors.tactile_data_processing.tactile_rnn import TactileRNN

from std_msgs.msg import String, Bool, Float64, Float64MultiArray
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
import threading
import PyKDL
import json
import copy
import time
import math
import tf

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


_seq_len = [5, 5, 5]
_dirName = "/home/riccardo/ros/wires_data/ml_files/wire_contact"
_reg_name = ["/wire_twist_x_regressor.h5",
             "/wire_twist_y_regressor.h5",
             "/wire_twist_z_regressor.h5"]
FORCE_REGRESSOR = [None, None, None]
for i in range(3):
    FORCE_REGRESSOR[i] = TactileRNN(model_h5_file_path=_dirName + _reg_name[i],
                                    seq_len=_seq_len[i])


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇    MLSpringTactileController   ▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class MLSpringTactileController(object):

    STANDARD_INPUT = {
    }

    DEFAULT_PARAMETERS = {
        "linear_gain": [1, 1, 1],
        "angular_gain": [0.5, 0.5, 0.5],
        "linear_action": True,
        "angular_action": False,
        "threshold": [0.5, 0.5, 0.5]
    }

    def __init__(self, robot_name="", params_dict={}):
        self.robot_name = robot_name
        self.is_active = False

        # Controller Parameters
        if params_dict == {} or params_dict == None:
            params_dict = self.DEFAULT_PARAMETERS

        self.params = params_dict
        self.linear_gain = self._param("linear_gain", params_dict)
        self.angular_gain = self._param("angular_gain", params_dict)
        self.threshold = self._param("threshold", params_dict)
        self.angular_action = self._param("angular_action", params_dict)
        self.linear_action = self._param("linear_action", params_dict)

        ###
        self.target_tf = None
        self.initial_tf = None
        self.current_tf = None
        self.last_msg = Float64MultiArray()
        x_axis = PyKDL.Vector(1, 0, 0)
        y_axis = PyKDL.Vector(0, 1, 0)
        z_axis = PyKDL.Vector(0, 0, 1)
        self.axis = [x_axis, y_axis, z_axis]
        self.bias = [0, 0, 0]

        # TODO per debug
        self.estimated_twist_pub = rospy.Publisher("/insertion_control/estimated_twist",
                                                   Twist,
                                                   queue_size=1)
        self.debug_somedata_pub = rospy.Publisher("/insertion_control/debug_data",
                                                  Float64MultiArray,
                                                  queue_size=1)

    def _param(self, name, param_dict):
        if name in param_dict.keys():
            par = param_dict[name]
            self.params[name] = par
            return par
        else:
            return self.params[name]

    def setParameters(self, params_dict=None, standard_index="default"):
        self.linear_gain = self._param("linear_gain", params_dict)
        self.angular_gain = self._param("angular_gain", params_dict)
        self.threshold = self._param("threshold", params_dict)
        self.angular_action = self._param("angular_action", params_dict)
        self.linear_action = self._param("linear_action", params_dict)

    def getParameters(self):
        return self.params

    def reset(self):
        self.is_active = False
        for i in range(3):
            self.bias[i] = FORCE_REGRESSOR[i].getMean()
        Logger.warning("bias {}".format(self.bias))

    def start(self, data):
        self.reset()
        self.initial_tf = None
        self.is_active = True

    def output(self, data):
        if "tactile" in data.keys():
            self.last_msg = data["tactile"]
            for i in range(3):
                FORCE_REGRESSOR[i].update(self.last_msg)

        Tr = PyKDL.Frame()
        self.current_tf = data["current_tf"]
        self.target_tf = data["target_tf"]

        if self.is_active:
            if self.initial_tf:
                dp = np.zeros(3)
                dr = np.zeros(3)
                pred = np.zeros(3)
                for i in range(3):
                    if not FORCE_REGRESSOR[i].isReady():
                        return Tr
                    else:
                        pred[i] = FORCE_REGRESSOR[i].getPrediction() - \
                            self.bias[i]
                        if math.fabs(pred[i]) > self.threshold[i]:
                            if self.linear_action:
                                dp[i] = 0.00001 * self.linear_gain[i] * pred[i]
                            if self.angular_action:
                                if i == 1:
                                    dr[2] = 0.001 * \
                                        self.angular_gain[2] * pred[1]
                                elif i == 2:
                                    dr[1] = 0.001 * \
                                        self.angular_gain[1] * pred[2]

                Tr.p = PyKDL.Vector(dp[0], -dp[1], -dp[2])
                Tr.M = PyKDL.Rotation.Rot(self.axis[1], dr[1]) * PyKDL.Rotation.Rot(self.axis[2], dr[2])

                self.target_tf = self.target_tf * Tr

                #
                # ||||||| \\\\
                # ||||||| ----  ONLY FOR DEBUG
                debug_msg = Float64MultiArray()
                debug_msg.data = [dp[0], -dp[1], -dp[2],  dr[1], dr[2], ]
                self.debug_somedata_pub.publish(debug_msg)
                self.estimated_twist_pub.publish(
                    transformations.TwistFormKDLVector(pred))
                print "Force: {}".format(pred)
                # ONLY FOR DEBUG ---- |||||||
                #                \\\\ |||||||
                #

            else:
                self.initial_tf = self.current_tf
                self.target_tf = self.current_tf

        return self.target_tf

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇    MLDampedForwardTactileController   ▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class MLDampedForwardTactileController(object):

    STANDARD_INPUT = {
    }

    DEFAULT_PARAMETERS = {
        "step_size": 0.00005,
        "direction_gain": 0,
        "regulation_angular_action": False,
        "regulation_linear_action": False,
        "linear_regulation_gain": [0, 0, 0],
        "angular_regulation_gain": [0, 0, 0],
        "regulation_threshold": [1, 1, 1],
        "direction_correction": False,
        "direction_compensation": False,
        "global_gain": 0
    }

    def __init__(self, robot_name="", params_dict={}):
        self.robot_name = robot_name

        # Controller Parameters
        if params_dict == {} or params_dict == None:
            params_dict = self.DEFAULT_PARAMETERS

        self.params = params_dict

        self.step_size = self._param("step_size",
                                     params_dict)
        self.direction_gain = self._param("direction_gain",
                                          params_dict)
        self.direction_correction = self._param("direction_correction",
                                                params_dict)
        self.direction_compensation = self._param("direction_compensation",
                                                  params_dict)
        self.global_gain = self._param("global_gain",
                                       params_dict)

        ###
        self.back_ctrl = MLSpringTactileController()
        ###
        self.last_msg = Float64MultiArray()
        self.axis = PyKDL.Vector(1, 0, 0)
        self.initial_tf = None
        self.target_tf = None
        self.current_tf = None
        self.is_active = False

    def _param(self, name, param_dict):
        if name in param_dict.keys():
            par = param_dict[name]
            self.params[name] = par
            return par
        else:
            return self.params[name]

    def setParameters(self, params_dict=None, standard_index="default"):
        try:
            self.step_size = self._param("step_size",
                                         params_dict)
            self.direction_gain = self._param("direction_gain",
                                              params_dict)
            self.direction_correction = self._param("direction_correction",
                                                    params_dict)
            self.direction_compensation = self._param("direction_compensation",
                                                      params_dict)
            self.global_gain = self._param("global_gain",
                                           params_dict)
            ####
            self.back_ctrl.angular_action = self._param("regulation_angular_action",
                                                        params_dict)
            self.back_ctrl.linear_action = self._param("regulation_linear_action",
                                                       params_dict)
            self.back_ctrl.linear_gain = self._param("linear_regulation_gain",
                                                     params_dict)
            self.back_ctrl.angular_gain = self._param("angular_regulation_gain",
                                                      params_dict)
            self.back_ctrl.threshold = self._param("regulation_threshold",
                                                   params_dict)

        except Exception as e:
            Logger.error(e)

    def getParameters(self):
        return self.params

    def reset(self):
        self.back_ctrl.reset()
        self.axis = PyKDL.Vector(1, 0, 0)
        self.initial_tf = None
        self.is_active = False

    def start(self, data):
        self.reset()
        self.back_ctrl.start(data)
        self.is_active = True

    def output(self, data):
        if "tactile" in data.keys():
            self.last_msg = data["tactile"]
            # self.back_ctrl.update(self.last_msg)

        Tr = PyKDL.Frame()
        self.current_tf = data["current_tf"]
        self.target_tf = data["target_tf"]

        if self.is_active:
            if self.initial_tf:
                # self.target_tf = self.back_ctrl.output(data)
                Tr_back = self.back_ctrl.output(data)

                if self.direction_correction:
                    # v_tr_back_yz = PyKDL.Vector(0, self.target_tf.M.UnitY(), self.target_tf.M.UnitZ())
                    v_tr_back_yz = PyKDL.Vector(0, Tr_back.p[1], Tr_back.p[2])
                    self.axis = self.axis + self.direction_gain * v_tr_back_yz

                elif self.direction_compensation:
                    R_t0_b = self.initial_tf.M
                    R_ti_b = self.current_tf.M
                    R_ti_t0 = R_ti_b.Inverse() * R_t0_b
                    self.axis = R_ti_t0 * PyKDL.Vector(1, 0, 0)
                    # self.axis = Tr_back.M.Inverse() * self.axis

                Tr_ahead = PyKDL.Frame(self.axis * self.step_size)
                # self.target_tf =  Tr_ahead * self.target_tf
                Tr = Tr_ahead * Tr_back
                Tr.p = Tr.p * self.global_gain
                self.target_tf = self.target_tf * Tr

            else:
                self.initial_tf = self.current_tf
                self.target_tf = self.current_tf

        return self.target_tf

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇    WireInsertionController   ▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

# Trasformazione di 'target_tf' con rotation_integral_scaling
# PARAMETERS = {
        # "step_size": 0.0001,
        # "force_p_gain": 2.0,
        # "regulation_p_gain": [0.03, 0.1, 0.3],
        # "regulation_i_gain": [0.003, 0.01, 0],
        # "regulation_i_size": [100, 100, 100],
        # "threshold": [0.5, 0.3, 0.3],
        # "force_projection": False,
        # "position_ball_radious": 0.0,
        # "position_ball_offset": 0.0,
        # "position_scaling_gain": 1.0,
        # "position_scaling_limits": [-1000, 1.0],
        # "integral_rotation_scaling": True
#     }


class SimpleOp(object):
    def __init__(self, integral_window=100):
        self.w_size = integral_window
        self.buffer = np.zeros(integral_window)

    def reset(self):
        self.buffer = np.zeros(self.w_size)

    def integrate(self, val):
        tmpw = np.append(self.buffer, [val], axis=0)
        self.buffer = np.delete(tmpw, 0, 0)
        return sum(self.buffer)

    @staticmethod
    def saturate(x, xmin=0, xmax=1, ymin=0, ymax=1):
        if x > xmax:
            y = ymax
        elif x < xmin:
            y = ymin
        else:
            m = (ymax - ymin) / (xmax - xmin)
            q = ymin - xmin * (ymax - ymin) / (xmax - xmin)
            y = m * x + q
        return y


class WireInsertionController(object):

    STANDARD_INPUT = {
    }

    DEFAULT_PARAMETERS = {
        "step_size": 0.0,
        "force_p_gain": 0.0,
        "force_threshold": 1,
        "regulation_p_gain": [0, 0, 0],
        "regulation_i_gain": [0, 0, 0],
        "regulation_i_size": [100, 100, 100],
        "threshold": [1, 1, 1],
        "force_projection": False,
        "position_ball_radious": 0.0,
        "position_ball_offset": 0.0,
        "position_offest_gripper": 0.0,
        "position_offest_hole": 0.0,
        "position_scaling_gain": 1.0,
        "position_scaling_limits": [-1000.0, 1.0],
        "integral_rotation_scaling": False,
        "lane_correction": False
    }

    # PARAMETERS_FUZZY_INSERTION = DEFAULT_PARAMETERS
    # PARAMETERS_FUZZY_INSERTION["step_size"] = 0.0001
    # PARAMETERS_FUZZY_INSERTION["force_p_gain"] = 1000.0
    # PARAMETERS_FUZZY_INSERTION["force_threshold"] = 0.4
    # PARAMETERS_FUZZY_INSERTION["threshold"] = [0.2, 2, 2]

    # PARAMETERS_CORRECT_INSERTION = DEFAULT_PARAMETERS
    # PARAMETERS_CORRECT_INSERTION["step_size"] = 0.0001
    # PARAMETERS_CORRECT_INSERTION["force_p_gain"] = 2.0
    # PARAMETERS_CORRECT_INSERTION["force_threshold"] = 0.4
    # PARAMETERS_CORRECT_INSERTION["threshold"] = [0.5, 0.3, 0.3]
    # PARAMETERS_CORRECT_INSERTION["regulation_p_gain"] = [0.1, 0.5, 0.1]
    # PARAMETERS_CORRECT_INSERTION["regulation_i_gain"] = [0.003, 0.03, 0]
    # PARAMETERS_CORRECT_INSERTION["regulation_i_size"] = [100, 150, 100]

    # PARAMETERS_MISALIGNED_INSERTION = {
    # }

    # STANDARD_PARAMETERS = {
    #     "default": DEFAULT_PARAMETERS,
    #     "fuzzy": PARAMETERS_FUZZY_INSERTION,
    #     "correct": PARAMETERS_CORRECT_INSERTION
    # }

    def __init__(self, robot_name="", params_dict={}):
        self.robot_name = robot_name

        ###
        self.bias = [None, None, None]
        self.is_unbiased = False
        self.force_vector_tl = PyKDL.Vector(0, 0, 0)
        self.error_reg_vector = PyKDL.Vector(0, 0, 0)
        self.wire_angle = 0
        self.last_wp_msg = Float64MultiArray()
        self.last_msg = Float64MultiArray()
        self.unitX = PyKDL.Vector(1, 0, 0)
        self.unitY = PyKDL.Vector(0, 1, 0)
        self.unitZ = PyKDL.Vector(0, 0, 1)
        self.axis = [self.unitX, self.unitY, self.unitZ]
        self.ur_axis = None
        self.yh_axis = None
        self.zh_axis = None
        self.hole_tf = None
        self.initial_tf = None
        self.target_tf = None
        self.current_tf = None
        self.done = False
        self.success = False
        self.is_active = False
        self.vectorThr = True  # if vectorThr==True, the threshold is applied on the vector's norm and NOT on the sigle components
        self.exitCondition = True

        # Controller Parameters
        if params_dict == {} or params_dict == None:
            params_dict = self.DEFAULT_PARAMETERS

        self.params = params_dict

        self.step_size = self._param("step_size",
                                     params_dict)
        self.force_p_gain = self._param("force_p_gain",
                                        params_dict)
        self.force_threshold = self._param("force_threshold",
                                           params_dict)
        self.regulation_p_gain = self._param("regulation_p_gain",
                                             params_dict)
        self.regulation_i_gain = self._param("regulation_i_gain",
                                             params_dict)
        self.regulation_i_size = self._param("regulation_i_size",
                                             params_dict)
        self.threshold = self._param("threshold",
                                     params_dict)
        self.force_projection = self._param("force_projection",
                                            params_dict)
        self.position_ball_radious = self._param("position_ball_radious",
                                                 params_dict)
        self.position_ball_offset = self._param("position_ball_offset",
                                                params_dict)
        self.position_offest_gripper = self._param("position_offest_gripper",
                                                   params_dict)
        self.position_offest_hole = self._param("position_offest_hole",
                                                params_dict)
        self.position_scaling_gain = self._param("position_scaling_gain",
                                                 params_dict)
        self.position_scaling_limits = self._param("position_scaling_limits",
                                                   params_dict)
        self.integral_rotation_scaling = self._param("integral_rotation_scaling",
                                                     params_dict)
        self.lane_correction = self._param("lane_correction",
                                           params_dict)

        self.rhor_op = SimpleOp(integral_window=self.regulation_i_size[0])
        self.thetay_op = SimpleOp(integral_window=self.regulation_i_size[1])
        self.thetaz_op = SimpleOp(integral_window=self.regulation_i_size[2])

        # Status Data Publishers
        self.estimated_twist_pub = rospy.Publisher("/insertion_control/estimated_twist",
                                                   Twist,
                                                   queue_size=1)
        self.regulation_error_pub = rospy.Publisher("/insertion_control/regulation_error",
                                                    Twist,
                                                    queue_size=1)
        self.target_distance_pub = rospy.Publisher("/insertion_control/target_distance",
                                                   Float64,
                                                   queue_size=1)
        self.debug_somedata_pub = rospy.Publisher("/insertion_control/debug_data",
                                                  Float64MultiArray,
                                                  queue_size=1)
        self.controller_status_pub = rospy.Publisher("/insertion_control/controller_status",
                                                     String,
                                                     queue_size=1)

    def _param(self, name, param_dict):
        if name in param_dict.keys():
            par = param_dict[name]
            self.params[name] = par
            return par
        else:
            return self.params[name]

    def setParameters(self, params_dict=None, standard_index="default"):
        if params_dict is None:
            # params_dict = self.STANDARD_PARAMETERS[standard_index]
            pass
        else:
            try:
                self.step_size = self._param("step_size",
                                             params_dict)
                self.force_p_gain = self._param("force_p_gain",
                                                params_dict)
                self.force_threshold = self._param("force_threshold",
                                                   params_dict)
                self.regulation_p_gain = self._param("regulation_p_gain",
                                                     params_dict)
                self.regulation_i_gain = self._param("regulation_i_gain",
                                                     params_dict)
                self.regulation_i_size = self._param("regulation_i_size",
                                                     params_dict)
                self.threshold = self._param("threshold",
                                             params_dict)
                self.force_projection = self._param("force_projection",
                                                    params_dict)
                self.position_ball_radious = self._param("position_ball_radious",
                                                         params_dict)
                self.position_ball_offset = self._param("position_ball_offset",
                                                        params_dict)
                self.position_offest_gripper = self._param("position_offest_gripper",
                                                           params_dict)
                self.position_offest_hole = self._param("position_offest_hole",
                                                        params_dict)
                self.position_scaling_gain = self._param("position_scaling_gain",
                                                         params_dict)
                self.position_scaling_limits = self._param("position_scaling_limits",
                                                           params_dict)
                self.integral_rotation_scaling = self._param("integral_rotation_scaling",
                                                             params_dict)
                self.lane_correction = self._param("lane_correction",
                                                   params_dict)

            except Exception as e:
                Logger.error(e)

    def getParameters(self):
        return self.params

    def reset(self):
        self.ur_axis = None
        self.yh_axis = None
        self.zh_axis = None
        self.initial_tf = None
        self.hole_tf = None
        self.wire_angle = 0
        self.is_active = False
        self.done = False
        self.success = False
        self.rhor_op.reset()
        self.thetay_op.reset()
        self.thetaz_op.reset()
        self.force_vector_tl = PyKDL.Vector(0, 0, 0)
        self.is_unbiased = False
        self._remove_bias()

    def _reset_regressor(self):
        for i in range(3):
            FORCE_REGRESSOR[i].reset()
            self.bias = [None, None, None]
            self.is_unbiased = False
        Logger.warning("Reset Force Regressor")

    def _remove_bias(self):
        self.is_unbiased = True
        for i in range(3):
            if FORCE_REGRESSOR[i].isReady():
                self.bias[i] = FORCE_REGRESSOR[i].getMean()
                if self.bias[i] is None:
                    Logger.error("bias[{}}] is None".format(i))
                    self.is_unbiased = False
            else:
                self.is_unbiased = False
                Logger.warning(" --- NOT ready to remove bias  --- ")
        Logger.warning("bias {}".format(self.bias))

    def start(self, data):
        self._reset_regressor()
        time.sleep(0.1)
        self.reset()
        time.sleep(0.1)
        self.is_active = True
        self.hole_tf = data["hole_tf"]
        if type(self.hole_tf) != PyKDL.Frame:  # PyKDL is not JSON serializable
            self.hole_tf = transformations.tfToKDL(self.hole_tf)
        self.wire_angle = data["wire_angle"]
        self.xh_axis = self.hole_tf.M * self.unitX  # R_b_h * [1,0,0] = x_b_h
        self.yh_axis = self.hole_tf.M * self.unitY  # R_b_h * [0,1,0] = y_b_h
        self.zh_axis = self.hole_tf.M * self.unitZ  # R_b_h * [0,0,1] = z_b_h
        self.ur_axis = self.xh_axis

    def saturate_angle(self,  err_theta):   # NOT USED!!!!!!!!!!!!!
        max_angle = math.pi / 6
        vtl = self.unitX  # self.current_tf.M * self.unitX
        hole_tf_tl = self.current_tf.Inverse() * self.hole_tf
        vh = hole_tf_tl.M * self.unitX
        angle_tl_h = np.arccos(PyKDL.dot(vtl, vh))
        utl = self.unitY
        sgn_angle = PyKDL.dot(vh * vtl, utl)
        Logger.error("angolo={}, sign={}".format(angle_tl_h * (180 / 3.1415), sgn_angle))
        if math.fabs(angle_tl_h) > max_angle:
            if sgn_angle > 0 and err_theta < 0:
                return err_theta
            elif sgn_angle < 0 and err_theta > 0:
                return err_theta
            else:
                diff_angle = math.fabs(angle_tl_h - max_angle)
                margin_angle = 5 * math.pi / 360  # 10°
                m = -1 / margin_angle
                q = 1
                y = m * (diff_angle) + q
                return err_theta * y
        else:
            return err_theta

    def output(self, data):
        print ">>>> OUTPUT "
        if "tactile" in data.keys():
            self.last_msg = data["tactile"]
            for i in range(3):
                FORCE_REGRESSOR[i].update(self.last_msg)

        Tr = PyKDL.Frame()
        self.current_tf = data["current_tf"]
        self.target_tf = data["target_tf"]

        if self.is_active:
            print ">>>> ACTIVE "

            # ⬢⬢⬢⬢➤ Exit Condition # TODO
            err_pos = 0
            w_ep = 1
            d_x = self.position_ball_offset
            offest_gripper = self.position_offest_gripper * (self.current_tf.M * self.unitX)   # we can add an offset along "x_tl"
            offest_hole = -self.position_offest_hole * self.xh_axis                            # we can add an offset along "-x_h"
            dist_vec = (self.hole_tf.p + offest_hole) - (self.current_tf.p + offest_gripper)
            dist_vec = transformations.KDLVectorToNumpyArray(dist_vec)
            err_pos = np.linalg.norm(dist_vec)
            if err_pos < self.position_ball_radious:
                self.done = True
                self.success = True

                # # We use this variable to scale the regualtion errors proportionally to de position error
                # k_pp = self.position_scaling_gain
                # w_ep = k_pp * SimpleOp.saturate(err_pos,
                #                                 xmin=self.position_scaling_limits[0],
                #                                 xmax=self.position_scaling_limits[1])

                # if self.exitCondition == "contact":
                #     if self.error_reg_vector[0] != 0:
                #         self.done = True
                #         self.success = True

            if self.done:
                self.is_active = False
                print "\n\n\n CONTROLLER STOP \n\n\n"
                return self.target_tf

            # ⬢⬢⬢⬢➤ Control Action
            if self.initial_tf:
                print ">>>> ACTIOOOON "

                # Estimated Force vector w.r.t. Gripper-Frame
                pred = np.zeros(3)
                for i in range(3):
                    if FORCE_REGRESSOR[i].isReady():
                        if self.is_unbiased and self.bias[i] is not None:
                            pred[i] = FORCE_REGRESSOR[i].getPrediction() - self.bias[i]
                            if math.fabs(pred[i]) < self.threshold[i] and not self.vectorThr:
                                pred[i] = 0
                        else:
                            self._remove_bias()
                            Logger.log("Waiting for bias removal")
                            return self.target_tf
                    else:
                        Logger.log("Regressor NOT ready")
                        return self.target_tf

                self.force_vector_tl = PyKDL.Vector(1.0 * min(pred[0], 0), pred[1], -pred[2])
                force_nparray_tl = transformations.KDLVectorToNumpyArray(self.force_vector_tl)
                norm_force = np.linalg.norm(np.array(pred))
                # norm_force = np.linalg.norm(force_nparray_tl)
                if norm_force < self.force_threshold and self.vectorThr:
                    self.force_vector_tl = PyKDL.Vector(0, 0, 0)
                    norm_force = 0

                fxtl = self.force_vector_tl[0]  # always <0
                fytl = self.force_vector_tl[1]
                fztl = self.force_vector_tl[2]

                # Estimated Force vector  w.r.t. Hole-Frame
                R_tl_base = self.current_tf.M
                R_h_base = self.hole_tf.M
                force_vector_b = R_tl_base * (self.force_vector_tl)
                force_vector_h = R_h_base.Inverse() * (force_vector_b)
                fxh = force_vector_h[0]
                fyh = force_vector_h[1]
                fzh = force_vector_h[2]

                # System Errors
                err_rhor = w_ep * (math.fabs(min(0, fxh)))  # (we take the absolute value of the negative part)
                err_thetay = w_ep * (fztl)
                err_thetaz = w_ep * (fytl)
                # err_thetay = self.saturate_angle(err_thetay)
                # err_thetaz = self.saturate_angle(err_thetaz)
                self.error_reg_vector = PyKDL.Vector(err_rhor, err_thetay, err_thetaz)

                # ⬢⬢⬢⬢➤ Wire Grasp Angle Correction
                if self.force_projection:
                    Rw = PyKDL.Rotation.RotY(self.wire_angle)
                    tmp = PyKDL.Frame(Rw) * PyKDL.Frame(force_vector)
                    force_vector = transformations.KDLtoNumpyVector(tmp)

                # ⬢⬢⬢⬢➤ Back-Regulation Transformation: Mb(t) = Mbx(t) * Mbyz(t)

                # ➤➤ Translation along hole's axes xh
                krp = 0.001 * self.regulation_p_gain[0]
                kri = 0.001 * self.regulation_i_gain[0]
                rhor = krp * err_rhor + kri * self.rhor_op.integrate(err_rhor)
                kf = self.force_p_gain
                deltar = (self.step_size / (1 + kf * norm_force)) - rhor
                print "deltar = {}".format(deltar)
                M_rho = PyKDL.Frame(deltar * self.ur_axis)

                # if self.lane_correction:
                #     kur = 0.001
                #     self.ur_axis = self.ur_axis + kur * PyKDL.Vector(0, err_thetay, err_thetaz)
                #     v_htl = self.hole_tf - self.current_tf
                #     u_htl = v_htl / np.linalg.norm(v_htl)
                #     htl_xh = u_htl * self.xh_axis
                #     xh_XXX = u_htl * self.ur_axis
                #     XXX_htl = self.ur_axis * self.xh_axis
                #     sgn_xh = np.sign(PyKDL.dot(xh_XXX, htl_xh))
                #     sgn_htl = np.sign(PyKDL.dot(XXX_htl, htl_xh))
                #     if sgn_xh * sgn_htl > 0:  # same sign
                #         pass
                #     elif sgn_htl < 0:  # over htl
                #         self.ur_axis = u_htl
                #     elif sgn_xh < 0:  # over xh
                #         self.ur_axis = self.xh_axis

                s_rhor = 1
                if self.integral_rotation_scaling:
                    s_rhor = SimpleOp.saturate(rhor, xmax=0.0005)
                    # print "rhor={}, s_rhor={}".format(rhor, s_rhor)

                print "err_pos = {}".format(err_pos)

                # ➤➤ Rotation about hole's axes yh
                yh = self.unitY
                kyp = 0.001 * self.regulation_p_gain[1]
                kyi = 0.001 * self.regulation_i_gain[1]
                thetay = kyp * err_thetay + kyi * self.thetay_op.integrate(err_thetay) * s_rhor
                M_yh = PyKDL.Frame(PyKDL.Rotation().Rot(yh, thetay))

                # ➤➤ Rotation about hole's axes zh
                zh = self.unitZ
                kzp = 0.001 * self.regulation_p_gain[2]
                kzi = 0.001 * self.regulation_i_gain[2]
                thetaz = kzp * err_thetaz + kzi * self.thetaz_op.integrate(err_thetaz) * s_rhor
                M_zh = PyKDL.Frame(PyKDL.Rotation().Rot(zh, thetaz))

                M_theta = self.hole_tf * M_yh * M_zh * self.hole_tf.Inverse()

                # ⬢⬢⬢⬢➤ Output Transformation M(t) = Ma(t) * Mbx(t) * Mbyz(t)
                Tr = M_rho * M_theta

                self.target_tf = Tr * self.target_tf

                #
                # ||||||| \\\\
                # ||||||| ----  ONLY FOR DEBUG
                self.estimated_twist_pub.publish(
                    transformations.TwistFormKDLVector(self.force_vector_tl))
                self.regulation_error_pub.publish(
                    transformations.TwistFormKDLVector(self.error_reg_vector))
                self.target_distance_pub.publish(err_pos)
                debug_msg = Float64MultiArray()
                debug_msg.data = [1000 * deltar, norm_force, rhor, thetay, thetaz, err_pos]
                self.debug_somedata_pub.publish(debug_msg)
                self.controller_status_pub.publish(json.dumps({"estimated_force": transformations.KDLVectorToList(self.force_vector_tl),
                                                               "regulation_error": transformations.KDLVectorToList(self.error_reg_vector),
                                                               "position_error": err_pos}))
                print "Force: {}".format(self.force_vector_tl)
                # ONLY FOR DEBUG ---- |||||||
                #                \\\\ |||||||
                #

            else:
                self.initial_tf = self.current_tf

        return self.target_tf
