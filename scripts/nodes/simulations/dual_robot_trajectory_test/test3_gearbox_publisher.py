#!/usr/bin/env python
"""
This nodes is a testing node to show CAD Library tools to Load XML files and show result in RVIZ and as STL files
"""
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.utils.visualization as visualization

import PyKDL
import numpy as np
import rospy
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point
import random
import math
import tf

# MARKER ARRAY TOPIC
topic = 'visualization_marker_array'
publisher = rospy.Publisher(topic, MarkerArray, queue_size=1)

# NODE
rospy.init_node('testing_database')
rate = rospy.Rate(10)  # 10hz
br = tf.TransformBroadcaster()
listener = tf.TransformListener()

# PARAMS
output_stl = rospy.get_param('~output_stl', False)
filtered_components = rospy.get_param('~filtered_components', "")
random_pos = rospy.get_param('~random_pos', False)

# Gearbox
files_path = '/home/daniele/Scrivania/gearbox_zero/'
gearbox = Gearbox("gearbox00001", files_path)

if random_pos:
    gearbox.base.p = PyKDL.Vector(
        random.uniform(-1, 1), random.uniform(-1, 1), random.uniform(-1, 1))
    gearbox.base.M.DoRotX(random.uniform(-0.7, 0.7))
    gearbox.base.M.DoRotY(random.uniform(-0.7, 0.7))
    gearbox.base.M.DoRotZ(random.uniform(-0.7, 0.7))

# Output STL files
if output_stl:
    gearbox.scene.exportSTLData(files_path + 'out', base_transform=False)
    gearbox.scene.exportSTLData(files_path + 'out', False,
                                prefix='corrected_', correction=True)
    gearbox.scene.exportSTLData(files_path + 'out', False,
                                prefix='boxed_', correction=True, box_rf=True)


def broadcastTransform(br, frame, frame_id, parent_frame, time=rospy.get_rostime()):
    br.sendTransform((frame.p.x(), frame.p.y(), frame.p.z()),
                     frame.M.GetQuaternion(),
                     time,
                     frame_id,
                     parent_frame)


transforms = []
gearbox.scene.deepSearch(transforms, only_useful=True)
markerArray = MarkerArray()
package_path = 'file://' + files_path + 'out/'


# for trans in gearbox.useful_transforms:
# print str(trans), '->', str(trans.getRoot())

id_counter = 0
for trans in transforms:

    if len(filtered_components) > 0:
        if trans.item_id.id_list[0] not in filtered_components:
            continue

    parent = trans.parent
    path = package_path + trans.getName() + ".stl"
    color = visualization.Color(0.5, 0.5, 0.5, 1)
    if trans.getName() == 'ID000074' or trans.getName() == 'ID000039':
        color = visualization.Color(1, 0, 0, 1)
    marker = visualization.createMesh(parent.getName(), mesh_path=path,
                                      transform=trans, color=color)
    marker.id = len(markerArray.markers)
    markerArray.markers.append(marker)

    if len(trans.item_id.id_list) == 3:
        part = gearbox.parts.getPartById(trans.item_id.id_list[2])
        if part != None:
            for terminal in part.terminals:

                arrow = visualization.createArrow(
                    trans.getName() + "_final", terminal)
                arrow.id = len(markerArray.markers)
                markerArray.markers.append(arrow)

# Display Channels
for channel_id in gearbox.channels.channel_map:
    channel = gearbox.getChannel(channel_id)
    transform = channel.getRelativeFrame()
    arrow = visualization.createArrow(
        "gearbox", transform, length=1.3)
    arrow.id = len(markerArray.markers)
    markerArray.markers.append(arrow)

    points = channel.get2DStartEndPoint()
    marker = visualization.createLineList(
        "gearbox", thickness=0.002, points=points, color=visualization.Color(0, 1, 0, 1))
    marker.id = len(markerArray.markers)
    markerArray.markers.append(marker)

for intersection in gearbox.channels.getIntersections():
    int_trans = intersection.getFrame()
    sphere = visualization.createSphere(
        "gearbox", transform=int_trans, radius=0.1)
    sphere.id = len(markerArray.markers)
    markerArray.markers.append(sphere)


while not rospy.is_shutdown():

    # Move Gearbox on gearbox_rf if it is available
    try:
        tf_gearbox = listener.lookupTransform(
            '/world', '/gearbox_rf', rospy.Time(0))

        correction = gearbox.scene.getCorrection(
            gearbox.base, box_correction=False)

        tf_gearbox_frame = transformations.tfToKDL(tf_gearbox)
        tf_gearbox_frame = tf_gearbox_frame * correction.Inverse()
        gearbox.base.p = tf_gearbox_frame.p
        gearbox.base.M = tf_gearbox_frame.M
    except (tf.ExtrapolationException, tf.LookupException) as e:
        print e

    current_time = rospy.get_rostime()

    for marker in markerArray.markers:
        marker.header.stamp = current_time
    publisher.publish(markerArray)

    # Broadcast Channel tF
    for intersection in gearbox.channels.getIntersections():
        broadcastTransform(br, intersection.getFrame(), intersection.getName(),
                           "gearbox", time=current_time)

    # Broadcast Component tF
    for trans in gearbox.useful_transforms:
        if trans.item_id == None:
            continue

        if trans.getName() == 'ID000074':
            relative_test = gearbox.getProjectedFrame(trans)
            broadcastTransform(br, relative_test, "PINO",
                               "gearbox", time=current_time)

        if trans.isRoot():
            broadcastTransform(br, trans, trans.getName(),
                               "world", time=current_time)

            correction = gearbox.scene.getCorrection(
                trans, box_correction=False)
            broadcastTransform(br, correction, "gearbox",
                               trans.getName(), time=current_time)

        else:
            broadcastTransform(br, trans, trans.getName(),
                               trans.parent.getName(), time=current_time)

            boxed_transform = gearbox.scene.getCorrection(trans)
            broadcastTransform(br, boxed_transform, trans.getName() + "_final",
                               trans.getName(), time=current_time)

            centered_transform = gearbox.scene.getCorrection(
                trans, box_correction=False)
            broadcastTransform(br, centered_transform, trans.getName() + "_centered",
                               trans.getName(), time=current_time)

    # DEBUG TEST
    part = gearbox.parts.getPartByCadId("ID000074")
    rtt = gearbox.getRelativeFrame(part)
    broadcastTransform(br, rtt, "PLUTO",
                       "gearbox", time=current_time)

    i = 0
    for t in part.terminals:
        ttt = gearbox.getRelativeFrame(t)
        broadcastTransform(br, ttt, "PLUTO_{}".format(i),
                           "gearbox", time=current_time)
        proj = gearbox.getProjectedFrame(t)
        broadcastTransform(br, proj, "PLUTO_PROJ_{}".format(i),
                           "gearbox", time=current_time)

        i += 1

    rate.sleep()
