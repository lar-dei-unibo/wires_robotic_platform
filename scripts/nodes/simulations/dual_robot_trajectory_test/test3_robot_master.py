#!/usr/bin/env python
# license removed for brevity

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import math
import pprint
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
from wires_robotic_platform.robots.controllers import RobotController, GeneralIKService
from wires_robotic_platform.srv import IKService, IKServiceResponse
import wires_robotic_platform.utils.transformations as transformations
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import tf
from scipy.interpolate import interp1d
import numpy as np
from std_msgs.msg import String


rospy.init_node('testing_trajectory_1_commander')
listener = tf.TransformListener()
rate = rospy.Rate(10)  # 10hz
pub = rospy.Publisher('command', String, queue_size=10)

commands = [
    '',
    'reset',
    'gripper_up',
    'ID000074#down',
    'gripper_down2',
    'gripper_up',
    'I_ID000074_-U3#down',
    'I_ID000074_-U3#right',
    'I_-U3_-U6#right',
    'I_-U3_-U6#down',
    'I_-U2_-U6#down',
    'I_-U2_-U6#right',
    'I_ID000039_-U2#right',
    'I_ID000039_-U2#up',
    'ID000039#up',
    'ID000039#up#insert',
    'gripper_down',
    'gripper_up',
    'reset'
]

commands_pointer = 0

first = True
while not rospy.is_shutdown():

    something = raw_input()

    if first:
        pub.publish('reset')

        first = False
    else:
        print commands[commands_pointer]
        pub.publish(commands[commands_pointer])
        commands_pointer += 1
        if commands_pointer >= len(commands):
            rospy.signal_shutdown(0)
    rate.sleep()
