#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
import message_filters
from sensor_msgs.msg import Image, CameraInfo
from geometry_msgs.msg import Point
from sensor_msgs.msg import PointCloud
from cv_bridge import CvBridge, CvBridgeError
import std_msgs
from wires_robotic_platform.robots.controllers import RobotController, GeneralIKService, Robot
from wires_robotic_platform.robots.trajectories import FuzzyTrajectoryGenerator
from wires_robotic_platform.robots.market import RobotMarket
from wires_robotic_platform.partdb.proxy import GearboxProxy
from wires_robotic_platform.srv import IKService, IKServiceResponse
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.sfm.machines as machines
from wires_robotic_platform.utils.logger import Logger

from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import tf
from scipy.interpolate import interp1d
import numpy as np
from std_msgs.msg import String
from transitions import Machine

rospy.init_node('testing_trajectory_1')
listener = tf.TransformListener()


# Gearbox GearboxProxy
GearboxProxy.init(tf_listener=listener)

# Creates Robot from Market
comau_robot = RobotMarket.createRobotByName("comau_smart_six#v1")
comau_trajectories_generator = FuzzyTrajectoryGenerator(comau_robot)

grasshopper = RobotMarket.createRobotByName("grasshopper#v1")
grasshopper_trajecotries_generator = FuzzyTrajectoryGenerator(grasshopper)


# COMMANDS
waypoint = ""
commands = ['reset', 'calibration', 'reset']

# FRAMES
gearbox_frames = None
current_target = None
end_frame = PyKDL.Frame()

# TOOL
tool = PyKDL.Frame()
tool.p = PyKDL.Vector(-0.06133, -0.004, 0)


# TRAJECTORY
current_trajectory = []
trajectory_time = 10


class SFM_ROBOT_MOVE(object):

    def __init__(self):
        self.current_command = ""
        self.command_list = []
        self.playground_object_frame = None
        self.current_tool = None
        self.robot_ee = None
        self.robot_base = None
        self.stripe_1 = None
        self.stripe_2 = None
        self.current_trajectory = []
        self.counter = 1000

    def resetWaitCounter(self):
        self.counter = 200

    def isWaitCounterActive(self):
        self.counter -= 1
        return self.counter >= 0

    #⬢⬢⬢⬢⬢➤ PREPARING ENTER
    def on_enter_preparing(self):
        Logger.log("Entering on Preparing")

    #⬢⬢⬢⬢⬢➤ PREPARING LOOPING
    def on_loop_preparing(self):
        self.robot_ee = comau_robot.getEEFrame()
        self.stripe_1 = transformations.retrieveTransform(
            listener, "world", "stripe_1", print_error=True)
        self.stripe_2 = transformations.retrieveTransform(
            listener, "world", "stripe_2", print_error=True)

        self.robot_base = transformations.retrieveTransform(
            listener, "world", "comau_smart_six/base_link", print_error=True)

        self.current_tool = transformations.retrieveTransform(
            listener, "comau_smart_six/link6", "camera_rf", print_error=True)

        if self.robot_ee:
            if self.stripe_1 and self.stripe_2:
                if self.robot_base:
                    if self.current_tool:
                        self.stripe_1 = self.robot_base.Inverse() * \
                            self.stripe_1
                        self.stripe_2 = self.robot_base.Inverse() * \
                            self.stripe_2
                        self.begin()

    def _moveCartesian(self, target):
        q_in = comau_robot.getController().getQ()
        ee_target = target * self.current_tool.Inverse()
        comau_trajectories_generator.current_trajectory = comau_trajectories_generator.generateTrajectoryFromFrames(
            q_in, comau_robot.getEEFrame(), ee_target, hz,
            trajectory_time)

    #⬢⬢⬢⬢⬢➤
    def on_loop_waitcommand(self):
        if len(self.command_list) > 0:
            command = self.command_list.pop(0)

            chunks = command.split("#")
            tip = chunks[0]
            version = chunks[1]
            Logger.log("Command: {},{}".format(tip, version))

            delta = PyKDL.Frame()
            if version == "top":
                delta = PyKDL.Frame(PyKDL.Vector(0.0, 0, -0.3))
            elif version == "up":
                delta = PyKDL.Frame(PyKDL.Vector(0.5, 0, 0))
            elif version == "topup":
                delta = PyKDL.Frame(PyKDL.Vector(0.8, 0, -0.3))

            if tip == "s1":
                self._moveCartesian(self.stripe_1 * delta)
            if tip == "s2":
                self._moveCartesian(self.stripe_2 * delta)

            self.waitmoving()

    def on_loop_waitmoving(self):
        if comau_robot.getController().isMoving():
            self.moving()

    def on_loop_moving(self):
        if not comau_robot.getController().isMoving():
            self.steady()

    def on_loop_steady(self):
        self.begin()


SFM_MODEL = SFM_ROBOT_MOVE()
sfm = machines.SFMachine(name="sfm_inserting", model=SFM_MODEL)
sfm.addState("start")
sfm.addState("preparing")
sfm.addState("waitcommand")
sfm.addState("waitmoving")
sfm.addState("moving")
sfm.addState("steady")
sfm.addState("end")
sfm.addTransition("start", "start", "preparing")
sfm.addTransition("begin", "preparing", "waitcommand")
sfm.addTransition("begin", "steady", "waitcommand")
sfm.addTransition("waitmoving", "waitcommand", "waitmoving")
sfm.addTransition("moving", "waitmoving", "moving")
sfm.addTransition("steady", "moving", "steady")
sfm.addTransition("end", "steady", "end")
sfm.create()


SFM_MODEL.command_list = [
    's1#top',
    's1#topup',
    's2#topup',
    's2#top',
    's2#n',
    's2#up',
    's1#up',
    's1#n'
]


sfm.set_state("start")
sfm.getModel().start()


hz = 100
rate = rospy.Rate(hz)  # 10hz
start_time = rospy.get_time()
f = 0.1

cvbridge = CvBridge()

camera = {
    "fx": 512,
    "fy": 512,
    "cx": 641 / 2,
    "cy": 481 / 2
}
cloud_pub = rospy.Publisher(
    'pointcloud_debug',
    PointCloud,
    queue_size=10
)


while not rospy.is_shutdown():
    # print sfm.getModel().state
    sfm.loop()

    # grasshopper_trajecotries_generator.applyTrajectory(rospy.Time.now())
    comau_trajectories_generator.applyTrajectory(rospy.Time.now())

    # comau_robot.getController().jointCommand(
    #    [.78, 0, -1.57, 0, 1.57, 0], rospy.Time.now())
    # if len(SFM_MODEL.current_trajectory) > 0:
    #    curr2 = SFM_MODEL.current_trajectory.pop(0)
    #    if curr2.isValid():
    # comau_robot.getController().jointCommand(curr2.q, rospy.Time.now())

    rate.sleep()
