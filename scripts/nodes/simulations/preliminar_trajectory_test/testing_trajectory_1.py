#!/usr/bin/env python
# license removed for brevity

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import math
import pprint
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
from wires_robotic_platform.robots.controllers import RobotController, GeneralIKService
from wires_robotic_platform.srv import IKService, IKServiceResponse
import wires_robotic_platform.utils.transformations as transformations
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import tf
from scipy.interpolate import interp1d
import numpy as np
from std_msgs.msg import String


rospy.init_node('testing_trajectory_1')
listener = tf.TransformListener()

comau = RobotController("comau_smart_six", [
    'base_to_link1',
    'link1_to_link2',
    'link2_to_link3',
    'link3_to_link4',
    'link4_to_link5',
    'link5_to_link6',
])

grasshopper = RobotController("grasshopper", [
    'grasshopper_base_to_link1',
    'grasshopper_link1_to_link2',
    'grasshopper_link2_to_finger_l',
    'grasshopper_link2_to_finger_r'
])

comau_ik_service = GeneralIKService(
    '/comau_smart_six/ik_service_node/ik_service')


rate = rospy.Rate(1000)  # 10hz
start_time = rospy.get_time()
f = 0.1


def interpolation(frame1, frame2, steps):
    x = np.linspace(frame1.p.x(), frame2.p.x(), num=steps, endpoint=True)
    y = np.linspace(frame1.p.y(), frame2.p.y(), num=steps, endpoint=True)
    z = np.linspace(frame1.p.z(), frame2.p.z(), num=steps, endpoint=True)

    q1 = frame1.M.GetQuaternion()
    q2 = frame2.M.GetQuaternion()

    qx = np.linspace(q1[0], q2[0], num=steps, endpoint=True)
    qy = np.linspace(q1[1], q2[1], num=steps, endpoint=True)
    qz = np.linspace(q1[2], q2[2], num=steps, endpoint=True)
    qw = np.linspace(q1[3], q2[3], num=steps, endpoint=True)

    frames = []
    for i in range(0, len(x)):
        f = PyKDL.Frame()
        f.M = PyKDL.Rotation.Quaternion(
            qx[i],
            qy[i],
            qz[i],
            qw[i]
        )
        f.p.x(x[i])
        f.p.y(y[i])
        f.p.z(z[i])
        frames.append(f)
    return frames


target_frame = Frame()
target_frame.p = Vector(0.77, 0, 0.37)
target_frame.M.DoRotY(-math.pi / 2)

in_trajectory = False

elevation = PyKDL.Frame()
elevation.p.z(0.6)

start_position = PyKDL.Frame()
start_position.p = PyKDL.Vector(-0.25, -0.72, 1.09)
trajectory = []


def cloneFrame(frame):
    f2 = PyKDL.Frame()
    f2.p = frame.p
    f2.M = frame.M
    return f2


def retrieveGearboxTree():
    channels = []
    for i in range(1, 7):
        channels.append('-U{}'.format(i))

    intersections = [
        'I_-U1_-U5',
        'I_-U2_-U5',
        'I_-U3_-U5',
        'I_-U4_-U5',
        'I_-U1_-U6',
        'I_-U2_-U6',
        'I_-U3_-U6',
        'I_-U4_-U6',
    ]

    int_frames = {}
    for intersection in intersections:
        frame = transformations.retrieveTransform(
            listener, "world", intersection, print_error=True)
        if frame:
            int_frames[intersection] = frame
        else:
            return None

    c1 = PyKDL.Frame()
    c1.p = PyKDL.Vector(0.18, 0.1, 0)

    c2 = PyKDL.Frame()
    c2.p = PyKDL.Vector(0.3, 1, 0)

    c1 = int_frames['I_-U3_-U5'] * c1
    c2 = int_frames['I_-U1_-U5'] * c2

    int_frames['I_ID000074_-U3'] = int_frames['I_-U3_-U5'] * \
        PyKDL.Frame(PyKDL.Vector(0.18, 0, 0))
    int_frames['ID000074'] = int_frames['I_-U3_-U5'] * \
        PyKDL.Frame(PyKDL.Vector(0.18, 0.06, 0))
    int_frames['I_ID000039_-U2'] = int_frames['I_-U2_-U5'] * \
        PyKDL.Frame(PyKDL.Vector(0.29, 0, 0))
    int_frames['ID000039'] = int_frames['I_-U2_-U5'] * \
        PyKDL.Frame(PyKDL.Vector(0.29, -0.04, 0))

    temp = {}
    for key, value in int_frames.iteritems():
        temp[key + "#up"] = cloneFrame(value)
        temp[key + "#up"].M.DoRotZ(1.57)
        temp[key + "#left"] = cloneFrame(value)
        temp[key + "#left"].M.DoRotZ(3.14)
        temp[key + "#down"] = cloneFrame(value)
        temp[key + "#down"].M.DoRotZ(3.14 + 1.57)
        temp[key + "#right"] = value

    int_frames.update(temp)

    temp = {}
    for key, value in int_frames.iteritems():
        temp[key + "#insert"] = cloneFrame(value)

        temp[key + "#insert"] = temp[key + "#insert"] * PyKDL.Frame(
            PyKDL.Vector(0, 0, -0.05))
        print "Added: ", key + "#insert"
    int_frames.update(temp)
    return int_frames


gripper_map = {
    'gripper_up': [0.05, 0, 0, 0],
    'gripper_middle': [0.02, 0, 0, 0],
    'gripper_down': [-0.07, 0, 0, 0],
    'gripper_down2': [-0.09, 0, 0, 0]
}

# FRAMES
gearbox_frames = None
current_target = None

# TOOL
tool = PyKDL.Frame()
tool.p = PyKDL.Vector(-0.04, 0, 0)

waypoint = ""


def command_callback(data):
    global waypoint
    waypoint = data.data
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)


rospy.Subscriber("command", String, command_callback)

while not rospy.is_shutdown():
    # comau.jointCommand([0,0,-1.57,0,0,0], rospy.Time.now())
    if gearbox_frames == None:
        gearbox_frames = retrieveGearboxTree()

    robot_ee = transformations.retrieveTransform(
        listener, "world", "comau_smart_six/link6", print_error=True)

    robot_base = transformations.retrieveTransform(
        listener, "world", "comau_smart_six/base_link", print_error=True)

    if waypoint in gripper_map:
        grasshopper.jointCommand(
            gripper_map[waypoint], rospy.Time.now())
        waypoint = ''

    if waypoint == 'reset':
        comau.jointCommand([-0.4, 0.2, -1.0, 0.1, 0.9, 0.1], rospy.Time.now())
        trajectory = []
        waypoint = ''

    if robot_base and gearbox_frames and len(waypoint) > 0:
        print "NEW WAYPOINT!", waypoint
        C1 = gearbox_frames[waypoint]

        if C1:
            C1 = C1 * elevation
            C1.M.DoRotZ(0)
            current_target = robot_base.Inverse() * C1
            current_target = current_target * tool.Inverse()
            # trajectory = interpolation(start_position, target_frame, 100)
            # print target_frame

    if robot_ee and robot_base:
        if current_target:
            if len(trajectory) == 0 and len(waypoint) > 0:
                waypoint = ''
                start_frame = robot_base.Inverse() * robot_ee
                trajectory = interpolation(start_frame, current_target, 500)
                print len(trajectory)

    # Griupper

    if len(trajectory) > 0:
        q_in = comau.getQ()
        current_target = trajectory.pop(0)
        response = comau_ik_service.simple_ik(current_target, q_in)
        print response
        if response.status == IKServiceResponse.STATUS_OK:
            q = response.q_out.data
            comau.jointCommand(q, rospy.Time.now())
            in_trajectory = True

    rate.sleep()
