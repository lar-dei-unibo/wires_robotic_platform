#!/usr/bin/env python
"""
This nodes is a testing node to show CAD Library tools to Load XML files and show result in RVIZ and as STL files
"""
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel, ItemType
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData

import PyKDL
import numpy as np
import rospy
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point
from std_msgs.msg import String
from std_srvs.srv import Trigger, TriggerResponse
import random
import math
import tf
import time

from enum import Enum

COMPLETE_KEYWORD = 'COMPLETE: '
ERROR_KEYWORD = 'ERROR: '


class State(Enum):
    STAND_BY = 1
    VISUAL = 2
    CONNECTION = 3


# MARKER ARRAY TOPIC
topic = 'visualization_marker_array'
publisher = rospy.Publisher(topic, MarkerArray, queue_size=1)

old_ID = ''
state = State.STAND_BY

# NODE
rospy.init_node('testing_database')
rate = rospy.Rate(1)  # 10hz
br = tf.TransformBroadcaster()
listener = tf.TransformListener()

# PARAMS
output_stl = rospy.get_param('~output_stl', False)

# Gearbox
#files_path = '/home/luca/catkin_wires/src/wires_robotic_platform/data/gearbox_zero/'
files_path = rospy.get_param('~files_path', "")
cad_name = rospy.get_param('~cad_name', "")
#gearbox = Gearbox("gearbox00001", files_path)
gearbox = Gearbox(cad_name, files_path)

components_file_name = rospy.get_param('~component_file', "")
gearbox_file_name = cad_name + "_ids.wri"
wiring_file_name = rospy.get_param('~wiring_file', "")
dict_gearbox = GearboxPartsData(files_path, gearbox_file_name, components_file_name, wiring_file_name)

# Output STL files
if output_stl:
    gearbox.scene.exportSTLData(files_path + 'out', base_transform=False)
    gearbox.scene.exportSTLData(files_path + 'out', False,
                                prefix='corrected_', correction=True)
    gearbox.scene.exportSTLData(files_path + 'out', False,
                                prefix='boxed_', correction=True, box_rf=True)


def broadcastTransform(br, frame, frame_id, parent_frame, time=rospy.get_rostime()):
    br.sendTransform((frame.p.x(), frame.p.y(), frame.p.z()),
                     frame.M.GetQuaternion(),
                     time,
                     frame_id,
                     parent_frame)


transforms = gearbox.useful_transforms  # transforms are read by the Gearbox constructor
# gearbox.scene.deepSearch(transforms, only_useful=True) # read x3d files and generates switchgear tree structure
markerArray = MarkerArray()
package_path = 'file://' + files_path + 'out/'


# for trans in gearbox.useful_transforms:
# print str(trans), '->', str(trans.getRoot())

for trans in transforms:

    parent = trans.parent
    path = package_path + trans.getName() + ".stl"
    color = visualization.Color(0.5, 0.5, 0.5, 1)
    # visualize red components
    # if trans.getName() == 'ID000074' or trans.getName() == 'ID000039':
    #    color = visualization.Color(1, 0, 0, 1)

    # check for the component list in wri file if 3 columns are present
    if len(trans.item_id.id_list) == 3:
        marker = visualization.createMesh(parent.getName(), mesh_path=path,
                                          transform=trans, color=color)
        marker.id = len(markerArray.markers)
        marker.text = trans.item_id.id_list[0]
        markerArray.markers.append(marker)

        part = gearbox.parts.getPartById(trans.item_id.id_list[2])  # select the component commercial ID
        if part != None:
            # print rospy.get_caller_id() + " Part " + trans.item_id.id_list[2] + ' with cad ID ' + trans.item_id.id_list[0] + ' and name ' + trans.item_id.id_list[1] + ' found'
            for terminal in part.terminals:
                # print 'get terminal for node ' + trans.item_id.id_list[2]
                arrow = visualization.createArrow(
                    trans.getName() + "_final", terminal, 0.02)
                arrow.id = len(markerArray.markers)
                markerArray.markers.append(arrow)

part_trans = None
part_index = 0

connection_service = 'connection_tf_service'
connection_index = 0
connection = {}
connection_source = ''


def connection_service_callback(msg):

    global part_trans
    global connection_index
    global state
    global connection
    global connection_source

    resp = TriggerResponse()
    state = State.STAND_BY
    part_trans = None
    resp.success = False
    resp.message = ERROR_KEYWORD + 'Connection data not available'

    try:
        connection = dict_gearbox.connection_list[connection_index]
        connection_index = connection_index + 1

    except IndexError:
        connection_index = 0
        resp.message = COMPLETE_KEYWORD + 'Connection scan completed'
        return resp

    for source in ['orig', 'dest']:
        if connection[source]['screw'] and connection[source]['hole']:
            resp.success = True
            resp.message = connection[source]['part_cadID']
            connection_source = source
            state = State.CONNECTION

            for trans in gearbox.useful_transforms:
                if trans.getID().getCadID() == connection[source]['part_cadID']:
                    part_trans = trans
                    break

            break

    return resp


rospy.Service(connection_service, Trigger, connection_service_callback)

tf_service = 'part_tf_service'


def ID_service_callback(msg):

    global part_trans
    global part_index
    global state

    resp = TriggerResponse()

    while True:
        try:
            part_trans = gearbox.useful_transforms[part_index]
            part_index = part_index + 1
        except IndexError:
            part_index = 0
            #part_trans = gearbox.useful_transforms[part_index]
            part_trans = None
            resp.success = False
            resp.message = COMPLETE_KEYWORD + 'Part scan completed'
            state = State.STAND_BY
            break

        if part_trans.getID().getType() == ItemType.PART:
            resp.success = True
            resp.message = part_trans.getID().getCadID()
            state = State.VISUAL
            break

    return resp


rospy.Service(tf_service, Trigger, ID_service_callback)

# Display Channels
for channel_id in gearbox.channels.channel_map:
    channel = gearbox.getChannel(channel_id)
    transform = channel.getRelativeFrame()
    arrow = visualization.createArrow(
        "gearbox", transform, length=1.3)
    arrow.id = len(markerArray.markers)
    arrow.text = 'arrow' + channel_id
    markerArray.markers.append(arrow)

    points = channel.get2DStartEndPoint()
    marker = visualization.createLineList(
        "gearbox", thickness=0.002, points=points, color=visualization.Color(0, 1, 0, 1))
    marker.id = len(markerArray.markers)
    marker.text = 'line' + channel_id
    markerArray.markers.append(marker)

for intersection in gearbox.channels.getIntersections():
    int_trans = intersection.getFrame()
    sphere = visualization.createSphere(
        "gearbox", transform=int_trans, radius=0.1)
    sphere.id = len(markerArray.markers)
    sphere.text = 'sphere' + channel_id
    markerArray.markers.append(sphere)


while not rospy.is_shutdown():

    # Move Gearbox on gearbox_rf if it is available
    try:
        tf_gearbox = listener.lookupTransform(
            '/world', '/gearbox_rf', rospy.Time(0))

        correction = gearbox.scene.getCorrection(
            gearbox.base, box_correction=False)

        tf_gearbox_frame = transformations.tfToKDL(tf_gearbox)
        tf_gearbox_frame = tf_gearbox_frame * correction.Inverse()
        gearbox.base.p = tf_gearbox_frame.p
        gearbox.base.M = tf_gearbox_frame.M
    # except (tf.ExtrapolationException, tf.LookupException) as e:
    #    print e
    except AttributeError:
        if not tf_gearbox_frame:
            print "tf_gearbox_frame empty"

    current_time = rospy.get_rostime()

    for marker in markerArray.markers:
        marker.header.stamp = current_time
    publisher.publish(markerArray)

    # Broadcast Channel tF
    # for intersection in gearbox.channels.getIntersections():
    #    broadcastTransform(br, intersection.getFrame(), intersection.getName(),
    #                       "gearbox", time=current_time)

    root = trans.getRoot()

    broadcastTransform(br, root, root.getName(),
                       "gearbox_rf", time=current_time)

    correction = gearbox.scene.getCorrection(
        root, box_correction=False)
    broadcastTransform(br, correction, "gearbox",
                       root.getName(), time=current_time)

    if part_trans:

        broadcastTransform(br, part_trans, 'part_origin', part_trans.parent.getName(), time=current_time)

        back_trans = gearbox.scene.retrieveTransform(part_trans, False, True, False)
        #broadcastTransform(br, back_trans, 'back_trans', root.getName(), time=current_time)

        corrected_transform = correction.Inverse() * back_trans  # gearbox.scene.retrieveTransform(back_trans)#,True,True,False)# * back_trans.Inverse()
        #broadcastTransform(br, corrected_transform, 'back_trans_gearbox', 'gearbox', time=current_time)

        corner_trans_gearbox = part_trans.getKDLFrameFromBoundingBox(corrected_transform.Inverse())
        #broadcastTransform(br, corner_trans_gearbox, 'corner_trans_gearbox', 'gearbox', time=current_time)

        corner_trans = corrected_transform.Inverse() * corner_trans_gearbox
        broadcastTransform(br, corner_trans, 'part_corner', 'part_origin', time=current_time)

        if state == State.VISUAL:
            # try:

            #     commercial_id = dict_gearbox.dict_board[part_trans.getID().getCadID()]["product"]
            #     dic_tfs = dict_gearbox.component_dict[commercial_id]

            #     broadcastTransform(br, dic_tfs['visual'],
            #                             "target",
            #                             'part_corner',
            #                             time=current_time)
            # except KeyError:
            tf = PyKDL.Frame()
            dim = part_trans.getWholeShape().transformedShape(corrected_transform.Inverse()).getXYZSize()
            #[x_dim, y_dim, z_dim] = dim

            tf.p = PyKDL.Vector(dim[0] / 2, dim[1] / 2, dim[2] + 0.15)
            tf.M = root.M
            tf.M.DoRotX(math.pi)
            broadcastTransform(br, tf,
                               "target",
                               'part_corner',
                               time=current_time)

        if state == State.CONNECTION:

            try:
                broadcastTransform(br, connection[connection_source]['hole'],
                                   "target_h",
                                   'part_corner',
                                   time=current_time)
            except KeyError:
                print "hole data not available for part " + connection[connection_source]['part_label'] + " and terminal " + connection[connection_source]['pin']

            try:
                broadcastTransform(br, connection[connection_source]['screw'],
                                   "target_s",
                                   'part_corner',
                                   time=current_time)
            except KeyError:
                print "screw data not available for part " + connection[connection_source]['part_label'] + " and terminal " + connection[connection_source]['pin']

        parent = part_trans.parent

        while not parent.isRoot():
            broadcastTransform(br, parent, parent.getName(), parent.parent.getName(), time=current_time)

            #boxed_transform = gearbox.scene.getCorrection(parent)
            #broadcastTransform(br, boxed_transform, parent.getName() + "_final", parent.getName(), time=current_time)
            parent = parent.parent

    rate.sleep()
