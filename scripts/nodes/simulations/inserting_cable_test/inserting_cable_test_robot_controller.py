#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
from wires_robotic_platform.robots.controllers import RobotController, GeneralIKService, Robot
from wires_robotic_platform.robots.trajectories import FuzzyTrajectoryGenerator
from wires_robotic_platform.robots.market import RobotMarket
from wires_robotic_platform.partdb.proxy import GearboxProxy
from wires_robotic_platform.srv import IKService, IKServiceResponse
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.sfm.machines as machines
from wires_robotic_platform.utils.logger import Logger

from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import tf
from scipy.interpolate import interp1d
import numpy as np
from std_msgs.msg import String
from transitions import Machine

rospy.init_node('testing_trajectory_1')
listener = tf.TransformListener()


# Gearbox GearboxProxy
GearboxProxy.init(tf_listener=listener)

# Creates Robot from Market
comau_robot = RobotMarket.createRobotByName("comau_smart_six#v1")
comau_trajectories_generator = FuzzyTrajectoryGenerator(comau_robot)

grasshopper = RobotMarket.createRobotByName("grasshopper#v1")
grasshopper_trajecotries_generator = FuzzyTrajectoryGenerator(grasshopper)


# COMMANDS
waypoint = ""
commands = ['reset', 'calibration', 'reset']

# FRAMES
gearbox_frames = None
current_target = None
end_frame = PyKDL.Frame()

# TOOL
tool = PyKDL.Frame()
tool.p = PyKDL.Vector(-0.06133, -0.004, 0)


# TRAJECTORY
current_trajectory = []
trajectory_time = 10


class SFM_ROBOT_MOVE(object):

    def __init__(self):
        self.current_command = ""
        self.command_list = []
        self.playground_object_frame = None
        self.current_tool = None
        self.robot_ee = None
        self.robot_base = None
        self.current_trajectory = []
        self.counter = 1000

    def resetWaitCounter(self):
        self.counter = 200

    def isWaitCounterActive(self):
        self.counter -= 1
        return self.counter >= 0

    #⬢⬢⬢⬢⬢➤ PREPARING ENTER
    def on_enter_preparing(self):
        Logger.log("Entering on Preparing")

    #⬢⬢⬢⬢⬢➤ PREPARING LOOPING
    def on_loop_preparing(self):
        self.robot_ee = comau_robot.getEEFrame()
        self.playground_object_frame = transformations.retrieveTransform(
            listener, "world", "playground_component/hole", print_error=True)
        self.robot_base = transformations.retrieveTransform(
            listener, "world", "comau_smart_six/base_link", print_error=True)

        self.current_tool = transformations.retrieveTransform(
            listener, "comau_smart_six/link6", "grasshopper/driver", print_error=True)

        if self.robot_ee:
            if self.playground_object_frame:
                if self.robot_base:
                    if self.current_tool:
                        self.playground_object_frame = self.robot_base.Inverse() * \
                            self.playground_object_frame
                        self.ready()

    #⬢⬢⬢⬢⬢➤ READY ENTER
    def on_enter_ready(self):
        Logger.log("Ready!")
        self.approach()

    def on_enter_approach(self):
        Logger.log("Approach!")
        q_in = comau_robot.getController().getQ()
        target = self.playground_object_frame * \
            PyKDL.Frame(PyKDL.Vector(0, 0, 0.05))
        ee_target = target * self.current_tool.Inverse()
        comau_trajectories_generator.current_trajectory = comau_trajectories_generator.generateTrajectoryFromFrames(
            q_in, comau_robot.getEEFrame(), ee_target, hz,
            trajectory_time)
        self.resetWaitCounter()

    def on_loop_approach(self):
        if not self.isWaitCounterActive():
            if not comau_robot.getController().isMoving():
                self.insert()
                pass

    def on_enter_insert(self):
        Logger.log("Insert!")
        q_in = comau_robot.getController().getQ()
        target = self.playground_object_frame * \
            PyKDL.Frame(PyKDL.Vector(0, 0, 0))
        ee_target = target * self.current_tool.Inverse()
        comau_trajectories_generator.current_trajectory = comau_trajectories_generator.generateTrajectoryFromFrames(
            q_in, comau_robot.getEEFrame(), ee_target, hz,
            trajectory_time)
        self.resetWaitCounter()

    def on_loop_insert(self):
        if not self.isWaitCounterActive():
            if not comau_robot.getController().isMoving():
                self.align()

    def on_enter_align(self):
        Logger.log("Align!")
        grass_q_in = grasshopper.getController().getQ()
        grass_q_out = [-0.060, 0.045, 0.003, 0.003]
        #grass_q_out = [-0.067, 0.045, 0.003, 0.003]
        grasshopper_trajecotries_generator.current_trajectory = grasshopper_trajecotries_generator.generateTrajectoryFromShapes(
            grass_q_in, grass_q_out, hz, trajectory_time
        )
        self.resetWaitCounter()

    def on_loop_align(self):
        if not self.isWaitCounterActive():
            if not grasshopper.getController().isMoving():
                self.plug()

    def on_enter_plug(self):
        Logger.log("Plug!")
        grass_q_in = grasshopper.getController().getQ()
        grass_q_out = [-0.060, 0.075, 0.003, 0.003]
        #grass_q_out = [-0.067, 0.045, 0.003, 0.003]
        grasshopper_trajecotries_generator.current_trajectory = grasshopper_trajecotries_generator.generateTrajectoryFromShapes(
            grass_q_in, grass_q_out, hz, trajectory_time
        )
        self.resetWaitCounter()


SFM_MODEL = SFM_ROBOT_MOVE()
sfm = machines.SFMachine(name="sfm_inserting", model=SFM_MODEL)
sfm.addState("start")
sfm.addState("preparing")
sfm.addState("ready")
sfm.addState("approach")
sfm.addState("insert")
sfm.addState("align")
sfm.addState("plug")
sfm.addTransition("prepare", "start", "preparing")
sfm.addTransition("ready", "preparing", "ready")
sfm.addTransition("approach", "ready", "approach")
sfm.addTransition("insert", "approach", "insert")
sfm.addTransition("align", "insert", "align")
sfm.addTransition("plug", "align", "plug")
sfm.create()


SFM_MODEL.command_list = [
    'shape_hook#NW',
    'I_-U4_-U5#gateway#E',
    'I_-U4_-U5#E',
    'I_-U4_-U6#E',
    'shape_debug1',  # 'I_-U4_-U6#S', NON GIRA NEL EVRSO GIUSTO
    'I_-U2_-U6#S',
    'I_-U2_-U6#W',
    'I_-U2_-U5#W',
    'I_-U2_-U5#S',
    'I_-U1_-U5#S',
    'shape_debug2',  # 'I_-U1_-U5#E', NO NGIRA ANCORA
    'I_-U1_-U6#E',
    'I_-U1_-U6#N',
    'I_-U4_-U6#N',
    'I_-U4_-U6#W',
    'I_-U4_-U5#gateway#W',
    'shape_hook#NW'
]


sfm.set_state("start")
sfm.getModel().prepare()


hz = 100
rate = rospy.Rate(hz)  # 10hz
start_time = rospy.get_time()
f = 0.1


while not rospy.is_shutdown():
    # print sfm.getModel().state
    sfm.loop()

    grasshopper_trajecotries_generator.applyTrajectory(rospy.Time.now())
    comau_trajectories_generator.applyTrajectory(rospy.Time.now())

    # if len(SFM_MODEL.current_trajectory) > 0:
    #    curr2 = SFM_MODEL.current_trajectory.pop(0)
    #    if curr2.isValid():
    # comau_robot.getController().jointCommand(curr2.q, rospy.Time.now())

    rate.sleep()
