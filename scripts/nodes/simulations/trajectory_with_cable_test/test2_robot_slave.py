#!/usr/bin/env python
# license removed for brevity

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
from wires_robotic_platform.robots.controllers import RobotController, GeneralIKService, Robot
from wires_robotic_platform.robots.trajectories import FuzzyTrajectoryGenerator
from wires_robotic_platform.robots.market import RobotMarket
from wires_robotic_platform.partdb.proxy import GearboxProxy
from wires_robotic_platform.srv import IKService, IKServiceResponse
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.sfm.machines as machines
from wires_robotic_platform.utils.logger import Logger

from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import tf
from scipy.interpolate import interp1d
import numpy as np
from std_msgs.msg import String
from transitions import Machine

rospy.init_node('testing_trajectory_1')
listener = tf.TransformListener()


# Gearbox GearboxProxy
GearboxProxy.init(tf_listener=listener)

# Creates Robot from Market
comau_robot = RobotMarket.createRobotByName("comau_smart_six#v1")
comau_trajectories_generator = FuzzyTrajectoryGenerator(comau_robot)

grasshopper = RobotMarket.createRobotByName("grasshopper#v1")


# COMMANDS
waypoint = ""
commands = ['reset', 'calibration', 'reset']

# FRAMES
gearbox_frames = None
current_target = None
end_frame = PyKDL.Frame()

# TOOL
tool = PyKDL.Frame()
tool.p = PyKDL.Vector(-0.06133, -0.004, 0)


# TRAJECTORY
current_trajectory = []
trajectory_time = 50


class SFM_ROBOT_MOVE(object):

    def __init__(self):
        self.current_command = ""
        self.command_list = []
        self.gearbox_frames = []
        self.robot_ee = None
        self.robot_base = None
        self.current_trajectory = []

    def consumeCommand(self, command):
        Logger.log("Executing command {}".format(command))
        if command.startswith("shape_"):
            shape = command.split("_")[1]
            Logger.log("Shape recognized '{}'".format(shape))
            q_out = comau_robot.getShapeQ(shape)
            q_in = comau_robot.getController().getQ()
            self.current_trajectory = comau_trajectories_generator.generateTrajectoryFromShapes(
                q_in, q_out, hz, trajectory_time)

            return

        print command in self.gearbox_frames
        if command in self.gearbox_frames:
            C1 = self.gearbox_frames[command]
            C1 = C1 * elevation
            C1.M.DoRotZ(0)
            self.robot_ee = comau_robot.getEEFrame()
            self.robot_base = transformations.retrieveTransform(
                listener, "world", "comau_smart_six/base_link", print_error=True)
            current_target = self.robot_base.Inverse() * C1
            current_target = current_target * tool.Inverse()
            start_frame = self.robot_ee
            q_in = comau_robot.getController().getQ()
            self.current_trajectory = comau_trajectories_generator.generateTrajectoryFromFrames(
                q_in, start_frame, current_target, hz, trajectory_time)

    def isCommandAvailable(self):
        return len(self.command_list) > 0

    def fetchCommand(self):
        if self.isCommandAvailable():
            return self.command_list.pop(0)
        else:
            Logger.error("Trying to fetch a command, but none found!")

    #
    # -> ENTERING IN IDLE
    #
    def on_enter_idle(self):
        Logger.log("Entering on Idle")
        if self.gearbox_frames == None:
            self.prepare()
        else:
            self.check()

    #
    # -> ENTERING IN PREPARING
    #
    def on_enter_preparing(self):
        Logger.log("Entering on PREPARING")
        self.gearbox_frames = GearboxProxy.retrieveGearboxTFTree()
        self.robot_ee = comau_robot.getEEFrame()
        self.robot_base = transformations.retrieveTransform(
            listener, "world", "comau_smart_six/base_link", print_error=True)

        if len(self.gearbox_frames) > 16 and self.robot_ee and self.robot_base:
            self.wait()
        else:
            time.sleep(1)
            self.prepare()

    #
    # -> ENTERING IN CHECK COMMAND
    #
    def on_enter_checkcommand(self):
        Logger.log("Entering on Checkcommand")
        Logger.log("Commands: {}".format(len(self.command_list)))
        if self.isCommandAvailable():
            self.current_command = self.fetchCommand()
        else:
            self.current_command = None
            time.sleep(1)
            self.wait()

    #
    # -> ENTERING IN COMMAND READY
    #
    def on_enter_commandready(self):
        Logger.log("Entering on Commandready")
        self.consumeCommand(self.current_command)
        self.current_command = None

    #
    # -> ENTERING IN MOVING
    #
    def on_enter_moving(self):
        print("Entering in MOVING")

    #
    # -> ENTERING IN STEADY
    #
    def on_enter_steady(self):
        print("Entering in STEADY")

    #
    # -> LOOPING IN CHECKCOMMAND
    #
    def on_loop_checkcommand(self):
        if self.current_command:
            self.ready()

    #
    # -> LOOPING IN COMMANDREADY
    #
    def on_loop_commandready(self):
        if comau_robot.getController().isMoving():
            self.start()

    #
    # -> LOOPING IN STEADY
    #
    def on_loop_steady(self):
        if comau_robot.getController().isMoving():
            self.move()
        else:
            self.wait()

    #
    # -> LOOPING IN IDLE
    #
    def on_loop_idle(self):
        if comau_robot.getController().isMoving():
            self.start()

    #
    # -> LOOPING IN MOVING
    #
    def on_loop_moving(self):

        if not comau_robot.getController().isMoving():
            self.stand()


state_idle = machines.SFMachineState("idle")
state_commandready = machines.SFMachineState("idle")
state_checkcommand = machines.SFMachineState("checkcommand")
state_moving = machines.SFMachineState("moving")
state_steady = machines.SFMachineState("steady")

SFM_MODEL = SFM_ROBOT_MOVE()
sfm = machines.SFMachine(SFM_MODEL)
sfm.addState(state_idle)
sfm.addState(machines.SFMachineState("commandready"))
sfm.addState(machines.SFMachineState("preparing"))
sfm.addState(state_checkcommand)
sfm.addState(state_moving)
sfm.addState(state_steady)
sfm.addTransition("prepare", "preparing", "preparing")
sfm.addTransition("prepare", "idle", "preparing")
sfm.addTransition("wait", "preparing", "idle")
sfm.addTransition("wait", "commandready", "idle")
sfm.addTransition("wait", "checkcommand", "idle")
sfm.addTransition("check", "idle", "checkcommand")
sfm.addTransition("ready", "checkcommand", "commandready")
sfm.addTransition("start", "idle", "moving")
sfm.addTransition("start", "commandready", "moving")
sfm.addTransition("move", "steady", "moving")
sfm.addTransition("stand", "moving", "steady")
sfm.addTransition("wait", "steady", "idle")


SFM_MODEL.command_list = [
    'shape_hook#NW',
    'I_-U4_-U5#gateway#E',
    'I_-U4_-U5#E',
    'I_-U4_-U6#E',
    'shape_debug1',  # 'I_-U4_-U6#S', NON GIRA NEL EVRSO GIUSTO
    'I_-U2_-U6#S',
    'I_-U2_-U6#W',
    'I_-U2_-U5#W',
    'I_-U2_-U5#S',
    'I_-U1_-U5#S',
    'shape_debug2',  # 'I_-U1_-U5#E', NO NGIRA ANCORA
    'I_-U1_-U6#E',
    'I_-U1_-U6#N',
    'I_-U4_-U6#N',
    'I_-U4_-U6#W',
    'I_-U4_-U5#gateway#W',
    'shape_hook#NW'
]


sfm.set_state("idle")
# sfm.models[0].start()
sfm.getModel().prepare()


class Lump(object):
    def __init__(self):
        self.a = 2


hz = 100
rate = rospy.Rate(hz)  # 10hz
start_time = rospy.get_time()
f = 0.1


target_frame = Frame()
target_frame.p = Vector(0.77, 0, 0.37)
target_frame.M.DoRotY(-math.pi / 2)

in_trajectory = False

elevation = PyKDL.Frame()
elevation.p.z(0.6)

start_position = PyKDL.Frame()
start_position.p = PyKDL.Vector(-0.25, -0.72, 1.09)
trajectory = []
traj2 = []


gripper_map = {
    'gripper_up': [0.05, 0, 0, 0],
    'gripper_middle': [0.02, 0, 0, 0],
    'gripper_down': [-0.07, 0, 0, 0],
    'gripper_down2': [-0.09, 0, 0, 0]
}


def command_callback(data):
    global waypoint
    waypoint = data.data
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)


rospy.Subscriber("command", String, command_callback)

while not rospy.is_shutdown():
    # print sfm.getModel().state
    sfm.loop()

    grasshopper.getController().jointCommand(grasshopper.getShapeQ("testdown"))

    if len(SFM_MODEL.current_trajectory) > 0:
        curr2 = SFM_MODEL.current_trajectory.pop(0)
        if curr2.isValid():
            comau_robot.getController().jointCommand(curr2.q, rospy.Time.now())

    rate.sleep()
