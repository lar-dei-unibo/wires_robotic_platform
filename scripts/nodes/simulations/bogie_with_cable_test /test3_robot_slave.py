#!/usr/bin/env python
# license removed for brevity

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Twist
from std_msgs.msg import Header
import turtlesim.msg
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
from wires_robotic_platform.robots.controllers import RobotController, GeneralIKService, Robot
from wires_robotic_platform.robots.trajectories import FuzzyTrajectoryGenerator
from wires_robotic_platform.robots.market import RobotMarket
from wires_robotic_platform.partdb.proxy import GearboxProxy
from wires_robotic_platform.srv import IKService, IKServiceResponse
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.sfm.machines as machines
from wires_robotic_platform.utils.logger import Logger

from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import tf
from scipy.interpolate import interp1d
import numpy as np
from std_msgs.msg import String
from transitions import Machine

rospy.init_node('testing_trajectory_1')
listener = tf.TransformListener()
hz = 10
rate = rospy.Rate(hz)


def pose_cb(msg):
    print msg.x, msg.y


class SFM_TEST(object):

    def __init__(self):
        self.turning_counter = 0
        self.moving_counter = 0
        self.moving_speed = 1
        self.turning_speed = 0.7
        self.current_pose = np.array([0.0, 0.0, 0.0])
        self.cmd_vel = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=1)

        rospy.Subscriber('/turtle1/pose', turtlesim.msg.Pose,
                         self.pose_cb, queue_size=1)

    def pose_cb(self, msg):
        self.current_pose[0] = float(msg.x)
        self.current_pose[1] = float(msg.y)
        self.current_pose[2] = float(msg.theta)

    def on_enter_forward(self):
        Logger.log("Going forward!!")
        self.moving_counter = 0
    
    def on_exit_forward(self):
        Logger.log("Exiting forward!!")
        self.moving_counter = 0
    

    def on_enter_turn(self):
        Logger.log("Turning!!")
        self.turning_counter = 0

    def on_loop_forward(self):

        t = Twist()
        t.linear.x = self.moving_speed
        self.cmd_vel.publish(t)

        self.moving_counter +=  1
        if self.moving_counter > 25:
            self.turn()

        Logger.log("Moving counter: {} ".format(self.moving_counter))

    def on_loop_turn(self):
        t = Twist()
        t.angular.z = self.turning_speed
        self.cmd_vel.publish(t)

        self.turning_counter += 1
        if self.turning_counter > 25:
            self.go()

        Logger.log("Turning counter: {} ".format(self.turning_counter))


SFM_MODEL = SFM_TEST()
sfm = machines.SFMachine(SFM_MODEL)
sfm.addState("state0")
sfm.addState("forward")
sfm.addState("turn")

sfm.addTransition("go", "state0", "forward")
sfm.addTransition("go", "turn", "forward")
sfm.addTransition("turn", "forward", "turn")
sfm.addTransition("turn", "state0", "turn")

sfm.set_state("state0")
sfm.getModel().go()

while not rospy.is_shutdown():
    sfm.loop()

    rate.sleep()
