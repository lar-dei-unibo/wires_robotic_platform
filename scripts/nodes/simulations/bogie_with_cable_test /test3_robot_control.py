#!/usr/bin/env python
# license removed for brevity

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
from wires_robotic_platform.robots.controllers import RobotController, GeneralIKService, Robot
from wires_robotic_platform.robots.trajectories import FuzzyTrajectoryGenerator
from wires_robotic_platform.robots.market import RobotMarket
from wires_robotic_platform.partdb.proxy import GearboxProxy
from wires_robotic_platform.srv import IKService, IKServiceResponse
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.sfm.machines as machines
from wires_robotic_platform.utils.logger import Logger

from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import tf
from scipy.interpolate import interp1d
import numpy as np
from std_msgs.msg import String
from transitions import Machine

rospy.init_node('testing_bogie')


# Gearbox GearboxProxy
GearboxProxy.init(tf_listener=listener)

# Creates Robot from Market
comau_robot = RobotMarket.createRobotByName("comau_smart_six#v1")
comau_trajectories_generator = FuzzyTrajectoryGenerator(comau_robot)

grasshopper_robot = RobotMarket.createRobotByName("grasshopper#v1")

bogie_robot = RobotMarket.createRobotByName("bogie#v1")


hz = 100
rate = rospy.Rate(hz)  # 10hz
start_time = rospy.get_time()

while not rospy.is_shutdown():
    # print sfm.getModel().state
    sfm.loop()

    bogie_robot.getController().jointCommand(bogie_robot.getShapeQ("testpt1"))
    bogie_robot.getController().jointCommand(bogie_robot.getShapeQ("testpt2"))

    
    rate.sleep()
