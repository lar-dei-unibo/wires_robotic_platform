#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import rospy
import tf_conversions
from tf_conversions import posemath
from wires_robotic_platform.msg import ComauFollow
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.devices import ForceSensor, Joystick
from wires_robotic_platform.robotmotion.robot_motion_action import RobotMotionClient
from wires_robotic_platform.msg import *
from wires_robotic_platform.msg import RobotMotionResult, RobotMotionFeedback
from wires_robotic_platform.msg import RobotFollow
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.storage.mongo import MessageStorage
import wires_robotic_platform.sfm.machines as machines
from wires_robotic_platform.proxy.target_follower_proxy import TargetFollowerProxy
from wires_robotic_platform.proxy.alarm_proxy import AlarmProxy
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.robotsupervisor.robot_supervisor_state_machine import SupervisorSM
from std_msgs.msg import String, Float64, Bool
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
import threading
import PyKDL
import copy
import math
import tf
from wires_robotic_platform.param.global_parameters import Parameters


if __name__ == '__main__':
    robot_name = Parameters.get("BONMET_NAME")
    rospy.init_node('{}_supervisor_node'.format(robot_name))
    node_rate = 30
    rate = rospy.Rate(node_rate)

    bonmet_supervisor = SupervisorSM(robot_name)
    bonmet_supervisor .start()

    try:
        while not rospy.is_shutdown():
            bonmet_supervisor .stepForward()
            rate.sleep()
    except rospy.ROSInterruptException:
        pass
