#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import rospy

from wires_robotic_platform.msg import RobotFollow
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.robots.controllers import RobotStatus
from wires_robotic_platform.robotcontrol.robot_direct_motion import DirectCommander
from wires_robotic_platform.proxy.target_follower_proxy import TargetFollowerProxy
from wires_robotic_platform.proxy.alarm_proxy import AlarmProxy
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

from std_msgs.msg import String, Bool
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
import PyKDL
import copy
import math
import tf
from wires_robotic_platform.param.global_parameters import Parameters


if __name__ == '__main__':
    robot_name = Parameters.get("SCHUNK_NAME")
    rospy.init_node('{}_direct_motion'.format(robot_name))
    node_rate = 10
    rate = rospy.Rate(node_rate)

    cmd = DirectCommander(robot_name)
    try:
        while not rospy.is_shutdown():
            cmd.stepForward()
            rate.sleep()
    except rospy.ROSInterruptException:
        pass
