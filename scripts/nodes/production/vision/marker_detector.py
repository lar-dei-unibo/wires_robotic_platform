#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
import wires_robotic_platform.utils.conversions as conversions
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv

from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("marker_detector")

node.setupParameter("hz", 60)
node.setupParameter("MARKERS_MAP", '700=0.05 ; 701=0.05')
node.setupParameter("MARKER_RELATIVE_FRAME", '0 0 0.3 0 0 0')
node.setupParameter("VIZ", False)
node.setupParameter("CAMERA_TOPIC", "/camera/rgb/image_raw")
node.setupParameter("CAMERA_CONFIGURATION",
                    'asus_camera_1_may2017.yml')
node.setupParameter("CAMERA_TF_NAME", 'camera')
node.setHz(node.getParameter("hz"))


#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/' + node.getParameter("CAMERA_CONFIGURATION"))
camera_tf_name = node.getParameter("CAMERA_TF_NAME")
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=node.getParameter("CAMERA_TOPIC")
)

#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera.getCameraFile(), z_up=True)
markers_map = conversions.stringToMap(node.getParameter('MARKERS_MAP'))
marker_relative_transform = transformations.KDLFromString(
    node.getParameter('MARKER_RELATIVE_FRAME'), fmt='RPY')

Logger.log("Marker map:")
Logger.log("{}".format(markers_map))


def cameraCallback(frame):
    """ Camera callback. produce FrameRGBD object """
    output = frame.rgb_image.copy()

    #⬢⬢⬢⬢⬢➤ Detects markers
    markers = marker_detector.detectMarkersMap(
        frame.rgb_image, markers_map=markers_map)

    #⬢⬢⬢⬢⬢➤ draw markers
    for id, marker in markers.iteritems():
        marker.draw(output)

        #⬢⬢⬢⬢⬢➤ Publish Marker
        node.broadcastTransform(
            marker,
            marker.getName(),
            camera_tf_name,
            node.getCurrentTime()
        )

        # #⬢⬢⬢⬢⬢➤ Publish Approach
        # marker_approach = marker * marker_relative_transform
        # node.broadcastTransform(
        #     marker_approach, marker.getName() + "_approach",
        #     camera_tf_name,
        #     node.getCurrentTime()
        # )

    if node.getParameter('VIZ'):
        cv2.imshow("output", output)
        c = cv2.waitKey(1)



#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    # marker_pub.publish(plane)
    node.tick()
