#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger

from wires_robotic_platform.utils.ros import RosNode

import message_filters
import rospkg
import numpy as np
import math


def frameCameraFromBase():
    tf = None
    try:
        tf = node.retrieveTransform(frame_id="comau_smart_six/tool",
                                    parent_frame_id="comau_smart_six/base_link",
                                    time=-1)
    except Exception as e:
        print "Waiting for 'tf_storage_hole'..."
    return tf


def frameTerminalFromCamera():
    tf = None
    try:
        tf = node.retrieveTransform(frame_id="/terminal_measure_top/terminal_tf_camera",
                                    parent_frame_id="comau_smart_six/tool",
                                    time=-1)
    except Exception as e:
        print "Waiting for 'tf_storage_hole'..."
    return tf


if __name__ == '__main__':
    node = RosNode("terminal_detector_node")
    node.setupParameter("hz", 1)
    node.setHz(node.getParameter("hz"))
    node.setupParameter("FIXED_MARKER_HEIGHT", None)
    z_fixed = node.getParameter('FIXED_MARKER_HEIGHT')
    try:
        while node.isActive():
            T_base_camera = frameCameraFromBase()
            T_camera_terminal = frameTerminalFromCamera()
            if T_base_camera is not None and T_camera_terminal is not None:
                terminal_tf = T_base_camera * T_camera_terminal
                if terminal_tf is not None:
                    # terminal_tf.p[2] = z_fixed
                    z_rot_angle = terminal_tf.M.GetRPY()[2]
                    terminal_tf.M = PyKDL.Rotation()
                    terminal_tf.M.DoRotZ(z_rot_angle)
                if terminal_tf:
                    node.broadcastTransform(terminal_tf, "terminal_tf",
                                            "comau_smart_six/base_link", node.getCurrentTime())
            node.tick()
    except rospy.ROSInterruptException:
        pass
