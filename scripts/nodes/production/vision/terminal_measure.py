#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from std_msgs.msg import Float64MultiArray
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
import wires_robotic_platform.utils.conversions as conversions
import wires_robotic_platform.vision.visionutils as visionutils
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from sensor_msgs.msg import CompressedImage
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.param.global_parameters import Parameters

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import rospkg
import numpy as np
import math

show_th_image = False
show_debug_output = True
############################################################
############################################################
############################################################
############################################################
############################################################


class ImageInterestArea(PyKDL.Frame):

    def __init__(self, image, origin_frame, relative_frame=PyKDL.Frame(), polygon_points=[]):
        super(ImageInterestArea, self).__init__()
        self.image = image
        self.M = origin_frame.M
        self.p = origin_frame.p
        self.relative_frame = relative_frame
        self.polygon_points = polygon_points

    def getRelativePolygonPoints(self, dtype=int):
        polygon_origin = self * self.relative_frame
        relative_polygon_points = []
        for p in self.polygon_points:
            pext = PyKDL.Vector(p[0], p[1], 0.0)
            pext = polygon_origin * pext
            relative_polygon_points.append(np.array([
                pext.x(),
                pext.y()
            ],  dtype=dtype))
        return relative_polygon_points

    def buildMask(self, target_image):
        zeros = np.zeros(
            (target_image.shape[0], target_image.shape[1], 1), dtype=np.int8)
        points = self.getRelativePolygonPoints()
        visionutils.drawPolygon(zeros, points, True, (255, 255, 255), -1)
        return zeros

    @staticmethod
    def crop_image(img, tol=0):
        # img is image data
        # tol  is tolerance
        mask = img > tol
        return img[np.ix_(mask.any(1), mask.any(0))]

    def buildCrop(self, target_image, debug_output=False):
        mask = self.buildMask(target_image)
        masked = cv2.bitwise_and(target_image, target_image, mask=mask)

        polygon_origin = self * self.relative_frame
        angle = math.atan2(
            polygon_origin.M[1, 0], polygon_origin.M[0, 0]) * 180 / math.pi
        cols = target_image.shape[1]
        rows = target_image.shape[0]
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
        masked_rot = cv2.warpAffine(masked, M, (cols, rows))
        cropped = ImageInterestArea.crop_image(masked_rot)

        if debug_output:
            cv2.namedWindow("cropped", cv2.WINDOW_NORMAL)
            # cv2.imshow("rotated", masked_rot)
            # cv2.imshow("mask", mask)
            # cv2.imshow("masked", masked)
            cv2.imshow("cropped", cropped)

        return cropped

    @staticmethod
    def extractPoints(image, th=200):
        points = []
        max_x = 0
        max_x_point = None
        for i in range(0, image.shape[0]):
            for j in range(0, image.shape[1]):
                if image[i, j] > th:
                    if j > max_x:
                        max_x = j
                        max_x_point = np.array([j, i])
                    points.append(np.array([j, i]))
        if len(points) <= 0:
            return [], np.array([0, 0])
        else:
            return points, max_x_point

    @staticmethod
    def saltAndPepperThresholdFilter(image, th=200, kerner_size=2):
        kernel = np.ones((kerner_size, kerner_size), np.uint8)
        cropped = cv2.erode(image, kernel)
        image = cv2.dilate(cropped, kernel)
        image[image < th] = [0]
        return image

    @staticmethod
    def fitALine(points, second_point_dist=20):
        try:
            points = np.array(points)
            [vx, vy, x, y] = cv2.fitLine(
                points,
                cv2.DIST_L2,
                0,
                0.01,
                0.01
            )

            line_center = np.array([x, y], dtype=int)
            line_arrow = line_center + np.array([vx, vy]) * second_point_dist

            m = (line_arrow[1] - line_center[1]) / (line_arrow[0] - line_center[0])
            q = line_center[1] - m * line_center[0]
            return [m, q, line_center, line_arrow]
        except:
            return [1, 0, np.array([0.0, 0.0]), np.array([0.0, 0.0])]

    @staticmethod
    def buildExtrema(m, q, points, max_x):
        x_inf = 10000
        y_inf = m * x_inf + q
        y_minf = m * -x_inf + q
        p_inf = np.array([x_inf, y_inf], dtype=int)
        p_minf = np.array([-x_inf, y_minf], dtype=int)
        max_x_point = np.array([max_x[0], 0])
        max_x_point[1] = m * max_x[0] + q
        zero_point = np.array([0, int(q)])
        return zero_point, max_x_point

    @staticmethod
    def buildRealLine(m, q, source_image, pixel_ratio, invert_m=True, invert_q=True):
        h = int(source_image.shape[0] * 0.5)
        q_rel = q - h

        m_sign = -1 if invert_m else 1
        q_sign = -1 if invert_q else 1
        out_m = m_sign * m
        out_q = q_sign * q_rel * pixel_ratio * 1000  # millimeter
        return out_m, out_q[0]

    @staticmethod
    def cropOnTerminal(source_image, terminal_probable_point, radius=50):
        sphere_mask = np.zeros(
            (source_image.shape[0], source_image.shape[1], 1),
            dtype=np.int8
        )
        cv2.circle(sphere_mask, tuple(terminal_probable_point), 50, (255), -1)
        masked_on_terminal = cv2.bitwise_and(
            source_image,
            source_image,
            mask=sphere_mask
        )
        return masked_on_terminal

    @staticmethod
    def estimatesCableRFs(zero_point, max_x_point, pixel_ratio):
        cable_rf_x = max_x_point - zero_point
        cable_rf_x = cable_rf_x / np.linalg.norm(cable_rf_x)
        cable_rf_y = np.array([cable_rf_x[1], -cable_rf_x[0]])

        #⬢⬢⬢⬢⬢➤ Estimate Cable RF on zero axis
        cable_rf = transformations.KDLFrom2DRF(
            cable_rf_x,
            -cable_rf_y,
            zero_point
        )

        #⬢⬢⬢⬢⬢➤ Estimates Terminal RF on terminal tip
        terminal_rf = transformations.KDLFrom2DRF(
            cable_rf_x,
            -cable_rf_y,
            max_x_point
        )

        cable_length = np.linalg.norm(zero_point - max_x_point) * pixel_ratio
        return cable_rf, terminal_rf, cable_length

    def drawDebug(self, image):
        polygon_origin = self * self.relative_frame
        visionutils.draw2DReferenceFrame(self, image)
        visionutils.draw2DReferenceFrame(polygon_origin, image)
        polygon_points = self.getRelativePolygonPoints()
        visionutils.drawPolygon(image, polygon_points)

############################################################
############################################################
############################################################
############################################################
############################################################


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("terminal_measurement")
node_name = node.getName()
robot_name = Parameters.get("BONMET_NAME")
node.setupParameter("hz", 1)
node.setupParameter("MARKER_ID", 703)
node.setupParameter("MARKER_SIZE", 0.01)
node.setupParameter("POLYGON_INDEX", 0)
node.setupParameter("VIZ", True)
node.setupParameter("CAMERA_TOPIC", "/usb_cam/image_raw/compressed")
node.setupParameter("CAMERA_CONFIGURATION", 'tiny_camera.txt')
node.setupParameter("CAMERA_TF_NAME", '/' + robot_name + '/tool')
fixed_rf = node.setupParameter("FIXED_RF", False)
fixed_2d_rf = node.setupParameter("FIXEX_2D_RF", "-0.999044;0.0437166;338.466;273.77", array_type=float)
#"FIXEX_2D_RF", "-0.996195;-0.0871557;326.517;248.205", array_type=float)

z_offset = node.setupParameter("Z_OFFSET", 0)

fixed_pixel_ratio = node.setupParameter(
    #"FIXED_PIXEL_RATIO", 0.0002975126876346885)
    "FIXED_PIXEL_RATIO", 0.00029539571324978134)
fixed_ratio_correction = node.setupParameter(
    "FIXED_RATION_CORRECTION", 0.83
)
sensor_offset = node.setupParameter(
    "SENSOR_OFFSET", "0.0088;-0.0024", array_type=float)
#"SENSOR_OFFSET", "-0.01;-0.0024", array_type=float)
node.setHz(node.getParameter("hz"))

publish_detection = node.setupParameter("PUBLISH_DETECTION", False)
if publish_detection:
    publish_detection_topic = node.createPublisher("~images/detection/compressed", CompressedImage)

#⬢⬢⬢⬢⬢➤ Offset publisher
wire_params_pub = node.createPublisher("~wire_params", Float64MultiArray)

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage('wires_robotic_platform', 'data/camera_calibration/' + node.getParameter("CAMERA_CONFIGURATION"))
camera_tf_name = node.getParameter("CAMERA_TF_NAME")
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=node.getParameter("CAMERA_TOPIC"),
    compressed_image='compressed' in node.getParameter("CAMERA_TOPIC")
)


# Opencv Frame

click_index = 0
click_points = []
click_points.append(np.array([0.0, 0.0]))
click_points.append(np.array([0.0, 0.0]))


def mouseCallback(event, x, y, flags, param):
    global click_index, click_points
    if event == cv2.EVENT_RBUTTONDOWN:
        click_points[1] = np.array([x, y])
    if event == cv2.EVENT_LBUTTONDOWN:
        click_points[0] = np.array([x, y])


window_setup = False


#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera.getCameraFile(), z_up=True)

markers_map = {
    node.getParameter("MARKER_ID"): node.getParameter("MARKER_SIZE")
}

Logger.log("Marker map:")
Logger.log("{}".format(markers_map))


reference_size = node.getParameter("MARKER_SIZE")

# Lista dei punti che configurano la maskera da analizzare. Si possono pesacre da file
# O creare un set di poligoni buoni da utilizzare. Il rettangolo
# potrebbe sempre andar bene

polygon_index = node.getParameter("POLYGON_INDEX")
if polygon_index == 1:
    c = 0
    d = 10
    pw = 90
    ph = 50
else:
    # polygon_points = np.array([
    #     [0, 0],
    #     [8, 70],
    #     [50, 70],
    #     [150, 70],
    #     [150, -70],
    #     [50, -70],
    #     [8, -70]
    # ])
    c = 0
    d = 30
    pw = 90
    ph = 40

polygon_points = np.array([
    [c, 0],
    [d, ph],
    [pw, ph],
    [pw, -ph],
    [d, -ph]
])
polygon_points = polygon_points * 2  # fattore di scala del poligono


def cameraCallback(frame):
    """ Camera callback. produce FrameRGBD object """
    global window_setup, node
    if not window_setup:
        window_setup = True
        cv2.namedWindow('output')
        cv2.setMouseCallback('output', mouseCallback)

    node.rate.sleep()

    #
    # UNDISTORT IMAGE
    #
    frame.rgb_image = cv2.undistort(frame.rgb_image, camera.camera_matrix, camera.distortion_coefficients)

    # print("CAMERA DISTORTION", camera.distortion_coefficients)
    output_gray = cv2.cvtColor(frame.rgb_image, cv2.COLOR_BGR2GRAY)

    kernel = np.ones((5, 5), np.float32) / 25
    output_gray = cv2.filter2D(output_gray, -1, kernel)

    # output_gray = cv2.fastNlMeansDenoisingMulti(
    #     output_gray, 2, 5, None, 4, 7, 35)
    output = frame.rgb_image.copy()

    # Threshold adattivo sull'immagine per fare backgorund removal
    thresh1 = cv2.adaptiveThreshold(output_gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                    cv2.THRESH_BINARY, 11, 2)
    thresh1[thresh1 == 255] = [50]
    thresh1[thresh1 == 0] = [255]
    if show_th_image:
        cv2.imshow("th", thresh1)

    #⬢⬢⬢⬢⬢➤ Detects markers
    markers = marker_detector.detectMarkersMap(
        frame.rgb_image, markers_map=markers_map)

    reference_marker = None
    #⬢⬢⬢⬢⬢➤ draw markers
    for id, marker in markers.iteritems():
        # print("MARKER", marker)
        marker.draw(output)
        #⬢⬢⬢⬢⬢➤ Publish Marker
        node.broadcastTransform(marker, marker.getName(),
                                camera_tf_name, node.getCurrentTime())
        reference_marker = marker
        break

    if reference_marker != None or fixed_rf:

        #print("Sensor offset", sensor_offset)

        if fixed_rf:
            pixel_ratio = fixed_pixel_ratio
            #print("Fixed Pixel ratio:", pixel_ratio)
        else:
            # compute pixel ratio
            pixel_ratio = reference_marker.measurePixelRatio(reference_size)
            #print("Pixel ratio:", pixel_ratio)

        if fixed_rf:
            #print("FIXED RF!")
            marker_2d_rf = PyKDL.Frame()
            marker_2d_rf.M = PyKDL.Rotation(
                fixed_2d_rf[0], -fixed_2d_rf[1], 0,
                fixed_2d_rf[1], fixed_2d_rf[0], 0,
                0, 0, -1
            )
            marker_2d_rf.p = PyKDL.Vector(
                fixed_2d_rf[2], fixed_2d_rf[3], 0)
        else:
            # compute Marker 2D Reference frame
            marker_2d_rf = reference_marker.get2DFrame()
            #print("Computed rf:", marker_2d_rf)

        # compute a relative frame, potrebbe essere calcolato dalla dimensione del dito
        # del gripper. L'orientamento dipende dal setup. in questo caso è
        # girato per errore
        relative_frame = transformations.KDLFromRPY(0, 0, math.pi)
        relative_frame = relative_frame * \
            PyKDL.Frame(PyKDL.Vector(
                sensor_offset[0] / pixel_ratio, sensor_offset[1] / pixel_ratio, 0))

        # Crea un ImageInterestArea che serve ad analizzare/croppare l'area di
        # interesse
        image_area = ImageInterestArea(
            output, marker_2d_rf, relative_frame, polygon_points)
        image_area.drawDebug(output)
        cropped = image_area.buildCrop(thresh1, debug_output=show_debug_output)

        # Filtraggio dell'otuput croppato, semplice erosion/dilation per
        # rimuovere il sale e pepe
        cable_image = ImageInterestArea.saltAndPepperThresholdFilter(
            cropped,
            kerner_size=3
        )

        cropped_on_terminal = 2
        while cropped_on_terminal > 0:
            # Compute all white points, and the max X point
            cable_points, max_x_point = ImageInterestArea.extractPoints(
                cable_image)

            #⬢⬢⬢⬢⬢➤ Fit a line on all white points
            m, q, line_center, line_arrow = ImageInterestArea.fitALine(
                cable_points)

            #print(m, q, line_center, line_arrow)
            #⬢⬢⬢⬢⬢➤ Compute cable extrema

            zero_point, max_x_point = ImageInterestArea.buildExtrema(
                m, q,
                cable_points,
                max_x_point
            )

            cable_image = ImageInterestArea.cropOnTerminal(
                cable_image,
                max_x_point,
                radius=50
            )
            #⬢⬢⬢⬢⬢➤ Emulates a Do-Until loop
            cropped_on_terminal -= 1

        if len(cable_points) <= 0:
            print("NO POINTS!!")
            cv2.imshow("output", output)
            cv2.waitKey(1)
            return

        #⬢⬢⬢⬢⬢➤ Builds a real line in meter
        out_m, out_q = ImageInterestArea.buildRealLine(
            m, q,
            cable_image,
            pixel_ratio,
            invert_m=True, invert_q=True
        )

        #⬢⬢⬢⬢⬢➤ Estimates Cable length
        cable_rf, terminal_rf, cable_length = ImageInterestArea.estimatesCableRFs(
            zero_point, max_x_point, pixel_ratio)

        #⬢⬢⬢⬢⬢➤ Angle distance correction
        angle_distance_correction = sensor_offset[0] / math.cos(math.atan(m))
        #print("Angle distance_correction:", angle_distance_correction)

        #⬢⬢⬢⬢⬢➤ Publishes Wire Params
        # - sensor_offset[1] * 1000
        sensor_q = out_m * -sensor_offset[0] + out_q / 1000.0 + sensor_offset[1]
        length_correct = cable_length / fixed_ratio_correction + angle_distance_correction
        out_msg = Float64MultiArray()
        out_msg.data = [out_m, sensor_q, length_correct]
        # print out_msg.data
        offset = length_correct * math.sin(out_m) - sensor_q
        print offset
        wire_params_pub.publish(out_msg)

        #⬢⬢⬢⬢⬢➤ DEBUG DRAW
        #print("Out", out_m, out_q, type(out_q))
        #print("Eq:", m, q)
        #print("Wire params:", out_m, out_q)
        angle_str = "{:.2f} deg".format(math.atan(out_m) * 180 / np.pi)
        offset_str = "{:.2f} mm".format(out_q)
        #print("angle", angle_str)
        #print("q", offset_str)

        # L'output viene anche ristrasformato a colori solo per uno scopo di
        # visualizzazione
        cable_image = cv2.cvtColor(cable_image, cv2.COLOR_GRAY2BGR)

        visionutils.draw2DReferenceFrame(cable_rf, cable_image)
        visionutils.draw2DReferenceFrame(terminal_rf, cable_image)

        h = int(cable_image.shape[0] * 0.5)
        correction_frame = PyKDL.Frame(PyKDL.Vector(0, -h, 0))
        rel_frame = marker_2d_rf * relative_frame
        rel_frame = rel_frame * correction_frame
        global_terminal_frame = rel_frame * terminal_rf
        visionutils.draw2DReferenceFrame(global_terminal_frame, output)

        #print("GLOBAL", terminal_rf, relative_frame, global_terminal_frame)

        # # broadcast terminal-frame w.r.t. the camera-frame

        # dx = global_terminal_frame.p.x() * pixel_ratio
        # dy = global_terminal_frame.p.y() * pixel_ratio
        # dz = 0.0
        # theta = math.asin(global_terminal_frame.M[0, 0])
        # marker_to_cable = PyKDL.Frame()
        # marker_to_cable.p = PyKDL.Vector(dx, dy, dz)
        # cable_theta = PyKDL.Frame()
        # cable_theta.M = PyKDL.Rotation.RPY(0, 0, -theta)
        # marker_to_cable *= cable_theta

        # node.broadcastTransform(reference_marker * marker_to_cable, node_name + "/terminal_tf_camera", robot_name + "/tool", time=node.getCurrentTime())

        # TODO
        pino = relative_frame * correction_frame * terminal_rf
        dz = reference_marker.p.z() + z_offset
        dx = (dz / camera.fx) * (global_terminal_frame.p.x() - camera.cx)  # -pino.p.x() * pixel_ratio
        dy = (dz / camera.fy) * (global_terminal_frame.p.y() - camera.cy)  # pino.p.y() * pixel_ratio
        theta = math.acos(global_terminal_frame.M[0, 0])
        cable_from_camera = PyKDL.Frame()
        cable_from_camera.p = PyKDL.Vector(dx, dy, dz)
        cable_from_camera.M = reference_marker.M
        cable_theta = PyKDL.Frame()
        cable_theta.M = PyKDL.Rotation.RPY(0, 0, -math.pi / 2 + theta)
        cable_from_camera *= cable_theta
        node.broadcastTransform(cable_from_camera, node_name + "/terminal_tf_camera", robot_name + "/tool", time=node.getCurrentTime())

        cv2.circle(cable_image, tuple(line_center), 5, (255, 255, 0), -1)
        cv2.arrowedLine(cable_image, tuple(line_center), tuple(
            line_arrow), (255, 255, 0), 2, tipLength=0.3)

        line_dir = max_x_point - zero_point
        p_minf = zero_point - line_dir * 1000
        p_inf = zero_point + line_dir * 1000
        cv2.line(cable_image, tuple(p_minf.astype(int)),
                 tuple(p_inf.astype(int)), (255, 0, 255), 1)

        cv2.circle(cable_image, tuple(max_x_point), 5, (0, 0, 255), -1)
        cv2.circle(cable_image, tuple(zero_point), 5, (255), -1)

        cv2.line(cable_image, (0, h),
                 (cable_image.shape[1], h), (255, 255, 0), 1)

        tpt1 = np.array([cable_image.shape[1] * 0.7,
                         cable_image.shape[0] * 0.7], dtype=int)
        tpt2 = np.array([cable_image.shape[1] * 0.7,
                         cable_image.shape[0] * 0.8], dtype=int)
        cv2.putText(cable_image, angle_str, tuple(tpt1),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0, 0, 255))
        cv2.putText(cable_image, offset_str, tuple(tpt2),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0, 0, 255))

        cv2.namedWindow("cable", cv2.WINDOW_NORMAL)
        cv2.imshow("cable", cable_image)

        ''' MISURA CON DUE PUNTI FATTA A MANO SOLO A SCOPO DI DEBUG
        p1 = click_points[0]
        p2 = click_points[1]
        cv2.circle(output, (int(p1[0]), int(p1[1])), 5, (255, 255, 0), 2)
        cv2.circle(output, (int(p2[0]), int(p2[1])), 5, (255, 255, 0), 2)
        cv2.line(output, (int(p1[0]), int(p1[1])),
                 (int(p2[0]), int(p2[1])), (255, 255, 0), 2)
        measure_line = np.linalg.norm(p1 - p2)
        measure_line_meter = measure_line * \
            reference_marker.measurePixelRatio(reference_size)
        # print(measure_line, measure_line_meter)
        '''

    if node.getParameter('VIZ'):

        #######################################
        # Publish detection image
        #######################################
        if publish_detection:
            msg = CompressedImage()
            msg.header.stamp = rospy.Time.now()
            msg.format = "jpeg"
            msg.data = np.array(cv2.imencode('.jpg', output)[1]).tostring()
            publish_detection_topic.publish(msg)

        cv2.imshow("output", output)

        c = cv2.waitKey(1)



#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():
    # marker_pub.publish(plane)
   # rate.sleep()
    node.tick()
