#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("terminal_detector_2")

node.setupParameter("hz", 60)
node.setupParameter("ORB_FEATURES", 20)
node.setupParameter("FEATURES_CLUSTERS_TH", 20)
node.setupParameter("TERMINAL_RADIUS_AREA", 20)
node.setupParameter("TERMINAL_MIN_INLIERS", 10)
node.setupParameter("TERMINAL_OUTLIER_RATIO", 0.5)
node.setupParameter("TERMINAL_OUTLIER_OCCUPANCY_RATIO", 0.01)
node.setupParameter("TRAY_MARKER_ID", 700)
node.setupParameter("TRAY_MARKER_SIZE", 0.03)
node.setupParameter("TRAY_MARKER_HIDE_FACTOR", 2)
node.setupParameter("TRAY_RELATIVE_POSE", '-0.05 0.08 0.0 0.0 0.0 -1.57')
node.setupParameter("TRAY_SIZE", '0.17 0.15')
node.setupParameter("MARKER_DEPTH_ERROR", 0)
node.setupParameter("TERMINAL_FIXED_Z", None)


node.setHz(node.getParameter("hz"))

#⬢⬢⬢⬢⬢➤ MARKER PUBLISHER
marker_pub = node.createPublisher(
    "visualization",
    MarkerArray,
)

output_pub = node.createPublisher(
    "terminal_output",
    Image
)


visualization_objects = MarkerArray()

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/asus_camera_1_may2017.yml')
print("CAMERA:", camera_file)
camera_tf_name = "camera"
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic="/camera/rgb/image_raw"
)

#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera.getCameraFile(), z_up=True)


#⬢⬢⬢⬢⬢➤ Terminal Detector
feature_detectror = cv2.ORB()
feature_detectror.setInt("nFeatures", node.getParameter("ORB_FEATURES"))
terminal_detector = TerminalDetector(feature_detectror, camera)
tray_frame = transformations.KDLFromString(
    node.getParameter('TRAY_RELATIVE_POSE')
)
tray_size = map(float, node.getParameter('TRAY_SIZE').split(' '))

correction_frame = PyKDL.Frame()
marker_depth_error = node.getParameter("MARKER_DEPTH_ERROR")
if math.fabs(marker_depth_error) > 0.00001:
    correction_frame.p.z(marker_depth_error)


def cameraCallback(frame):
    visualization_objects.markers = []
    """ Camera callback. produce FrameRGBD object """
    output = frame.rgb_image.copy()

    camera_frame = node.retrieveTransform(
        "camera",
        "/comau_smart_six/base_link",
        -1
    )
    if camera_frame == None:
        print("Unavailable camera frame")
        return

    #⬢⬢⬢⬢⬢➤ Detects markers
    markers = marker_detector.detectMarkersMap(
        frame.rgb_image, markers_metric_size=node.getParameter('TRAY_MARKER_SIZE'))

    #⬢⬢⬢⬢⬢➤ draw markers
    for id, marker in markers.iteritems():
        marker.applyCorrection(correction_frame)
        if id == node.getParameter("TRAY_MARKER_ID"):
            marker.draw(output, scale=3)
            marker_name = "marker_{}".format(id)

            terminal_fixed_z = node.getParameter("TERMINAL_FIXED_Z")
            if terminal_fixed_z != None and not isinstance(terminal_fixed_z, str):
                marker_global = camera_frame * marker
                marker_global.p.z(terminal_fixed_z)
                new_marker = camera_frame.Inverse() * marker_global
                marker.M = PyKDL.Rotation(
                    0, -1, 0,
                    -1, 0, 0,
                    0, 0, -1
                )
                marker.p = new_marker.p

            node.broadcastTransform(
                marker,
                marker_name,
                camera_tf_name,
                node.getCurrentTime()
            )

            #⬢⬢⬢⬢⬢➤ Build Tray Model
            tray = terminal_detector.buildTray(
                marker,
                relative_transform=tray_frame, size=tray_size
            )

            visualization_objects.markers.append(
                tray.createVisualObject(marker_name))

            terminals = terminal_detector.detectTerminals(
                frame.rgb_image,
                marker_hide_factor=node.getParameter(
                    'TRAY_MARKER_HIDE_FACTOR'),
                cluster_distance_th=node.getParameter('FEATURES_CLUSTERS_TH'),
                search_area=node.getParameter('TERMINAL_RADIUS_AREA'),
                min_inilers=node.getParameter('TERMINAL_MIN_INLIERS'),
                outlier_ratio=node.getParameter('TERMINAL_OUTLIER_RATIO'),
                occupancy_outlier_ratio=node.getParameter(
                    'TERMINAL_OUTLIER_OCCUPANCY_RATIO')
            )

            masked = terminal_detector.maskImage(frame.rgb_image)

            # masked_gray = cv2.cvtColor(masked, cv2.COLOR_BGR2GRAY)

            # Threshold adattivo sull'immagine per fare backgorund removal
            # thresh1 = cv2.adaptiveThreshold(masked_gray, 255,
            # cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
            #  cv2.THRESH_BINARY, 11, 2)

            # cv2.imshow("th", thresh1)

            for terminal in terminals:
                terminal.drawPoints(masked)
                if terminal.isValid():
                    print("Rf2D", terminal.get2DReferenceFrame())
                    terminal.drawZones(masked)
                    terminal.drawReferenceFrame(masked)
                    wname = "Terminal{}".format(terminal.label)
                    cv2.namedWindow(wname, cv2.WINDOW_NORMAL)
                    cv2.imshow(wname, terminal.buildCropsImage())
                    rf3D = terminal_detector.estimate3DReferenceFrameOfTerminal(
                        terminal)

                    if terminal_fixed_z != None and not isinstance(terminal_fixed_z, str):
                        terminal_global = camera_frame * rf3D
                        terminal_global.p.z(terminal_fixed_z)
                        new_rf3D = camera_frame.Inverse() * terminal_global
                        rf3D.M = new_rf3D.M
                        rf3D.p = new_rf3D.p

                    node.broadcastTransform(
                        rf3D, wname, camera_tf_name, node.getCurrentTime())

            cv2.imshow("original", frame.rgb_image)
            cv2.imshow("masked", masked)
            output_frame = CvBridge().cv2_to_imgmsg(masked)
            output_pub.publish(output_frame)

    c = cv2.waitKey(1)



#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    marker_pub.publish(visualization_objects)
    # marker_pub.publish(plane)
    node.tick()
