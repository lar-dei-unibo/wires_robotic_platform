#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode

# Vision
import cv2
from wires_robotic_platform.vision.cameras import CameraHandler
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.image import ImageAreaOfInterest, ImageProcessing
import wires_robotic_platform.vision.visionutils as visionutils
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String


from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData
from wires_robotic_platform.param.global_parameters import Parameters

#⬢⬢⬢⬢⬢➤ ROBOT
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
robot_name = Parameters.get("BONMET_NAME")

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class MarkerHandler(object):
    def __init__(self, camera):
        self.camera = camera
        self.image = None
        self.reference_marker = None
        self.pixel_ratio = 0
        self.marker_tf_2d = None

        # Area of interest
        self.image_area = ImageAreaOfInterest()

        # Creates marker detector
        self.marker_detector = MarkerDetector(
            camera_file=self.camera.getCameraFile(), z_up=True)

    def detect(self, image, markers_map, camera_tf_name=None, marker_tf_broadcast=False):
        ''' Detects the markers on the image.
        \nIt recieves a map with the marker 'id' as 'key' and the marker 'size' as 'value'
        \nIt returns a map with the marker 'id' as 'key' and the marker 'tf' as 'value'. '''
        self.image = image
        if image is None:
            return

        markers = self.marker_detector.detectMarkersMap(
            self.image, markers_map=markers_map)

        self.reference_marker = None
        for id, marker in markers.iteritems():
            # print("MARKER", marker)
            reference_size = markers_map[id]

            # Publish marker tf
            if camera_tf_name and marker_tf_broadcast:
                node.broadcastTransform(marker,
                                        "component_marker",  # marker.getName(),
                                        camera_tf_name,
                                        node.getCurrentTime())

            self.reference_marker = marker
            break  # we take only the first one detected

        if self.reference_marker:
            self.pixel_ratio = self.reference_marker.measurePixelRatio(
                reference_size)  # compute meters/pixes ratio
            # 2D tf of the marker (tf on the marker's plane)
            self.marker_tf_2d = self.reference_marker.get2DFrame()

    def isDetected(self):
        return self.reference_marker is not None

    def getMarkerPoseFromCamera(self):
        return self.reference_marker

    def draw(self):
        ''' Draw the marker bourders and its reference frame on the source image'''
        if self.reference_marker:
            self.reference_marker.draw(image=self.image, color=np.array(
                [0, 0, 255]), scale=1, draw_center=True)
            visionutils.draw2DReferenceFrame(self.marker_tf_2d, self.image)

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class ComponentDetectorProxyServer(object):
    def __init__(self):
        self.command_proxy_server_name = "component_detector_supervisor"
        self.command_server = CommandProxyServer(
            self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(command)
                    self.result_flg = True
                    # if command == 'start':
                    #     pass  # it may receive the component id
                    # else:
                    #     Logger.error("INVALID input")
                    #     self.sendResponse(False)
                else:
                    self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            print "\nTask Ended!!! \t Success: {}".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def component_id_cb(msg):
    global marker_id, component_id
    component_id = msg.data
    # marker_id = int(component_id[-3:])
    marker_id = int(component_id.split("ID")[1])
    print("marker_id = {}".format(marker_id))


def getCameraPoseFromBase():
    tf = None
    ct = 50
    while tf is None or ct > 50:
        ct -= 1
        try:
            tf = node.retrieveTransform(frame_id="{}/tool".format(robot_name),
                                        parent_frame_id="{}/base_link".format(
                                            robot_name),
                                        time=-1)
        except Exception as e:
            print "Waiting for tf..."
    return tf


def getComponentPoseFromMarker(point="corner"):
    components_tf = None
    try:
        product_id = dict_gearbox.getComponentIDs(component_id)["product"]
        components_tf = dict_gearbox.getComponentTFs(product_id, components_file_name)["marker"]
    except Exception as e:
        print "Fail: {}".format(e)

    if point == "corner":
        pass
    elif point == "top":
        top_tf = dict_gearbox.getComponentTFs(product_id, components_file_name)["visual"]
        components_tf = top_tf.Inverse() * components_tf

    return components_tf

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("component_marker_detector")
    node.setupParameter("hz", 10)  #
    node.setHz(node.getParameter("hz"))

    #⬢⬢⬢⬢⬢➤ CAMERA
    # /camera/rgb/image_raw/compressed
    node.setupParameter("CAMERA_TOPIC", "/usb_cam_1/image_raw/compressed")
    # asus_camera_1_may2017.yml
    node.setupParameter("CAMERA_CONFIGURATION",
                        'microsoft_live_camera_focus5.yml')
    node.setupParameter("CAMERA_TF_NAME", "{}/tool".format(robot_name))
    camera_tf_name = node.getParameter("CAMERA_TF_NAME")
    camera_topic = node.getParameter("CAMERA_TOPIC")
    camera_calibration_file_path = node.getFileInPackage('wires_robotic_platform',
                                                         'data/camera_calibration/' + node.getParameter("CAMERA_CONFIGURATION"))

    camera = CameraHandler(camera_calibration_file_path, camera_topic)

    #⬢⬢⬢⬢⬢➤ MARKER
    marker_id = None
    node.setupParameter("MARKER_SIZE", 0.01)
    marker_size = node.getParameter("MARKER_SIZE")
    marker = MarkerHandler(camera.getCamera())
    node.setupParameter("MARKER_CAMERA_FIXED_DISTANCE", None)
    z_fixed = node.getParameter("MARKER_CAMERA_FIXED_DISTANCE")
    node.createSubscriber("component_id", String, component_id_cb)

    detector_server = ComponentDetectorProxyServer()

    #⬢⬢⬢⬢⬢➤ GEARBOX
    folder_path = Parameters.get(param="PACKAGE_DATA_FOLDER") + "/gearbox_zero"
    gearbox_file_name = "gearbox00001_ids.txt"
    components_file_name = "Component_tf.xml"
    dict_gearbox = GearboxPartsData(folder_path, gearbox_file_name)

    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    cv2.namedWindow("output", cv2.WINDOW_NORMAL)
    cv2.resizeWindow('output', 1000, 800)

    while node.isActive():
        image = camera.getImage()
        markers_map = {marker_id: marker_size}

        if image is not None:

            # MARKER DETECTION
            if marker_id is not None:
                marker.detect(image=image,
                              markers_map=markers_map,
                              camera_tf_name=camera_tf_name,
                              marker_tf_broadcast=False)

                if marker.isDetected():
                    marker.draw()

                    # COMPONENT TF wrt BASE TF
                    Tr_camera_marker = marker.getMarkerPoseFromCamera()
                    if Tr_camera_marker is not None:
                        if z_fixed is not None:
                            Tr_camera_marker.p[2] = z_fixed
                        yaw_angle = Tr_camera_marker.M.GetRPY()[2]
                        Tr_camera_marker.M = PyKDL.Rotation()
                        Tr_camera_marker.M.DoRotZ(yaw_angle)
                    Tr_base_camera = getCameraPoseFromBase()
                    Tr_marker_component = getComponentPoseFromMarker(point="corner")

                    if Tr_camera_marker is not None and Tr_base_camera is not None and Tr_marker_component is not None:
                        component_tf = Tr_base_camera * Tr_camera_marker * Tr_marker_component

                        # TF BROADCAST
                        node.broadcastTransform(component_tf,
                                                "target_component_detected",
                                                camera_tf_name,
                                                node.getCurrentTime())

                        detector_server.sendResponse(True)

                cv2.imshow("output", image)
                c = cv2.waitKey(1)
            else:
                Logger.warning("marker_id NOT defined")
        else:
            Logger.warning("image is None")

        node.tick()
