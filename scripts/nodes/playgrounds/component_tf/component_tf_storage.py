#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode

# Vision
import cv2
from wires_robotic_platform.vision.cameras import CameraHandler
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.image import ImageAreaOfInterest, ImageProcessing
import wires_robotic_platform.vision.visionutils as visionutils
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer

from std_msgs.msg import String


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class ComponentDetectorProxyServer(object):

    def __init__(self, json_file_path):

        self.json_file_path = json_file_path

        #⬢⬢⬢⬢⬢➤ Command Server
        self.command_proxy_server_name = "component_tf_storage_supervisor"
        self.command_server = CommandProxyServer(self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

        #⬢⬢⬢⬢⬢➤ TF Filter Command Client
        self.tf_filter_comand_proxy = CommandProxyClient("tf_filter_supervisor")
        self.tf_filter_comand_proxy.registerDoneCallback(self.tffilter_done_callback)

        #⬢⬢⬢⬢⬢➤ Frames Dictionary
        self.frames_dictionary = {}

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(command)
                    self.result_flg = True
                    message = SimpleMessage(receiver="tf_filter_supervisor",
                                            command="clearstart")
                    message.setData("tf_name", "target_component_detected")
                    self.tf_filter_comand_proxy.sendCommand(message.toString())
                    print("Filtering")
                else:
                    self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            print "\nTask Ended!!! \t Success: {}".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None

    def tffilter_done_callback(self, response_command):
        print("Filtered")
        tf = getFrame("target_component_detected_filtered")  # this function depends on the RosNode!!!
        tf_wrt_parent = component_parent_pose.Inverse()
        self.frames_dictionary[component_id] = transformations.KDLFrameToList(tf_wrt_parent)
        print self.frames_dictionary
        json.dump(self.frames_dictionary, open(self.json_file_path, 'wb'), ensure_ascii=False)
        print("Saved")
        self.sendResponse(True)


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def component_id_cb(msg):
    global component_id
    component_id = msg.data
    print("component_id = {}".format(component_id))


def component_parent_cb(msg):
    global component_parent_pose
    component_parent_pose = transformations.PoseToKDL(msg)
    print("component_parent = {}".format(component_parent_pose))


def getFrame(frame_id):
    tf = None
    ct = 50
    while tf is None or ct > 50:
        ct -= 1
        try:
            tf = node.retrieveTransform(frame_id=frame_id,
                                        parent_frame_id="world",
                                        time=-1)
        except Exception as e:
            print "Waiting for tf..."
    return tf

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("component_tf_storage")
    node.setupParameter("hz", 10)  #
    node.setHz(node.getParameter("hz"))

    #⬢⬢⬢⬢⬢➤ JSON
    component_id = None
    component_parent_pose = PyKDL.Frame()
    node.setupParameter("DETECTED_FRAMES_FILE", "vision_detected_component_frames.json")
    json_file_path = node.getFileInPackage('wires_robotic_platform', 'data/gearbox/' + node.getParameter("DETECTED_FRAMES_FILE"))

    node.createSubscriber("component_id", String, component_id_cb)
    node.createSubscriber("component_parent", String, component_parent_cb)

    detector_server = ComponentDetectorProxyServer(json_file_path)

    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    while node.isActive():

        node.tick()
