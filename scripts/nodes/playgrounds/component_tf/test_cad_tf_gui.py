#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode

# Vision
import cv2
from wires_robotic_platform.vision.cameras import CameraHandler
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.image import ImageAreaOfInterest, ImageProcessing
import wires_robotic_platform.vision.visionutils as visionutils
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String


from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData
from wires_robotic_platform.param.global_parameters import Parameters


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("test_cad_gui")
    node.setupParameter("hz", 10)  #
    node.setHz(node.getParameter("hz"))

    node.setupParameter("product_id", "aaa")
    product_id = node.getParameter("product_id")
    node.setupParameter("point_id", "h1")
    point_id = node.getParameter("point_id")

    #⬢⬢⬢⬢⬢➤ GEARBOX
    folder_path = Parameters.get(param="PACKAGE_DATA_FOLDER") + "/gearbox_zero"
    components_file_name = "Component_tf.xml"
    gearbox_file_name = "gearbox00001_ids.txt"
    dict_gearbox = GearboxPartsData(folder_path, gearbox_file_name)

    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    dic_tfs = dict_gearbox.getComponentTFs(product_id, components_file_name)

    while node.isActive():
        for tf_id in dic_tfs.keys():
            print dic_tfs
            # exit()
            node.broadcastTransform(dic_tfs["hole"][point_id],
                                    point_id + "_h",
                                    "world",
                                    node.getCurrentTime())
            node.broadcastTransform(dic_tfs["screw"][point_id],
                                    point_id + "_s",
                                    "world",
                                    node.getCurrentTime())
            node.broadcastTransform(dic_tfs["visual"],
                                    "visual",
                                    "world",
                                    node.getCurrentTime())
            node.broadcastTransform(dic_tfs["marker"],
                                    "marker",
                                    "world",
                                    node.getCurrentTime())

            # node.broadcastTransform(dic_tfs[tf_id],
            #                         "corner",
            #                         "world",
            #                         node.getCurrentTime())

        node.tick()
