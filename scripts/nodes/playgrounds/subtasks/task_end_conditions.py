#!/usr/bin/env python
# -*- coding: utf-8 -*-


import numpy as np
import rospy

from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.robots.controllers import RobotStatus
from wires_robotic_platform.proxy.target_follower_proxy import TargetFollowerProxy
from wires_robotic_platform.proxy.alarm_proxy import AlarmProxy
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

from std_msgs.msg import String, Bool, Float64, Float64MultiArray
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
import threading
import PyKDL
import json
import copy
import time
import math
import tf


class TaskConditionInspector(object):

    def __init__(self):
        self.done = False
        self.success = False
        self.error = False

        # fuzzy insertion
        self.contact_force_limit = 0.005
        self.position_bound = 0.002  # [meters]

    def setCondition(self, condition_name, settings):
        try:
            if condition_name == "fuzzy_insertion":
                self.contact_force_limit = settings["contact_force_limit"]
                self.position_bound = settings["position_bound"]
            else:
                pass
        except:
            Logger.error("Error in 'Set Condition' action")
            pass

    # @staticmethod
    def checkEndTask(self, condition_name, controller_status):
        # response = {"done": False, "success": False, "error": False}
        self.done = False
        self.success = False
        self.error = False

        if controller_status:
            if condition_name == "fuzzy_insertion":
                estimated_force = controller_status["estimated_force"]
                regulation_error = controller_status["regulation_error"]
                position_error = controller_status["position_error"]

                # print regulation_error[0]
                succ = "\033[91m" + "\033[92m" * self.success + "\033[1m\033[4m{}\033[0m".format(self.success)
                print "{}: {}".format(position_error, succ)

                if regulation_error[0] > self.contact_force_limit:
                    self.done = True
                    if position_error < self.position_bound:
                        self.success = True
            else:
                self.error = True
        else:
            self.error = True

        return self.done
