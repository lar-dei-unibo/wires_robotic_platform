#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import rospy
import scipy

from wires_robotic_platform.msg import RobotFollow
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.sfm.machines as machines
from wires_robotic_platform.utils.transformations import FrameVectorFromKDL, FrameVectorToKDL
from wires_robotic_platform.utils.transformations import ListToKDLVector, KDLVectorToList
from wires_robotic_platform.utils.transformations import TwistToKDLVector, TwistFormKDLVector
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.robots.controllers import RobotStatus
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.proxy.alarm_proxy import AlarmProxy
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from task_end_conditions import TaskConditionInspector  # TODO

from std_msgs.msg import String, Bool, Float64MultiArray
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
import PyKDL
import copy
import json
import math
import tf

import time


class SFMachineSubTaskSupervisorDefinition(object):
    def __init__(self, subtask_name):
        self.subtask_name = subtask_name
        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)
        self.controller_status = None

        self.condition = None
        self.condition_inspector = TaskConditionInspector()

        # subtask outer command
        self.command_server = CommandProxyServer(
            "{}_supervisor".format(self.subtask_name))
        self.command_server.registerCallback(
            self.command_action_callback)
        self.command_server_last_message = None

    # ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ CALLBACKS ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    def update(self, msg):
        # print msg.data
        try:    # if data field is JSON
            self.controller_status = json.loads(msg.data)
        except:
            self.controller_status = msg.data

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == "{}_supervisor".format(self.subtask_name):
                    command = msg.getCommand()
                    self.result_flg = True
                    if command == "waitend":
                        self.condition = msg.getData("condition")
                        time.sleep(0.5)
                        self.action()
                        Logger.warning("Sub Task {} - {} {}".format(self.subtask_name, command, self.condition))
                    elif command == "setcondition":
                        condition = msg.getData("condition")
                        settings = msg.getData("settings")
                        time.sleep(0.5)
                        # TODO
                        self.idle()
                        Logger.log("Sub Task '{}': ".format(self.condition))
                    else:
                        self.idle()
                        Logger.error("INVALID input")
                        self._send_command_result(False)

        except Exception as e:
            print(e)

    def _send_command_result(self, success):
        if self.command_server_last_message:
            print "\nTask Ended!!! \t Success: {}".format(success)
            self.idle()
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None

    # ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ STATES ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    #
    #⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢ IDLE ⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢
    #

    # ➤ ➤ ➤ ➤ ➤ IDLE: enter
    def on_enter_idle(self):
        Logger.log("State:  IDLE")

    # ➤ ➤ ➤ ➤ ➤ IDLE: loop
    def on_loop_idle(self):
        pass
    #
    #⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢ ACTION ⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢
    #

    # ➤ ➤ ➤ ➤ ➤ ACTION: enter
    def on_enter_action(self):
        Logger.log("State:  ACTION")

    # ➤ ➤ ➤ ➤ ➤ ACTION: loop
    def on_loop_action(self):
        if self.condition:
            if self.controller_status is None:
                Logger.warning("controller_status={}".format(self.controller_status))
            else:
                done = self.condition_inspector.checkEndTask(self.condition, self.controller_status)
                if done:
                    self._send_command_result(self.condition_inspector.success)
        else:
            Logger.error("condition={}, controller_status={}".format(self.condition, self.controller_status))
            self._send_command_result(False)
            self.idle()


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇    SUPERVISOR SM CLASS    ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class SubTaskSupervisorSM(object):
    def __init__(self, subtask_name):
        self.subtask_name = subtask_name

        # CREATE STATE MACHINE
        self.subTaskSupervisorSFM = SFMachineSubTaskSupervisorDefinition(
            self.subtask_name)
        self.sfm = machines.SFMachine(name="sfm_" + self.subtask_name + "subtask_supervisor",
                                      model=self.subTaskSupervisorSFM)

        # DEFINE SM STATES
        self.sfm.addState("start")
        self.sfm.addState("idle")
        self.sfm.addState("action")

        # DEFINE SM TRANSITIONS
        # start ...
        self.sfm.addTransition("idle", "start", "idle")
        # idle ...
        self.sfm.addTransition("idle", "idle", "idle")
        self.sfm.addTransition("action", "idle", "action")
        # action ...
        self.sfm.addTransition("idle", "action", "idle")
        self.sfm.addTransition("action", "action", "action")

        self.sfm.create()
        self.sfm.set_state("start")
        Logger.log("\n\n ************* SFM ready to start ***********")

    def start(self):
        Logger.log("\n\n ************* SFM start ***********")
        self.sfm.getModel().idle()

    def update(self, msg):
        self.sfm.getModel().update(msg)

    def stepForward(self):
        self.sfm.loop()
