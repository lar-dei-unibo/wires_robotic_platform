#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
import numpy as np
import time
import tf
import math
import time
import PyKDL
import random
from PyKDL import Frame, Vector, Rotation

from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32, Float64MultiArray
from wires_robotic_platform.srv import PolyDetectionService

import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode
from subtask_supervisor import SubTaskSupervisorSM


# def controller_status_callback(msg):
#     global subtask
#     subtask.update(msg)


if __name__ == '__main__':
    node = RosNode("insertion_task_node")
    node.setupParameter("hz", 250)
    node.setHz(node.getParameter("hz"))

    subtask_name = "insertion_task"
    subtask = SubTaskSupervisorSM(subtask_name)
    node.createSubscriber("/insertion_control/controller_status", String, subtask.update)

    try:
        subtask.start()
        while node.isActive():
            subtask.stepForward()
            node.tick()
    except rospy.ROSInterruptException:
        pass
