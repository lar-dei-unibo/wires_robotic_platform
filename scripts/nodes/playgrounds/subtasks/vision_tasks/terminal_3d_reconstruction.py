#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode

from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String, Float64MultiArray
from geometry_msgs.msg import Pose
from std_srvs.srv import Trigger, TriggerResponse
from wires_robotic_platform.srv import StringSrv

from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.taskmanager.task_manager_state_machine import TaskManagerSM

#⬢⬢⬢⬢⬢➤ ROBOT
robot_name = Parameters.get("BONMET_NAME")  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Hb_open = "\033[94m\033[1m\033[4m"
Hg_open = "\033[92m\033[1m\033[4m"
Hr_open = "\033[91m\033[1m\033[4m"
H_close = "\033[0m"

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class Terminal3DProxyServer(object):
    def __init__(self):
        self.command_proxy_server_name = "3d_reconstruction_supervisor"
        self.command_server = CommandProxyServer(self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(self.command_proxy_server_name.replace("_supervisor", "") + ": " + command)
                    trigger()
                else:
                    self.command_server_last_message = None
            else:
                self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            print "\n" + Hg_open + "Task Ended!!!" + H_close + " \t Success: {}".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def trigger():
    global is_task_active
    is_task_active = True


def setTaskEnd():
    global is_task_active
    is_task_active = False
    proxy_server.sendResponse(True)


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':
    node = RosNode("terminal_3d")
    node.setupParameter("hz", 50)
    node.setHz(node.getParameter("hz"))

    task_name = "terminal_3d_reconstruction"

    robot_name = Parameters.get("BONMET_NAME")
    robot_list = [robot_name]
    sensor_list = []

    capture_image_task_name = "capture_image_supervisor"
    subtask_list = [capture_image_task_name]

    instruction_list = [

        "system set repete:::False",

        robot_name + " jumptotf terminal_pose_3d_1 gripper",
        "system sleep 1",
        capture_image_task_name + " 1",
        "system sleep 0.5",
        robot_name + " jumptotf terminal_pose_3d_0 gripper",
        "system sleep 1",
        capture_image_task_name + " 2",
        "system sleep 0.5",
        robot_name + " jumptotf terminal_pose_3d_2 gripper",
        "system sleep 1",
        capture_image_task_name + " 3",
        "system sleep 0.5"

    ]

    ###############################################################################################
    ###############################################################################################
    ###############################################################################################

    tskm = TaskManagerSM(task_name)

    tskm.start(robot_list, sensor_list, instruction_list, subtask_list)

    while node.isActive():
        tskm.stepForward()
        node.tick()
