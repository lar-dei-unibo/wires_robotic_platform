#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import time
import cv2
import numpy as np
import PyKDL

from wires_robotic_platform.utils.ros import RosNode
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.cameras import CameraRGB

from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String, Float64MultiArray
from geometry_msgs.msg import Pose
from std_srvs.srv import Trigger, TriggerResponse
from wires_robotic_platform.srv import StringSrv


#⬢⬢⬢⬢⬢➤ ROBOT
robot_name = Parameters.get("BONMET_NAME")  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Hb_open = "\033[94m\033[1m\033[4m"
Hg_open = "\033[92m\033[1m\033[4m"
Hr_open = "\033[91m\033[1m\033[4m"
H_close = "\033[0m"

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class CaptureImageProxyServer(object):
    def __init__(self):
        self.command_proxy_server_name = "capture_image_supervisor"
        self.command_server = CommandProxyServer(self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(self.command_proxy_server_name.replace("_supervisor", "") + ": " + command)
                    trigger(command)
                else:
                    self.command_server_last_message = None
            else:
                self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            print "\n" + Hg_open + "Task Ended!!!" + H_close + " \t Success: {}".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def trigger(name):
    if name == "capture":
        name = int(time.time() * 10)
    frame_name = "frame_{}.png".format(name)
    filename = os.path.join(node.getParameter("OUTPUT_PATH"), frame_name)
    cv2.imwrite(filename, last_image)
    if tf_enabled:
        tffile_name = "pose_{}.txt".format(name)
        filenametf = os.path.join(node.getParameter("OUTPUT_PATH"), tffile_name)
        tf_target = getFrame(frame_id=node.getParameter("TF_TARGET"),
                             parent_id=node.getParameter("TF_BASE"))
        np.savetxt(
            filenametf,
            transformations.KDLtoNumpyVector(tf_target)
        )
    Logger.log("Saved frames:{}".format(name))
    proxy_server.sendResponse(True)


def cameraCallback(frame):
    global last_image
    """ Camera callback. produce FrameRGBD object """
    last_image = frame.rgb_image.copy()


def getFrame(frame_id, parent_id="world"):
    tf = None
    ct = 50
    while tf is None or ct > 50:
        ct -= 1
        try:
            tf = node.retrieveTransform(frame_id=frame_id,
                                        parent_frame_id=parent_id,
                                        time=-1)
        except Exception as e:
            tf = None
            print Hr_open + "Waiting for tf:" + H_close + " {}...".format(frame_id)
    return tf


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("capture_image")

    node.setupParameter("hz", 10)  #
    node.setHz(node.getParameter("hz"))

    node.setupParameter("CAMERA_TOPIC", "/usb_cam_1/image_raw")
    node.setupParameter("OUTPUT_PATH", "/tmp/saved_frames")
    node.setupParameter("CAMERA_CONFIGURATION", 'tiny_camera.yml')  # asus_camera_1_may2017.yml
    node.setupParameter("SAVE_WITH_TF", False)
    node.setupParameter("TF_BASE",  "world")
    node.setupParameter("TF_TARGET",  "camera_tf")

    tf_enabled = node.getParameter("SAVE_WITH_TF")
    camera_calibration_file = node.getParameter("CAMERA_CONFIGURATION")

    if not os.path.exists(node.getParameter("OUTPUT_PATH")):
        os.makedirs(node.getParameter("OUTPUT_PATH"))

    #⬢⬢⬢⬢⬢➤ Create sCamera Proxy Client
    camera_file = node.getFileInPackage('wires_robotic_platform', 'data/camera_calibration/' + camera_calibration_file)

    camera = CameraRGB(
        configuration_file=camera_file,
        rgb_topic=node.getParameter("CAMERA_TOPIC")
    )

    #⬢⬢⬢⬢⬢➤ Camera Msgs Callback
    last_image = None
    camera.registerUserCallabck(cameraCallback)

    #⬢⬢⬢⬢⬢➤ Task Proxy Server
    proxy_server = CaptureImageProxyServer()

    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    while node.isActive():
        node.tick()
