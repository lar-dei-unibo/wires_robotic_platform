#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode

from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String, Float64MultiArray
from geometry_msgs.msg import Pose
from std_srvs.srv import Trigger, TriggerResponse
from wires_robotic_platform.srv import StringSrv, StringSrvResponse

from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData
from wires_robotic_platform.param.global_parameters import Parameters

#⬢⬢⬢⬢⬢➤ ROBOT
robot_name = Parameters.get("BONMET_NAME")  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

IDS_SCAN_LIST = Parameters.get("IDS_SCAN_LIST")  # ["ID000045", "ID000049", "ID000050", "ID000053", "ID000064", "ID000066", "ID000068"]  # "ID000051","ID000062",
BUFFER_SIZE = Parameters.get("BUFFER_VISION_WIRE_OFFSET")
MEAN_OFFSET_VALUE = Parameters.get("MEAN_VISION_WIRE_OFFSET")

hole_x_offset = Parameters.get("HOLE_X_OFFSET")
hole_y_offset = Parameters.get("HOLE_Y_OFFSET")
hole_z_offset = Parameters.get("HOLE_Z_OFFSET")

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class ConnectionsScanProxyServer(object):
    def __init__(self):
        self.command_proxy_server_name = "connections_scan_supervisor"
        self.command_server = CommandProxyServer(self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(self.command_proxy_server_name.replace("_supervisor", "") + ": " + command)
                    res_id = ""
                    if len(IDS_SCAN_LIST) == 0:
                        res_id = nextConnection()
                    else:
                        while res_id not in IDS_SCAN_LIST or res_id is None:
                            res_id = nextConnection()
                else:
                    self.command_server_last_message = None
            else:
                self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def nextConnection():
    global is_next_ready, component_id, is_process_active
    is_process_active = True
    try:
        resp = next_connection()
        is_next_ready = resp.success
        resp_message = str(resp.message)
        print resp
        if resp_message.startswith("COMPLETE"):
            is_process_active = False
            component_id = None
            proxy_server.sendResponse(False)
            print "\n\n\033[91m\033[92m\033[1m\033[4mCOMPLETED!!\033[0m\n\n "
        elif resp_message.startswith("ERROR"):
            component_id = ""
        else:
            component_id = resp_message
        # print component_id
    except Exception as e:
        is_next_ready = False
        component_id = None
        print "Service call failed: %s" % e

    return component_id


def set_offset_cb(msg):
    global wire_params
    wire_params_str = msg.input
    m, q, l = wire_params_str.split(",")
    wire_params = [float(m), float(q), float(l)]
    m, q, l = wire_params
    offset = l * math.sin(m) - q
    updateOffesetBuffer(offset)
    resp = StringSrvResponse()
    return resp


def updateOffesetBuffer(new_val):
    global offset_buffer
    if len(offset_buffer) == 0:
        offset_buffer = [new_val] * BUFFER_SIZE
    else:
        offset_buffer.pop()
        offset_buffer.append(new_val)


def getWireTerminalOffset():
    if wire_params is not None:
        if MEAN_OFFSET_VALUE:
            m, q, l = wire_params
            offset = l * math.sin(m) - q
        else:
            offset = sum(offset_buffer) / float(len(offset_buffer))
        # print "Wire Terminal Offset: {}".format(offset)  # for debug
    else:
        offset = 0.0
    return offset


def getFrame(frame_id, parent_id="world"):
    tf = None
    ct = 50
    while tf is None or ct > 50:
        ct -= 1
        try:
            tf = node.retrieveTransform(frame_id=frame_id,
                                        parent_frame_id=parent_id,
                                        time=-1)
        except Exception as e:
            tf = None
            print "Waiting for tf..."
    return tf


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("connections_scan")

    node.setupParameter("hz", 10)  #
    node.setHz(node.getParameter("hz"))

    proxy_server = ConnectionsScanProxyServer()

    component_tf = None
    component_id = None
    is_next_ready = False
    is_process_active = False
    wire_params = None
    offset_buffer = []

    component_id_pub = node.createPublisher("component_id", String)
    rospy.Service("set_terminal_measure_service", StringSrv, set_offset_cb)

    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    rospy.wait_for_service('connection_tf_service')
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    next_connection = rospy.ServiceProxy('connection_tf_service', Trigger)
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@"

    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    while node.isActive():
        if is_process_active:
            if component_id is not None:
                component_id_pub.publish(component_id)
                screw_tf = getFrame("target_s", "world")
                hole_tf = getFrame("target_h", "world")
            else:
                print "component id is None"
                screw_tf = None
                hole_tf = None

            if screw_tf is not None or hole_tf is not None:

                # Wire terminal Offset correction
                vision_y_offset = getWireTerminalOffset()
                Tr = PyKDL.Frame()
                Tr.p = PyKDL.Vector(hole_x_offset, -vision_y_offset + hole_y_offset, hole_z_offset)
                hole_tf = hole_tf * Tr

                #⬢⬢⬢⬢⬢➤ TF BROADCAST
                node.broadcastTransform(screw_tf,
                                        "target_screw",
                                        "world",
                                        node.getCurrentTime())
                node.broadcastTransform(hole_tf,
                                        "target_hole",
                                        "world",
                                        node.getCurrentTime())
                proxy_server.sendResponse(True)
            else:
                print "screw or hole tf are None" 

        node.tick()
