#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
from numpy.linalg import norm
import time
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode

from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String
from std_msgs.msg import Float32
from geometry_msgs.msg import Pose, Twist
from sensor_msgs.msg import JointState
from std_srvs.srv import Trigger, TriggerResponse

from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData
from wires_robotic_platform.param.global_parameters import Parameters


#⬢⬢⬢⬢⬢➤ ROBOT
robot_name = Parameters.get("BONMET_NAME")  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Hb_open = "\033[94m\033[1m\033[4m"
Hg_open = "\033[92m\033[1m\033[4m"
Hr_open = "\033[91m\033[1m\033[4m"
H_close = "\033[0m"

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

END_OF_STROKE_LOW = Parameters.get("END_OF_STROKE_LOW") #0.0011
END_OF_STROKE_HIGH = Parameters.get("END_OF_STROKE_HIGH") #0.0339
DIST_ADVANCE = Parameters.get("CABLE_PUSH_ADVANCE_DIST") #0.001
DIST_RETURN = Parameters.get("CABLE_PULL_RETRIEVE_DIST") #0.034
DIST_STOP_RETRIEVE = Parameters.get("CABLE_PUSH_RETRIEVE_DIST_TACTILE_STOP") #0.001
ADDITIONAL_OFFSET = Parameters.get("CABLE_LENGTH_ADDITIONAL_OFFSET") #0.01
GRASSHOPPER_SISTEMATIC_OFFSET = Parameters.get("GRASSHOPPER_POSITION_SISTEMATIC_OFFSET") #0.01
THR_PULL = Parameters.get("THRESHOLD_PULL") #0.1
THR_PUSH = Parameters.get("THRESHOLD_PUSH") #0.08
THR_RESET = Parameters.get("THRESHOLD_TACTILE_RESET") #0.005


class GrasshopperInsTacProxyServer(object):
    def __init__(self):
        self.command_proxy_server_name = "grasshopper_insertion_tactile_supervisor"
        self.command_server = CommandProxyServer(self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(command)
                    if command == "push":
                        try:
                            hole_tf_name = msg.getData("tf_name") 
                        except Exception as e:
                            hole_tf_name = None 
                        if hole_tf_name is None:
                            setTrigger(command) 
                        else:
                            setTargetHole(hole_tf_name)
                            setTrigger(command) 
                    elif command == "pull":
                        setTrigger(command) 
                    else:
                        print "error"  
                else:
                    self.command_server_last_message = None
            else:
                self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            print "\n" + Hg_open + "Task Ended!!!" + H_close + " \t Success: {}".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def setTrigger(param):
    global mode
    global is_task_active
    global flag_stop, flag_insertion, goal_stop
    
    mode = param

    while contact_force_estim is None or abs(contact_force_estim) > THR_RESET:
        reset_msg = String()
        reset_msg.data = ''
        reset_tact.publish(reset_msg)

    
    print("----------->tactile reset") 
    time.sleep(2.0)

    is_task_active = True
    flag_stop = False
    flag_insertion = False
    goal_stop= True

    if mode == "push": 
        if hole_tf_name is not None:
            pos_c[1]=DIST_ADVANCE
            step.position = list(pos_c)
            cartesian_pub.publish(step)
        else:
            p[1] = DIST_ADVANCE #if tf_hole not setted
            step.position =list (p)
            cartesian_pub.publish(step)
    elif mode == "pull":
        p= list (pos_c)
        p[1] = DIST_RETURN
        step.position = list (p)
        cartesian_pub.publish(step)
    else:
        Logger.error("Grasshopper Inserction Tactile: Invalid Command")
    

    
def endTask_detector():
    global goal_stop,c
    if mode == "push": 
        if flag_stop and goal_stop:
            p= list (pos_c)
            p[1]=current_pos[1] + DIST_STOP_RETRIEVE
            c=np.array(p)
            step.position = list(c)
            cartesian_pub.publish(step)
            print("Wire Insertion: Stopped")
            goal_stop=False

        if goal_stop==False and checkPos(c):
            print("reached stop point")
            if current_pos[1] < (3*END_OF_STROKE_LOW): #correzione con vision
                setTaskEnd(True)
                print("Wire Insertion: Completed")
            else:
                setTaskEnd(False)
                print("Wire Insertion: Failed")

        elif current_pos[1] < (END_OF_STROKE_LOW):
            setTaskEnd(True)
            print("Wire Insertion: Completed")

    elif mode == "pull": 
        if current_pos[1] > END_OF_STROKE_HIGH:
            print("Wire Retrieve: Completed")
            if flag_insertion:
                setTaskEnd(True)
            else:
                setTaskEnd(False)


def setTaskEnd(succ=True):
    global is_task_active, hole_tf_name
    hole_tf_name = None
    is_task_active = False
    proxy_server.sendResponse(succ)


def tactile_cb(msg):
    global contact_force_estim
    global flag_stop, flag_insertion

    contact_force_estim = msg.linear.y
    if abs(contact_force_estim)>= force_thr_push:
        flag_stop = True
    if abs(contact_force_estim)>= force_thr_pull:
        flag_insertion = True


def current_pos_cb(msg):
    global current_pos , p
    current_pos = np.array(msg.position)
    p = list(current_pos)


def setTargetHole(tf_name):
    global hole_tf_name, pos_c
    cable_length = Parameters.get("CABLE_LENGTH") 
    hole_tf_name = tf_name
    hole_pose = transformations.KDLtoNumpyVector(getFrame(hole_tf_name, parent_id="bonmetc60/link6"))
    pos = hole_pose[0][0:3]  # x y z  (hole wrt link6)
    pos_c = np.array([pos[0], pos[1], pos[2]])
    pos_c[0] = -(pos[2] - 0.392)
    pos_c[1] = pos[1] + 0.0491 + cable_length + ADDITIONAL_OFFSET
    pos_c[2] = pos[0]
    step.position = list(pos_c)
    cartesian_pub.publish(step)
    while not checkPos(pos_c):
        pass
    print ("arrived to target hole")


def checkPos(target_pos, epsilon=0.0005):
    dist = norm(current_pos - target_pos) - GRASSHOPPER_SISTEMATIC_OFFSET
    #print "Grasshopper Distance from Target= {}".format(dist) for debug
    return dist < epsilon


def getFrame(frame_id, parent_id="world"):
    tf = None
    ct = 50
    while tf is None or ct > 50:
        ct -= 1
        try:
            tf = node.retrieveTransform(frame_id=frame_id,
                                        parent_frame_id=parent_id,
                                        time=-1)
        except Exception as e:
            tf = None
            print Hr_open + "Waiting for tf:" + H_close +" {}...".format(frame_id)
    return tf



# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("grasshopper_insertion")
    node.setHz(100)

    node.setupParameter("is_task_active", False)
    is_task_active = node.getParameter("is_task_active")

    proxy_server = GrasshopperInsTacProxyServer()

    cartesian_pub = node.createPublisher("/cartesian/joint_command", JointState)
    reset_tact = node.createPublisher("/tactile_reset", String)

    node.createSubscriber("/insertion_control/tact_estimate", Twist, tactile_cb)
    node.createSubscriber("/cartesian/joint_states", JointState, current_pos_cb)

    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    force_thr_push = THR_PUSH    
    force_thr_pull = THR_PULL  
    mode = None
    step = JointState()
    step.name = ['X', 'Y', 'Z']
    hole_tf_name = None
    contact_force_estim = None
    current_pos = None
    p = None
    flag_stop = False
    flag_insertion = False
    c=None
    

    while node.isActive():
        if is_task_active:
            endTask_detector()
        node.tick()
