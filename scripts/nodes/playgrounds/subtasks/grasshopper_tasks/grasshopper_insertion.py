#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
from numpy.linalg import norm
import time
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode

from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String
from std_msgs.msg import Float32
from geometry_msgs.msg import Pose
from sensor_msgs.msg import JointState
from std_srvs.srv import Trigger, TriggerResponse

from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData
from wires_robotic_platform.param.global_parameters import Parameters

#⬢⬢⬢⬢⬢➤ ROBOT
robot_name = Parameters.get("BONMET_NAME")  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Hb_open = "\033[94m\033[1m\033[4m"
Hg_open = "\033[92m\033[1m\033[4m"
Hr_open = "\033[91m\033[1m\033[4m"
H_close = "\033[0m"

STD_CABLE_LENG = Parameters.get("STD_CABLE_LENGTH")

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class GrasshopperInsProxyServer(object):
    def __init__(self):
        self.command_proxy_server_name = "grasshopper_insertion_supervisor"
        self.command_server = CommandProxyServer(self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(self.command_proxy_server_name.replace("_supervisor", "") + ": " + command)
                    if command == "back":
                        dist = float(msg.getData("dist"))
                        goBack(dist)
                    else:
                        setTargetHole(command)
                else:
                    self.command_server_last_message = None
            else:
                self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            print "\n" + Hg_open + "Task Ended!!!" + H_close + " \t Success: {}\n\n".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

def goBack(dist=0.01):
    global is_task_active, go_back, back_distance
    is_task_active = True
    go_back = True
    back_distance = dist


def setTargetHole(tf_name):
    global is_task_active, hole_tf_name
    is_task_active = True
    hole_tf_name = tf_name


def setTaskEnd():
    global is_task_active, hole_tf_name, go_back
    hole_tf_name = None
    go_back = False
    is_task_active = False
    proxy_server.sendResponse(True)


def checkPos(target_pos, epsilon=0.0001):
    if current_pos is None or target_pos is None:
        return False
    dist = norm(current_pos - target_pos) - cable_length
    print "Grasshopper Distance from Target= {}".format(dist)
    return dist < epsilon


def current_pos_cb(msg):
    global current_pos
    current_pos = np.array(msg.position)


def getFrame(frame_id, parent_id="world"):
    tf = None
    ct = 50
    while tf is None or ct > 50:
        ct -= 1
        try:
            tf = node.retrieveTransform(frame_id=frame_id,
                                        parent_frame_id=parent_id,
                                        time=-1)
        except Exception as e:
            tf = None
            print Hr_open + "Waiting for tf:" + H_close + " {}...".format(frame_id)
    return tf


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("grasshopper_insertion")
    node.setHz(1)

    proxy_server = GrasshopperInsProxyServer()
    cartesian_pub = node.createPublisher("/cartesian/joint_command", JointState)
    node.createSubscriber("/cartesian/joint_states", JointState, current_pos_cb)

    hole_tf_name = None
    current_pos = None
    is_task_active = False
    go_back = False
    cable_length = STD_CABLE_LENG

    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    while node.isActive():
        pos = None
        if is_task_active:

            if hole_tf_name is not None:
                hole_pose = transformations.KDLtoNumpyVector(getFrame(hole_tf_name, parent_id="bonmetc60/link6"))

                if hole_pose is not None:
                    cable_length = Parameters.get("CABLE_LENGTH")  # it may be modified by vision feedback
                    pos = hole_pose[0][0:3]  # x y z  (hole wrt link6)
                    pos_c = np.array([pos[0], pos[1], pos[2]])
                    pos_c[0] = -(pos[2] - 0.392)
                    pos_c[1] = pos[1] + 0.0491 + cable_length
                    pos_c[2] = pos[0]
                    msg = JointState()
                    msg.position = list(pos_c)
                    cartesian_pub.publish(msg)
                    hole_tf_name = None

            if go_back:
                if current_pos is not None:
                    pos = current_pos
                    pos[1] -= back_distance
                    msg = JointState()
                    msg.position = list(pos)
                    cartesian_pub.publish(msg)
                    go_back = None

            if checkPos(pos_c):
                setTaskEnd()

        node.tick()
