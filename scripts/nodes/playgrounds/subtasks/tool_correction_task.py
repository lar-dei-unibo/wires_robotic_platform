#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
import numpy as np
import time
import tf
import math
import time
import PyKDL
import random
from PyKDL import Frame, Vector, Rotation

from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32, Float64MultiArray
from wires_robotic_platform.srv import PolyDetectionService

import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode

from wires_robotic_platform.param.global_parameters import Parameters


class WireToolCorrection(object):

    def __init__(self, robot_name, subtask_name):
        self.subtask_name = subtask_name
        self.robot_name = robot_name

        self.wire_z_offset = 0
        self.wire_x_offset = 0
        self.wire_angle = 0
        self.correction_source = "vision"  # "tactile"
        self.selected_tool = "gripper"

        # subtask outer command
        self.command_server = CommandProxyServer("{}_supervisor".format(self.subtask_name))
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        # robot command
        self.robot_command_client = CommandProxyClient("{}_supervisor".format(self.robot_name))
        self.robot_command_client.registerDoneCallback(self.robot_done_callback)

    # ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ CALLBACKS ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == "{}_supervisor".format(self.subtask_name):
                    command = msg.getCommand()
                    Logger.warning(command)
                    self.result_flg = True
                    if command == "correct":
                        self.selected_tool = msg.getData("tool_name")
                        self.wireToolCorrection()
                    elif command == "set":
                        self.correction_source = msg.getData("correction")
                        if self.correction_source not in ["vision", "tactile"]:
                            Logger.error("Wrong Correction Source Setting")
                            self._send_command_result(False)
                    elif command == "waitend":
                        self.condition = msg.getData("condition")
                        pass  # TODO
                        Logger.log("Sub Task {} - {} {}".format(self.subtask_name, command, self.condition))
                        self._send_command_result(False)
                    elif command == "setcondition":
                        condition = msg.getData("condition")
                        settings = msg.getData("settings")
                        pass  # TODO
                        Logger.log("Sub Task '{}': ".format(self.condition))
                        self._send_command_result(False)
                    else:
                        pass  # TODO
                        Logger.error("INVALID input")
                        self._send_command_result(False)

        except Exception as e:
            print(e)

    def robot_done_callback(self, response_command):
        if response_command:
            print(response_command.response_data)
            success = (response_command.status == CommandMessage.COMMAND_STATUS_OK)
            sent_command = response_command.getSentMessage().getCommand()
        else:
            sent_command = "NONE"
            success = False
        print "robot: {} >>> SUCCESS={}".format(sent_command, success)
        self._send_command_result(success)

    def wire_params_callback(self, msg):
        if self.correction_source == "tactile":
            if math.fabs(msg.data[0]) < 50 and math.fabs(msg.data[1]) < 50:
                self.wire_angle = np.arctan(msg.data[0])
                self.wire_z_offset = (msg.data[1]) / 1000.0
                self.wire_x_offset = 0
            else:
                self.wire_angle = 0
                self.wire_z_offset = 0
                self.wire_x_offset = 0

    def vision_wire_params_callback(self, msg):
        if self.correction_source == "vision":
            self.wire_angle = np.arctan(msg.data[0])
            self.wire_z_offset = (msg.data[1]) / 1000.0
            self.wire_x_offset = (msg.data[2]) / 1000.0

    # ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ PRIVAT METHODS ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    def _send_command_result(self, success):
        if self.command_server_last_message:
            print "\nTask Ended!!! \t Success: {}".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None

    def wireToolCorrection(self):
        # correction_filtered = self._filter_correction()
        # x_offset = correction_filtered[0]
        # z_offset = correction_filtered[1]
        # angle_offset = correction_filtered[2]

        x_offset = self.wire_x_offset
        z_offset = self.wire_z_offset
        angle_offset = self.wire_angle

        tr1 = PyKDL.Frame(PyKDL.Vector(0,
                                       0,
                                       -z_offset))
        tr2 = PyKDL.Frame(PyKDL.Vector(x_offset,
                                       0,
                                       0))
        print("Z:{} X:{} Angle:{}".format(
            z_offset, x_offset, angle_offset))

        tr1.M = tr1.M.RotY(angle_offset)
        tr = tr1 * tr2
        self.set_new_tool(transformations.FrameVectorFromKDL(tr))

    def set_new_tool(self, transf):
        simple_message = SimpleMessage(command="settool",
                                       receiver="{}_supervisor".format(self.robot_name))
        simple_message.setData("tool_name", str(self.selected_tool))
        simple_message.setData("new_tool_name", "dynamic")
        simple_message.setData("transformation", transf)
        print(simple_message.toString())
        self.robot_command_client.sendCommand(simple_message.toString())


if __name__ == '__main__':
    node = RosNode("tool_correction_task_node")
    node.setupParameter("hz", 250)
    node.setHz(node.getParameter("hz"))

    comau_name = Parameters.get("COMAU_NAME")
    subtask_name = "tool_correction_task"
    subtask = WireToolCorrection(comau_name, subtask_name)

    node.createSubscriber("/wire_params", Float64MultiArray, subtask.wire_params_callback)
    node.createSubscriber("/terminal_measurement_side/wire_params", Float64MultiArray, subtask.vision_wire_params_callback)

    try:
        while node.isActive():
            node.tick()
    except rospy.ROSInterruptException:
        pass
