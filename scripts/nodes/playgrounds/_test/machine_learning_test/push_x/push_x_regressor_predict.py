#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.utils.logger import Logger
from scipy.ndimage.measurements import center_of_mass

import cv2
import aruco
import rospkg
import math
import sys
import random

#⬢⬢⬢⬢⬢➤
import matplotlib.pyplot as plt

import numpy as np
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn import linear_model
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.naive_bayes import GaussianNB
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error
from scipy.io import loadmat, savemat


##########################################################################
##########################################################################
##########################################################################
##########################################################################
##########################################################################


# ⬢⬢⬢⬢⬢➤ DATA
data = loadmat(
    '/home/riccardo/Wires/machine_learning_data/wire_push_x/dataset3/allDataMat/elabData.mat')
xTrain = data['xTrain']
data = loadmat(
    '/home/riccardo/Wires/machine_learning_data/wire_push_x/tac20elab.mat')
xTest = np.transpose(data['tactile_temp'])


# mu = np.array([0.266117287924386, 0.0916979759921811, 0.0825451290018885, 0.0358568658956006, 0.144286419693290, 0.119184565787752, 0.187335370717526, 0.107187941831536,
#                0.234070098686318, 0.190644451265488, 0.302234880038332, 0.194837754314652, 0.124072442602016, 0.127309378522718, 0.141802139431530, 0.144991753681966])
# sigma = np.array([0.352340793427298, 0.183721359152859, 0.234120165041890, 0.107004733128514, 0.215635326736652, 0.170597883837399, 0.322919206314109, 0.294821543119655,
#                   0.330645183358957, 0.282932679679583, 0.454957728447078, 0.309542260276251, 0.242701331898588, 0.282627949294398, 0.314748975384951, 0.333648037749226])
# x = xTest[0, :] - mu
# x = np.concatenate([[x], [xTest[1, :] - mu]])
# for i in range(2, 12182):
#     x = np.concatenate([x, [xTest[i, :] - mu]])

reg = joblib.load(
    '/home/riccardo/Wires/machine_learning_data/pkl_files/wire_push_x/contact_regressor_MLP.pkl')
yPred = reg.predict(xTest)

plt.figure()
# plt.plot(yTest)
plt.plot(yPred)
plt.show()


# data_all_dict = {}
# data_all_dict["yTest"] = yTest
# data_all_dict["yPred"] = yPred


# savemat(
#     '/home/riccardo/Wires/machine_learning_data/wire_push_x/tac20output.mat', data_all_dict)
