import os
import time
import warnings
import numpy as np
import matplotlib.pyplot as plt
from numpy import newaxis
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Hide messy TensorFlow warnings
warnings.filterwarnings("ignore")  # Hide messy Numpy warnings

LAST = -1


def load_data(filename, seq_len, normalise_window):

    # extract the data from .csv file
    f = open(filename, 'rb').read()
    raw_data = f.decode().split('\n')

    # organize the data flow vector [data_flow_length x 1]
    # in a multitimensional-vector [data_flow_length x sequence_length]
    # having in each element '(i)th' a sequence of samples,
    # where we remove the sample 'x(i+1-sequence_length)' from the '(i-1)th' sequence
    # and we add the sample 'x(i)
    # >>> data(i-1) = [x(i-1), x(i-2),  ...,    ..., x(i-sequence_length)]
    # >>> data(i) =   [x(i),   x(i-1), x(i-2),  ..., x(i+1-sequence_length)]
    # >>> data(i+1) = [x(i+1), x(i),   x(i-1),  ..., x(i+2-sequence_length)]
    sequence_length = seq_len + 1
    data_flow_length = len(raw_data)
    data = []
    for index in range(data_flow_length - sequence_length):
        window_data = raw_data[index: index + sequence_length]
        data.append(window_data)

    if normalise_window:
        data = normalise_windows(data)

    data = np.array(data)

    # we use the 90% of the data for the training
    # and 10% for the test
    row = round(0.9 * data.shape[0])
    train = data[0:int(row), :]
    test = data[int(row):, :]

    # we shuffle the training sequences (windows) around so that we can make sure
    # we're predicting based on just the windows individual rather than any longer
    # term sequences or windows in sequence
    np.random.shuffle(train)

    x_train = train[:, 0:-1]
    y_train = train[:, -1]
    x_test = test[:, 0:-1]
    y_test = test[:, -1]

    print "\nx_train = {}\ny_train ={}\nx_test ={}\ny_test ={}".format(x_train.shape, y_train.shape, x_test.shape, y_test.shape)

    # x_train = np.reshape(
    #     x_train, (x_train.shape[0], x_train.shape[1], 1))  # TODO???
    # x_test = np.reshape(
    #     x_test, (x_test.shape[0], x_test.shape[1], 1))  # TODO???

    print "\nx_train_reshape = {} \nx_test_reshape ={} ".format(x_train.shape, y_test.shape)

    return [x_train, y_train, x_test, y_test]


def normalise_windows(window_data_list):
    # reflect percentage changes from the start of that window
    # (so the data at point i=0 will always be 0)
    normalised_window_data_list = []
    for window_data in window_data_list:
        normalised_window_data = [((float(sample) / float(window_data[0])) - 1)
                                  for sample in window_data]
        normalised_window_data_list.append(normalised_window_data)
    return normalised_window_data_list


def build_model(layers=[1, 50, 100, 1]):
    model = Sequential()

    model.add(LSTM(
        input_shape=(layers[1], layers[0]),
        output_dim=layers[1],
        return_sequences=True))
    model.add(Dropout(0.2))

    model.add(LSTM(
        layers[2],
        return_sequences=False))
    model.add(Dropout(0.2))

    model.add(Dense(
        output_dim=layers[3]))
    model.add(Activation("linear"))

    start = time.time()
    model.compile(loss="mse", optimizer="rmsprop")
    print("> Compilation Time : ", time.time() - start)
    return model


def build_model_0(layers=None):
    model = Sequential()

    model.add(LSTM(
        units=50,           # no. of Cells
        input_length=50,    # no. of Time Steps
        input_dim=1,        # no. of Inputs
        return_sequences=True))
    model.add(Dropout(0.2))

    model.add(LSTM(
        units=100,           # no. of Cells
        return_sequences=False))
    model.add(Dropout(0.2))

    model.add(Dense(1))
    model.add(Activation("linear"))

    start = time.time()
    model.compile(loss="mse", optimizer="rmsprop")
    print("> Compilation Time : ", time.time() - start)
    return model


def build_model_1(layers=None):
    model = Sequential()

    model.add(LSTM(units=100,
                   input_shape=(50, 1)))
    model.add(Dropout(0.2))

    model.add(Dense(16, activation="tanh"))
    model.add(Dense(4, activation="tanh"))
    model.add(Dense(1, activation="tanh"))

    start = time.time()
    model.compile(loss="mse", optimizer="rmsprop")
    print("> Compilation Time : ", time.time() - start)
    return model


def build_model_2(layers=None):
    model = Sequential()

    model.add(LSTM(units=100,
                   input_shape=(50, 1),
                   return_sequences=True))
    model.add(Dropout(0.2))

    model.add(LSTM(units=30,
                   return_sequences=False))
    model.add(Dropout(0.2))

    model.add(Dense(16, activation="tanh"))
    model.add(Dense(4, activation="tanh"))
    model.add(Dense(1, activation="tanh"))

    start = time.time()
    model.compile(loss="mse", optimizer="rmsprop")
    print("> Compilation Time : ", time.time() - start)
    return model


def predict_point_by_point(model, data):
    # Predict each timestep given the last sequence of true data, in effect
    # only predicting 1 step ahead each time
    predicted = model.predict(data)
    predicted = np.reshape(predicted, (predicted.size,))
    return predicted


def predict_sequence_full(model, data, window_size):
    # Shift the window by 1 new prediction each time, re-run predictions on
    # new window
    curr_frame = data[0]
    predicted = []
    for i in range(len(data)):
        predicted.append(model.predict(curr_frame[newaxis, :, :])[0, 0])
        curr_frame = curr_frame[1:]
        curr_frame = np.insert(
            curr_frame, [window_size - 1], predicted[-1], axis=0)
    return predicted


def predict_sequences_multiple(model, data, window_size, prediction_len):
    # Predict sequence of 50 steps before shifting prediction run forward by
    # 50 steps
    prediction_seqs = []
    for i in range(int(len(data) / prediction_len)):
        curr_frame = data[i * prediction_len]
        predicted = []
        for j in range(prediction_len):
            predicted.append(model.predict(curr_frame[newaxis, :, :])[0, 0])
            curr_frame = curr_frame[1:]
            curr_frame = np.insert(
                curr_frame, [window_size - 1], predicted[-1], axis=0)
        prediction_seqs.append(predicted)
    return prediction_seqs
