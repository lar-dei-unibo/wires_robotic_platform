import lstm
import time
import matplotlib.pyplot as plt
from keras.utils import plot_model
from wires_robotic_platform.param.parameters import Parameters


def plot_results(predicted_data, true_data):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    plt.plot(predicted_data, label='Prediction')
    plt.legend()
    plt.show()


def plot_results_multiple(predicted_data, true_data, prediction_len):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    # Pad the list of predictions to shift it in the graph to it's correct
    # start
    for i, data in enumerate(predicted_data):
        padding = [None for p in range(i * prediction_len)]
        plt.plot(padding + data, label='Prediction')
        plt.legend()
    plt.show()


# Main Run Thread
if __name__ == '__main__':
    global_start_time = time.time()
    epochs = 10
    seq_len = 50

    print('> Loading data... ')

    data_folder = '{}/machine_learning_data/_tmp/test_keras'.format(Parameters.get("DATA_FOLDER"))
    # X_train, y_train, X_test, y_test = lstm.load_data(
    #     '{}/sinwave.csv'.format(data_folder), seq_len, False)
    X_train, y_train, X_test, y_test = lstm.load_data('{}/sp500.csv'.format(data_folder),
                                                      seq_len,
                                                      True)

    print('> Data Loaded. Compiling...')

    model = lstm.build_model_2()

    plot_model(model, to_file='model.png')

    model.fit(
        X_train,
        y_train,
        batch_size=512,
        nb_epoch=epochs,
        validation_split=0.05)

    # X_test = X_train
    # y_test = y_train

    # predicted = lstm.predict_sequences_multiple(model, X_test, seq_len, 50)
    # predicted = lstm.predict_sequence_full(model, X_test, seq_len)
    predicted = lstm.predict_point_by_point(model, X_test)

    print('Training duration (s) : ', time.time() - global_start_time)
    # plot_results_multiple(predicted, y_test, 50)
    plot_results(predicted, y_test)
