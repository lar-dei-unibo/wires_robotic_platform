#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import math
import time
import PyKDL
from scipy.ndimage.measurements import center_of_mass

import cv2
import aruco
import math
import sys
import random
import pickle

#⬢⬢⬢⬢⬢➤
import matplotlib.pyplot as plt

import numpy as np
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, AdaBoostClassifier
from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.externals import joblib
from sklearn.metrics import precision_recall_curve, average_precision_score

from scipy.io import loadmat, savemat


def save_obj(obj, name):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)


def trainingTestCLF(clf_name, clf_dict, xTrain, yTrain, xTest, yTest, score_th=0.7, data_export_dict={}):
    score = 0.0
    ap_max = 0
    clf_score_dict = {}
    plt.figure()
    for k in clf_dict.keys():
        t0 = time.time()
        clf_dict[k].fit(xTrain, yTrain)
        train_time = time.time() - t0
        score = clf_dict[k].score(xTest, yTest)
        ct = 20
        while score < score_th:
            ct = ct - 1
            print("{}/{}__________ {}, score={}".format(ct, score_th, k, score))
            t0 = time.time()
            clf_dict[k].fit(xTrain, yTrain)
            train_time = time.time() - t0
            score = clf_dict[k].score(xTest, yTest)
            if ct <= 0:
                ct = 20
                score_th = 0.9 * score_th

        #⬢⬢⬢⬢⬢➤ TESTING

        if clf_name == "SVC"or clf_name == "ada":
            yPredProb = clf_dict[k].decision_function(xTest)

        else:
            yPredProb = clf_dict[k].predict_proba(xTest)
            yPredProb = yPredProb[:, 1]
        print("\n{}\nscore={}".format(k, score))

        t0 = time.time()
        yPred = clf_dict[k].predict(xTest)
        time_pred = time.time() - t0

        precision, recall, _ = precision_recall_curve(yTest, yPredProb)
        average_precision = average_precision_score(yTest, yPredProb)
        if average_precision > ap_max:
            ap_max = average_precision
            k_ap_max = k
        print("average_precision = {}".format(average_precision))

        data_export_dict["recall_{}_{}".format(clf_name, k)] = recall
        data_export_dict["precision_{}_{}".format(clf_name, k)] = precision
        data_export_dict["auc_{}_{}".format(
            clf_name, k)] = average_precision
        data_export_dict["tTrain_{}_{}".format(clf_name, k)] = train_time
        data_export_dict["tPred_{}_{}".format(clf_name, k)] = time_pred
        data_export_dict["yPred_{}_{}".format(clf_name, k)] = yPred

        plt.step(recall, precision, where='post', label=str(k))

    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.legend()

    print("\n\n##############################################################")
    print("########################### BEST CLF  ############################")
    print("##################################################################")

    data_export_dict["keys_{}".format(clf_name)] = clf_dict.keys()
    data_export_dict["best_{}".format(clf_name)] = k_ap_max

    # joblib.dump(clf_dict[k_ap_max], dirPKLName + '/grasping_classifier_{}.pkl'.format(clf_name))

    plt.figure()
    yPred = data_export_dict["yPred_{}_{}".format(clf_name, k_ap_max)]
    plt.plot(yPred, '.', label="{}".format(k_ap_max))
    plt.plot(yTest)
    plt.legend()
    axes = plt.gca()
    axes.set_ylim([-0.1, 1.1])

    print("{}: AUC={}, t_pred={}, t_train={}".format(
        k_ap_max,
        ap_max,
        data_export_dict["tPred_{}_{}".format(clf_name, k_ap_max)],
        data_export_dict["tTrain_{}_{}".format(clf_name, k_ap_max)]))

    return data_export_dict

##########################################################################
##########################################################################
##########################################################################
##########################################################################
##########################################################################


dirDataName = '/home/riccardo/Wires/machine_learning_data/wire_grasping/test2'
dirPKLName = '/home/riccardo/Wires/machine_learning_data/pkl_files/wire_grasping'

# ⬢⬢⬢⬢⬢➤ DATA
data = loadmat(dirDataName + '/allDataMat/elabData.mat')
xTrain = data['xTrain']
yTrain = np.ravel(data['yTrain'])
xTest = data['xTest']
yTest = np.ravel(data['yTest'])

data_all_dict = {}
data_all_dict["yTest"] = yTest

#⬢⬢⬢⬢⬢➤ RANDOM FOREST
clf_dict = {}
# clf_dict["t10_f1"] = RandomForestClassifier(
#     n_estimators=10, n_jobs=8,min_samples_split=2, max_features=1)
# clf_dict["t10_f4"] = RandomForestClassifier(
#     n_estimators=10, n_jobs=8,min_samples_split=2, max_features=4)
# clf_dict["t10_f6"] = RandomForestClassifier(
#     n_estimators=10, n_jobs=8, min_samples_split=2,max_features=6)
# clf_dict["t10_f8"] = RandomForestClassifier(
#     n_estimators=10, n_jobs=8,min_samples_split=2, max_features=8)
# clf_dict["t29_f1"] = RandomForestClassifier(
#     n_estimators=20, n_jobs=8,min_samples_split=2, max_features=1)
# clf_dict["t20_f4"] = RandomForestClassifier(
#     n_estimators=20, n_jobs=8, min_samples_split=2,max_features=4)
# clf_dict["t20_f6"] = RandomForestClassifier(
#     n_estimators=20, n_jobs=8,min_samples_split=2, max_features=6)
# clf_dict["t20_f8"] = RandomForestClassifier(
#     n_estimators=20, n_jobs=8,min_samples_split=2, max_features=8)
# clf_dict["t50_f1"] = RandomForestClassifier(
#     n_estimators=50, n_jobs=8,min_samples_split=2, max_features=1)
# clf_dict["t50_f4"] = RandomForestClassifier(
#     n_estimators=50, n_jobs=8,min_samples_split=2, max_features=4)
# clf_dict["t50_f6"] = RandomForestClassifier(
#     n_estimators=50, n_jobs=8,min_samples_split=2, max_features=6)
# clf_dict["t50_f8"] = RandomForestClassifier(
#     n_estimators=50, n_jobs=8, min_samples_split=2,max_features=8)
# clf_dict["t100_f1"] = RandomForestClassifier(
#     n_estimators=100, n_jobs=8,min_samples_split=2, max_features=1)
# clf_dict["t100_f4"] = RandomForestClassifier(
#     n_estimators=100, n_jobs=8,min_samples_split=2, max_features=4)
# clf_dict["t100_f6"] = RandomForestClassifier(
#     n_estimators=100, n_jobs=8, min_samples_split=2,max_features=6)
# clf_dict["t100_f8"] = RandomForestClassifier(
#     n_estimators=100, n_jobs=8, min_samples_split=2,max_features=8)
clf_dict["t200_f1_s2"] = RandomForestClassifier(
    n_estimators=200, n_jobs=8, min_samples_split=10, max_features=1)
clf_dict["t200_f1_s10"] = RandomForestClassifier(
    n_estimators=200, n_jobs=8, min_samples_split=10, max_features=1)
# clf_dict["t200_f8"] = RandomForestClassifier(
#     n_estimators=200, n_jobs=8,min_samples_split=2, max_features=8)
# clf_dict["t200_f6"] = RandomForestClassifier(
#     n_estimators=200, n_jobs=8,min_samples_split=2, max_features=6)
clf_dict["t200_f4_s2"] = RandomForestClassifier(
    n_estimators=200, n_jobs=8, min_samples_split=2, max_features=4)
clf_dict["t200_f4_s10"] = RandomForestClassifier(
    n_estimators=200, n_jobs=8, min_samples_split=10, max_features=4)
clf_dict["t200_f4_s20"] = RandomForestClassifier(
    n_estimators=200, n_jobs=8, min_samples_split=20, max_features=4)
clf_dict["t200_f4_s40"] = RandomForestClassifier(
    n_estimators=200, n_jobs=8, min_samples_split=40, max_features=4)
# clf_dict["t500_f1"] = RandomForestClassifier(
#     n_estimators=500, n_jobs=8, min_samples_split=2,max_features=1)
clf_dict["t500_f4_s2"] = RandomForestClassifier(
    n_estimators=500, n_jobs=8, min_samples_split=2, max_features=4)
clf_dict["t500_f4_s10"] = RandomForestClassifier(
    n_estimators=500, n_jobs=8, min_samples_split=10, max_features=4)
clf_dict["t500_f4_s30"] = RandomForestClassifier(
    n_estimators=500, n_jobs=8, min_samples_split=30, max_features=4)
# clf_dict["t700_f1"] = RandomForestClassifier(
#     n_estimators=700, n_jobs=8, min_samples_split=2,max_features=1)
# clf_dict["t700_f4"] = RandomForestClassifier(
# n_estimators=700, n_jobs=8, min_samples_split=10,max_features=4)

data_all_dict = trainingTestCLF(
    "RF", clf_dict, xTrain, yTrain, xTest, yTest, score_th=0.87, data_export_dict=data_all_dict)

#⬢⬢⬢⬢⬢➤ NEURAL NETWORK
clf_dict = {}
# clf_dict["64l32l16l8l2_a01"] = MLPClassifier(
#     hidden_layer_sizes=(64, 32, 16, 8, 2), alpha=0.01, activation="relu",
# max_iter = 1000,  learning_rate = "adaptive")
clf_dict["32l16l8l2_a01"] = MLPClassifier(
    hidden_layer_sizes=(32, 16, 8, 2), alpha=0.01, activation="relu",
    max_iter=1000, learning_rate="adaptive")
clf_dict["16l8l2_a01"] = MLPClassifier(
    hidden_layer_sizes=(16, 8, 2), alpha=0.01, activation="relu", max_iter=1000,  learning_rate="adaptive")
clf_dict["8l2_a01"] = MLPClassifier(
    hidden_layer_sizes=(8, 2), alpha=0.01, activation="relu", max_iter=1000,
    learning_rate="adaptive")
# clf_dict["64l32l16l8l2_a001"] = MLPClassifier(
#     hidden_layer_sizes=(64, 32, 16, 8, 2), alpha=0.001, activation="relu",
# max_iter = 1000,  learning_rate = "adaptive")
clf_dict["32l16l8l2_a001"] = MLPClassifier(
    hidden_layer_sizes=(32, 16, 8, 2), alpha=0.001, activation="relu",
    max_iter=1000, learning_rate="adaptive")
clf_dict["16l8l2_a001"] = MLPClassifier(
    hidden_layer_sizes=(16, 8, 2), alpha=0.001, activation="relu", max_iter=1000, learning_rate="adaptive")
clf_dict["8l2_a001"] = MLPClassifier(
    hidden_layer_sizes=(8, 2), alpha=0.001, activation="relu",
    max_iter=1000, learning_rate="adaptive")
# clf_dict["64l32l16l8l2_a0001"] = MLPClassifier(
#     hidden_layer_sizes=(64, 32, 16, 8, 2), alpha=0.0001, activation="relu",
#     max_iter=1000,  learning_rate="adaptive")
clf_dict["32l16l8l2_a0001"] = MLPClassifier(
    hidden_layer_sizes=(32, 16, 8, 2), alpha=0.0001, activation="relu",
    max_iter=1000, learning_rate="adaptive")
clf_dict["16l8l2_a0001"] = MLPClassifier(
    hidden_layer_sizes=(16, 8, 2), alpha=0.0001, activation="relu",
    max_iter=1000, learning_rate="adaptive")
clf_dict["8l2_a0001"] = MLPClassifier(
    hidden_layer_sizes=(8, 2), alpha=0.0001, activation="relu",
    max_iter=1000, learning_rate="adaptive")
clf_dict["4l2_a0001"] = MLPClassifier(
    hidden_layer_sizes=(4, 2), alpha=0.0001, activation="relu",
    max_iter=1000, learning_rate="adaptive")
clf_dict["4l2_a001"] = MLPClassifier(
    hidden_layer_sizes=(4, 2), alpha=0.001, activation="relu",
    max_iter=1000, learning_rate="adaptive")
clf_dict["4l2_a01"] = MLPClassifier(
    hidden_layer_sizes=(4, 2), alpha=0.01, activation="relu",
    max_iter=1000, learning_rate="adaptive")
clf_dict["4l4_a01"] = MLPClassifier(
    hidden_layer_sizes=(4, 4), alpha=0.01, activation="relu",
    max_iter=1000, learning_rate="adaptive")

data_all_dict = trainingTestCLF("MLP", clf_dict, xTrain,
                                yTrain, xTest, yTest, score_th=0.9, data_export_dict=data_all_dict)

#⬢⬢⬢⬢⬢➤ Support Vector Machines
clf_dict = {}
# clf_dict["k_rbf_0c01"] = SVC(C=0.01, kernel='rbf')
# clf_dict["k_rbf_0c1"] = SVC(C=0.1, kernel='rbf')
# clf_dict["k_rbf_10c"] = SVC(C=10.0, kernel='rbf')
clf_dict["k_rbf_100c"] = SVC(C=100.0, kernel='rbf')
clf_dict["k_rbf_1000c"] = SVC(C=1000.0, kernel='rbf')
clf_dict["k_rbf_5000c"] = SVC(C=5000.0, kernel='rbf')
clf_dict["k_rbf_10000c"] = SVC(C=10000.0, kernel='rbf')


data_all_dict = trainingTestCLF("SVC", clf_dict, xTrain,
                                yTrain, xTest, yTest, score_th=0.95, data_export_dict=data_all_dict)

#⬢⬢⬢⬢⬢➤ AdaBoost
clf_dict = {}
clf_dict["ada"] = AdaBoostClassifier(n_estimators=150)
# k_opt_ada = trainingTestCLF("ada", clf_dict, xTrain,
# yTrain, xTest, yTest, score_th=0.8, data_export_dict=data_all_dict)

#⬢⬢⬢⬢⬢➤ Gaussian Naive Bayes
clf_dict = {}
clf_dict["GNB"] = GaussianNB()
# k_opt_gnb = trainingTestCLF("GNB", clf_dict, xTrain, yTrain, xTest,
# yTest, score_th=0.8, data_export_dict=data_all_dict)

# savemat(dirDataName + '/allDataMat/outputData.mat', data_all_dict)

##########################################################################
##########################################################################
##########################################################################
##########################################################################
##########################################################################

k_opt_mlp = data_all_dict["best_MLP"]
precision_mlp = data_all_dict["precision_MLP_{}".format(k_opt_mlp)]
recall_mlp = data_all_dict["recall_MLP_{}".format(k_opt_mlp)]

k_opt_rf = data_all_dict["best_RF"]
precision_rf = data_all_dict["precision_RF_{}".format(k_opt_rf)]
recall_rf = data_all_dict["recall_RF_{}".format(k_opt_rf)]

k_opt_svc = data_all_dict["best_SVC"]
precision_svc = data_all_dict["precision_SVC_{}".format(k_opt_svc)]
recall_svc = data_all_dict["recall_SVC_{}".format(k_opt_svc)]

plt.figure()
plt.step(recall_mlp, precision_mlp, where='post', label="mlp")
plt.step(recall_rf, precision_rf, where='post', label="rf")
plt.step(recall_svc, precision_svc, where='post', label="svc")
# plt.fill_between(recall, precision, step='post', alpha=0.2,
# color='b')

plt.xlabel('Recall')
plt.ylabel('Precision')
plt.ylim([0.0, 1.05])
plt.xlim([0.0, 1.0])
plt.legend()

plt.show()
