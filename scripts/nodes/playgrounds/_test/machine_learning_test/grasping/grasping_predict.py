#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32,  Float64MultiArray
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import math
import sys
import random

#⬢⬢⬢⬢⬢➤
import matplotlib.pyplot as plt

from wires_robotic_platform.utils.ros import RosNode
import numpy as np
from sklearn.ensemble import RandomForestClassifier,  RandomForestRegressor
from sklearn.neural_network import MLPRegressor
import os
from sklearn.externals import joblib
from scipy.io import loadmat


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("grasping_classification")

node.setupParameter("hz", 250)
node.setHz(node.getParameter("hz"))
node_rate = node.getParameter("hz")

xNew = np.zeros(16)
mu = np.array([0.266117287924386, 0.0916979759921810, 0.0825451290018885, 0.0358568658956006, 0.144286419693290, 0.119184565787752, 0.187335370717526, 0.107187941831536,
               0.234070098686318, 0.190644451265488, 0.302234880038332, 0.194837754314652, 0.124072442602016, 0.127309378522718, 0.141802139431530, 0.144991753681966])
sigma = np.array([0.352340793427298, 0.183721359152859, 0.234120165041890, 0.107004733128514, 0.215635326736652, 0.170597883837400, 0.322919206314109, 0.294821543119655,
                  0.330645183358957, 0.282932679679584, 0.454957728447078, 0.309542260276251, 0.242701331898588, 0.282627949294399, 0.314748975384951, 0.333648037749226])


def tactile_callback(msg):
    global xNew
    dataNew = np.ravel(msg.data)
    xNew = dataNew - mu


pred_pub = node.createPublisher('/grasp_clf_pred', Float64MultiArray)
tactile_sub = node.createSubscriber(
    "/tactile", Float64MultiArray, tactile_callback)

clf_flg = False
if os.path.exists('/home/riccardo/Wires/machine_learning_data/pkl_files/wire_grasping/grasping_classifier_MLP.pkl'):
    print("One classifier found!!")
    global clf
    clf = joblib.load(
        '/home/riccardo/Wires/machine_learning_data/pkl_files/wire_grasping/grasping_classifier_MLP.pkl')
    clf_flg = True
else:
    print("None classifier found! Train a classifier first.")
    clf_flg = False

raw_input("\n\n\n\ndid you reset the tactile sensor?")

while node.isActive():
    if clf_flg:
        yPred = clf.predict(xNew.reshape(1, -1))
        print("pred={}".format(yPred))
        pred = Float64MultiArray()
        pred.data = [yPred]
        pred_pub.publish(pred)
    node.tick()
