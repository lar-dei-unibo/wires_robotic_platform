#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32,  Float64MultiArray
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import math
import sys
import random

#⬢⬢⬢⬢⬢➤
import matplotlib.pyplot as plt

from wires_robotic_platform.utils.ros import RosNode
import numpy as np
from sklearn.ensemble import RandomForestClassifier,  RandomForestRegressor
from sklearn.neural_network import MLPRegressor
from scipy.io import loadmat


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("tf_manager")

node.setupParameter("hz", 250)
node.setHz(node.getParameter("hz"))
node_rate = node.getParameter("hz")

xNew = np.array([])
xCurrent = np.array([])
xCurrent = np.zeros(16)


def tactile_callback(msg):
    global xCurrent, xNew
    xNew = np.ravel(msg.data)
    # if len(xCurrent) == 0:
    #     xCurrent = xNew
    # else:
    #     xCurrent = np.vstack((xCurrent, xNew))


pred_pub = node.createPublisher('/pred_ml', Float64MultiArray)
tactile_sub = node.createSubscriber(
    "/tactile", Float64MultiArray, tactile_callback)

# ⬢⬢⬢⬢⬢➤ DATA
WIRES_DATA_FOLDER = "/media/riccardo/87060f92-ea5c-42c7-bb3e-5662219d0bd0/projects/wires"
data = loadmat( '{}/machine_learning_data/wire_inserting/dataset3/allDataMat/elabData.mat'.format(WIRES_DATA_FOLDER))
xTrain = data['xTrain']
yTrain = np.ravel(data['yTrain'])
xTest = data['xTest']
yTest = np.ravel(data['yTest'])

plt.figure()
plt.plot(xTrain)

#⬢⬢⬢⬢⬢➤ LEARNING

# # ➤ classifier
# clf = RandomForestClassifier(n_estimators=300, n_jobs=8)
# clf.fit(xTrain, yTrain)
# yPred_clf = clf.predict(xTest)


# # ➤ regressor
reg = RandomForestRegressor(n_estimators=2000, n_jobs=6, verbose=2)
# reg = MLPRegressor(hidden_layer_sizes=(16 * 4, ),
#    max_iter=2000,
#    verbose=True,
#    tol=1e-6)
reg.fit(xTrain, yTrain)
yPred_reg = reg.predict(xTest)


# plt.figure( )
# plt.plot( xTrain)
# plt.figure( )
# plt.plot( xTest)
# plt.figure( )
# plt.plot( yPred_clf)
# plt.plot( yTest)
plt.figure()
plt.plot(yPred_reg)
plt.plot(yTest)
plt.show()

# while node.isActive():
#     yPred = clf.predict(xNew)
#     # print("yPred {}".format(yPred.shape))
#     pred = Float64MultiArray()
#     pred.data = [yPred]
#     pred_pub.publish(pred)
#     node.tick()
