#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf

import rospy
import rospkg
import math
import random
import sys
import time
import numpy as np

#⬢⬢⬢⬢⬢➤
import matplotlib.pyplot as plt
from scipy.io import loadmat, savemat


##########################################################################
##########################################################################
##########################################################################
from scipy.signal import butter, lfilter, filtfilt, freqz


class LowPassFilter(object):
    def __init__(self, w_size):
        self.w_size = w_size
        self.data_window = np.zeros(w_size)

    def _update_data(self, new_data):
        tmp = np.append(self.data_window, [new_data], axis=0)
        self.data_window = np.delete(tmp, 0, 0)

    def build(self, cutoff, fs, order=5):
        nyq = 0.5 * fs
        normal_cutoff = cutoff / nyq
        self.b, self.a = butter(order, normal_cutoff,
                                btype='low', analog=False)

    def filter(self, data):
        self._update_data(data)
        y = lfilter(self.b, self.a, self.data_window, axis=0)
        # y = filtfilt(b, a, data, axis=0)
        return y[self.w_size - 1]


##########################################################################
##########################################################################
##########################################################################

from wires_robotic_platform.param.parameters import Parameters
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.utils.logger import Logger
from std_msgs.msg import Header, Float64, Float32,  Float64MultiArray
from geometry_msgs.msg import Twist
from wires_robotic_platform.sensors.tactile_data_processing.tactile_rnn import TactileRNN

# from keras.models import load_model

# class TactileRNN(object):

#     def __init__(self, model_h5_file_path, seq_len):
#         self.data = RNNDataSequence(seq_len)
#         try:
#             self.model = load_model(model_h5_file_path)
#             print("Regressors found and loaded!")
#         except Exception as e:
#             Logger.error("None regressor found! " )
#             print e

#     def update(self,msg):
#         self.data.tactile_update(msg)

#     def getPrediction(self):
#         return self.model.predict(self.data.getWindow())

# class RNNDataSequence(object):

#     def __init__(self, seq_len):
#         self.window = np.zeros((seq_len,16))
#         self.new_sample = None

#     def tactile_update(self, msg):
#         print "{}".format(msg.data)
#         self.new_sample = np.ravel(msg.data).reshape((1,16))
#         self._window_update(self.new_sample)

#     def _window_update(self,new_sample):
#         tmp_window = np.append(self.window, new_sample, axis=0)
#         self.window = np.delete(tmp_window, 0, 0)

#     def getWindow(self):
#         return np.reshape(self.window, (1,self.window.shape[0],self.window.shape[1]))

##########################################################################
##########################################################################
##########################################################################


def tactile_callback(msg):
    global x_rnn, y_rnn, z_rnn
    x_rnn.update(msg)
    y_rnn.update(msg)
    z_rnn.update(msg)


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("wire_twist_pred")

node.setupParameter("hz", 120)
node.setHz(node.getParameter("hz"))

pred_pub = node.createPublisher('/wire_twist_pred', Twist)
filter_pub = node.createPublisher('/wire_twist_filtered', Twist)

seq_len_x = 5
seq_len_y = 5
seq_len_z = 5

N = 10

# '{}/machine_learning_data/h5_files'.format(Parameters.get("DATA_FOLDER"))
dirPKLName = "/home/riccardo/ros/wires_data/ml_files/wire_contact"
x_rnn = TactileRNN(model_h5_file_path=dirPKLName + '/wire_twist_x_regressor.h5',
                   seq_len=seq_len_x,
                   bias_window_size=N)
y_rnn = TactileRNN(model_h5_file_path=dirPKLName + '/wire_twist_y_regressor.h5',
                   seq_len=seq_len_y,
                   bias_window_size=N)
z_rnn = TactileRNN(model_h5_file_path=dirPKLName + '/wire_twist_z_regressor.h5',
                   seq_len=seq_len_z,
                   bias_window_size=N)

node.createSubscriber(
    "/tactile", Float64MultiArray, tactile_callback)

##########################################################################
##########################################################################
##########################################################################

raw_input("\n\n\n\ndid you reset the tactile sensor?")
time.sleep(2)

xset = np.zeros(N)
yset = np.zeros(N)
zset = np.zeros(N)
for i in range(N):
    xset[i] = x_rnn.getPrediction()
    yset[i] = y_rnn.getPrediction()
    zset[i] = z_rnn.getPrediction()

xbias = x_rnn.getMean()
ybias = y_rnn.getMean()
zbias = z_rnn.getMean()

# # Filter requirements.
# order = 6
# fs = 120.0
# cutoff = 10.0
# lp_filter = LowPassFilter(10)
# lp_filter.build(cutoff, fs, order)

while node.isActive():
    yPred_x = x_rnn.getPrediction() - xbias
    yPred_y = y_rnn.getPrediction() - ybias
    yPred_z = z_rnn.getPrediction() - zbias
    print("pred_x={}, pred_y={}, pred_z={}".format(yPred_x, yPred_y, yPred_z))

    pred = Twist()
    pred.linear.x = yPred_x
    pred.linear.y = yPred_y
    pred.linear.z = yPred_z
    pred_pub.publish(pred)

    ######################################
    # x_filtered = lp_filter.filter(yPred_x[0][0])
    # y_filtered = lp_filter.filter(yPred_y[0][0])
    # z_filtered = lp_filter.filter(yPred_z[0][0])

    # filtered = Twist()
    # filtered.linear.x = x_filtered
    # filtered.linear.y = y_filtered
    # filtered.linear.z = z_filtered
    # filter_pub.publish(filtered)

    node.tick()
