#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from wires_robotic_platform.proxy.target_follower_proxy import TargetFollowerProxy
from wires_robotic_platform.msg import RobotFollow
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.transformations import FrameVectorFromKDL, FrameVectorToKDL
from wires_robotic_platform.utils.transformations import ListToKDLVector, KDLVectorToList
from wires_robotic_platform.utils.transformations import TwistToKDLVector, TwistFormKDLVector
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.param.global_parameters import Parameters
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import sys
import random
from geometry_msgs.msg import Twist
import PyKDL
import copy
from math import sin, cos
import tf


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("sin_tf")
node.setupParameter("hz", 100)
node.setHz(node.getParameter("hz"))
node_rate = node.getParameter("hz")

br = tf.TransformBroadcaster()
listener = tf.TransformListener()
robot_name = Parameters.get("COMAU_NAME")
target_follower = TargetFollowerProxy(robot_name)
u = 0.0
v = 0.0
k = 0.005  # m

msg = Float32()
target_tf = None
while target_tf == None:
    try:
        target_tf = transformations.retrieveTransform(listener, robot_name + "/base_link",
                                                      robot_name + "/tool", none_error=True,
                                                      time=rospy.Time(0), print_error=True)
    except:
        pass

print("GOT IT")

z0 = target_tf.p.z()
y0 = target_tf.p.y()
x0 = target_tf.p.x()

t_stp = 60.0
t_sleep = 10.0

tx1 = 2 * t_stp
tx2 = tx1 + t_sleep + t_stp
tx = tx2
tz1 = tx + 2 * t_stp
tz2 = tz1 + t_stp
tz = tz2

t0 = node.getElapsedTimeInSecs()
timer = t0

while node.isActive():

    timer = node.getElapsedTimeInSecs() - t0

    if timer < tx:
        if timer < tx1:
            print("WALK - X")
            t = 2 * np.pi * 0.3 * node.getCurrentTimeInSecs()
            u = k * (0.3 * sin(0.6 * t)
                     + sin(0.35 * t)
                     + 0.8 * sin(0.2 * t)
                     + 0.5 * cos(t) * sin(0.3 * t))
            v = -k * (0.3 * sin(0.43 * t)
                      + sin(0.12 * t)
                      + 0.8 * sin(0.3 * t)
                      + 0.5 * cos(0.2 * t) * sin(t))
        elif timer < tx1 + t_sleep:
            print("SLEEP - X")
        elif timer < tx2:
            print("SIN - X")
            u = k * math.sin(2 * np.pi * 0.1 * node.getCurrentTimeInSecs())
            v = k * math.cos(2 * np.pi * 0.1 * node.getCurrentTimeInSecs())
        target_tf.p[0] = u + x0
        target_tf.p[2] = v + z0

    elif timer < tx + t_sleep:
        print("SLEEP - XZ")

    # elif timer < tz:
    #     if timer < tz1:
    #         print("WALK - Z")
    #         t = 2 * np.pi * 0.3 * node.getCurrentTimeInSecs()
    #         u = k * (0.3 * sin(0.6 * t)
    #                  + sin(0.35 * t)
    #                  + 0.8 * sin(0.2 * t)
    #                  + 0.5 * cos(t) * sin(0.3 * t))
    #     elif timer < tz1 + t_sleep:
    #         print("SLEEP - Z")
    #     elif timer < tz2:
    #         print("SIN - Z")
    #         u = k * math.sin(2 * np.pi * 0.1 * node.getCurrentTimeInSecs())
    #     target_tf.p[2] = u + z0

    else:
        print("THE END")

    transformations.broadcastTransform(br, target_tf, "sin_tf",
                                       robot_name + "/base_link", time=rospy.Time.now())
    target_follower.setTarget(target=target_tf,
                              target_type=TargetFollowerProxy.TYPE_POSE,
                              target_source=TargetFollowerProxy.SOURCE_DIRECT)

    node.tick()
