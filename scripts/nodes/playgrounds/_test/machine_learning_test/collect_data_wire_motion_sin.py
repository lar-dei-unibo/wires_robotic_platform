#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from wires_robotic_platform.proxy.target_follower_proxy import TargetFollowerProxy
from wires_robotic_platform.msg import RobotFollow
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.transformations import FrameVectorFromKDL, FrameVectorToKDL
from wires_robotic_platform.utils.transformations import ListToKDLVector, KDLVectorToList
from wires_robotic_platform.utils.transformations import TwistToKDLVector, TwistFormKDLVector
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.param.global_parameters import Parameters
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import sys
import random
from geometry_msgs.msg import Twist
import PyKDL
import copy
from math import sin, cos
import tf
import time


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("sin_tf")
node.setupParameter("hz", 100)
tactile_reset_pub = node.createPublisher("/tactile_reset", String)
node.setHz(node.getParameter("hz"))
node_rate = node.getParameter("hz")

gripper_pub = node.createPublisher(
    "/schunk_pg70/joint_setpoint", JointState)
br = tf.TransformBroadcaster()
listener = tf.TransformListener()
robot_name = Parameters.get("COMAU_NAME")
target_follower = TargetFollowerProxy(robot_name)
u = 0.0
v = 0.0
k = 0.005  # m

msg = Float32()
target_tf = None
while target_tf == None:
    try:
        target_tf = transformations.retrieveTransform(listener, robot_name + "/base_link",
                                                      robot_name + "/tool", none_error=True,
                                                      time=rospy.Time(0), print_error=True)
    except:
        pass

print("GOT IT")

z0 = target_tf.p.z()
y0 = target_tf.p.y()
x0 = target_tf.p.x()

t0 = node.getElapsedTimeInSecs()
timer = t0
tEnd = 30.0

t_sleep1 = 5
t_sleep2 = 7


def replacewire():
    time.sleep(t_sleep1)
    print("open")
    p = 20  # int(input("open gripper? p="))
    js = JointState()
    js.position = [p]
    js.velocity = [50]
    js.effort = [50]
    gripper_pub.publish(js)

    time.sleep(t_sleep2 / 2)
    print("reset tactile")
    tactile_reset_pub.publish("")
    Logger.error("**********************")
    Logger.error("*** PLACE THE WIRE ***")
    Logger.error("**********************")
    time.sleep(t_sleep2)

    print("close")
    p = 2  # int(input("close gripper? p="))
    js = JointState()
    js.position = [p]
    js.velocity = [50]
    js.effort = [50]
    gripper_pub.publish(js)
    time.sleep(t_sleep1)


replacewire()

while node.isActive():

    timer = node.getElapsedTimeInSecs() - t0

    if timer < tEnd:
        print("GOING ")
        u = k * math.sin(2 * np.pi * 0.1 * node.getCurrentTimeInSecs())
        # target_tf.p[0] = u + x0
        target_tf.p[1] = u + y0
        # target_tf.p[2] = u + z0

    else:
        print("*** THE END ***")
        target_tf.p[0] = x0
        target_tf.p[1] = y0
        target_tf.p[2] = z0
        replacewire()
        t0 = node.getElapsedTimeInSecs()

    transformations.broadcastTransform(br, target_tf, "sin_tf",
                                       robot_name + "/base_link", time=rospy.Time.now())
    target_follower.setTarget(target=target_tf,
                              target_type=TargetFollowerProxy.TYPE_POSE,
                              target_source=TargetFollowerProxy.SOURCE_DIRECT)

    node.tick()
