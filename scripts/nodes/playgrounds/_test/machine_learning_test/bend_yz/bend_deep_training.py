#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
import math
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.param.parameters import Parameters

import math
import os
import time
import warnings
import sys
import random

#⬢⬢⬢⬢⬢➤
import numpy as np
from scipy.io import loadmat, savemat
import matplotlib.pyplot as plt

from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Hide messy TensorFlow warnings
warnings.filterwarnings("ignore")  # Hide messy Numpy warnings


def groupDataWindow(raw_data, sequence_length):
    data_flow_length = len(raw_data)
    data = []
    for index in range(data_flow_length - sequence_length):
        window_data = raw_data[index: index + sequence_length, :]
        data.append(window_data)
    # data =normalise_windows(data,sequence_length)
    data = np.array(data)
    return data


def extractData(axis, x, y, seq_len):

    y = y[:, axis]

    x = groupDataWindow(x, seq_len)
    y = y[seq_len:]
    y = np.reshape(y, (y.shape[0], 1))

    return x, y


def normalise_windows(window_data_list, sequence_length):
    # reflect percentage changes from the start of that window
    # (so the data at point i=0 will always be 0)
    normalised_window_data_list = []
    for window_data in window_data_list:
        normalised_window_data = np.zeros((sequence_length, 16))
        for j in range(sequence_length):
            for i in range(16):
                w0 = window_data[0][i]
                normalised_window_data[j, i] = (float(window_data[j][i]) / float(w0)) - 1
        normalised_window_data_list.append(normalised_window_data)
    return normalised_window_data_list


def trainNetwork(name, seq_len, epochs, lstm_cells, dropout_value, dense_neurons, xTrain, yTrain):

    #
    #⬢⬢⬢⬢⬢➤ MODEL
    #

    lstm_layers = len(lstm_cells)
    dense_layers = len(dense_neurons)
    rs = lstm_layers > 1

    model = Sequential()

    model.add(LSTM(units=lstm_cells[0],
                   input_shape=(seq_len, 16),
                   return_sequences=rs))
    model.add(Dropout(dropout_value[0]))

    if rs:
        for layer_index in range(lstm_layers - 2):
            model.add(LSTM(units=lstm_cells[layer_index],
                           input_shape=(seq_len, 16),
                           return_sequences=True))
            model.add(Dropout(dropout_value[layer_index]))

        model.add(LSTM(units=lstm_cells[-1],
                       input_shape=(seq_len, 16),
                       return_sequences=False))
        model.add(Dropout(dropout_value[-1]))

    for layer_index in range(dense_layers):
        model.add(Dense(dense_neurons[layer_index], activation="tanh"))

    #
    #⬢⬢⬢⬢⬢➤ COMPILE
    #

    model.compile(loss="mse", optimizer="rmsprop")

    #
    #⬢⬢⬢⬢⬢➤ FIT
    #

    model.fit(xTrain,
              yTrain,
              # batch_size=512,
              nb_epoch=epochs,
              validation_split=0.05)

    global dirPKLName
    model.save(dirPKLName + '/wire_bend_{}_regressor.h5'.format(name))

    return model


def testNetwork(model, xTest, yTest, name):
    #
    #⬢⬢⬢⬢⬢➤ PREDICTION TEST
    #

    yPred = model.predict(xTest)
    yPred = np.reshape(yPred, (yPred.shape[0], 1))
    print " \nxTest ={}\nyTest ={}\nyPred = {}".format(xTest.shape, yTest.shape, yPred.shape)

    plt.figure()
    plt.plot(yPred, label="yPred")
    plt.plot(yTest, label="yTest")
    plt.legend()
    plt.title(name)

##########################################################################
##########################################################################
##########################################################################
##########################################################################
##########################################################################


dirDataName_train = '{}/machine_learning_data/wire_bend_yz/test_6'.format(Parameters.get("DATA_FOLDER"))
dirDataName_valid = '{}/machine_learning_data/wire_bend_yz/test_7'.format(Parameters.get("DATA_FOLDER"))
dirDataName_test = '{}/machine_learning_data/wire_bend_yz/test_8'.format(Parameters.get("DATA_FOLDER"))
dirDataName = '{}/machine_learning_data/wire_bend_yz_4/test_4'.format(Parameters.get("DATA_FOLDER"))
dirPKLName = "/home/riccardo/ros/wires_data/ml_files"  # '{}/machine_learning_data/h5_files'.format(Parameters.get("DATA_FOLDER"))

# ⬢⬢⬢⬢⬢➤ DATA
data = loadmat(dirDataName_train + '/allDataMat/elabData.mat')
xTrain = data['tactile']
yTrain = data['force']
data = loadmat(dirDataName_valid + '/allDataMat/elabData.mat')
xVal = data['tactile']
yVal = data['force']
data = loadmat(dirDataName_test + '/allDataMat/elabData.mat')
xTest = data['tactile']
yTest = data['force']

# xVal = xTrain
# yVal = yTrain

#  Tool-frame (-> 0:X, 1:Y, 2:Z ) (//Force-Sensor-Frame)
X_axis_id = 0
Y_axis_id = 1
Z_axis_id = 2

#
seq_len_Y = 5  # 5
xTrain_Y, yTrain_Y = extractData(Y_axis_id, xTrain, yTrain, seq_len_Y)
xVal_Y, yVal_Y = extractData(Y_axis_id, xVal, yVal, seq_len_Y)
xTest_Y, yTest_Y = extractData(Y_axis_id, xTest, yTest, seq_len_Y)
print "--- Y axis ---"
print "\nxTrain = {}\nyTrain ={}\nxVal ={}\nyVal ={}\nxTest ={}\nyTest ={}".format(xTrain_Y.shape, yTrain_Y.shape, xVal_Y.shape, yVal_Y.shape, xTest_Y.shape, yTest_Y.shape)
#
seq_len_Z = 5  # 5
xTrain_Z, yTrain_Z = extractData(Z_axis_id, xTrain, yTrain, seq_len_Z)
xVal_Z, yVal_Z = extractData(Z_axis_id, xVal, yVal, seq_len_Z)
xTest_Z, yTest_Z = extractData(Z_axis_id, xTest, yTest, seq_len_Z)
print "--- Z axis ---"
print "\nxTrain = {}\nyTrain ={}\nxVal ={}\nyVal ={}\nxTest ={}\nyTest ={}".format(xTrain_Z.shape, yTrain_Z.shape, xVal_Z.shape, yVal_Z.shape, xTest_Z.shape, yTest_Z.shape)


# ⬢⬢⬢⬢⬢➤ TRAINING & TEST
epochs = 2  # 1

lstm_cells = [100, 100]  # [100,100]
dropout_value = [0.2, 0.2]  # [0.2,0.2]
dense_neurons = [16, 16, 8, 4, 1]  # [16,16,8,4,1]

model_Y = trainNetwork("y", seq_len_Y, epochs, lstm_cells, dropout_value, dense_neurons, xTrain_Y, yTrain_Y)
testNetwork(model_Y, xVal_Y, yVal_Y, "Y validation")
testNetwork(model_Y, xTest_Y, yTest_Y, "Y test")

lstm_cells = [100, 100]  # [100,100]
dropout_value = [0.2, 0.2]  # [0.2,0.2]
dense_neurons = [16, 16, 8, 4, 1]  # [16,16,8,4,1]

model_Z = trainNetwork("z", seq_len_Z, epochs, lstm_cells, dropout_value, dense_neurons, xTrain_Z, yTrain_Z)
testNetwork(model_Z, xVal_Z, yVal_Z, "Z validation")
testNetwork(model_Z, xTest_Z, yTest_Z, "Z test")

plt.figure()
for i in range(16):
    plt.plot(xVal_Y[:, 1, i], label="ch_{}".format(i))
plt.legend()
plt.title("validation")

plt.show()
