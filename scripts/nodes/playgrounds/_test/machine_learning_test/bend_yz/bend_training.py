#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.param.parameters import Parameters

import cv2
import rospkg
import math
import sys
import random

#⬢⬢⬢⬢⬢➤
import matplotlib.pyplot as plt

import numpy as np
from sklearn.multioutput import MultiOutputRegressor
from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn import linear_model
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.naive_bayes import GaussianNB
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error
from scipy.io import loadmat, savemat


def trainingTestREG(reg_name, reg_dict, xTrain, yTrain, xTest, yTest, score_th=0.7, data_export_dict={}, multi_reg=False):
    score = 0.0
    best_mse = 1.0
    for k in reg_dict.keys():
        reg = reg_dict[k]
        if multi_reg:
            reg = MultiOutputRegressor(reg)
            reg_dict[k] = reg

        score = 0
        ct = 20
        while score < score_th:
            ct = ct - 1
            print('\033[91m'
                  + "{}/{}__________ {}, score={}".format(ct, score_th, k, score)
                  + '\033[0m')
            t0 = time.time()

            reg.fit(xTrain, yTrain)

            train_time = time.time() - t0
            score = reg.score(xTest, yTest)
            if ct <= 0:
                ct = 20
                score_th = 0.9 * score_th

        #⬢⬢⬢⬢⬢➤ TESTING

        print("\n{}\nscore={}".format(k, score))

        t0 = time.time()
        yPred = reg.predict(xTest)
        time_pred = time.time() - t0

        mse = mean_squared_error(yTest, yPred, multioutput='uniform_average')
        data_export_dict["tTrain_{}_{}".format(reg_name, k)] = train_time
        data_export_dict["tPred_{}_{}".format(reg_name, k)] = time_pred
        data_export_dict["yPred_{}_{}".format(reg_name, k)] = yPred
        data_export_dict["mse_{}_{}".format(reg_name, k)] = mse
        print("****************************** MES={}".format(mse))

        if mse < best_mse:
            best_mse = mse
            k_best = k

    print("\n\n##################################################################")
    print("########################### BEST REG  ############################")
    print("##################################################################")

    data_export_dict["keys_{}".format(reg_name)] = reg_dict.keys()
    data_export_dict["best_{}".format(reg_name)] = k_best

    joblib.dump(reg_dict[k_best],
                dirPKLName + '/move_regressor_{}.pkl'.format(reg_name))

    yPred = data_export_dict["yPred_{}_{}".format(reg_name, k_best)]
    plt.figure()
    plt.plot(yPred[:, 0], label="yPred X")
    plt.plot(yTest[:, 0], label="yTest X")
    plt.legend()
    axes = plt.gca()
    axes.set_ylim([-1.0, 1.0])
    plt.title("mse = {}, \n reg = {}".format(best_mse, k_best))

    plt.figure()
    plt.plot(yPred[:, 1], label="yPred Y")
    plt.plot(yTest[:, 1], label="yTest Y")
    plt.legend()
    axes = plt.gca()
    axes.set_ylim([-1.0, 1.0])
    plt.title("mse = {}, \n reg = {}".format(best_mse, k_best))

    plt.figure()
    plt.plot(yPred[:, 2], label="yPred Z")
    plt.plot(yTest[:, 2], label="yTest Z")
    plt.legend()
    axes = plt.gca()
    axes.set_ylim([-1.0, 1.0])
    plt.title("mse = {}, \n reg = {}".format(best_mse, k_best))

    print("{}: AUC={}, t_pred={}, t_train={}\n\n\n".format(
        k_best,
        best_mse,
        data_export_dict["tPred_{}_{}".format(reg_name, k_best)],
        data_export_dict["tTrain_{}_{}".format(reg_name, k_best)]))
    print("##################################################################")

    return data_export_dict


##########################################################################
##########################################################################
##########################################################################
##########################################################################
##########################################################################


dirDataName = '{}/machine_learning_data/wire_bend_yz/test_2'.format(Parameters.get("DATA_FOLDER"))
dirPKLName = '{}/machine_learning_data/pkl_files/wire_bend_yz'.format(Parameters.get("DATA_FOLDER"))

# ⬢⬢⬢⬢⬢➤ DATA
data = loadmat(dirDataName + '/allDataMat/elabData.mat')
xTrain = data['xTrain']
yTrain = data['yTrain']
xTest = data['xTest']
yTest = data['yTest']

print xTrain.shape
print yTrain.shape

plt.figure()
plt.plot(xTrain)

data_all_dict = {}
data_all_dict["yTest"] = yTest

vb_flg = True

#⬢⬢⬢⬢⬢➤ Neural Network
reg_dict = {}
reg_dict["32l16l8_0a01"] = MLPRegressor(activation="relu",
                                        learning_rate="adaptive",
                                        hidden_layer_sizes=(32, 16, 8),
                                        alpha=0.01,
                                        tol=1e-5,
                                        verbose=vb_flg)
reg_dict["32l32l32_0a1"] = MLPRegressor(activation="relu",
                                        learning_rate="adaptive",
                                        hidden_layer_sizes=(32, 32, 32),
                                        alpha=0.1,
                                        tol=1e-5,
                                        verbose=vb_flg)
reg_dict["16l16l16_0a01"] = MLPRegressor(activation="relu",
                                         learning_rate="adaptive",
                                         hidden_layer_sizes=(16, 16, 16),
                                         alpha=0.01,
                                         tol=1e-5,
                                         verbose=vb_flg)
reg_dict["8l8l8_0a001"] = MLPRegressor(activation="relu",
                                       learning_rate="adaptive",
                                       hidden_layer_sizes=(8, 8, 8),
                                       alpha=0.001,
                                       tol=1e-5,
                                       verbose=vb_flg)


# data_all_dict = trainingTestREG(
#     "MLP", reg_dict, xTrain, yTrain, xTest, yTest, score_th=0.5, data_export_dict=data_all_dict, multi_reg=True)

# #⬢⬢⬢⬢⬢➤ Random Forest
reg_dict = {}
if vb_flg:
    vb_flg = 2
# reg_dict["t100_f4"] = RandomForestRegressor(n_estimators=100,
#                                             max_features=4,
#                                             n_jobs=8,
#                                             verbose=vb_flg)
# reg_dict["t100_f8"] = RandomForestRegressor(n_estimators=100,
#                                             max_features=8,
#                                             n_jobs=8,
#                                             verbose=vb_flg)
# reg_dict["t200_f4"] = RandomForestRegressor(n_estimators=200,
#                                             max_features=4,
#                                             n_jobs=8,
#                                             verbose=vb_flg)
# reg_dict["t200_f8"] = RandomForestRegressor(n_estimators=200,
#                                             max_features=8,
#                                             n_jobs=8,
#                                             verbose=vb_flg)  # verbose=2

reg_dict["t500_f4"] = RandomForestRegressor(n_estimators=500,
                                            max_features=4,
                                            n_jobs=8,
                                            verbose=vb_flg)

# reg_dict["t500_f8"] = RandomForestRegressor(n_estimators=500,
#                                             max_features=8,
#                                             n_jobs=8,
#                                             verbose=vb_flg)

data_all_dict = trainingTestREG(
    "RF", reg_dict, xTrain, yTrain, xTest, yTest, score_th=0.1, data_export_dict=data_all_dict, multi_reg=True)

# savemat( dirDataName + '/allDataMat/outputData.mat', data_all_dict)

# # #⬢⬢⬢⬢⬢➤ Support Vector Machines
# reg_dict = {}
# # reg_dict["k_rbf_1c"] = SVR(C=1.0, kernel='rbf', verbose=True)
# # reg_dict["k_rbf_10c"] = SVR(C=10.0, kernel='rbf', verbose=True)
# # reg_dict["k_rbf_100c"] = SVR(C=100.0, kernel='rbf', verbose=True)
# reg_dict["k_rbf_1000c"] = SVR(C=1000.0, kernel='rbf', verbose=True)
# # reg_dict["k_rbf_5000c"] = SVR(C=5000.0, kernel='rbf', verbose=True)
# # reg_dict["k_linear"] = SVR(kernel='linear')
# # reg_dict["k_poly"] = SVR(kernel='poly')
# # reg_dict["k_sigmoid"] = SVR(kernel='sigmoid')

# data_all_dict = trainingTestREG(
#     "SVR", reg_dict, xTrain, yTrain, xTest, yTest, score_th=0.5, data_export_dict=data_all_dict, multi_reg=False)

# savemat(  dirDataName + '/allDataMat/outputData.mat', data_all_dict)

##########################################################################
##########################################################################
##########################################################################
##########################################################################
##########################################################################

plt.show()
