
seq_len_X = 7 
seq_len_Z = 10 
epochs = 1

### X axis
lstm_cells =  [120,70]
dropout_value =  [0.1,0.1]
dense_neurons = [2,1] 
  
### Z axis
lstm_cells =  [50,50]
dropout_value =  [0.1,0.1]
dense_neurons = [2,2,1] 

###################################################################################################################

lstm_cells =  [50,50]
dropout_value =  [0.1,0.1]
dense_neurons = [2,1] 

lstm_cells =  [70,50]
dropout_value =  [0.1,0.1]
dense_neurons = [2,1,1]  
 