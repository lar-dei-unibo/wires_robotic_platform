#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf 
 
import rospy 
import rospkg
import math 
import random
import sys
import time
import numpy as np 

#⬢⬢⬢⬢⬢➤
import matplotlib.pyplot as plt
from scipy.io import loadmat, savemat


##########################################################################
##########################################################################
##########################################################################


from wires_robotic_platform.param.parameters import Parameters
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.utils.logger import Logger 
from std_msgs.msg import Header, Float64, Float32,  Float64MultiArray
from geometry_msgs.msg import Twist
 
from keras.models import load_model
 
class TactileRNN(object):

    def __init__(self, model_h5_file_path, seq_len):
        self.data = RNNDataSequence(seq_len)
        try:        
            self.model = load_model(model_h5_file_path)
            print("Regressors found and loaded!") 
        except Exception as e:
            Logger.error("None regressor found! " ) 
            print e
        
    def update(self,msg):
        self.data.tactile_update(msg)

    def getPrediction(self):
        return self.model.predict(self.data.getWindow())

class RNNDataSequence(object):

    def __init__(self, seq_len):
        self.window = np.zeros((seq_len,16))
        self.new_sample = None

    def tactile_update(self, msg): 
        # print "{}".format(msg.data)
        self.new_sample = np.ravel(msg.data).reshape((1,16))
        self._window_update(self.new_sample)

    def _window_update(self,new_sample): 
        tmp_window = np.append(self.window, new_sample, axis=0)
        self.window = np.delete(tmp_window, 0, 0)   

    def getWindow(self):
        return np.reshape(self.window, (1,self.window.shape[0],self.window.shape[1])) 

##########################################################################
##########################################################################
########################################################################## 
 
def tactile_callback(msg):
    global y_rnn, z_rnn
    y_rnn.update(msg)
    z_rnn.update(msg)


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("wire_bend_pred")

node.setupParameter("hz", 5)
node.setHz(node.getParameter("hz")) 

pred_pub = node.createPublisher('/wire_bend_pred', Twist)

seq_len_y=5
seq_len_z=5 

dirPKLName = "/home/riccardo/ros/wires_data/ml_files/wire_bend"   # '{}/machine_learning_data/h5_files'.format(Parameters.WIRES_DATA_FOLDER) 
y_rnn = TactileRNN(model_h5_file_path = dirPKLName + '/wire_bend_y_regressor.h5',
                seq_len= seq_len_y)
z_rnn = TactileRNN(model_h5_file_path = dirPKLName + '/wire_bend_z_regressor.h5',
                seq_len= seq_len_z)

node.createSubscriber(
    "/tactile", Float64MultiArray, tactile_callback) 

##########################################################################
##########################################################################
##########################################################################
 
raw_input("\n\n\n\ndid you reset the tactile sensor?")
time.sleep(2)

N = 10
yset = np.zeros(N)
zset = np.zeros(N)
for i in range(N):
    yset[i] = y_rnn.getPrediction()
    zset[i] = z_rnn.getPrediction()

ybias = sum(yset)/N
zbias = sum(zset)/N

while node.isActive():  
    yPred_y = y_rnn.getPrediction() -ybias
    yPred_z = z_rnn.getPrediction() -zbias
    print("pred_y={}, pred_z={}".format(yPred_y, yPred_z))

    pred = Twist()
    pred.linear.y = yPred_y
    pred.linear.z = yPred_z
    pred_pub.publish(pred) 

    node.tick()
