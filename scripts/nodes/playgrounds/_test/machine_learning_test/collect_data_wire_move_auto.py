#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32, Float64MultiArray
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

import message_filters
from sensor_msgs.msg import Image, CameraInfo
from sensor_msgs.msg import Joy
import cv2
import aruco
import rospkg
import numpy as np
import math
import sys
import random
from filterpy.kalman import KalmanFilter
from filterpy.common import Q_discrete_white_noise
from wires_robotic_platform.utils.devices import Joystick

import time

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("ml_gripper")

node.setupParameter("hz", 250)
node.setHz(node.getParameter("hz"))
node_rate = node.getParameter("hz")


def joy_callback(msg):
    global joystick
    joystick.update(msg)


def joystick_key_callback(down, up):
    global joystick
    global tactile_reset_pub
    if joystick.KEY_BACK in down:
        gripper(2, t=1)
    if joystick.KEY_START in down:
        gripper(20, t=1)
    if joystick.KEY_LOG in down:
        tactile_reset_pub.publish("")


def gripper(pos, t=1):
    gr_msg = JointState()
    gr_msg.position = [pos]
    gr_msg.velocity = [70]
    gr_msg.effort = [50]
    gripper_pub.publish(gr_msg)
    time.sleep(t)


gripper_pub = node.createPublisher("/schunk_pg70/joint_setpoint",
                                   JointState)
tactile_reset_pub = node.createPublisher("/tactile_reset",
                                         String)

joy_sub = rospy.Subscriber('/joy', Joy,
                           joy_callback, queue_size=1)

joystick = Joystick(translation_mag=0.0005,
                    rotation_mag=0.0008)
joystick.registerCallback(joystick_key_callback)
 
while node.isActive(): 
 
    node.tick()
