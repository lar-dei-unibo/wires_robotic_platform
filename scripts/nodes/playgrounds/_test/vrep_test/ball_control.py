#!/usr/bin/env python

# this node sends, through a ROS topic, the commands to a prismatic joint (in V-REP) that moves a ball up and down with a sinusoidal behavior

import rospy
import numpy
from numpy import *

from std_msgs.msg import Float32

def ballController():
    rospy.init_node('ball_controller', anonymous=True)
    jointPublisher = rospy.Publisher('ball_command', Float32, queue_size=10)
    rate = rospy.Rate(10) # 10hz

   
    jointCommand = Float32()
    x = 1

    while not rospy.is_shutdown():
        
        x += 1
        jointCommand.data = abs(0.5*sin(0.1*x))
        jointPublisher.publish(jointCommand)   

        rospy.loginfo(["ball position",str(jointCommand.data)])

        rate.sleep()

if __name__ == '__main__':
    try:
        ballController()
    except rospy.ROSInterruptException:
        pass
