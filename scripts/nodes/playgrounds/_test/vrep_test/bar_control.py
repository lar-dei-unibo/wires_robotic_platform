#!/usr/bin/env python

# This node recives the position of a revolute joint (in V-REP), through a ROS topic "bar_status". 
# Such a joint is indirectly moved by a rigid bar hit by a ball jumping up and down.
# A controller (PID) is implemented in order keep the bar horizontal. 
# The actuation on the bar is done by a prismatic joint (in V-REP) which moves the base of the bar
# The commands to the prismatic joint are send through a ROS topic "bar_command"

import rospy
import numpy
from numpy import *

from std_msgs.msg import Float32

class Bar():
    def __init__(self):
        rospy.init_node('bar_controller', anonymous=True)    
        self.barStatus = 0
        self.rate = rospy.Rate(10) # 10hz
        self.P_gain = 0.6
        self.I_gain = 0.5
        self.integralErr = 0
        self.barTarget = 0
        self.time0 = 0

    def _callbackBarStatus(self, msg):
        self.barStatus = msg.data

    def barController(self):        # PI controller
        Kp = self.P_gain     
        err = self.barStatus - self.barTarget
        time = rospy.get_time()
        dTime = time - self.time0
        self.integralErr = self.integralErr + err*dTime
        return Kp*err

    def startControl(self):

        rospy.Subscriber("bar_status", Float32, self._callbackBarStatus)
        jointPublisher = rospy.Publisher('bar_command', Float32, queue_size=10)
        
        jointCommand = Float32()
        self.time0 = rospy.get_time()

        while not rospy.is_shutdown():
            jointCommand.data = self.barController()
            jointPublisher.publish(jointCommand)   
        
            rospy.loginfo(["bar position", str(self.barStatus)])

            self.rate.sleep()

if __name__ == '__main__':
    bar = Bar()
    try:
        bar.startControl()
    except rospy.ROSInterruptException:
        pass
