#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This example demonstrates a very basic use of flowcharts: filter data,
displaying both the input and output of the filter. The behavior of
he filter can be reprogrammed by the user.
Basic steps are:
  - create a flowchart and two plots
  - input noisy data to the flowchart
  - flowchart connects data to the first plot, where it is displayed
  - add a gaussian filter to lowpass the data, then display it in the second plot.
"""


import numpy as np
import rospy
from wires_robotic_platform.msg import ComauFollow
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.devices import ForceSensor, Joystick
from std_msgs.msg import String, Float64
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
import PyKDL
import copy
import math
import tf


def getTTrans(translation_mag):
    frame = PyKDL.Frame(PyKDL.Vector(
        0,
        0,
        -translation_mag))
    return frame


def getTRot():
    frame = PyKDL.Frame()
    return frame


def linearMovement(frame_to_transform, translation_mag):
    new_frame = frame_to_transform
    # Trans
    new_frame = new_frame * getTTrans(translation_mag)
    # Rot
    new_frame = new_frame * getTRot()
    return new_frame


##################################################################
##################################################################
##################################################################
##################################################################


def joyCallback(msg):
    joystick.update(msg)


translation_mag = 0.0001


def forceSensorCallback(msg):
    global translation_mag
    forceSensor.update(msg)
    if msg.linear.z <= -2.2:
        translation_mag = translation_mag * 0.9999


rospy.init_node('comau_bump_node', anonymous=True)

follow_pub = rospy.Publisher('/comau_smart_six/target_to_follow',
                             ComauFollow, queue_size=1)
bump_k_pub = rospy.Publisher('/bump_k/', Float64, queue_size=1)
joy_sub = rospy.Subscriber('/joy', Joy, joyCallback, queue_size=1)
force_sensor_sub = rospy.Subscriber(
    '/atift', Twist, forceSensorCallback, queue_size=1)
rate = rospy.Rate(30)  # 10hz


br = tf.TransformBroadcaster()
listener = tf.TransformListener()


# Joystick
joystick = Joystick(translation_mag=0.005, rotation_mag=0.003)

# Force sensor
forceSensorFrame = PyKDL.Frame()
forceSensorFrame.M.DoRotX(np.pi)

forceSensor = ForceSensor(relative_frame=forceSensorFrame,
                          translation_mag=0.00005,
                          rotation_mag=0.003)
forceSensor.setTransThreshold([1, 1, 1])


def joystickKeyCallback(down, up):
    global force_target_tf
    if Joystick.KEY_START in down:
        # Logger.log("Start pressed!")
        joystick.setForcedTarget(joystick.nearestOrthogonalFrame(current_tf))


joystick.registerCallback(joystickKeyCallback)


# Tfs
current_tf = None
target_tf = None
force_target_tf = None
first_tf = True
target_name = "follow_target"

start_time = rospy.get_time()
while not rospy.is_shutdown():

    current_tf = transformations.retrieveTransform(
        listener, "comau_smart_six/base_link", "comau_smart_six/link6", none_error=True)

    if current_tf != None:
        if first_tf:
            first_tf = False
            target_tf = current_tf

        target_tf = linearMovement(target_tf, translation_mag)

        target_tf = joystick.transformT(target_tf)

        target_tf = forceSensor.transformT(target_tf)

        transformations.broadcastTransform(
            br, target_tf, target_name, "comau_smart_six/base_link", time=rospy.Time.now())

        follow_msg = ComauFollow()
        follow_msg.action = ComauFollow.ACTION_SETTARGET
        follow_msg.target_pose = transformations.KDLToPose(target_tf)

        follow_pub.publish(follow_msg)

        # Bump K
        k = Float64()
        k.data = 5.0 * (translation_mag / 0.0001)
        bump_k_pub.publish(k)
    rate.sleep()
