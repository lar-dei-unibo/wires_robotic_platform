#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode
import numpy as np
import math
import rospy
from geometry_msgs.msg import Twist
import wires_robotic_platform.utils.visualization as visualization
# from visualization import createArrow
from visualization_msgs.msg import MarkerArray
import sys
import tf
import random


def error_cb(msg):
    global debug_pub
    force = np.zeros(3)
    force[0] = msg.linear.x
    force[1] = 0  # msg.linear.y
    force[2] = 0  # msg.linear.z

    arr = visualization.createArrow("tf_storage_hole", ap1=np.zeros(3), ap2=0.5 * force, shaft=0.01)
    marr = MarkerArray()
    marr.markers.append(arr)
    debug_error_pub.publish(marr)


def force_cb(msg):
    global debug_pub
    force = np.zeros(3)
    force[0] = msg.linear.x
    force[1] = msg.linear.y
    force[2] = msg.linear.z

    # arr = visualization.createArrow("follow_target", ap1=np.zeros(3), ap2=0.5 * force, shaft=0.01, color=visualization.Color(1, 1, 0, 1))
    arr = visualization.createArrow("/comau_smart_six/tool", ap1=np.zeros(3), ap2=0.5 * force, shaft=0.01, color=visualization.Color(1, 1, 0, 1))
    marr = MarkerArray()
    marr.markers.append(arr)
    debug_force_pub.publish(marr)


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("porcodio")

node.setupParameter("hz", 20)
node.setHz(node.getParameter("hz"))

listener = tf.TransformListener()

node.createSubscriber(topic_name="/insertion_control/regulation_error", msg_type=Twist, callback=error_cb)
node.createSubscriber(topic_name="/insertion_control/estimated_twist", msg_type=Twist, callback=force_cb)
debug_force_pub = node.createPublisher(topic_name="/insertion_control/debug_force", msg_type=MarkerArray)
debug_error_pub = node.createPublisher(topic_name="/insertion_control/debug_error", msg_type=MarkerArray)

# current_tf = None

# while current_tf is None:
#     current_tf = transformations.retrieveTransform(listener, "/comau_smart_six/base_link",
#                                                    "/comau_smart_six/tool", none_error=True,
#                                                    time=rospy.Time(0), print_error=True)

while node.isActive():
    # try:
    #     current_tf = transformations.retrieveTransform(listener, "/comau_smart_six/base_link",
    #                                                    "/comau_smart_six/tool", none_error=True,
    #                                                    time=rospy.Time(0), print_error=True)
    # except Exception as e:
    #     pass

    node.tick()
