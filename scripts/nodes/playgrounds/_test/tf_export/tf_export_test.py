#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
from wires_robotic_platform.utils.ros import RosNode
import wires_robotic_platform.utils.transformations as transformations
import os
import cv2
from cv_bridge import CvBridge, CvBridgeError
import cv_bridge
import numpy as np
import PyKDL

# DA SPOSTARE
import message_filters
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState, Image, CompressedImage
from geometry_msgs.msg import Twist


def topicsCallback(*args):
    print(args)


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("tf_export_test")

node.setupParameter("hz", 300)
node.setHz(node.getParameter("hz"))
node.setupParameter("output_path", "/home/daniele/tmp/tmp_bag")
node.setupParameter("time_window", 139.0)


tf_base = 'comau_smart_six/base_link'
tf_list = ['comau_smart_six/link6']

topic_map = {
    '/comau_smart_six/joint_states': eval("JointState"),
    '/atift': eval("Twist"),
    #'/camera/rgb/image_raw/compressed': eval("CompressedImage"),
    '/tactile': eval("Float64MultiArray")
    #'/wire_params': eval("Float64MultiArray")
}

container_map = {}
callback_map = {}
callback_managers = {}
data_map = {}
datatf_map = {}
times = []


def pushNewData():
    for topic, data in container_map.iteritems():
        print(topic, "DARTA")
        data_map[topic].append(data['value'])
        print(topic, len(data_map[topic]))
    for tf in tf_list:
        datatf_map[tf].append(tf_frames[tf])
    times.append(node.getCurrentTimeInSecs())


class DataManager():
    TIMINGS_NAME = "timings"

    def __init__(self, output_path):
        self.data_matrix = {}
        self.data_types = {}
        self.times = []
        self.output_path = output_path

    @staticmethod
    def converMsgInData(msg, data_type):
        if data_type == PyKDL.Frame:
            return transformations.KDLtoNumpyVector(msg).reshape(7)
        if data_type == Twist:
            return np.array([
                msg.linear.x,
                msg.linear.y,
                msg.linear.z,
                msg.angular.x,
                msg.angular.y,
                msg.angular.z
            ]).T
        if data_type == JointState:
            arr = np.array([])
            if len(msg.position) > 0:
                arr = np.asarray(msg.position)
            if len(msg.velocity) > 0:
                arr = np.hstack((arr, np.asarray(msg.velocity)))
            return arr.T
        if data_type == Float64MultiArray:
            return np.array([msg.data]).T

        if data_type == CompressedImage:
            np_arr = np.fromstring(msg.data, np.uint8)
            return cv2.imdecode(np_arr, cv2.CV_LOAD_IMAGE_COLOR)

    def pushData(self, topic_name, time, msg, data_type):
        if topic_name not in self.data_matrix:
            self.data_matrix[topic_name] = []

        self.data_types[topic_name] = data_type
        raw_data = DataManager.converMsgInData(msg, data_type)
        self.data_matrix[topic_name].append(raw_data)

    def pushTime(self, time):
        self.times.append(time)

    def setTimes(self, times):
        self.data_matrix[DataManager.TIMINGS_NAME] = (
            np.array(times) * 1000.0).astype(int)
        print("Times", self.data_matrix[DataManager.TIMINGS_NAME])
        self.data_types[DataManager.TIMINGS_NAME] = Float64MultiArray

    @staticmethod
    def purgeTopicName(topic_name):
        if topic_name.startswith('/'):
            topic_name = topic_name[1:]
        return topic_name.replace('/', '_')

    def saveData(self):
        for topic, data in self.data_matrix.iteritems():
            purged_name = DataManager.purgeTopicName(topic)
            filename = os.path.join(self.output_path, purged_name)

            if self.data_types[topic] == CompressedImage or self.data_types[topic] == Image:
                pass
            else:
                np.savetxt(filename + ".txt",
                           np.array(self.data_matrix[topic]))


class CallbackManager():
    def __init__(self, topic):
        self.topic = str(topic)

    def callback(self, msg):
        container_map[self.topic]['value'] = msg

    @staticmethod
    def createACallback(name):

        callback_managers[name] = CallbackManager(name)
        return callback_managers[name].callback


for t, msg_type in topic_map.iteritems():
    topic = str(t)
    container_map[topic] = {'value': None}
    data_map[topic] = []

    callback_map[topic] = CallbackManager.createACallback(topic)
    node.createSubscriber(topic, msg_type, callback_map[topic])

for tf in tf_list:
    datatf_map[tf] = []


while node.isActive():
    if node.getElapsedTimeInSecs() >= node.getParameter("time_window"):
        break
    try:
        found = True
        tf_frames = {}
        for tf in tf_list:
            tf_frame = node.retrieveTransform(
                tf, tf_base, node.getCurrentTime())
            if tf_frame != None:
                tf_frames[tf] = tf_frame

            else:
                found = False
                break

        if found:
            pushNewData()
            print("Sync time ready!")

        node.tick()
    except KeyboardInterrupt:
        print("Interrupt")


CV_BRIDGE = CvBridge()
output_path = node.getParameter("output_path")
data_manager = DataManager(output_path)

print("Saving")
print("Times", len(times))
for topic, data in container_map.iteritems():
    for i in range(0, len(data_map[topic])):
        time = times[i]
        data_manager.pushData(
            topic, time, data_map[topic][i], topic_map[topic])

for tf, data in datatf_map.iteritems():
    for i in range(0, len(datatf_map[tf])):
        time = times[i]
        data_manager.pushData(
            'tf#' + tf,
            time,
            datatf_map[tf][i],
            PyKDL.Frame
        )

data_manager.setTimes(times)
data_manager.saveData()
