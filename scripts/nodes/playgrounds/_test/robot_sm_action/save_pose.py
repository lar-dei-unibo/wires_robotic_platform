#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

import rospy

from std_msgs.msg import *


import random
import numpy
from numpy import *
import numpy as np
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.storage.mongo import MessageStorage

from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState
import PyKDL
from PyKDL import Frame, Vector, Rotation
import threading

from std_msgs.msg import String


import tf
import cv2
import time


from rospkg import RosPack


class SavePose():

    def __init__(self):
        self.robot_name = "comau_smart_six"
        self.joints_state_sub = rospy.Subscriber("/comau_smart_six/joint_states",
                                                 JointState, self._jointStateCallback)
        self.message_database = MessageStorage()
        self.joint_states_msg = JointState()

    def _jointStateCallback(self, msg):
        self.joint_states_msg = msg

    def save(self, input_name):
        self.message_database.replace(input_name, self.joint_states_msg)


if __name__ == '__main__':

    rospy.init_node('pose_saver')
    node_rate = 10
    rate = rospy.Rate(node_rate)

    sv = SavePose()
    try:
        while not rospy.is_shutdown():
            input_name = raw_input("Save Current Position as...  ")
            if input_name != "":
                sv.save(input_name)
            rate.sleep()
    except rospy.ROSInterruptException:
        pass
