#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

import pkgutil
import rospy

from std_msgs.msg import *

import random
import numpy
from numpy import *
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from wires_robotic_platform.utils.logger import Logger

import PyKDL
from PyKDL import Frame, Vector, Rotation

from std_msgs.msg import String

from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

from wires_robotic_platform.msg import *
from wires_robotic_platform.msg import RobotMotionResult, RobotMotionFeedback
from wires_robotic_platform.robotmotion.robot_motion_action import RobotMotionClient

from wires_robotic_platform.param.global_parameters import Parameters

import tf


class TrajectoryCommander(object):

    def __init__(self, robot_name):
        self.robot_name = robot_name
        self.robot_action_client = RobotMotionClient(self.robot_name,
                                                     ext_callback=self.ac_callback)
        self.message_proxy = SimpleMessageProxy(
            "{}_inner_message".format(self.robot_name))
        self.message_proxy.register(self.command_callback)
        self.success_pub = rospy.Publisher('/{}/trajectory_commander_success'.format(robot_name),
                                           Bool, queue_size=1)

    def command_callback(self, msg):
        if msg.isValid():
            if msg.getReceiver() == "trajectory_commander":
                cmd_txt = msg.getCommand()
                Logger.log("Executing Command: " + cmd_txt)
                self.robot_action_client.generateNewGoal(cmd_txt)

    def ac_callback(self, cb_type, msg):
        if cb_type == self.robot_action_client.FEEDBACK:
            self.current_state = msg.current_state
            self.executing_command = msg.executing_command
            self.text = msg.text
            self.progress = msg.progress
            self.status = msg.status

        if cb_type == self.robot_action_client.RESULT:
            self.success = msg.success
            self.pose = msg.pose
            self.q = msg.q
            self.error = msg.error

            self.next()
            self.success_pub.publish(self.success)
            Logger.log("Done")

    def reset(self):
        self.robot_action_client.generateNewGoal("shape_test2")
        self.next()

    def next(self):
        # command = raw_input("command: ")
        # self.robot_action_client.generateNewGoal(str(command))
        pass


if __name__ == '__main__':

    rospy.init_node('robot_trajectory_commander')
    cmd_comau = TrajectoryCommander(Parameters.get("COMAU_NAME"))
    cmd_comau.next()
    rospy.spin()
