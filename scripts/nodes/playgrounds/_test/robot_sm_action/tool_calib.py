#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import rospy
from wires_robotic_platform.msg import ComauFollow
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.devices import ForceSensor, Joystick
from wires_robotic_platform.robotmotion.robot_motion_action import RobotMotionClient
from wires_robotic_platform.msg import *
from wires_robotic_platform.msg import RobotMotionResult, RobotMotionFeedback
from wires_robotic_platform.param.global_parameters import Parameters
from std_msgs.msg import String, Float64
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
import threading
import PyKDL
import copy
import math
import tf


FILENAME = "/home/riccardo/Ros/wires_ws/src/wires_robotic_platform/data/tool_calib.txt"


def rotateTf(tf, theta, gamma, phi):
    new_tf = PyKDL.Frame()
    x = tf.p.x()
    y = tf.p.y()
    z = tf.p.z()
    new_tf.p = PyKDL.Vector(x, y, z)
    new_tf.M = PyKDL.Rotation.EulerZYX(theta, gamma, phi)
    return new_tf

# ############################################################################
# ############################################################################
# ############################################################################


class Commander(object):

    def __init__(self):
        self.comau_action_client = RobotMotionClient(Parameters.get("COMAU_NAME"),
                                                     ext_callback=self.ac_callback)
        self.current_state = None
        self.executing_command = None
        self.text = None
        self.progress = None
        self.status = None
        self.success = None
        self.pose = None
        self.q = None
        self.error = None
        self.executed_command = None

    def ac_callback(self, cb_type, msg):
        if cb_type == self.comau_action_client.FEEDBACK:
            self.current_state = msg.current_state
            self.executing_command = msg.executing_command
            self.text = msg.text
            self.progress = msg.progress
            self.status = msg.status

        if cb_type == self.comau_action_client.RESULT:
            self.success = msg.success
            self.pose = msg.pose
            self.q = msg.q
            self.error = msg.error
            self.executed_command = msg.executed_command

        self.next(cb_type)

    def next(self, cb_type):
        # if cb_type == self.comau_action_client.RESULT:
        #     if self.executed_command == ""
        # self.comau_action_client.generateNewGoal("shape_test2")
        pass

    def waitEndMovement(self):
        while not self.success:
            pass

# ############################################################################
# ############################################################################
# ############################################################################


class Calibration(object):

    def __init__(self):

        # Tfs
        self.current_tf = None
        self.target_tf = None
        self.force_target_tf = None
        self.first_tf = True
        self.target_name = "tool_calib"

        self.joystick = Joystick(translation_mag=0.005, rotation_mag=0.003)
        self.joystick.registerCallback(self.joystickKeyCallback)

        self.br = tf.TransformBroadcaster()
        self.listener = tf.TransformListener()
        self.calib_tf_list = []
        self.cmd = Commander()

    # ############################################################################

    def toolcalib(self):
        open(FILENAME, 'w').close()
        initial_tf = self.current_tf
        self.calib_tf_list = [rotateTf(initial_tf, 0.1, 0, 0),
                              rotateTf(initial_tf, 0, 0.1, 0),
                              rotateTf(initial_tf, 0, 0, 0.1),
                              rotateTf(initial_tf, -0.1, 0, 0)]

        for i in range(1, len(self.calib_tf_list)):
            raw_input("press ENTER to continue")
            self.cmd.comau_action_client.generateNewGoal(
                "gototf calib_" + str(i))
            self.cmd.waitEndMovement()
            if self.cmd.current_state != "live":
                self.cmd.comau_action_client.setMode(
                    RobotMotionGoal().DIRECT_MODE)

            print("------> Move to POSE n.{} ".format(i))
            raw_input("press ENTER to save...")
            self.saveCurrentTf("pose_{}".format(i))
            if self.cmd.current_state == "live":
                self.cmd.comau_action_client.setMode(
                    RobotMotionGoal().TRAJACTORY_MODE)

        self.endProcedure()

    def saveCurrentTf(self, name):
        fff = open(FILENAME, "a")
        pos = self.current_tf.p
        rot = self.current_tf.M.GetQuaternion()
        fff.write("{} {} {} {} {} {} {} {}\n".format(name,
                                                     pos.x(),
                                                     pos.y(),
                                                     pos.z(),
                                                     rot[0],
                                                     rot[1],
                                                     rot[2],
                                                     rot[3]))
        print("....Saved!!!")

    def endProcedure(self):
        print("Completed!")
        # calibthread.join()

    # ############################################################################

    def joyCallback(self, msg):
        self.joystick.update(msg)

    def joystickKeyCallback(self, down, up):
        if Joystick.KEY_START in down:
            # Logger.log("Start pressed!")
            self.joystick.setForcedTarget(
                self.joystick.nearestOrthogonalFrame(self.current_tf))

    # ############################################################################

    def start(self):

        rate = rospy.Rate(30)  # 10hz

        follow_pub = rospy.Publisher('/comau_smart_six/target_to_follow',
                                     ComauFollow, queue_size=1)
        joy_sub = rospy.Subscriber('/joy', Joy, self.joyCallback, queue_size=1)

        self.current_tf = transformations.retrieveTransform(
            self.listener, "comau_smart_six/base_link", "comau_smart_six/link6", none_error=True)

        calibthread = threading.Thread(target=self.toolcalib)
        calibthread.start()

        start_time = rospy.get_time()

        self.cmd.comau_action_client.generateNewGoal("shape_test2")

        while not rospy.is_shutdown():

            t = rospy.get_rostime()
            for k in range(0, len(self.calib_tf_list)):
                transformations.broadcastTransform(
                    self.br, self.calib_tf_list[k], "calib_" + str(k + 1), "comau_smart_six/base_link", t)

            self.current_tf = transformations.retrieveTransform(
                self.listener, "comau_smart_six/base_link", "comau_smart_six/link6", none_error=True)

            if self.current_tf != None:
                if self.first_tf:
                    self.first_tf = False
                    self.target_tf = self.current_tf

                self.target_tf = self.joystick.transformT(self.target_tf)

                transformations.broadcastTransform(
                    self.br, self.target_tf, self.target_name, "comau_smart_six/base_link", time=rospy.Time.now())

                follow_msg = ComauFollow()
                follow_msg.action = ComauFollow.ACTION_SETTARGET
                follow_msg.target_pose = transformations.KDLToPose(
                    self.target_tf)

                follow_pub.publish(follow_msg)

            rate.sleep()


# ############################################################################
# ############################################################################
# ############################################################################

if __name__ == '__main__':
    rospy.init_node('comau_bump_node', anonymous=True)

    cbt = Calibration()
    cbt.start()
    rospy.spin()
