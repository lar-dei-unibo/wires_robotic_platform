#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

import rospy
from std_msgs.msg import String
import numpy
from numpy import *
from wires_robotic_platform.robotmotion.robot_motion_action import RobotMotionServer
from wires_robotic_platform.msg import *
from wires_robotic_platform.msg import RobotMotionResult, RobotMotionFeedback, RobotMotionAction

from wires_robotic_platform.utils.logger import Logger

from wires_robotic_platform.param.global_parameters import Parameters


if __name__ == '__main__':
    rospy.init_node('bogie_trajectory_motion')
    robot_name = Parameters.get("BOGIE_NAME")
    node_rate = Parameters.get(obj=robot_name, param="NODE_FREQUENCY")
    rate = rospy.Rate(node_rate)

    bogie_action_server = RobotMotionServer(robot_name)

    try:
        while not rospy.is_shutdown():
            bogie_action_server.stepForward()
            rate.sleep()
    except rospy.ROSInterruptException:
        pass
