#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

import pkgutil
import rospy

from std_msgs.msg import *

import random
import numpy
from numpy import *
import numpy as np
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from std_msgs.msg import Float64MultiArray
import PyKDL
from PyKDL import Frame, Vector, Rotation
import threading

from std_msgs.msg import String
from sensor_msgs.msg import JointState
from robot_motion_action import RobotMotionClient
from mongodb_store.message_store import MessageStoreProxy


from global_parameters import Parameters

import tf
import cv2


from rospkg import RosPack

tf_target = None


class Spiral(object):

    def __init__(self, start_frame, dist, alphas, betas):
        self.start_frame = start_frame
        self.frames = []

        self.frames.append(self.spike(start_frame, 0, 0, dist))
        print("Creating SPike", alphas)
        for alpha in alphas:
            print(alpha)
            for beta in betas:
                frame = self.spike(start_frame, alpha, beta, dist)
                self.frames.append(frame)

    def spike(self, frame, alpha, beta, dist):

        frame_alpha = PyKDL.Frame()
        frame_beta = PyKDL.Frame()
        frame_dist = PyKDL.Frame(PyKDL.Vector(0.0, 0.0, dist))
        frame_alpha.M.DoRotZ(alpha)
        frame_beta.M.DoRotX(beta)
        new_frame = PyKDL.Frame()
        new_frame.p = frame.p
        new_frame.M = frame.M
        new_frame = new_frame * frame_alpha
        new_frame = new_frame * frame_beta
        new_frame = new_frame * frame_dist
        return new_frame


class Target(object):
    KEY_UP = 65362  # 1113938  #
    KEY_DOWN = 65364  # 1113940  #
    KEY_RIGHT = 65363  # 1113939  #
    KEY_LEFT = 65361  # 1113937  #
    KEY_ENTER = 10  # 1048586  #
    KEY_S = 115

    def __init__(self, name, x, y, z, roll, pitch, yaw):
        self.name = name
        self.x = x
        self.y = y
        self.z = z
        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw
        self.seletected = 0
        self.current_joint_state = JointState

    def getT(self):
        frame = PyKDL.Frame()
        frame.p = PyKDL.Vector(self.x, self.y, self.z)
        frame.M = PyKDL.Rotation.RPY(
            self.roll * np.pi / 180.0,
            self.pitch * np.pi / 180.0,
            self.yaw * np.pi / 180.0
        )
        return frame

    def setT(self, frame):
        self.x = round(frame.p.x(), 3)
        self.y = round(frame.p.y(), 3)
        self.z = round(frame.p.z(), 3)
        self.roll, self.pitch, self.yaw = frame.M.GetRPY()
        self.roll = int(self.roll * 180.0 / np.pi)
        self.pitch = int(self.pitch * 180.0 / np.pi)
        self.yaw = int(self.yaw * 180.0 / np.pi)

    def incDec(self, index, inc):
        if index < 3:
            mag = 0.01
        else:
            mag = 1

        if index == 0:
            self.x += inc * mag
        if index == 1:
            self.y += inc * mag
        if index == 2:
            self.z += inc * mag
        if index == 3:
            self.roll += inc * mag
        if index == 4:
            self.pitch += inc * mag
        if index == 5:
            self.yaw += inc * mag

    def commit(self):
        if comau_action_client.done_flg:
            comau_action_client.generateNewGoal("gototf " + self.name)

    def publish(self):
        broadcastTransform(br, tf_target.getT(), self.name,
                           "comau_smart_six/base_link", rospy.Time.now())

    def manageKey(self, key):
        if key == Target.KEY_ENTER:
            self.commit()

        if key == Target.KEY_UP:
            self.seletected -= 1
        if key == Target.KEY_DOWN:
            self.seletected += 1

        if key == Target.KEY_RIGHT:
            self.incDec(self.seletected, 1)
        if key == Target.KEY_LEFT:
            self.incDec(self.seletected, -1)

        if key == Target.KEY_S:
            print("Save")
        self.seletected = self.seletected % 6


def broadcastTransform(br, frame, frame_id, parent_frame, time):
    br.sendTransform((frame.p.x(), frame.p.y(), frame.p.z()),
                     frame.M.GetQuaternion(),
                     time,
                     frame_id,
                     parent_frame)


def updateUI():
    gui_mat = m = numpy.zeros((640, 480, 3), dtype=numpy.uint8)

    color = (255, 255, 255)
    selected_color = (255, 0, 0)
    scale = 2
    font = cv2.FONT_HERSHEY_PLAIN

    y = 40
    delta = 20 * scale

    cv2.putText(gui_mat, "X:{}".format(tf_target.x),
                (20, y), font, scale, selected_color if tf_target.seletected == 0 else color)
    y += delta
    cv2.putText(gui_mat, "Y:{}".format(tf_target.y),
                (20, y), font, scale, selected_color if tf_target.seletected == 1 else color)

    y += delta
    cv2.putText(gui_mat, "Z:{}".format(tf_target.z),
                (20, y), font, scale, selected_color if tf_target.seletected == 2 else color)

    y += delta
    cv2.putText(gui_mat, "Roll:{}".format(tf_target.roll),
                (20, y), font, scale, selected_color if tf_target.seletected == 3 else color)

    y += delta
    cv2.putText(gui_mat, "Pitch:{}".format(tf_target.pitch),
                (20, y), font, scale, selected_color if tf_target.seletected == 4 else color)

    y += delta
    cv2.putText(gui_mat, "Yaw:{}".format(tf_target.yaw),
                (20, y), font, scale, selected_color if tf_target.seletected == 5 else color)

    cv2.imshow("gui", gui_mat)
    c = cv2.waitKey(1)
    if c > 0:
        print(c)
    tf_target.manageKey(c)


def saveCurrentTf(name):
    global tf_target
    print("Saveing!!", name)
    p_id = msg_store.insert_named(name, tf_target.current_joint_state)


def loadState(name):
    p = msg_store.query_named(name, JointState._type, single=True)
    p = p[0]
    if p == None:
        return
    print("LOad", p)
    joints = np.array(p.position)
    joints = np.around(joints, decimals=2)
    joint_string = np.array2string(joints, precision=2, separator=',',
                                   suppress_small=True)
    joint_string = joint_string.replace("[", "")
    joint_string = joint_string.replace("]", "")
    joint_string = joint_string.replace(" ", "")
    joint_string = joint_string.replace(".,", ".0,")
    command = "joints {}".format(joint_string)
    if comau_action_client.done_flg:
        comau_action_client.generateNewGoal(command)


def publishTfTarget():
    while not rospy.is_shutdown():
        tf_target.publish()
        updateUI()

        tf = transformations.KDLtoTf(current_tf)
        msg = Float64MultiArray()
        msg.data = [
            tf[0][0],
            tf[0][1],
            tf[0][2],
            tf[1][0],
            tf[1][1],
            tf[1][2],
            tf[1][3]
        ]
        pose_pub.publish(msg)

        if False:
            counter = 0
            for spike in spiral.frames:
                broadcastTransform(br, spike, "spike_{}".format(counter),
                                   "comau_smart_six/base_link", rospy.Time.now())
                counter = counter + 1


def jointCallback(msg):
    global tf_target
    if tf_target:
        tf_target.current_joint_state = msg


def parseCommand(command):
    if command.startswith("save"):
        saveCurrentTf(command.split(" ")[1])
    if command.startswith("load"):
        name = command.split(" ")[1]
        loadState(name)
    if command.startswith("goto"):
        tf_name = command.split(" ")[1]
        print("Going to tf:", tf_name)
        if comau_action_client.done_flg:
            comau_action_client.generateNewGoal("gototf " + tf_name)
    if command == "start":
        jointStatesLoader.next()


class JointStatesLoader(object):

    def __init__(self, base_name, start, end):
        self.base_name = base_name
        self.start = start
        self.end = end
        self.pointer = start

    def getNext(self):
        p = "{}_{}".format(self.base_name, self.pointer)
        self.pointer += 4
        return p

    def next(self):
        if self.pointer == self.end:
            return False
        else:
            name = self.getNext()
            print(name)
            loadState(name)
            return True


jointStatesLoader = JointStatesLoader("pino", 0, 500)


def doneCallback(result):
    pass
    # jointStatesLoader.next()


def say(text):
    print("Saying", text)
    msg = String()
    msg.data = text
    voice_pub.publish(msg)


def voiceCallback(msg):
    command = msg.data
    print("Received:", command)
    if 'pino' in command or 'Pino' in command:
        if 'salva' in command:
            saveCurrentTf("testing")
            say('posizione salvata')
        if 'senti' in command:
            say('si ti  sento')
        if 'vivo' in command:
            say('si sono vivo')
        if 'ciao' in command:
            say('ciao a te')
        if 'posizione' in command or 'vai' in command:
            if 'calibrazione' in command:
                say('vado in calibrazione')
                loadState("voice_0")
            if 'sinistra' in command:
                loadState("voice_1")
                say('vado a sinistra')
            if 'destra' in command:
                loadState("voice_2")
                say('vado a destra')
            if 'tavolo' in command:
                loadState("voice_3")
                say('ma mica sono il tuo schiavo')


if __name__ == '__main__':
    msg_store = MessageStoreProxy()
    rospy.init_node('robot_motion_action_client')
    node_rate = 10
    rate = rospy.Rate(node_rate)  # 10hz
    pose_pub = rospy.Publisher(
        '/comau_smart_six/ee_pose', Float64MultiArray, queue_size=1)

    joint_callback = rospy.Subscriber(
        '/comau_smart_six/joint_states', JointState, jointCallback, queue_size=1)

    voice_sub = rospy.Subscriber(
        '/jarvis/message', String, voiceCallback, queue_size=1)
    voice_pub = rospy.Publisher(
        '/jarvis/listen', String, queue_size=1
    )

    br = tf.TransformBroadcaster()
    listener = tf.TransformListener()

    current_tf = None

    # Spiral
    spiral_frame = PyKDL.Frame(PyKDL.Vector(0.75, 0.1, -0.5))
    spiral_frame.M = PyKDL.Rotation.RPY(-np.pi, 0, -np.pi / 2.0)
    spiral = Spiral(
        spiral_frame, -0.3,
        np.linspace(0, np.pi * 2, 10),
        [np.pi / 6]
    )

    while current_tf == None:
        current_tf = transformations.retrieveTransform(
            listener, "comau_smart_six/base_link", "comau_smart_six/link6", none_error=True)
        rate.sleep()

    print("OK", current_tf)
    # ---- test command lists -----

    tf_target = Target("target_tf", 0.0, 0.1, 0, 0, 0, 0)
    tf_target.setT(current_tf)

    comau_action_client = RobotMotionClient(Parameters.get("COMAU_NAME"))

    comau_action_client.user_callback = doneCallback

    # comau_action_client.generateNewGoal("joints 0,0,-2,0.1,1,0")

    thread = threading.Thread(target=publishTfTarget)
    thread.start()
    try:
        while not rospy.is_shutdown():
            command = raw_input("\ncommand: ")
            parseCommand(command)
            # if comau_action_client.done_flg:
            #     command = raw_input("command: ")
            #     comau_action_client.generateNewGoal(str(command))
            # else:
            #     pass

            rate.sleep()
    except rospy.ROSInterruptException:
        pass

    thread.join()
