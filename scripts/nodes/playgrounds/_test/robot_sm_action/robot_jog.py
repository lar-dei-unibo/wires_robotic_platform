#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

import pkgutil
import rospy

from std_msgs.msg import *

import random
import numpy
from numpy import *
import numpy as np
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations

from std_msgs.msg import Float64MultiArray
import PyKDL
from PyKDL import Frame, Vector, Rotation
import threading

from std_msgs.msg import String

from wires_robotic_platform.robotmotion.robot_motion_action import RobotMotionClient

from wires_robotic_platform.param.global_parameters import Parameters

import tf
import cv2


from rospkg import RosPack


class Target(object):
    KEY_UP = 1113938  # 65362#
    KEY_DOWN = 1113940  # 65364#
    KEY_RIGHT = 1113939  # 65363 #
    KEY_LEFT = 1113937  # 65361 #
    KEY_ENTER = 1048586  # 10#

    def __init__(self, name, x, y, z, roll, pitch, yaw):
        self.name = name
        self.x = x
        self.y = y
        self.z = z
        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw
        self.seletected = 0

    def getT(self):
        frame = PyKDL.Frame()
        frame.p = PyKDL.Vector(self.x, self.y, self.z)
        frame.M = PyKDL.Rotation.RPY(
            self.roll * np.pi / 180.0,
            self.pitch * np.pi / 180.0,
            self.yaw * np.pi / 180.0
        )
        return frame

    def setT(self, frame):
        self.x = round(frame.p.x(), 3)
        self.y = round(frame.p.y(), 3)
        self.z = round(frame.p.z(), 3)
        self.roll, self.pitch, self.yaw = frame.M.GetRPY()
        self.roll = int(self.roll * 180.0 / np.pi)
        self.pitch = int(self.pitch * 180.0 / np.pi)
        self.yaw = int(self.yaw * 180.0 / np.pi)

    def incDec(self, index, inc):
        if index < 3:
            mag = 0.01
        else:
            mag = 1

        if index == 0:
            self.x += inc * mag
        if index == 1:
            self.y += inc * mag
        if index == 2:
            self.z += inc * mag
        if index == 3:
            self.roll += inc * mag
        if index == 4:
            self.pitch += inc * mag
        if index == 5:
            self.yaw += inc * mag

    def commit(self):
        if comau_action_client.done_flg:
            comau_action_client.generateNewGoal("gototf " + self.name)

    def publish(self):
        broadcastTransform(br, tf_target.getT(), self.name,
                           "comau_smart_six/base_link", rospy.Time.now())

    def manageKey(self, key):
        if key == Target.KEY_ENTER:
            self.commit()

        if key == Target.KEY_UP:
            self.seletected -= 1
        if key == Target.KEY_DOWN:
            self.seletected += 1

        if key == Target.KEY_RIGHT:
            self.incDec(self.seletected, 1)
        if key == Target.KEY_LEFT:
            self.incDec(self.seletected, -1)

        self.seletected = self.seletected % 6


def broadcastTransform(br, frame, frame_id, parent_frame, time):
    br.sendTransform((frame.p.x(), frame.p.y(), frame.p.z()),
                     frame.M.GetQuaternion(),
                     time,
                     frame_id,
                     parent_frame)


def updateUI():
    gui_mat = m = numpy.zeros((640, 480, 3), dtype=numpy.uint8)

    color = (255, 255, 255)
    selected_color = (255, 0, 0)
    scale = 2
    font = cv2.FONT_HERSHEY_PLAIN

    y = 40
    delta = 20 * scale

    cv2.putText(gui_mat, "X:{}".format(tf_target.x),
                (20, y), font, scale, selected_color if tf_target.seletected == 0 else color)
    y += delta
    cv2.putText(gui_mat, "Y:{}".format(tf_target.y),
                (20, y), font, scale, selected_color if tf_target.seletected == 1 else color)

    y += delta
    cv2.putText(gui_mat, "Z:{}".format(tf_target.z),
                (20, y), font, scale, selected_color if tf_target.seletected == 2 else color)

    y += delta
    cv2.putText(gui_mat, "Roll:{}".format(tf_target.roll),
                (20, y), font, scale, selected_color if tf_target.seletected == 3 else color)

    y += delta
    cv2.putText(gui_mat, "Pitch:{}".format(tf_target.pitch),
                (20, y), font, scale, selected_color if tf_target.seletected == 4 else color)

    y += delta
    cv2.putText(gui_mat, "Yaw:{}".format(tf_target.yaw),
                (20, y), font, scale, selected_color if tf_target.seletected == 5 else color)

    cv2.imshow("gui", gui_mat)
    c = cv2.waitKey(1)
    if c > 0:
        print(c)
    tf_target.manageKey(c)


def saveCurrentTf(name):
    fff = open("/home/lar/Scrivania/test.txt", "a")
    fff.write("{} {} {} {} {} {} {}\n".format(name,
                                              tf_target.x,
                                              tf_target.y,
                                              tf_target.z,
                                              tf_target.roll,
                                              tf_target.pitch,
                                              tf_target.yaw))


def publishTfTarget():
    while not rospy.is_shutdown():
        tf_target.publish()
        updateUI()

        tf = transformations.KDLtoTf(current_tf)
        msg = Float64MultiArray()
        msg.data = [
            tf[0][0],
            tf[0][1],
            tf[0][2],
            tf[1][0],
            tf[1][1],
            tf[1][2],
            tf[1][3]
        ]
        pose_pub.publish(msg)


if __name__ == '__main__':

    rospy.init_node('robot_motion_action_client')
    node_rate = 10
    rate = rospy.Rate(node_rate)  # 10hz
    pose_pub = rospy.Publisher(
        '/comau_smart_six/ee_pose', Float64MultiArray, queue_size=1)

    br = tf.TransformBroadcaster()
    listener = tf.TransformListener()

    current_tf = None

    while current_tf == None:
        current_tf = transformations.retrieveTransform(
            listener, "comau_smart_six/base_link", "comau_smart_six/link6", none_error=True)
        rate.sleep()

    print("OK", current_tf)
    # ---- test command lists -----

    tf_target = Target("target_tf", 0.0, 0.1, 0, 0, 0, 0)
    tf_target.setT(current_tf)

    comau_action_client = RobotMotionClient(Parameters.get("COMAU_NAME"))

    comau_action_client.generateNewGoal("joints 0,0,-2,0.1,1,0")

    thread = threading.Thread(target=publishTfTarget)
    thread.start()
    try:
        while not rospy.is_shutdown():
            name = raw_input("\nrecord name: ")
            if name != "":
                saveCurrentTf(name)
            # if comau_action_client.done_flg:
            #     command = raw_input("command: ")
            #     comau_action_client.generateNewGoal(str(command))
            # else:
            #     pass

            rate.sleep()
    except rospy.ROSInterruptException:
        pass

    thread.join()
