#!/usr/bin/env python
# This Python file uses the following encoding: utf-8

import pkgutil
import rospy

from std_msgs.msg import *

import random
import numpy
from numpy import *
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from wires_robotic_platform.utils.logger import Logger

import PyKDL
from PyKDL import Frame, Vector, Rotation

from std_msgs.msg import String

from wires_robotic_platform.robotmotion.robot_motion_action import RobotMotionClient

from wires_robotic_platform.msg import *
from wires_robotic_platform.msg import RobotMotionResult, RobotMotionFeedback, RobotMotionAction, RobotMotionGoal

from wires_robotic_platform.param.global_parameters import Parameters

import tf


def broadcastTransform(br, frame, frame_id, parent_frame, time):
    br.sendTransform((frame.p.x(), frame.p.y(), frame.p.z()),
                     frame.M.GetQuaternion(),
                     time,
                     frame_id,
                     parent_frame)


def readSavedTf(filename):
    lines = [line.rstrip('\n') for line
             in open('/home/riccardo/Desktop/{}.txt'.format(str(filename)))]

    tf_list = []
    name_list = []
    for ln in lines:
        if ln == "":
            pass
        name = ln.split(" ")[0]
        name_list.append(name)
        target = PyKDL.Frame()
        x = float(ln.split(" ")[1])
        y = float(ln.split(" ")[2])
        z = float(ln.split(" ")[3])
        target.p = PyKDL.Vector(x, y, z)
        theta = float(ln.split(" ")[1])
        gamma = float(ln.split(" ")[2])
        phi = float(ln.split(" ")[3])
        target.M = PyKDL.Rotation.EulerZYX(theta, gamma, phi)
        tf_list.append(target)

    return {"name": name_list, "tf": tf_list}


br = tf.TransformBroadcaster()


if __name__ == '__main__':

    rospy.init_node('robot_motion_action_client')
    node_rate = 10
    rate = rospy.Rate(node_rate)  # 10hz

    comau_action_client = RobotMotionClient(Parameters.get("COMAU_NAME"))

    i = -1
    # tf_list = ["shape_test2"]
    tf_list = []
    saved_tf = readSavedTf("test")
    tf_name = saved_tf["name"]
    tf_list = saved_tf["tf"]
    list_ready = False
    comau_action_client.generateNewGoal("joints 0,0,-2,0.1,1,0")
    # comau_action_client.generateNewGoal("joints 0,-0.5,-2,0.1,1,0")

    try:
        while not rospy.is_shutdown():

            ###################################################################
            ###################################################################
            ###################################################################
            ###################################################################
            ###################################################################

            # if comau_action_client.done_flg:
            #     command = raw_input("command: ")
            #     comau_action_client.generateNewGoal(str(command))
            # else:
            #     pass

            ###################################################################
            ###################################################################
            ###################################################################
            ###################################################################
            ###################################################################

            t = rospy.get_rostime()
            for k in range(0, len(tf_list)):
                broadcastTransform(
                    br, tf_list[k], tf_name[k], "comau_smart_six/base_link", t)

            n = len(tf_list)
            if comau_action_client.done_flg:
                raw_input("Press Enter to continue...")
                i += 1
                if i >= n:
                    i = 0
                command = "gototf {}".format(tf_name[i])
                comau_action_client.generateNewGoal(command)
            else:
                pass

            ###################################################################
            ###################################################################
            ###################################################################
            ###################################################################
            ###################################################################

            # PI = numpy.pi
            # alpha = PI / 6
            # h = 0.5
            # r = h * numpy.tan(alpha)
            # target = PyKDL.Frame()
            # target.p = PyKDL.Vector(0.8, 0.1, -0.3)

            # n = 5
            # t = rospy.get_rostime()
            # for angle in range(0, n):
            #     p_tf = generateBla((angle * (360 / n)) * PI / 180.0, alpha, h)
            #     p_tf = target * p_tf
            #     broadcastTransform(br, p_tf, "pn_{}".format(
            #         angle), "comau_smart_six/base_link", t)
            #     if not list_ready:
            #         tf_list.append("gototf pn_{}".format(angle))
            # list_ready = True

            # broadcastTransform(br, target, "target",
            #                    "comau_smart_six/base_link", t)

            # if comau_action_client.done_flg:
            #     comau_action_client.generateNewGoal(tf_list[i])
            #     i += 1
            #     if i > n:
            #         i = 0
            # else:
            #     pass

            rate.sleep()
    except rospy.ROSInterruptException:
        pass
