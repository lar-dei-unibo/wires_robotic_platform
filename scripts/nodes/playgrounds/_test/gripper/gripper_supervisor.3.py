#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import rospy

from wires_robotic_platform.msg import RobotFollow
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.robots.controllers import RobotStatus
from wires_robotic_platform.robotcontrol.robot_direct_motion import DirectCommander
from wires_robotic_platform.proxy.target_follower_proxy import TargetFollowerProxy
from wires_robotic_platform.proxy.alarm_proxy import AlarmProxy
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
import wires_robotic_platform.sfm.machines as machines

from std_msgs.msg import String, Bool, Float64MultiArray
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState
import PyKDL
import copy
import math
import tf
from wires_robotic_platform.param.global_parameters import Parameters


OPEN_POS = 20
CLOSE_POS = 2
VEL = 50
EFF = 50

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇    STATE MACHINE DEFINITION    ▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class SFMachineGripperSupervisorDefinition(object):

    def __init__(self,  robot_name, node):
        self.robot_name = robot_name
        self.command_server = CommandProxyServer(
            "{}_supervisor".format(self.robot_name))
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.p_target = None
        self.tmp_status = None

        # gripper node
        self.node = node
        self.js = JointState()
        self.gripper_pub = self.node.createPublisher(
            "/schunk_pg70/joint_setpoint", JointState)
        self.gripper_sub = self.node.createSubscriber(
            "/schunk_pg70/joint_states", JointState, self.griper_state_callback)

        # grasp pred
        self.grasp_sub = self.node.createSubscriber(
            "/grasp_clf_pred", Float64MultiArray, self.grasp_callback)
        self.pred_buffer = [-1] * 5

    # ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ CALLBACKS ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    def griper_state_callback(self, msg):
        joint_state = msg.position[0] * 2000
        velocity = msg.velocity[0]
        print(self.p_target)
        print(joint_state)
        if self.target_state:
            j_min = self.p_target * 0.99
            j_max = self.p_target * 1.01
            if j_min <= joint_state <= j_max:

                if self.target_state == "open":
                    self.open()
                elif self.target_state == "close":
                    self.close()

                self.target_state = None
                self.p_target = None

                grasp = self.check_gasp()
                self._send_command_result(True, grasp)
            else:
                pass

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        if msg.isValid():
            if msg.getReceiver() == "{}_supervisor".format(self.robot_name):
                command = msg.getCommand()
                try:
                    if command == "open":
                        try:
                            p = float(msg.getData("position"))
                        except:
                            p = OPEN_POS
                        self.target_state = "open"
                        self.set_gripper(p)
                    elif command == "close":
                        try:
                            p = float(msg.getData("position"))
                        except:
                            p = CLOSE_POS
                        self.target_state = "close"
                        self.set_gripper(p)
                    else:
                        Logger.error("INVALID input")
                except Exception as e:
                    print(e)

    def grasp_callback(self, msg):
        if len(self.pred_buffer) != 0:
            self.pred_buffer.pop(0)
            self.pred_buffer.append(int(msg.data[0]))

    # ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ PRIVATE METHODS ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    def set_gripper(self, p):
        print("...{}-ing".format(self.target_state))
        self.p_target = p
        self.js.position = [p]
        self.js.velocity = [VEL]
        self.js.effort = [EFF]
        self.gripper_pub.publish(self.js)
        self.moving()

    def _send_command_result(self, done, grasp):
        if self.command_server_last_message:
            self.command_server_last_message.addResponseData("done",
                                                             done)
            self.command_server_last_message.addResponseData("grasp",
                                                             grasp)
            if done:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)

            self.command_server_last_message = None

    def check_gasp(self):
        print("check_gasp")
        self.pred_buffer = [-1] * len(self.pred_buffer)
        while self.pred_buffer.count(-1) != 0:
            pass

        if self.pred_buffer.count(1) >= len(self.pred_buffer) * 2 / 3:
            return True
        else:
            return False

    # ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ STATES ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
  #
    #⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢ IDLE ⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢
    #

    # ➤ ➤ ➤ ➤ ➤ IDLE: enter
    def on_enter_idle(self):
        Logger.log("State:  IDLE")

    # ➤ ➤ ➤ ➤ ➤ IDLE: loop
    def on_loop_idle(self):
        pass

    #
    #⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢ OPEN ⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢
    #

    # ➤ ➤ ➤ ➤ ➤ OPEN: enter
    def on_enter_open(self):
        Logger.log("State:  OPEN")

    # ➤ ➤ ➤ ➤ ➤ OPEN: loop
    def on_loop_open(self):
        pass

    # ➤ ➤ ➤ ➤ ➤ OPEN: exit
    def on_exit_open(self):
        if not self.target_state:
            self.previous_state = "open"
 #
    #⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢ CLOSE ⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢
    #

    # ➤ ➤ ➤ ➤ ➤ CLOSE: enter
    def on_enter_close(self):
        Logger.log("State:  CLOSE")

    # ➤ ➤ ➤ ➤ ➤ CLOSE: loop
    def on_loop_close(self):
        pass

    # ➤ ➤ ➤ ➤ ➤ CLOSE: exit
    def on_exit_close(self):
        if not self.target_state:
            self.previous_state = "close"
 #
    #⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢ MOVING ⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢
    #

    # ➤ ➤ ➤ ➤ ➤ MOVING: enter
    def on_enter_moving(self):
        Logger.log("State:  MOVING")
        if self.previous_state == "close":
            self.target_state = "open"
        elif self.previous_state == "open":
            self.target_state = "close"

    # ➤ ➤ ➤ ➤ ➤ MOVING: loop
    def on_loop_moving(self):
        pass

    # ➤ ➤ ➤ ➤ ➤ MOVING: exit
    def on_exit_moving(self):
        self.target_state = None

    #⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢ ALARM ⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢⬢
    #

    # ➤ ➤ ➤ ➤ ➤ ALARM: enter
    def on_enter_alarm(self):
        Logger.log("State:  ALARM")

    # ➤ ➤ ➤ ➤ ➤ ALARM: loop
    def on_loop_alarm(self):
        pass

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇    SUPERVISOR SM CLASS    ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class GripperSupervisorSM(object):
    def __init__(self, robot_name, node):
        self.robot_name = robot_name

        # CREATE STATE MACHINE
        self.supervisorSFM = SFMachineGripperSupervisorDefinition(self.robot_name,
                                                                  node)
        self.sfm = machines.SFMachine(name="sfm_" + self.robot_name + "_supervisor",
                                      model=self.supervisorSFM)

        # DEFINE SM STATES
        self.sfm.addState("start")
        self.sfm.addState("idle")
        self.sfm.addState("close")
        self.sfm.addState("open")
        self.sfm.addState("moving")
        self.sfm.addState("alarm")

        # DEFINE SM TRANSITIONS
        # start ...
        self.sfm.addTransition("idle", "start", "idle")
        # close ...
        self.sfm.addTransition("close", "close", "close")
        self.sfm.addTransition("moving", "close", "moving")
        self.sfm.addTransition("alarm", "close", "alarm")
        # open ...
        self.sfm.addTransition("open", "open", "open")
        self.sfm.addTransition("moving", "open", "moving")
        self.sfm.addTransition("alarm", "open", "alarm")
        # moving ...
        self.sfm.addTransition("open", "moving", "open")
        self.sfm.addTransition("close", "moving", "close")
        self.sfm.addTransition("alarm", "moving", "alarm")
        # alarm ...
        self.sfm.addTransition("alarm", "alarm", "alarm")
        self.sfm.addTransition("idle", "alarm", "idle")

        self.sfm.create()
        self.sfm.set_state("start")
        Logger.log("\n\n ************* SFM ready to start ***********")

    def start(self):
        Logger.log("\n\n ************* SFM start ***********")
        self.sfm.getModel().idle()

    def stepForward(self):
        self.sfm.loop()


if __name__ == '__main__':
    node = RosNode("simple_gripper_supervisor")
    node_rate = 10
    node.setupParameter("hz", node_rate)
    node.setHz(node.getParameter("hz"))

    gripper_supervisor = GripperSupervisorSM("simple_gripper", node)
    gripper_supervisor.start()

    try:
        while node.isActive():
            gripper_supervisor.stepForward()
            node.tick()
    except rospy.ROSInterruptException:
        pass
