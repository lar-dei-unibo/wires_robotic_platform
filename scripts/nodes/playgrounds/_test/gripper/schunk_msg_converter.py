#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import rospy
import tf_conversions
from tf_conversions import posemath
from wires_robotic_platform.msg import ComauFollow
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.devices import ForceSensor, Joystick
from wires_robotic_platform.robotmotion.robot_motion_action import RobotMotionClient
from wires_robotic_platform.msg import *
from wires_robotic_platform.msg import RobotMotionResult, RobotMotionFeedback
from wires_robotic_platform.msg import RobotFollow
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.storage.mongo import MessageStorage
import wires_robotic_platform.sfm.machines as machines
from wires_robotic_platform.proxy.target_follower_proxy import TargetFollowerProxy
from wires_robotic_platform.proxy.alarm_proxy import AlarmProxy
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.robotsupervisor.robot_supervisor_state_machine import SupervisorSM
from std_msgs.msg import String, Float64, Bool
from sensor_msgs.msg import JointState
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
import threading
import PyKDL
import copy
import math
import tf
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.utils.ros import RosNode


class SchunkMsgConverter(object):
    def __init__(self, node):
        self.node = node
        self.pub = self.node.createPublisher(
            "/schunk_pg70/joint_setpoint", JointState)
        self.sub = self.node.createSubscriber(
            "/schunk_pg70/joint_command", JointState, self.callback)

    def wait(self):
        pass

    def callback(self, msg):
        out_msg = JointState()
        out_msg.name = []
        out_msg.position = [msg.position[0] * 2000]
        out_msg.velocity = [80.0]
        out_msg.effort = [50.0]
        self.pub.publish(out_msg)


if __name__ == '__main__':
    node = RosNode("schunk_msg_converter")
    node.setupParameter("hz", 250)
    node.setHz(node.getParameter("hz"))
    smc = SchunkMsgConverter(node)
    try:
        while node.isActive():
            smc.wait()
            node.tick()
    except rospy.ROSInterruptException:
        pass
