#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import rospy

from wires_robotic_platform.msg import RobotFollow
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.robots.controllers import RobotStatus
from wires_robotic_platform.robotcontrol.robot_direct_motion import DirectCommander
from wires_robotic_platform.proxy.target_follower_proxy import TargetFollowerProxy
from wires_robotic_platform.proxy.alarm_proxy import AlarmProxy
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer

from std_msgs.msg import String, Bool, Float64MultiArray
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState
import PyKDL
import copy
import math
import tf
from wires_robotic_platform.param.global_parameters import Parameters


class GripperSupervisor(object):
    OPEN_P = 20
    CLOSE_P = 2
    VEL = 50
    EFF = 50

    def __init__(self, node, robot_name):
        self.robot_name = robot_name
        self.command_server = CommandProxyServer(
            "{}_supervisor".format(self.robot_name))
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.p_target = None
        self.status = "open"
        self.tmp_status = None

        # gripper node
        self.node = node
        self.js = JointState()
        self.gripper_pub = self.node.createPublisher(
            "/schunk_pg70/joint_setpoint", JointState)
        self.gripper_sub = self.node.createSubscriber(
            "/schunk_pg70/joint_states", JointState, self.griper_state_callback)

        # grasp perd
        self.grasp_sub = self.node.createSubscriber(
            "/grasp_clf_pred", Float64MultiArray, self.grasp_callback)
        self.pred_buffer = [-1] * 5

    def idle(self):
        pass

    def griper_state_callback(self, msg):
        joint_state = msg.position[0] * 2000
        velocity = msg.velocity[0]
        print(self.p_target)
        print(joint_state)
        if self.p_target:
            print("BOIOAAAA DIOOOO")
            j_min = self.p_target * 0.99
            j_max = self.p_target * 1.01
            if j_min <= joint_state <= j_max:

                self.status = self.tmp_status
                grasp = self.check_gasp()
                done = True
                self.p_target = None
                if self.regrasp_on and done:
                    self.grasp()
                else:
                    self._send_command_result(done, grasp)

            else:
                # grasp = False
                # done = False
                pass

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        if msg.isValid():
            if msg.getReceiver() == "{}_supervisor".format(self.robot_name):
                command = msg.getCommand()
                try:
                    if command == "open":
                        Logger.log("open")
                        try:
                            p = float(msg.getData("position"))
                        except:
                            p = GripperSupervisor.OPEN_P
                        self.tmp_status = "open"
                        self.set_gripper(p)
                    elif command == "close":
                        Logger.log("close")
                        try:
                            p = float(msg.getData("position"))
                        except:
                            p = GripperSupervisor.CLOSE_P
                        self.tmp_status = "close"
                        self.set_gripper(p)
                    elif command == "grasp":
                        if self.status == "open":
                            Logger.log("grasp")
                            try:
                                n = int(msg.getData("max_tries"))
                            except:
                                n = 3
                            self.regrasp_on = True
                            self.grasp_tries = n
                            self.grasp()
                        else:
                            Logger.error("First Open Gripper")
                    else:
                        Logger.error("INVALID input")
                except Exception as e:
                    print(e)

    def set_gripper(self, p):
        print(self.status)
        print("...{}-ing".format(self.tmp_status))
        self.p_target = p
        self.js.position = [p]
        self.js.velocity = [GripperSupervisor.VEL]
        self.js.effort = [GripperSupervisor.EFF]
        self.gripper_pub.publish(self.js)

    def _send_command_result(self, done, grasp):
        if self.command_server_last_message:
            self.command_server_last_message.addResponseData("done",
                                                             done)
            self.command_server_last_message.addResponseData("grasp",
                                                             grasp)
            if done:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)

            self.command_server_last_message = None

    def grasp(self):
        print("grasp")
        if self.status == "close":
            grasp = self.check_gasp()
            if grasp:
                self._send_command_result(True, grasp)
                self.grasp_tries = 0
                self.regrasp_on = False
            else:
                if self.grasp_tries > 0:
                    self.grasp_tries -= 1
                    self.tmp_status = "open"
                    self.set_gripper(GripperSupervisor.OPEN_P)
                else:
                    self._send_command_result(True, False)
                    self.grasp_tries = 0
                    self.regrasp_on = False
        else:
            self.tmp_status = "close"
            self.set_gripper(GripperSupervisor.CLOSE_P)

    def grasp_callback(self, msg):
        if len(self.pred_buffer) != 0:
            self.pred_buffer.pop(0)
            self.pred_buffer.append(int(msg.data[0]))

    def check_gasp(self):
        print("check_gasp")
        self.pred_buffer = [-1] * len(self.pred_buffer)
        while self.pred_buffer.count(-1) != 0:
            pass

        if self.pred_buffer.count(1) >= len(self.pred_buffer) * 2 / 3:
            return True
        else:
            return False


if __name__ == '__main__':
    node = RosNode("simple_gripper_supervisor")
    node_rate = 10
    node.setupParameter("hz", node_rate)
    node.setHz(node.getParameter("hz"))

    grsu = GripperSupervisor(node, "simple_gripper")
    grsu.idle()
    try:
        while node.isActive():
            node.tick()
    except rospy.ROSInterruptException:
        pass
