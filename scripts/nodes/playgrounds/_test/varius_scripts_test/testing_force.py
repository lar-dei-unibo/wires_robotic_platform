#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32, Float64MultiArray
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import sys
import random
from filterpy.kalman import KalmanFilter
from filterpy.common import Q_discrete_white_noise
import time

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("force_gripper")

node.setupParameter("hz", 250)
node.setHz(node.getParameter("hz"))
node_rate = node.getParameter("hz")


ct = 0
ctu = 0
ctd = 0
p = 0

force_lim_sup = 6
force_lim_inf = 5

p_open = 20
p_close = 2


def force_callback(msg):
    f = np.linalg.norm([msg.linear.x, msg.linear.y, msg.linear.z])
    global ct, ctu, ctd, p
    if ctu > 1:
        ct = ct + 1
    if ct > 100:
        ctu = 0
        ctd = 0
        ct = 0
    if f > force_lim_sup:
        print("{} {} {} f={}".format(ct, ctu, ctd, f))
        if ctu == 0:
            ct = 0

        if ctu < 2:
            if ctu == 0 or (ctu == 1 and ctd > 1):
                ctu = ctu + 1
        else:
            gr_msg = JointState()
            if p == p_close:
                p = p_open
            else:
                p = p_close
            gr_msg.position = [p]
            gr_msg.velocity = [50]
            gr_msg.effort = [50]
            gripper_pub.publish(gr_msg)
            ctu = 0
            ctd = 0
            ct = 0
    elif f < force_lim_inf:
        if ctu == 1:
            ctd = ctd + 1


def wire_callback(msg):
    wire_angle = np.arctan(msg.data[0])
    wire_z_offset = msg.data[1] / 1000
    # if wire_angle > 1 or wire_z_offset > 0.5:
    #     gr_msg = JointState()
    #     gr_msg.position = [10]
    #     gr_msg.velocity = [50]
    #     gr_msg.effort = [50]
    #     gripper_pub.publish(gr_msg)


node.createSubscriber("/atift", Twist, force_callback)
node.createSubscriber("/wire_params", Float64MultiArray, wire_callback)
gripper_pub = node.createPublisher(
    "/schunk_pg70/joint_setpoint", JointState)


while node.isActive():

    node.tick()
