#!/usr/bin/env python
# license removed for brevity

import PyKDL
from wires_robotic_platform.storage.save_data import SaveAsMat
from wires_robotic_platform.storage.save_parameters import SaveParams
from wires_robotic_platform.utils.transformations import KDLFrameToList
# a1 = [1,2,3]
# a2 = [4,500,6]
# a22 = [4,5,6]

# b1 = PyKDL.Frame()
# b2 = PyKDL.Frame(PyKDL.Vector(1,2,3))
# b2.M = PyKDL.Rotation.Rot(PyKDL.Vector(1,1,1), 45)

# s = SaveAsMat("/home/riccardo/dioboia.mat")
# # s.saveData("a1",a1)
# # s.saveData("a2",a2)
# # s.saveData("a2",a22)

# s.saveCell("cella", a1)
# s.saveCell("cella", a2)
# s.saveCell("cella", a22)

# s.saveCell("frames_R", KDLFrameToList(b1)[0])
# s.saveCell("frames_R", KDLFrameToList(b2)[0])
# s.saveCell("frames_T", KDLFrameToList(b1)[1])
# s.saveCell("frames_T", KDLFrameToList(b2)[1])

s = SaveParams()
s.save("teststrl_1", {"boh": 124,
                      "csadf": [1, 2, 3]})
s.save("teststrl_2", {"434": 124,
                      "gfdf": [2131, 321, 54363]})
s.save("teststrl_3", {"ger": 124,
                      "as": [0, 0]})
print s.get("teststrl_")
