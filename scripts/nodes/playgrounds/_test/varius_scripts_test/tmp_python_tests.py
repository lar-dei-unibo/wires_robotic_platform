#!/usr/bin/env python
# -*- encoding: utf-8 -*-


from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData
from wires_robotic_platform.param.global_parameters import Parameters

folder_path = Parameters.get(param="PACKAGE_DATA_FOLDER") + "/gearbox_zero"
gearbox_file_name = "gearbox00001_ids.txt"
components_file_name = "Component_tf.xml"

dict_compo = GearboxPartsData(folder_path, gearbox_file_name)
product_id = dict_compo.getComponentIDs("ID000081")["product"]
components_tf = dict_compo.getComponentTFs(
    product_id, components_file_name)["corner"]

print components_tf
