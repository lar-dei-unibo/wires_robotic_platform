#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This example demonstrates a very basic use of flowcharts: filter data,
displaying both the input and output of the filter. The behavior of
he filter can be reprogrammed by the user.
Basic steps are:
  - create a flowchart and two plots
  - input noisy data to the flowchart
  - flowchart connects data to the first plot, where it is displayed
  - add a gaussian filter to lowpass the data, then display it in the second plot.
"""


import numpy as np
import rospy
import wires_robotic_platform.vision.colors as colors
import wires_robotic_platform.utils.transformations as transformations
import os
from os import walk
import sys
import shutil

models = "/home/daniele/Desktop/temp/sk_dataset_17/models"

dirs = []
for (dirpath, dirnames, filenames) in walk(models):
    dirs.extend(dirnames)
    break

models_map = {}
for d in dirs:
    path = os.path.join(models, d)
    try:
        subname = d.split("_")[1]
        print("Path", subname)
        models_map[subname] = 1
    except:
        pass

for m in sorted(models_map.keys()):
    print(m)
#path = "/home/daniele/Desktop/temp/sk_dataset_17/models/Boxgreen_topdarker"

# files = []
# for (dirpath, dirnames, filenames) in walk(path):
#     files.extend(filenames)
#     break

# for f in files:
#     if 'camerapose' in f:
#         filename = os.path.join(path, f)
#         mat = np.loadtxt(filename).reshape(4, 4)
#         frame = transformations.NumpyMatrixToKDL(mat)
#         v = transformations.KDLtoNumpyVector(frame)

#         i = f.split("_")[0]
#         outfilename = os.path.join(path, "{}_camera_pose.txt".format(i))
#         np.savetxt(outfilename, v)
#         os.remove(filename)
#         print(f, i, outfilename)
