#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32, Pose
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.ui.pyqt import PyQtWindow
from PyQt4 import QtGui, QtCore, Qt
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtGui import QImage, qRgb
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
import wires_robotic_platform.utils.visualization as visualization
import wires_robotic_platform.utils.conversions as conversions
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math


class ImageRefresh(QtCore.QObject):
    update = QtCore.pyqtSignal()

    def __init__(self):
        # Initialize the PunchingBag as a QObject
        QtCore.QObject.__init__(self)

    def punch(self):
        ''' Punch the bag '''
        self.update.emit()


class NotImplementedException:
    pass


gray_color_table = [qRgb(i, i, i) for i in range(256)]


def toQImage(im, copy=False):

    if im is None:
        return QtGui.QImage()

    if im.dtype == np.uint8:
        if len(im.shape) == 2:
            qim = QtGui.QImage(
                im.data, im.shape[1], im.shape[0], im.strides[0], QtGui.QImage.Format_Indexed8)

            qim.setColorTable(gray_color_table)

            return qim.copy() if copy else qim

        elif len(im.shape) == 3:
            if im.shape[2] == 1:
                qim = QtGui.QImage(
                    im.data, im.shape[1], im.shape[0], im.strides[0], QtGui.QImage.Format_Indexed8)
                qim.setColorTable(gray_color_table)
                return qim.copy() if copy else qim
            elif im.shape[2] == 3:
                qim = QtGui.QImage(
                    im.data, im.shape[1], im.shape[0], im.strides[0], QtGui.QImage.Format_RGB888)
                return qim.copy() if copy else qim
            elif im.shape[2] == 4:
                qim = QtGui.QImage(
                    im.data, im.shape[1], im.shape[0], im.strides[0], QtGui.QImage.Format_ARGB32)
                return qim.copy() if copy else qim

    raise NotImplementedException


def pixmapFromCv(img):
    qimg = toQImage(img)
    pixmap = QtGui.QPixmap.fromImage(qimg)
    return pixmap


class CustomWindow(PyQtWindow):

    def __init__(self, uifile, node):
        super(CustomWindow, self).__init__(uifile=uifile)
        self.current_pix = None
        self.connect(self, QtCore.SIGNAL("updateImage"), self.test)
        self.image.mousePressEvent = self.getPos

        ##
        self.next_button.clicked.connect(self.nextCommand)

        self.current_command_index = 0

    def getPos(self, event):
        x = event.pos().x()
        y = event.pos().y()
        print(x, y)

    def test(self):
        pix = self.current_pix
        pix = pix.scaled(self.image.size(), QtCore.Qt.KeepAspectRatio)
        self.image.setAlignment(QtCore.Qt.AlignCenter)
        self.image.setPixmap(pix)

    def externalUpdate(self, pix):
        self.current_pix = pix
        self.emit(QtCore.SIGNAL("updateImage"))

    def displayImage(self, img):
        pass
        # self.image.setPixmap(pixmap)

    def updateList(self, commands):
        self.commands = commands
        self.current_command_index = 0
        model = QStandardItemModel(self.list)
        for c in commands:
            item = QStandardItem(str(c))
            model.appendRow(item)
        self.list.setModel(model)
        self.list.selectionModel().currentChanged.disconnect()
        self.list.selectionModel().currentChanged.connect(self.listSelectionChanged)
        self.setSelectedCommand(self.current_command_index)

    def setSelectedCommand(self, index):
        index = self.list.model().index(index, 0)
        self.list.setCurrentIndex(index)

    def nextCommand(self):
        self.current_command_index += 1
        self.current_command_index = self.current_command_index % len(
            self.commands)
        self.setSelectedCommand(self.current_command_index)

    def listSelectionChanged(self, v):
        print(v.data().toString())
        command = v.data().toString()
        self.current_command_text.setText(command)


current_arp_pose = PyKDL.Frame()

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("image_test")

node.setupParameter("hz", 60)
node.setHz(node.getParameter("hz"))

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/asus_camera_1_may2017.yml')
camera_tf_name = node.getParameter("CAMERA_FRAME")
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic="/camera/rgb/image_raw"
)

ui_file = node.getFileInPackage(
    "wires_robotic_platform", "data/ui/arp_gui.ui")

w = CustomWindow(uifile=ui_file, node=node)


def cameraCallback(frame):
    global current_arp_pose
    """ Camera callback. produce FrameRGBD object """
    output = frame.rgb_image.copy()

    pix = pixmapFromCv(output)
    w.externalUpdate(pix)


#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


commands = []
for i in range(0, 30):
    commands.append("Command {}".format(i))

w.updateList(commands)

w.run()
