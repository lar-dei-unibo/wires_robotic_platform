#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
from wires_robotic_platform.utils.ros import RosNode
from sensor_msgs.msg import JointState
from numpy.linalg import norm
import numpy as np
from wires_robotic_platform.param.global_parameters import Parameters


def target_pos_cb(msg):
    global target_pos
    target_pos = np.array(msg.position)


def current_pos_cb(msg):
    global current_pos
    current_pos = np.array(msg.position)


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("tf_export_test")

node.setupParameter("hz", 30)
node.setHz(node.getParameter("hz"))

node.createSubscriber("/cartesian/joint_command", JointState, target_pos_cb)
node.createSubscriber("/cartesian/joint_states", JointState, current_pos_cb)
current_pos = 0
target_pos = 0

while node.isActive():
    dist = norm(current_pos - target_pos)
    print "Grasshopper Distance from Target= {}".format(dist)
    node.tick()
