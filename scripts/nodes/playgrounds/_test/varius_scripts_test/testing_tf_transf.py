#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
import scipy
import time
import PyKDL
import tf
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
import wires_robotic_platform.utils.conversions as conversions
from wires_robotic_platform.utils.ros import RosNode
import numpy as np
from sensor_msgs.msg import Joy
from wires_robotic_platform.utils.devices import Joystick
import math
from geometry_msgs.msg import Twist
import threading


def joy_callback(msg):
    global joystick
    joystick.update(msg)


def joystick_key_callback(down, up):
    global fz, joystick
    if joystick.KEY_A in down:
        if -0.3 < fz < 0.3:
            fz = -0.3
        fz = fz - 0.01
    if joystick.KEY_B in down:
        if -0.3 < fz < 0.3:
            fz = 0.3
        fz = fz + 0.01
    print fz



#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("test")

node.setupParameter("hz", 5)
node.setHz(node.getParameter("hz"))
broadcaster = tf.TransformBroadcaster()
listener = tf.TransformListener()

rospy.Subscriber('/joy', Joy, joy_callback, queue_size=1)
joystick = Joystick(translation_mag=0.0005,
                    rotation_mag=0.0008)
joystick.registerCallback(joystick_key_callback)


class Test(object):
    def __init__(self):
        self.broadcaster = tf.TransformBroadcaster()
        self.listener = tf.TransformListener()
        self.unitX = PyKDL.Vector(1, 0, 0)
        self.unitY = PyKDL.Vector(0, 1, 0)
        self.unitZ = PyKDL.Vector(0, 0, 1)
        self.regulation_p_gain = [10, 10, 10]
        self.threshold = [0.3, 0.3, 0.3]
        self.step_size = 0.001
        self.hole_tf = None
        self.ct = 0
        self.current_tf = None
        self.initial_tf = None
        self.debug_data_estimated_twist_pub = rospy.Publisher("/insertion_control/estimated_twist",
                                                              Twist,
                                                              queue_size=1)
        self.debug_data_regulation_error_pub = rospy.Publisher("/insertion_control/regulation_error",
                                                               Twist,
                                                               queue_size=1)

    def getTf(self, tf_id="follow_target", parent_id="comau_smart_six/base_link"):
        self.current_tf = None
        while not self.current_tf:
            try:
                self.current_tf = transformations.retrieveTransform(self.listener, parent_id,
                                                                    tf_id, none_error=True,
                                                                    time=rospy.Time(0), print_error=True)
            except:
                print "Waiting for 'tool'..."
        self.initial_tf = self.current_tf
        return self.current_tf

    def computeTf(self, force):
        while not self.hole_tf:
            try:
                self.hole_tf = transformations.retrieveTransform(self.listener,  "comau_smart_six/base_link",
                                                                 "tf_storage_hole", none_error=True,
                                                                 time=rospy.Time(0), print_error=True)
            except:
                print "Waiting for 'tf_storage_hole'..."
        self.ur_axis = self.hole_tf.M * self.unitX

        Tr = PyKDL.Frame()

        # Estimated Force vector w.r.t. Gripper-Frame
        force_vector_tl = PyKDL.Vector(1.0 * min(force[0], 0), force[1], force[2])
        force_nparray_tl = transformations.KDLVectorToNumpyArray(force_vector_tl)
        norm_force = np.linalg.norm(force_nparray_tl)
        if norm_force < self.threshold[0]:
            force_vector_tl = PyKDL.Vector(0, 0, 0)
        fxtl = force_vector_tl[0]  # always <0
        fytl = force_vector_tl[1]
        fztl = force_vector_tl[2]

        # Estimated Force vector  w.r.t. Hole-Frame
        R_tl_base = self.current_tf.M
        R_h_base = self.hole_tf.M
        force_vector_b = R_tl_base * (force_vector_tl)
        force_vector_h = R_h_base.Inverse() * (force_vector_b)
        fxh = force_vector_h[0]
        fyh = force_vector_h[1]
        fzh = force_vector_h[2]

        # System Errors
        w_ep = 1
        err_rhor = w_ep * (-math.fabs(min(0, fxh)))  # (always negative values)
        err_thetay = w_ep * (fztl)
        err_thetaz = w_ep * (fytl)
        error_reg_vector = PyKDL.Vector(err_rhor, err_thetay,  err_thetaz)

        # ||||||| \\\\
        # ||||||| ----  ONLY FOR DEBUG
        # ||||||| ////
        self.debug_data_estimated_twist_pub.publish(
            transformations.TwistFormKDLVector(force_vector_tl))
        self.debug_data_regulation_error_pub.publish(
            transformations.TwistFormKDLVector(error_reg_vector))
        #                //// |||||||
        # ONLY FOR DEBUG ---- |||||||
        #                \\\\ |||||||

        #⬢⬢⬢⬢➤ Back-Regulation Transformation

        #➤➤ Rotation about hole's axes yh
        yh = self.unitY
        ky = 0.001 * self.regulation_p_gain[1]
        thetay = ky * err_thetay
        M_yh = PyKDL.Frame(PyKDL.Rotation().Rot(yh, thetay))

        #➤➤ Rotation about hole's axes zh
        zh = self.unitZ
        kz = 0.001 * self.regulation_p_gain[2]
        thetaz = kz * err_thetaz
        M_zh = PyKDL.Frame(PyKDL.Rotation().Rot(zh, thetaz))

        M_theta = self.hole_tf * M_yh * M_zh * self.hole_tf.Inverse()

        #➤➤ Translation along hole's axes xh
        kx = 0.001 * self.regulation_p_gain[0]
        rhor = kx * err_rhor
        kf = 3
        deltar = (1 / (1 + kf * norm_force)) * self.step_size - math.fabs(rhor)
        M_rho = PyKDL.Frame(deltar * self.ur_axis)

        #⬢⬢⬢⬢➤ Output Transformation

        Tr = M_rho * M_theta
        self.current_tf = Tr * self.current_tf

        if self.ct > 100:
            self.ct = 0
            self.current_tf = self.getTf()
        else:
            self.ct += 1
        return self.current_tf


test = Test()
target_tf = test.getTf()
fz = 0.0

while node.isActive():
    try:
        # force = PyKDL.Vector(0,
        #                      10 * np.random.rand(1).item(),
        #                      10 * np.random.rand(1).item())
          # float(raw_input("fz = "))

        force = PyKDL.Vector(0,
                             0,
                             fz)
        target_tf = test.computeTf(force)
        transformations.broadcastTransform(broadcaster, target_tf, "target_tf_1",
                                           "world", time=rospy.Time.now())

        node.tick()
    except Exception as e:
        print e

#######################################################
#######################################################
#######################################################

# base_tf = PyKDL.Frame()
# current_h_tf = PyKDL.Frame(PyKDL.Rotation.RotX(scipy.pi),
#                            PyKDL.Vector(0, 0, 0.5))

# hole_tf = PyKDL.Frame(PyKDL.Rotation.RotX(scipy.pi / 6),
#                       PyKDL.Vector(0.5, 0.5, 0.5))

# ######################################################
# yh = base_tf.M.UnitY()
# thetay = scipy.pi / 3
# M_yh = PyKDL.Frame(
#     PyKDL.Rotation().Rot(yh, thetay))

# M_a = PyKDL.Frame(0.3 * hole_tf.M.UnitZ())


# while node.isActive():
#     transformations.broadcastTransform(broadcaster, current_h_tf, "current_h_tf",
#                                        "hole_tf", time=rospy.Time.now())
#     transformations.broadcastTransform(broadcaster, hole_tf, "hole_tf",
#                                        "world", time=rospy.Time.now())

#     current_tf = transformations.retrieveTransform(listener, "world",
#                                                    "current_h_tf", none_error=True,
#                                                    time=rospy.Time(0), print_error=True)
#     if current_tf:
#         target_tf = hole_tf * M_yh * hole_tf.Inverse() * current_tf
#         target_tf_1 = M_a * target_tf
#         target_tf_2 = M_a * target_tf_1
#         target_tf_3 = M_a * target_tf_2

#         transformations.broadcastTransform(broadcaster, target_tf, "target_tf",
#                                            "world", time=rospy.Time.now())
#         # transformations.broadcastTransform(broadcaster, target_tf_1, "target_tf_1",
#         #                                    "world", time=rospy.Time.now())
#         # transformations.broadcastTransform(broadcaster, target_tf_2, "target_tf_2",
#         #                                    "world", time=rospy.Time.now())
#         # transformations.broadcastTransform(broadcaster, target_tf_3, "target_tf_3",
#         #                                    "world", time=rospy.Time.now())

#     node.tick()

#######################################################
#######################################################
#######################################################
