#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv

from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.stubs.tf_manager_stub import TfManagerStub
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import sys
import random

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("message_tester")

node.setupParameter("hz", 20)
node.setupParameter("command", "update")
node.setupParameter("tf_name", "name")
node.setHz(node.getParameter("hz"))

message_proxy = SimpleMessageProxy()
tf_manager = TfManagerStub()
msg = String()

counter = 0
while node.isActive():

    if node.getParameter("command") == 'save':
        tf_manager.save(
            saving_name=node.getParameter("tf_name"),
            tf_parent="comau_smart_six/base_link",
            tf_name="comau_smart_six/tool"
        )
    else:
        simple_message = SimpleMessage(
            command=node.getParameter("command"),
            receiver="tf_manager",
            sender=""
        )
        simple_message.setData("tf_name", "comau_smart_six/tool")
        simple_message.setData("tf_parent", "comau_smart_six/base_link")
        simple_message.setData(
            "saving_name", node.getParameter("tf_name"))
        msg.data = simple_message.toString()
        counter += 1
        message_proxy.send(simple_message)
        print("Sending", simple_message.toString())
    node.tick()
    sys.exit(0)
