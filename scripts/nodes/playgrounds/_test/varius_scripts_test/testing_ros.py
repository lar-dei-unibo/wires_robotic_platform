#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv

from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.stubs.tf_manager_stub import TfManagerStub
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
from std_srvs.srv import Trigger, TriggerResponse
import sys
import random

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("ros_tester")

node.setupParameter("hz", 1)
node.setHz(node.getParameter("hz"))

rospy.wait_for_service('part_tf_service')
next_component_tf = rospy.ServiceProxy('part_tf_service', Trigger)
while node.isActive():
    try:
        print next_component_tf()
    except Exception as e:
        is_next_ready = False
        component_id = None
        print "Service call failed: %s" % e

    node.tick()
