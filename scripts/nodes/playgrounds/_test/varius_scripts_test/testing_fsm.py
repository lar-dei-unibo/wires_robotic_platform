#!/usr/bin/env python
# license removed for brevity

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import math
import pprint
import time
import thread
import random
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
from wires_robotic_platform.robots.controllers import RobotController, GeneralIKService, Robot
from wires_robotic_platform.robots.trajectories import FuzzyTrajectoryGenerator
from wires_robotic_platform.robots.market import RobotMarket
from wires_robotic_platform.partdb.proxy import GearboxProxy
from wires_robotic_platform.srv import IKService, IKServiceResponse
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.sfm.machines as machines
from wires_robotic_platform.utils.logger import Logger


from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
import tf
from scipy.interpolate import interp1d
import numpy as np
from std_msgs.msg import String
from transitions import Machine
import tornado.websocket
import tornado.httpserver
import json


class MyWebSocketServer(tornado.websocket.WebSocketHandler):

    def open(self):
        # metodo eseguito all'apertura della connessione
        Logger.log("Connection open")
        # MyWebSocketServer.clients.append(self)

    def check_origin(self, origin):
        return True

    def on_message(self, message):
        # metodo eseguito alla ricezione di un messaggio
        # la stringa 'message' rappresenta il messaggio

        # esempio: stampa il messaggio
        print 'Messaggio ricevuto: %s' % message

    def on_close(self):
        # metodo eseguito alla chiusura della connessione
        Logger.log("Connection close")


def server():

    tornado_app = tornado.web.Application([
        ('/pippo', MyWebSocketServer),
    ])
    server = tornado.httpserver.HTTPServer(tornado_app)
    server.listen(8080)
    print("Server Ready!")
    tornado.ioloop.IOLoop.instance().start()


#thread.start_new_thread(server, ())

rospy.init_node('testing_trajectory_1')
listener = tf.TransformListener()
hz = 1
rate = rospy.Rate(hz)  # 10hz
S_Counter = random.randint(1, 10)

# Gearbox GearboxProxy
GearboxProxy.init(tf_listener=listener)


class Buffer(object):

    def __init__(self):
        self.list = []

    def addElement(self, s):
        self.list.append(s)

###################### state machine 1 ######################


class SFM_BASTARDA(object):
    def __init__(self, buffer):
        self.buffer = buffer

    def on_exit_pippo(self):
        Logger.log("Sto uscendo da pippo")
        self.buffer.addElement("EXIT_PIPPO")

###################### state machine 2 ####################


class SFM_ROBOT_PIPPO(object):

    def __init__(self, buffer):
        self.buffer = buffer
        self.counter = 0

    def resetCounter(self):
        self.counter = S_Counter

    def waitCounter(self):
        self.counter -= 1
        Logger.log("Counter {}".format(self.counter))
        return self.counter > 0

    def on_enter_idle(self):
        Logger.log("Idle")
        self.resetCounter()

    def on_loop_idle(self):
        if not self.waitCounter():
            self.move()

    def on_enter_move(self):
        Logger.log("Move")
        self.resetCounter()

    def on_loop_move(self):
        if not self.waitCounter():
            self.stand()

    def on_enter_steady(self):
        Logger.log("Steady")
        self.resetCounter()

    def on_loop_steady(self):
        if not self.waitCounter():
            self.wait()


sahred_buffer = Buffer()
SFM_MODEL = SFM_ROBOT_PIPPO(sahred_buffer)
sfm = machines.SFMachine(name="~sssfm", model=SFM_MODEL)

start_name = "Start_{}".format(random.randint(0, 10000))

sfm.addState(start_name)
sfm.addState("idle")
sfm.addState("move")
sfm.addState("steady")
sfm.addState("end")
sfm.addTransition("start", start_name, "idle")
sfm.addTransition("move", "idle", "move")
sfm.addTransition("stand", "move", "steady")
sfm.addTransition("wait", "steady", "idle")
sfm.addTransition("stop", "move", "end")
sfm.addTransition("stop", "steady", "end")
sfm.addTransition("stop", "idle", "end")

sfm.create()
sfm.set_state(start_name)
sfm.getModel().start()

while not rospy.is_shutdown():
    # print sfm.getModel().state
    sfm.loop()
    rate.sleep()
