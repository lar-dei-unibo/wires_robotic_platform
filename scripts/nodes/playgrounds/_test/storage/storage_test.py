#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import numpy as np
import mongodb_store_msgs.srv as dc_srv
import mongodb_store.util as dc_util
from mongodb_store.message_store import MessageStoreProxy
from geometry_msgs.msg import Pose, Point, Quaternion
from sensor_msgs.msg import JointState
from wires_robotic_platform.storage.mongo import MessageStorage
from wires_robotic_platform.stubs.tf_manager_stub import *

import StringIO
import time
import struct


if __name__ == '__main__':
    rospy.init_node("example_message_store_client")

    message_database = MessageStorage()
    tf_manager = TfManagerStub()
    
    for i in range(100):
        time.sleep(0.5)
        tf_name = "tf_storage_tmp"
        print message_database.searchByName(tf_name, Pose, single=True) 
        result = message_database.searchByName(tf_name, Pose)
        result[0].position.z += (np.random.rand() - 0.5)*0.001
        message_database.replace(tf_name, result[0],meta=result[1])
        print message_database.searchByName(tf_name, Pose, single=True)
        tf_manager.update()

 

    try:

        pass
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e
