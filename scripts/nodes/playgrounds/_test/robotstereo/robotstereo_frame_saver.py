#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32, Pose
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient
from wires_robotic_platform.proxy.proxy_message import SimpleMessage
from wires_robotic_platform.utils.ros import RosNode
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.vision.cameras import CameraRGB
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import json
import os
import sys
from std_msgs.msg import String

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("robotstereo_frame_saver")
node.setupParameter("hz", 60)
node.setHz(node.getParameter("hz"))
output_path = node.setupParameter("output_path", "/tmp/robotstereo_frames")


if not os.path.exists(output_path):
    os.mkdir(output_path)

current_name = ""


def shotCallback(msg):
    global counter, current_name
    counter = 0
    current_name = msg.data
    print msg, current_name


event_pub = node.createSubscriber("shot_event", String, shotCallback)
pose_pub = node.createPublisher("/robot_pose", Pose)


#⬢⬢⬢⬢⬢➤ Creates Camera Proxy
camera_topic = node.setupParameter(
    "camera_topic",
    "/usb_cam/image_raw/compressed"
)
camera_file = node.getFileInPackage(
    'wires_robotic_platform',
    'data/camera_calibration/microsoft_live_camera.yml'
)
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=camera_topic,
    compressed_image="compressed" in camera_topic
)

save_correction = PyKDL.Frame()
save_correction.M.DoRotY(math.pi)
save_correction.M.DoRotZ(-math.pi * 0.5)
counter = 0


def cameraCallback(frame):
    global counter, current_name

    camera_pose = node.retrieveTransform(
        "camera",
        "comau_smart_six/base_link",
        -1
    )
    if camera_pose == None:
        return

    img = frame.rgb_image.copy()
    cv2.imshow("img", img)
    cv2.waitKey(1)

    if counter == 10:
        camera_pose = camera_pose * save_correction
        pose = transformations.KDLtoNumpyVector(camera_pose)
        print pose
        np.savetxt(os.path.join(output_path, current_name + ".txt"), pose)
        cv2.imwrite(os.path.join(output_path, current_name + ".jpg"), img)
        print "SAVE", os.path.join(output_path, current_name + ".jpg")

    counter = counter + 1


#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    node.tick()
