#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient
from wires_robotic_platform.utils.ros import RosNode
import wires_robotic_platform.utils.transformations as transformations
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import json
from wires_robotic_platform.utils.tfmatrix import TfMatrixCube, TfMatrixHyperCube, TfMatrixSphere

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("matrix_tf_generator")
node.setupParameter("hz", 1)
node.setupParameter("scale_cube", 0.75)
node.setupParameter("offset_cube", [0.0, 0, 0])
node.setHz(node.getParameter("hz"))


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ TF MATRIX ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


#⬢⬢⬢⬢⬢➤ Retrive Chessboard TF
base_frame = None
while base_frame is None:
    base_frame = node.retrieveTransform("tf_storage_chessboard", "/comau_smart_six/base_link", -1)

#⬢⬢⬢⬢⬢➤ Build the TF Matrix
sphere_center_frame = PyKDL.Frame()
sphere_center_frame.M = base_frame.M
sphere_center_frame.p = base_frame.p + PyKDL.Vector(0.00, +0.0, -0.0)  # coords first tf wrt base-tf
matrix_sphere = TfMatrixSphere(
    base=sphere_center_frame,
    size=np.array([8, 5, 1]),
    size_m=np.array([0.8, -0.02, +0.01]),
    indices_offset=np.array([0, 0, 1])
)
matrix_sphere.order(mode="reverse")
scale_factor = node.getParameter("scale_cube")
offset_cube = node.getParameter("offset_cube")
step_size = 0.02 * scale_factor
matrix_shape = [8, 6, 2]
cube_center_frame = PyKDL.Frame()
cube_center_frame.M = base_frame.M
cx = step_size * float(matrix_shape[0] - 1) / 2.0
cy = step_size * float(matrix_shape[1] - 1) / 2.0
cube_center_frame.p = base_frame.p + PyKDL.Vector(cx + offset_cube[0], cy + offset_cube[1], 0.0 + offset_cube[2])    # coords first tf wrt base-tf
matrix_cube = TfMatrixCube(
    base=cube_center_frame,
    size=np.array(matrix_shape),  #
    size_m=np.array([-step_size, -step_size, +step_size * 0.5])
)

print matrix_cube.frames_names
print matrix_sphere.frames_names
while node.isActive():

    for name, frame in matrix_cube.frames_map.iteritems():
        node.broadcastTransform(frame, name, "/comau_smart_six/base_link", node.getCurrentTime())

    for name, frame in matrix_sphere.frames_map.iteritems():
        node.broadcastTransform(frame, name, "/comau_smart_six/base_link", node.getCurrentTime())

    node.tick()
