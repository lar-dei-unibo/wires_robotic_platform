#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32, Pose
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient
from wires_robotic_platform.proxy.proxy_message import SimpleMessage
from wires_robotic_platform.utils.ros import RosNode
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.vision.cameras import CameraRGB
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import json
import sys
import os
from std_msgs.msg import String

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("matrix_tf_follower")
node.setupParameter("hz", 60)
node.setHz(node.getParameter("hz"))

output_path = node.setupParameter(
    'output_path', '/home/daniele/Desktop/temp/robot_stereo_output_frames')
dataset_name = node.setupParameter("dataset_name", "temp1")
calibration = node.setupParameter("calibration", False)

if not os.path.exists(output_path):
    os.makedirs(output_path)
output_path = os.path.join(output_path, dataset_name)
if not os.path.exists(output_path):
    os.makedirs(output_path)

#⬢⬢⬢⬢⬢➤ Creates Camera Proxy
camera_topic = node.setupParameter(
    "camera_topic",
    "/usb_cam/image_raw/compressed"
)
camera_file = node.getFileInPackage(
    'wires_robotic_platform',
    'data/camera_calibration/microsoft_live_camera_focus5.yml'
)
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=camera_topic,
    compressed_image="compressed" in camera_topic
)


event_pub = node.createPublisher("shot_event", String)
pose_pub = node.createPublisher("/robot_pose", Pose)

counter = 0
active_command = None

active_folder = None
active_name = None


def sendNewMessage():
    global counter
    if counter >= len(msgs):
        return
    command = msgs[counter % len(msgs)]
    counter += 1

    if command.startswith("gototf"):
        goToTf(command.split(" ")[1])

    if command.startswith("gotoshape"):
        goToShape(command.split(" ")[1])
    # message = SimpleMessage(
    #     receiver="comau_smart_six_supervisor", command="gototf")
    # message.setData("tf_name", command)
    # message.setData("tool_name", "camera")
    # print("Sending", command)
    # active_command = command_proxy_client.sendCommand(message.toString())


def goToTf(tf_name, tool="camera"):
    message = SimpleMessage(
        receiver="comau_smart_six_supervisor", command="gototf")
    message.setData("tf_name", tf_name)
    message.setData("tool_name", tool)
    print("Sending", message.toString())
    active_command = command_proxy_client.sendCommand(message.toString())


def goToShape(shape_name):
    message = SimpleMessage(
        receiver="comau_smart_six_supervisor", command="gotoshape")
    message.setData("shape_name", shape_name)
    print("Sending", message.toString())
    active_command = command_proxy_client.sendCommand(message.toString())


def done_example(command):
    global active_command, pool_acquisition, pool_counter, active_name, active_folder
    print("Response:", str(command))

    pay = json.loads(command.string_message)

    if 'tf_name' in pay:
        msg = String()
        msg.data = pay['tf_name']
        event_pub.publish(msg)

        active_folder = os.path.join(output_path, pay['tf_name'])
        active_name = pay['tf_name']
        if not os.path.exists(active_folder):
            os.makedirs(active_folder)

        if pool_acquisition == True:
            return

        pool_acquisition = True
    else:
        sendNewMessage()


command_proxy_client = CommandProxyClient("comau_smart_six_supervisor")
command_proxy_client.registerDoneCallback(done_example)


def addScanPose(array, pose_name, use_skew=False):
    array.append(pose_name)
    if use_skew:
        array.append(pose_name + "_s1")
        array.append(pose_name + "_s2")


msgs = []

use_skew = True


def generateMessagesForViewpoint(viewpoint_name, x_steps=10):
    messages = []
    command = "gototf"
    for i in range(1, x_steps, 1):
        messages.append("{} {}_dx_{}".format(command, viewpoint_name, i))
    return messages


for i in range(0, 5):
    msgs.append("gototf viewpoint_{}".format(i))
    msgs.extend(generateMessagesForViewpoint("viewpoint_{}".format(i)))
    if i == 2:
        msgs.append("gotoshape shape_tabletop")

counter = 0

pool_size = 30
pool_start_index = -10
pool_counter = pool_start_index
pool_acquisition = False
pool_imgs = []
save_counter = 0


def cameraCallback(frame):
    global counter, save_counter, current_name, pool_acquisition, pool_size, pool_counter, active_folder, active_name, pool_start_index, pool_imgs

    if calibration:
        camera_pose = node.retrieveTransform(
            "comau_smart_six/link6",
            "comau_smart_six/base_link",
            -1
        )
    else:
        camera_pose = node.retrieveTransform(
            "comau_smart_six/tool",
            "comau_smart_six/base_link",
            -1
        )

    if camera_pose == None:
        return

    # print transformations.KDLtoNumpyVector(camera_pose)

    img = frame.rgb_image.copy()
    cv2.imshow("img", img)
    cv2.waitKey(1)

    if pool_acquisition:
        if pool_counter >= 0:
            if pool_counter < pool_size:

                filename = "{}.jpg".format(str(pool_counter).zfill(5))
                print("Saving filename", filename, active_folder)

                if not calibration:
                    cv2.imwrite(os.path.join(active_folder, filename), img)

                pool_imgs.append(img.copy().astype(np.float32))
                print("Saving", pool_counter)
            else:
                mean_image = np.zeros(pool_imgs[0].shape).astype(np.float32)
                for i in range(0, len(pool_imgs)):
                    print("Acucmulate", np.min(
                        pool_imgs[i]), np.max(pool_imgs[i]))
                    cv2.accumulate(pool_imgs[i], mean_image)
                    print("Acucmulate", np.min(mean_image), np.max(mean_image))
                    #mean_image = mean_image + pool_imgs[i]

                mean_image = mean_image / float(len(pool_imgs))

                diff = pool_imgs[0] - pool_imgs[1]
                print("MINMAX", np.min(diff), np.max(diff))
                diff = (diff - np.min(diff)) / (np.max(diff) - np.min(diff))
                print("MINMAX", np.min(diff), np.max(diff))
                diff = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)

                if calibration:
                    number_name = str(save_counter).zfill(5)
                    cv2.imwrite(os.path.join(
                        output_path,  number_name + ".png"), mean_image)

                    np.savetxt(os.path.join(
                        output_path, "pose_" + number_name + ".txt"),
                        transformations.KDLtoNumpyVector(camera_pose)
                    )
                    save_counter = save_counter + 1
                else:
                    cv2.imwrite(os.path.join(
                        output_path,  active_name + ".png"), mean_image)

                    np.savetxt(os.path.join(
                        output_path, active_name + ".txt"),
                        transformations.KDLtoNumpyVector(camera_pose)
                    )
                pool_acquisition = False
                pool_counter = pool_start_index
                pool_imgs = []
                sendNewMessage()
        else:
            print("Waiting", pool_counter)
        pool_counter = pool_counter + 1

    # if counter == 10:
    #     camera_pose = camera_pose * save_correction
    #     pose = transformations.KDLtoNumpyVector(camera_pose)
    #     print pose
    #     np.savetxt(os.path.join(output_path, current_name + ".txt"), pose)
    #     cv2.imwrite(os.path.join(output_path, current_name + ".jpg"), img)
    #     print "SAVE", os.path.join(output_path, current_name + ".jpg")



#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


# for j in range(0, 1):
#     ni = i
#     nj = j
#     if i % 2 == 0:
#         print(i, j)
#     else:
#         ni = i
#         nj = m_size - j - 1
#         print(i, m_size - j - 1)
#     addScanPose(msgs, "matrix_frame_{}_{}".format(nj, ni))

# sys.exit(0)

print msgs

sendNewMessage()
while node.isActive():

    # wrist = node.retrieveTransform(
    #     "comau_smart_six/link6",
    #     "comau_smart_six/base_link",
    #     -1
    # )
    # if wrist:
    #     pose = transformations.KDLToPose(wrist)
    #     pose_pub.publish(pose)
    node.tick()
