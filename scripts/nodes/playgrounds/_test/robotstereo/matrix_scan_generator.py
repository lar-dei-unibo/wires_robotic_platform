#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient
from wires_robotic_platform.proxy.proxy_message import SimpleMessage
from wires_robotic_platform.utils.ros import RosNode
import wires_robotic_platform.utils.transformations as transformations
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import json
import collections


class Viewpoint(object):

    def __init__(self, node, base_frame_name, viewpoint_name, original_viewpoint, x_steps=10, small_baseline=0.02):
        self.node = node
        self.base_frame_name = base_frame_name
        self.viewpoint_name = viewpoint_name
        self.original = transformations.KDLFromArray(
            original_viewpoint, fmt='XYZQ')

        self.sub_viewpoints_x = []
        for x in range(1, x_steps + 1):
            d_frame = PyKDL.Frame(PyKDL.Vector(
                float(x) * small_baseline, 0.0, 0.0))
            d_frame = self.original * d_frame
            self.sub_viewpoints_x.append(d_frame)

    def broadcast(self):
        time = node.getCurrentTime()
        self.node.broadcastTransform(
            self.original,
            self.viewpoint_name,
            self.base_frame_name,
            time
        )

        for ix in range(0, len(self.sub_viewpoints_x)):
            frame = self.sub_viewpoints_x[ix]
            frame_name = self.viewpoint_name + "_dx_{}".format(ix + 1)
            self.node.broadcastTransform(
                frame,
                frame_name,
                self.base_frame_name,
                time
            )


    #⬢⬢⬢⬢⬢➤ NODE
node = RosNode("matrix_tf_generator")
node.setupParameter("hz", 1)
node.setHz(node.getParameter("hz"))

base_frame_name = "/comau_smart_six/base_link"
counter = 0
active_command = None

###############################################################################
###############################################################################
# PROXY COMMANDS
###############################################################################
###############################################################################


def goToTf(tf_name, tool="camera"):
    message = SimpleMessage(
        receiver="comau_smart_six_supervisor", command="gototf")
    message.setData("tf_name", tf_name)
    message.setData("tool_name", tool)
    print("Sending", message.toString())
    active_command = command_proxy_client.sendCommand(message.toString())


def goToShape(shape_name):
    message = SimpleMessage(
        receiver="comau_smart_six_supervisor", command="gotoshape")
    message.setData("shape_name", shape_name)
    print("Sending", message.toString())
    active_command = command_proxy_client.sendCommand(message.toString())


def done_example(command):
    global active_command, pool_acquisition, pool_counter, active_name, active_folder
    print("Response:", str(command))

    # pay = json.loads(command.string_message)

    # msg = String()
    # msg.data = pay['tf_name']
    # event_pub.publish(msg)

    # active_folder = os.path.join(output_path, pay['tf_name'])
    # active_name = pay['tf_name']
    # if not os.path.exists(active_folder):
    #     os.makedirs(active_folder)

    # if pool_acquisition == True:
    #     return

    # pool_acquisition = True


command_proxy_client = CommandProxyClient("comau_smart_six_supervisor")
command_proxy_client.registerDoneCallback(done_example)


###############################################################################
###############################################################################
# VIEWPOINTS
###############################################################################
###############################################################################

poses = []
poses.append([0.81928932,  0.14268584,  0.37118631,  0.70514023, -0.70461838,  0.06080856,
              -0.05091665])

poses.append([1.03196366,  0.48745059,  0.07321638, -
              0.01019172,  0.94983285, -0.31250855,   0.0072173])

poses.append([0.77980099,  0.46249864,  0.17069017, -0.32893994, 0.89495792, -0.25816936,
              0.15555518])

poses.append([0.54947223, -0.29918233,  0.15544419,
              0.84342557, -0.44312926,  0.10518696,  -0.28496573])

poses.append([0.87108062, -0.47263837,  0.18041146,  0.95317102,  0.0053322,   0.01294042,
              -0.3021078])

poses.append([1.19355212, -0.17116992,  0.0281179,   0.71693953,  0.68480104, -0.08397775,
              -0.09996492])

viewpoints = []
for i in range(0, len(poses)):
    viewpoints.append(Viewpoint(node, base_frame_name,
                                "viewpoint_{}".format(i), poses[i]))


###############################################################################
###############################################################################
# MAIN LOOP
###############################################################################
###############################################################################


while node.isActive():

    camera_pose = node.retrieveTransform(
        "camera", base_frame_name, -1)
    if camera_pose != None:
        print transformations.KDLtoNumpyVector(camera_pose)

        for v in viewpoints:
            v.broadcast()

    node.tick()
