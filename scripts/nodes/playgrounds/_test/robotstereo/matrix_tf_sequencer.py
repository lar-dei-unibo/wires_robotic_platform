#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient
from wires_robotic_platform.proxy.proxy_message import SimpleMessage
from wires_robotic_platform.utils.ros import RosNode
import wires_robotic_platform.utils.transformations as transformations
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import json
from wires_robotic_platform.utils.tfmatrix import TfMatrixCube
from std_msgs.msg import Int32
from geometry_msgs.msg import Pose
import threading

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("matrix_tf_sequencer")
node.setupParameter("hz", 30)
node.setHz(node.getParameter("hz"))
delay_time = node.setupParameter("delay", 2)

moving_pub = node.createPublisher("~moving_flag", Int32)
pose_pub = node.createPublisher("~tool_pose", Pose)

counter = 0
active_command = None

moving_flag = -1


def delayedFunction(secs, func):
    #⬢⬢⬢⬢⬢➤ Async Sleep
    print("Start Delay")
    time.sleep(secs)
    print("Delay end")
    func()
    return


def goToTf(tf_name, tool="camera"):
    #⬢⬢⬢⬢⬢➤ Go To Tf Helper Function
    global command_proxy_client, moving_flag
    message = SimpleMessage(
        receiver="comau_smart_six_supervisor", command="gototf")
    message.setData("tf_name", tf_name)
    message.setData("tool_name", tool)
    print("Sending", message.toString())
    active_command = command_proxy_client.sendCommand(message.toString())
    moving_flag = 1


def sendNewMessage():
    #⬢⬢⬢⬢⬢➤ Send New Message to Proxy
    global counter, matrix_cube
    frame_name, _ = matrix_cube.pickNextFrame()
    goToTf(frame_name)


def done_example(command):
    #⬢⬢⬢⬢⬢➤ Action Callback
    global moving_flag
    print("Response:", str(command))
    moving_flag = 0
    thread = threading.Thread(target=delayedFunction, args=(delay_time, sendNewMessage))
    thread.start()


command_proxy_client = CommandProxyClient("comau_smart_six_supervisor")
command_proxy_client.registerDoneCallback(done_example)


base_frame = PyKDL.Frame()
# base_frame.p = PyKDL.Vector(0.25, 0.615, -0.16)
# base_frame.M = PyKDL.Rotation.RPY(0, math.pi, math.pi)
base_frame.p = PyKDL.Vector(0.80, -0.1, 0.2)
base_frame.M = PyKDL.Rotation.RPY(0, math.pi, 0 * math.pi)

# Create a matrix Cube
# matrix_cube = TfMatrixCube(
#     base=base_frame,
#     size=np.array([40, 1, 4]),
#     size_m=np.array([0.02, 0.00, -0.15])
# )

# Create an matrix Cube with rotations about z
matrix_cube = TfMatrixHyperCube(
    base=base_frame,
    size_p=np.array([30, 30, 5]),
    size_m_p=np.array([-0.02, -0.02, -0.02]),
    size_r=np.array([1, 1, 10]),
    size_m_r=np.array([0., 0., 0.3])
)
matrix_cube.order(mode="xyzrpy")


sendNewMessage()

moving_msg = Int32()
while node.isActive():
    moving_msg.data = moving_flag
    moving_pub.publish(moving_msg)

    tool_pose = Pose()

    tool_frame = node.retrieveTransform("/comau_smart_six/tool", "/comau_smart_six/base_link", -1)
    if tool_frame is not None:
        tool_pose = transformations.KDLToPose(tool_frame)

    pose_pub.publish(tool_pose)
    node.tick()
