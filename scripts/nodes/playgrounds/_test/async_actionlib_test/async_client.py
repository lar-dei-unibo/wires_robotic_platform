#! /usr/bin/env python
import rospy
import actionlib
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.msg import PippoGoal, PippoAction
from actionlib_msgs.msg import GoalStatus


def goalStatusString(state):
    for member in GoalStatus.__dict__.iteritems():
        if state == getattr(GoalStatus, member[0]):
            return member[0]


class PippoClient:

    def __init__(self, name):
        self.action_client = actionlib.SimpleActionClient(
            name, PippoAction)
        self.action_client.wait_for_server()
        self.current_goal = None
        self.current_feedback = 0

    def generateNewGoal(self, value, name):
        """ Genera un nuovo goal con la coppia nome/valore e lo invia al server"""
        self.current_goal = PippoGoal(pippo_value=value, pippo_name=name)
        self.action_client.send_goal(self.current_goal,
                                     done_cb=self.done_callback,
                                     active_cb=self.active_callback,
                                     feedback_cb=self.feedback_callback)

    def getCurrentState(self):
        """ Ritorna la stringa che rappresenta lo stato corrente del Goal """
        # return
        # actionlib.SimpleGoalState.to_string(self.action_client.simple_state)
        return goalStatusString(self.action_client.get_state())

    def stepForward(self):
        """ Trigger esterno temporizzato """
        if self.current_goal:
            Logger.log("Current state: {}; Current feedback: {}".format(
                self.getCurrentState(), self.current_feedback))

    def feedback_callback(self, feedback):
        """ Callback chiamata sui feedback """
        self.current_feedback = feedback.pippo_feedback

    def done_callback(self, status, result):
        """ Callback chiamata al completamento del goal """
        Logger.log("Done !! : {} {}".format(status, result))

    def active_callback(self):
        """ Callback chiamata appena il Goal diventa attivo sul server """
        Logger.log("The goal is active!")


rospy.init_node('action_Pippo_client')
value = rospy.get_param('~value', default=4)

# Client
client = PippoClient("AsyncPippoServer")
hz = 15
rate = rospy.Rate(hz)  # 10hz

Logger.log("Sending goal")
client.generateNewGoal(value, "pino")

while not rospy.is_shutdown():
    rate.sleep()
    client.stepForward()
