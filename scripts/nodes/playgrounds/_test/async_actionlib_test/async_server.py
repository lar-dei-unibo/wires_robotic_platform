#! /usr/bin/env python
import rospy
import actionlib
from wires_robotic_platform.utils.logger import Logger

#from wires_robotic_platform.msg import DoDishesAction
from wires_robotic_platform.msg import PippoResult, PippoFeedback, PippoAction


class PippoServer:
    def __init__(self, name):
        self.action_server = actionlib.SimpleActionServer(
            name, PippoAction, auto_start=False)

        self.action_server.register_goal_callback(self.goal_callback)
        self.action_server.register_preempt_callback(self.preempt_callback)

        # internal
        self.current_goal = None
        self.internal_counter = 0

    def manageNewGoal(self, goal):
        """ Cosa succede nel modello quando inserisco un nuovo Goal. """
        self.current_goal = goal
        self.internal_counter = 0

    def consumeCurrentGoal(self):
        """Business logic di consumo di un Goal. Qui semplice decremento/incremento variabili"""
        if self.current_goal != None:
            if self.current_goal.pippo_value > 0:
                self.current_goal.pippo_value -= 1
                self.internal_counter += 1

                # Feedback
                feedback = PippoFeedback()
                feedback.pippo_feedback = self.internal_counter
                self.action_server.publish_feedback(feedback)

                return True
            else:
                return False
        else:
            return False

    def stepForward(self):
        """ Trigger esterno temporizzato """
        if self.current_goal != None:
            Logger.log("Current goal value: {}".format(
                self.current_goal.pippo_value))
            goal_empty = not self.consumeCurrentGoal()
            if goal_empty:
                result = PippoResult()
                result.pippo_result = self.internal_counter
                self.action_server.set_succeeded(result=result)
                self.current_goal = None
        else:
            Logger.log("No Goal to execute!")

    def preempt_callback(self):
        """ Callback chiamata quando c'e' gia un Goal attivo e ne arriva uno nuovo """
        if self.action_server.is_active():
            self.action_server.set_preempted()
            self.manageNewGoal(self.action_server.accept_new_goal())
            Logger.log("Preemption! Last goal deleted!")

    def goal_callback(self):
        """ Callback chiamata SEMPRE all'arrivo di un nuovo Goal """

        if not self.action_server.is_active():
            Logger.log("New goal accepted!")
            self.manageNewGoal(self.action_server.accept_new_goal())
        else:
            Logger.warning("New goal not accepted! Another goal is active")


rospy.init_node('action_Pippo_server')
hz = 10
rate = rospy.Rate(hz)  # 10hz

# Action Server
server = PippoServer("AsyncPippoServer")
server.action_server.start()

while not rospy.is_shutdown():
    rate.sleep()
    server.stepForward()
