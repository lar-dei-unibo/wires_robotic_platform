#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
from wires_robotic_platform.msg import UiEvent, UiFeedback


def talker():
    pub = rospy.Publisher('/ui/feedback', UiFeedback, queue_size=10)
    rospy.init_node('uitester', anonymous=True)
    rate = rospy.Rate(10)  # 10hz

    while not rospy.is_shutdown():
        feedback = UiFeedback()
        feedback.names = ["a", "b"]
        feedback.values = [1.1, 2.0]

        pub.publish(feedback)
        rate.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
