#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger 
import wires_robotic_platform.utils.visualization as visualization 
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

import message_filters 
import cv2 
import rospkg
import numpy as np
import math
import sys
import random
from filterpy.kalman import KalmanFilter
from filterpy.common import Q_discrete_white_noise


u = 0


# def sensor_cb(msg):
#     global u
#     u = msg.linear.z


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("tf_manager")

node.setupParameter("hz", 250)
node.setHz(node.getParameter("hz"))
node_rate = node.getParameter("hz")

pub = node.createPublisher("signal_to_filter", Float32)
# sub = node.createSubscriber("/atift", Twist, sensor_cb)
pub1 = node.createPublisher("signal_filtered", Float32)
mag = 0.1
f = 0.1


#⬢⬢⬢⬢⬢➤ FILTER
my_filter = KalmanFilter(dim_x=2, dim_z=1)
my_filter.x = np.array([[0.],
                        [0.]])       # initial state (location and velocity)

my_filter.F = np.array([[1., 0.1],
                        [0., 1.]])    # state transition matrix - A

my_filter.H = np.array([[1., 0.]])    # Measurement function - C
my_filter.P *= 0.1                # covariance matrix
my_filter.R = 0.2                      # state uncertainty
dt = 1 / node_rate
my_filter.Q = Q_discrete_white_noise(2, dt, 0.001)  # process uncertainty

msg = Float32()
while node.isActive():
    u = math.sin(2 * np.pi * f * node.getCurrentTimeInSecs()) + \
        random.uniform(-mag, mag)
    msg.data = u
    pub.publish(msg)

    my_filter.predict()
    my_filter.update(u)
    x = my_filter.x

    msg.data = x[0]
    pub1.publish(msg)

    node.tick()
