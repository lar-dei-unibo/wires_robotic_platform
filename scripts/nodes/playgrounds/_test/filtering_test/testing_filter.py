#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger 
import wires_robotic_platform.utils.visualization as visualization 
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

import message_filters 
import cv2 
import rospkg
import numpy as np
import math
import sys
import random
 

import matplotlib.pyplot as plt
# ... butter_lowpass
from scipy.signal import butter, lfilter, filtfilt, freqz  

class LowPassFilter(object):
    def __init__(self,w_size): 
        self.w_size = w_size
        self.data_window = np.zeros(w_size) 

    def _update_data(self,new_data): 
        tmp = np.append(self.data_window, [new_data], axis=0) 
        self.data_window = np.delete(tmp, 0, 0)  
        
    def build(self,cutoff, fs, order=5):
        nyq = 0.5 * fs
        normal_cutoff = cutoff / nyq
        self.b, self.a = butter(order, normal_cutoff, btype='low', analog=False) 
  
    def filter(self,data):
        self._update_data(data)
        y = lfilter(self.b, self.a, self.data_window, axis=0)
        # y = filtfilt(b, a, data, axis=0)
        return y[self.w_size-1]
    
##########################################################################
##########################################################################
##########################################################################

class TestingSignal(object):
    def __init__(self): 
        self.sample = 0
        self.window_signal = np.zeros(1000)

    def update(self,msg): 
        self.sample = msg.linear.y
        self.update_window(self.sample)
        print "sensor={}".format(self.sample) 

    def update_window(self, sample): 
        tmp = np.append(self.window_signal, [sample], axis=0) 
        self.window_signal = np.delete(tmp, 0, 0) 
     
    def signal(self,t):
        mag = 2.0
        f = 0.1
        mag_n = mag/10
        f_n = f*10
        u = mag*math.sin(2 * np.pi * f *t) + mag_n*math.sin(2 * np.pi * f_n * t)
        # print "signal={}".format(u)
        return u

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("tf_manager")

node.setupParameter("hz", 100)
node.setHz(node.getParameter("hz"))
node_rate = node.getParameter("hz")

raw_signal = TestingSignal()
filterd = TestingSignal()

sub = node.createSubscriber("/wire_twist_pred", Twist, raw_signal.update)
pub = node.createPublisher("/signal_to_filter", Float32)
pub1 = node.createPublisher("/signal_filtered", Float32)



#⬢⬢⬢⬢⬢➤ BUILD FILTER  
# order = 12
# fs = 100.0
# cutoff = 6.0  
# N = 50
order = 8 
fs = 120.0
cutoff = 3.5
N = 10
lp_filter = LowPassFilter(N)
lp_filter.build(cutoff, fs, order)
  

while node.isActive():
    # t = node.getCurrentTimeInSecs()
    # u = signal(t)
    # msg = Float32()
    # msg.data = u
    # pub.publish(msg)
  
    #⬢⬢⬢⬢⬢➤ FILTERING 
    y = 10000*lp_filter.filter(raw_signal.sample)
    filterd.update_window(y)

    # print data_filtered
    msg = Float32()
    msg.data = y
    pub1.publish(msg)
  
    node.tick()

#######
dt = 0.01 
Fs = 1/dt
time = np.arange(0, 10, dt)
s = raw_signal.window_signal

plt.figure()
plt.subplot(3, 2, 1)
plt.plot(time, s)

plt.subplot(3, 2, 3)
plt.magnitude_spectrum(s, Fs=Fs)

plt.subplot(3, 2, 4)
plt.magnitude_spectrum(s, Fs=Fs, scale='dB')

plt.subplot(3, 2, 5)
plt.angle_spectrum(s, Fs=Fs)

plt.subplot(3, 2, 6)
plt.phase_spectrum(s, Fs=Fs)

#######
ss = filterd.window_signal

plt.figure()
plt.subplot(3, 2, 1)
plt.plot(time, ss)

plt.subplot(3, 2, 3)
plt.magnitude_spectrum(ss, Fs=Fs)

plt.subplot(3, 2, 4)
plt.magnitude_spectrum(ss, Fs=Fs, scale='dB')

plt.subplot(3, 2, 5)
plt.angle_spectrum(ss, Fs=Fs)

plt.subplot(3, 2, 6)
plt.phase_spectrum(ss, Fs=Fs)

plt.show()