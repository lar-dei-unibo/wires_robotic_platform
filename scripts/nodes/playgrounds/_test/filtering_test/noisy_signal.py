#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger 
import wires_robotic_platform.utils.visualization as visualization 
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

import message_filters 
import cv2 
import rospkg
import numpy as np
import math
import sys
import random
 

# ... butter_lowpass
from scipy.signal import butter, lfilter, filtfilt, freqz 
  
u = 0
   
#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("noise_signal_gen")

node.setupParameter("hz", 250)
node.setHz(node.getParameter("hz"))
node_rate = node.getParameter("hz")

pub = node.createPublisher("signal_to_filter", Float32) 
mag = 0.1
f = 0.1
 
#⬢⬢⬢⬢⬢➤ FILTER 

msg = Float32()
while node.isActive():
    u = math.sin(2 * np.pi * f * node.getCurrentTimeInSecs()) + \
        random.uniform(-mag, mag)
    msg.data = u
    pub.publish(msg)
  

    node.tick()
