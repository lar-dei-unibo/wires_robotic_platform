#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header, Float64, Float32
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger 
import wires_robotic_platform.utils.visualization as visualization 
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy

import message_filters 
import cv2 
import rospkg
import numpy as np
import math
import sys
import random
 

# ... butter_lowpass
from scipy.signal import butter, lfilter, filtfilt, freqz 

y = 0

def sign_callback(msg):
    global y
    y = msg.data[0]

sign_sub = message_filters.Subscriber('signal_to_filter', Float32) 

ts = message_filters.TimeSynchronizer([sign_sub], queue_size=10)
ts.registerCallback(sign_callback) 
 
 
 
# def sensor_cb(msg):
#     global u
#     u = msg.linear.z
 
#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("filter")

node.setupParameter("hz", 250)
node.setHz(node.getParameter("hz"))
node_rate = node.getParameter("hz")
 
pub1 = node.createPublisher("signal_filtered", Float32) 
 

#⬢⬢⬢⬢⬢➤ FILTER 

msg_filtered = Float32()
while node.isActive():  

    msg_filtered.data = y
    pub1.publish(msg_filtered)

    node.tick()
