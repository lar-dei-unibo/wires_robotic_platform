#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv

from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("scane_tf_generator")

node.setupParameter("hz", 3)
node.setupParameter("base_tf", "world")
node.setupParameter("target_tf", "world")
node.setHz(node.getParameter("hz"))


def generateViewpoint(z, s, alpha, beta):
    rot_alpha = PyKDL.Frame()
    rot_beta = PyKDL.Frame()
    t_z = PyKDL.Frame(PyKDL.Vector(0, 0, z))
    t_x = PyKDL.Frame(PyKDL.Vector(s, 0, 0))
    rot_alpha.M.DoRotY(alpha)
    rot_beta.M.DoRotZ(beta)
    final = PyKDL.Frame()
    final = final * rot_beta
    final = final * t_x
    final = final * t_z
    final = final * rot_alpha

    return final


def generateViewpoints(angle_step=20, z=0.4, s=0.3, alpha=0.707):
    viewpoints = []
    steps = 360 / angle_step
    for a in range(0, steps):
        v = generateViewpoint(z, s, alpha, a * angle_step * np.pi / 180.0)
        print(a * 10 * np.pi / 180.0)
        viewpoints.append(v)
    return viewpoints


views = generateViewpoints(angle_step=45, z=0.2, s=0.3, alpha=0.3)
views.extend(generateViewpoints(angle_step=45, z=0, s=0.3, alpha=0.6))

tool_transform = PyKDL.Frame()
tool_transform.M.DoRotY(np.pi)
tool_transform.M.DoRotZ(np.pi * 0.5)

base_transform = PyKDL.Frame()
base_transform.p = PyKDL.Vector(0.8, 0, 0.0)

skew_transform1 = PyKDL.Frame()
skew_transform1.M.DoRotZ(np.pi / 4)
skew_transform2 = PyKDL.Frame()
skew_transform2.M.DoRotZ(-np.pi / 4)

trans_transform = PyKDL.Frame()
trans_transform.p.z(0.12)

trans_transform2 = PyKDL.Frame()
trans_transform2.p.z(-0.12)

while node.isActive():

    target_tf = node.retrieveTransform(
        node.getParameter("base_tf"),
        node.getParameter("target_tf"),
        node.getCurrentTime()
    )

    counter = 0
    if target_tf:
        node.broadcastTransform(
            base_transform,
            "scan_focus",
            node.getParameter("base_tf"),
            node.getCurrentTime()
        )

        for v in views:
            newv = v * tool_transform
            node.broadcastTransform(
                newv,
                "scan_tf_{}".format(counter),
                "scan_focus",
                node.getCurrentTime()
            )

            s2 = newv * trans_transform2
            node.broadcastTransform(
                s2,
                "scan_tf_{}_b".format(counter),
                "scan_focus",
                node.getCurrentTime()
            )
            #s1 = newv * skew_transform1
            # node.broadcastTransform(
            #    s1,
            #    "scan_tf_{}_s1".format(counter),
            #    "scan_focus",
            #    node.getCurrentTime()
            #)
            #s2 = newv * skew_transform2
            # node.broadcastTransform(
            #    s2,
            #    "scan_tf_{}_s2".format(counter),
            #    "scan_focus",
            #    node.getCurrentTime()
            #)

            counter += 1

    # marker_pub.publish(plane)
    node.tick()
