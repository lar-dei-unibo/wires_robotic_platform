#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv

from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("tf_path_generator")

node.setupParameter("hz", 15)
node.setupParameter("base_frame", "world")
node.setHz(node.getParameter("hz"))


center_frame = PyKDL.Frame()
center_frame.p = PyKDL.Vector(0.3, 0.3, 0.3)
center_frame.M = PyKDL.Rotation.Quaternion(0, 0, 0, 1)

random_t = 0.002
random_r = 0.01

while node.isActive():

    random_frame = PyKDL.Frame()
    random_frame.p = PyKDL.Vector(
        np.random.uniform(-random_t, random_t),
        np.random.uniform(-random_t, random_t),
        np.random.uniform(-random_t, random_t)
    )
    random_frame.M = PyKDL.Rotation.RPY(
        np.random.uniform(-random_r, random_r),
        np.random.uniform(-random_r, random_r),
        np.random.uniform(-random_r, random_r)
    )

    center_frame = center_frame * random_frame
    node.broadcastTransform(
        center_frame,
        node.getParameter("random_tf_name"),
        "world",
        node.getCurrentTime()
    )

    # marker_pub.publish(plane)
    node.tick()
