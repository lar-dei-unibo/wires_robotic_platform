#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import scipy
import json
import numpy as np
import time
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode
 
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String , Float32 , Float64MultiArray
from geometry_msgs.msg import Twist
from sensor_msgs.msg import JointState
from std_srvs.srv import Trigger, TriggerResponse


def estimate_gen_cb(msg):
    global estimate
    s=0.0
    for i in range(0,4):
        
        s += msg.data[i*4]-msg.data[i*4+3]

    estimate=s

if __name__ == '__main__':


    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("tactile_estimation_publisher")
    node.setHz(100)


    estimate_pub = node.createPublisher("/insertion_control/tact_estimate", Twist )
    node.createSubscriber("/tactile", Float64MultiArray , estimate_gen_cb)
 
 
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    
    tactile_twist=Twist()
    tactile_twist = transformations.TwistFormKDLVector(PyKDL.Vector(0.0,0.0,0.0)) 
    estimate=0.0


    while node.isActive():
        tactile_twist.linear.y = estimate
        #print(tactile_twist.linear)
        estimate_pub.publish(tactile_twist)

    node.tick()
