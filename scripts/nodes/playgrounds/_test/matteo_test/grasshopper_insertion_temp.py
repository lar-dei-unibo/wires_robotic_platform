#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
import time
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode
 
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String
from std_msgs.msg import Float32
from geometry_msgs.msg import Pose
from sensor_msgs.msg import JointState
from std_srvs.srv import Trigger, TriggerResponse

from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData
from wires_robotic_platform.param.global_parameters import Parameters

#⬢⬢⬢⬢⬢➤ ROBOT
robot_name = Parameters.get("BONMET_NAME")  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
 

class GrasshopperInsProxyServer(object):
    def __init__(self):
        self.command_proxy_server_name = "grasshopper_insertion_supervisor"
        self.command_server = CommandProxyServer(self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(command) 
                    setTrigger()
                else:
                    self.command_server_last_message = None
            else:
                self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            print "\nTask Ended!!! \t Success: {}".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def setTrigger():
    global is_task_active
    is_task_active = True

def setTaskEnd():
    global is_task_active
    is_task_active = False 
    proxy_server.sendResponse(True)


def newtact(msg):
    global contact_force_estim
    contact_force_estim = msg.data

def current_pos_cb(msg):
    global pos
    pos = list(msg.position)
    print(pos[1])



# def getFrame(frame_id, parent_id="world"):
#     tf = None
#     ct = 50
#     while tf is None or ct > 50:
#         ct -= 1
#         try:
#             tf = node.retrieveTransform(frame_id=frame_id,
#                                         parent_frame_id=parent_id,
#                                         time=-1)
#         except Exception as e:
#             tf = None
#             print "Waiting for tf..."
#     return tf


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("Grasshopper_insertion")
    node.setHz(1)

    node.setupParameter("delta",1)
    delta = node.getParameter("delta")
    node.setupParameter("insert",False)
    insert = node.getParameter("insert")
    node.setupParameter("is_task_active",False)
    is_task_active = node.getParameter("is_task_active")
    node.setupParameter("thr",1)
    force_thr = node.getParameter("thr")
 

    proxy_server = GrasshopperInsProxyServer()


    cartesian_pub=node.createPublisher("/cartesian/joint_command", JointState )

    node.createSubscriber("/tactilesim", Float32, newtact) #/insertion_control/estimated_twist
    node.createSubscriber("/cartesian/joint_states", JointState, current_pos_cb)
 
    
    # component_tf = None
    # component_id = None
    # is_next_ready = False

    #  = node.("component_id", String)

    # rospy.wait_for_service('part_tf_service')
    # next_component_tf = rospy.ServiceProxy('part_tf_service', Trigger)

    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    
    step=JointState()
    step.name=['X','Y','Z']
    adv = 0.001 *delta
    contact_force_estim=0
    pos_y=0.0
    pos = [0,0,0] 

    while node.isActive():
        # contact_force_estim= node.getData("/tactilesim").data
        # pos_y = node.getData("/cartesian/joint_states").position[1] 
        if is_task_active:
            p = pos
            if insert: 
                
                if contact_force_estim<force_thr:# and pos[1]>0.0:
                    p[1] -= adv
                    step.position = p
                    cartesian_pub.publish(step)
                    print("advanced")
                    if p[1]<0.001: 
                        setTaskEnd()  
                        print("insertion finished")
                elif contact_force_estim>=force_thr:
                    # step.position = pos
                    # cartesian_pub.publish(step)
                    setTaskEnd()   
                    print("stopped")
            else: 
                p[1] += adv
                step.position = p
                cartesian_pub.publish(step)
                print("retrieve")
                if p[1]>0.043: 
                    print("setup finished")
                    setTaskEnd() 
            
            # time.sleep(0.1)

    node.tick()

        # if component_tf is not None:
        #     component_visual_tf = component_tf
        #     # TF BROADCAST
        #     node.broadcastTransform(component_visual_tf,
        #                             "target_component_scan",
        #                             "world",
        #                             node.getCurrentTime())
        #     component_id_pub.publish(component_id)
        #     proxy_server.sendResponse(True)
        # else:
        #     print "component tf is None"

        
