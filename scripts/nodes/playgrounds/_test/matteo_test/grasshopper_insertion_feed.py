#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
import time
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode
 
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String
from std_msgs.msg import Float32
from geometry_msgs.msg import Pose , Twist
from sensor_msgs.msg import JointState
from std_srvs.srv import Trigger, TriggerResponse

from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData
from wires_robotic_platform.param.global_parameters import Parameters


#⬢⬢⬢⬢⬢➤ ROBOT
robot_name = Parameters.get("BONMET_NAME")  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
 

class GrasshopperInsProxyServer(object):
    def __init__(self):
        self.command_proxy_server_name = "grasshopper_perform_insertion_supervisor"
        self.command_server = CommandProxyServer(self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(command) 
                    setTrigger()
                else:
                    self.command_server_last_message = None
            else:
                self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            print "\nTask Ended!!! \t Success: {}".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def setTrigger():
    global is_task_active
    global insert
    global flag
    global notgoal

    is_task_active = True
    insert=True
    flag=False
    notgoal=True

    while contact_force_estim is None or abs(contact_force_estim) > 0.01:
        print contact_force_estim
        reset_tact.publish(reset_msg) 
        time.sleep(1.0)  
        print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"



def setTaskEnd():
    global is_task_active
    is_task_active = False 
    proxy_server.sendResponse(True)


def tactile_cb(msg):
    global contact_force_estim
    global flag
    global insert
    contact_force_estim = msg.linear.y
    if contact_force_estim>=force_thr_high or contact_force_estim<=force_thr_low :
        flag=True
        insert=False
    

def current_pos_cb(msg):
    global current_pos
    current_pos = list(msg.position)
    #print(current_pos[1])


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("grasshopper_insertion")
    node.setHz(100)

    node.setupParameter("delta",1)
    delta = node.getParameter("delta")
    node.setupParameter("insert",True)
    insert = node.getParameter("insert")
    node.setupParameter("is_task_active",False)
    is_task_active = node.getParameter("is_task_active")
    node.setupParameter("thr_high",+0.05) #2
    force_thr_high = node.getParameter("thr_high")
    node.setupParameter("thr_low",-0.05) #-10
    force_thr_low = node.getParameter("thr_low")
 

    proxy_server = GrasshopperInsProxyServer()
    

    cartesian_pub=node.createPublisher("/cartesian/joint_command", JointState )
    reset_tact=node.createPublisher("/tactile_reset", String )


    node.createSubscriber("/insertion_control/pippo", Twist, tactile_cb)
    node.createSubscriber("/cartesian/joint_states", JointState, current_pos_cb)
    

    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    alt=Trigger()
    step=JointState()
    reset_msg=String()
    step.name=['X','Y','Z']
    contact_force_estim=None
    notgoal=True
    current_pos = [0,0,0]
   
    flag=False
    reset_msg.data=''
    reset_tact.publish(reset_msg)


    # while contact_force_estim is None or abs(contact_force_estim) > 0.01:
    #     print contact_force_estim
    #     reset_tact.publish(reset_msg) 
    #     time.sleep(1.0)  
    # print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"


    while node.isActive():
        if is_task_active:
            print insert
            p = list(current_pos)
            if insert:  
                if notgoal:
                    p[1] = 0.001
                    step.position = p
                    cartesian_pub.publish(step)
                    notgoal=False
                if current_pos[1]<0.0011:
                    setTaskEnd()
                    print"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"   
                    print("insertion finished")
            else:
                if flag:
                    p = list(current_pos)
                    step.position = p
                    cartesian_pub.publish(step)
                    setTaskEnd()
                    flag=False
                print"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
                print("stop")
                
        node.tick()

        
