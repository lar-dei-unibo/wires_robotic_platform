#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
import time
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode

from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String
from std_msgs.msg import Float32
from geometry_msgs.msg import Pose, Twist
from sensor_msgs.msg import JointState
from std_srvs.srv import Trigger, TriggerResponse

from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData
from wires_robotic_platform.param.global_parameters import Parameters


#⬢⬢⬢⬢⬢➤ ROBOT
robot_name = Parameters.get("BONMET_NAME")  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Hb_open = "\033[94m\033[1m\033[4m"
Hg_open = "\033[92m\033[1m\033[4m"
Hr_open = "\033[91m\033[1m\033[4m"
H_close = "\033[0m"

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


class GrasshopperInsTacProxyServer(object):
    def __init__(self):
        self.command_proxy_server_name = "grasshopper_insertion_tactile_supervisor"
        self.command_server = CommandProxyServer(self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(command)
                    setTrigger(command)
                else:
                    self.command_server_last_message = None
            else:
                self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            print "\n" + Hg_open + "Task Ended!!!" + H_close + " \t Success: {}".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def setTrigger(param):
    global mode
    global is_task_active
    global insert
    global flag_stop, flag_insertion
    global notgoal
    mode = param
    is_task_active = True
    insert = True
    flag_stop = False
    flag_insertion = False
    notgoal = True

    while contact_force_estim is None or abs(contact_force_estim) > 0.01:
        # print contact_force_estim # for debug
        reset_tact.publish(reset_msg)
        time.sleep(1.0)


def setTaskEnd(succ=True):
    global is_task_active
    is_task_active = False
    proxy_server.sendResponse(succ)


def tactile_cb(msg):
    global contact_force_estim
    global flag_stop, flag_insertion
    global insert

    contact_force_estim = msg.linear.y
    if contact_force_estim >= force_thr_high or contact_force_estim <= force_thr_low:
        flag_stop = True
        flag_insertion = True
        insert = False


def current_pos_cb(msg):
    global current_pos
    current_pos = list(msg.position)
    # print(current_pos[1])


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("grasshopper_insertion")
    node.setHz(100)

    node.setupParameter("is_task_active", False)
    is_task_active = node.getParameter("is_task_active")
    node.setupParameter("insert", True)
    insert = node.getParameter("insert")
    # node.setupParameter("thr_high",+0.05) #2
    # force_thr_high = node.getParameter("thr_high") /////da settare diversi se necessario
    # node.setupParameter("thr_low",-0.05) #-10
    # force_thr_low = node.getParameter("thr_low")

    node.setupParameter("thr_high", +0.3)  # 2
    force_thr_high = node.getParameter("thr_high")
    node.setupParameter("thr_low", -0.3)  # -10
    force_thr_low = node.getParameter("thr_low")

    proxy_server = GrasshopperInsTacProxyServer()

    cartesian_pub = node.createPublisher("/cartesian/joint_command", JointState)
    reset_tact = node.createPublisher("/tactile_reset", String)

    node.createSubscriber("/insertion_control/pippo", Twist, tactile_cb)
    node.createSubscriber("/cartesian/joint_states", JointState, current_pos_cb)

    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    mode = None
    step = JointState()
    reset_msg = String()
    step.name = ['X', 'Y', 'Z']
    contact_force_estim = None
    notgoal = True
    current_pos = [0, 0, 0]
    p = [0, 0, 0]
    flag_stop = False
    flag_insertion = False

    reset_msg.data = ''
    reset_tact.publish(reset_msg)

    while node.isActive():
        if is_task_active:
            p = list(current_pos)
            if mode == "push":
                if insert:
                    if notgoal:
                        p[1] = 0.001
                        step.position = p
                        cartesian_pub.publish(step)
                        notgoal = False
                    if current_pos[1] < 0.0011:
                        setTaskEnd(True)
                        # print"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
                        print("Wire Insertion: Completed")
                        # print"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
                else:
                    if flag_stop:
                        p = list(current_pos)
                        step.position = p
                        cartesian_pub.publish(step)
                        setTaskEnd(False)
                        flag_stop = False
                        # print"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
                        print("Wire Insertion: Stopped")
                        # print"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

            elif mode == "pull":
                if notgoal:
                    p[1] = 0.044
                    step.position = p
                    cartesian_pub.publish(step)
                    notgoal = False
                if current_pos[1] > 0.043:
                    # print"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
                    print("Wire Retrieve: Completed")
                    # print"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
                    if flag_insertion:
                        setTaskEnd(True)
                    else:
                        setTaskEnd(False)
            else:
                Logger.error("Grasshopper Inserction Tactile: Invalid Command")

        node.tick()
