#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import rospy
import math
import json
import numpy as np
import time
import PyKDL
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode
 
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage, CommandProxyServer
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel
from wires_robotic_platform.partdb.data_dictionaries import *
from std_msgs.msg import String
from std_msgs.msg import Float32
from geometry_msgs.msg import Pose , Twist
from sensor_msgs.msg import JointState
from std_srvs.srv import Trigger, TriggerResponse

from wires_robotic_platform.partdb.data_dictionaries import GearboxPartsData
from wires_robotic_platform.param.global_parameters import Parameters


#⬢⬢⬢⬢⬢➤ ROBOT
robot_name = Parameters.get("BONMET_NAME")  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
 

class GrasshopperInsCheckProxyServer(object):
    def __init__(self):
        self.command_proxy_server_name = "grasshopper_check_insertion_supervisor"
        self.command_server = CommandProxyServer(self.command_proxy_server_name)
        self.command_server.registerCallback(self.command_action_callback)
        self.command_server_last_message = None

        self.message_proxy = SimpleMessageProxy()
        self.message_proxy.register(self.command_callback)

    def command_action_callback(self, cmd):
        self.command_server_last_message = cmd
        cmd_msg = cmd.getSentMessage()
        self.command_callback(cmd_msg)

    def command_callback(self, msg):
        try:
            if msg.isValid():
                if msg.getReceiver() == self.command_proxy_server_name:
                    command = msg.getCommand()
                    Logger.log(command) 
                    setTrigger()
                else:
                    self.command_server_last_message = None
            else:
                self.command_server_last_message = None

        except Exception as e:
            print(e)

    def sendResponse(self, success):
        if self.command_server_last_message:
            print "\nTask Ended!!! \t Success: {}".format(success)
            if success:
                self.command_server.resolveCommand(
                    self.command_server_last_message)
            else:
                self.command_server.rejectCommand(
                    self.command_server_last_message)
            self.command_server_last_message = None


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def setTrigger():
    global is_task_active
    global flag_insertion
    global notgoal
    flag_insertion=False
    notgoal=True

    while contact_force_estim is None or abs(contact_force_estim) > 0.01:
        print contact_force_estim
        reset_tact.publish(reset_msg) 
        time.sleep(1.0)  
    print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"

    is_task_active = True
   

def setTaskEnd():
    global is_task_active
    is_task_active = False 
    proxy_server.sendResponse(True)


def tactile_cb(msg):
    global contact_force_estim
    global force_thr
    global flag_insertion
    contact_force_estim = msg.linear.y
    if contact_force_estim>=force_thr_high or contact_force_estim<=force_thr_low :
        flag_insertion=True
    

def current_pos_cb(msg):
    global current_pos
    current_pos = list(msg.position)
    #print(current_pos[1])



# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("grasshopper_insertion_check")
    node.setHz(100)

   
    node.setupParameter("extract",True)
    extract = node.getParameter("extract")
    node.setupParameter("is_task_active",False)
    is_task_active = node.getParameter("is_task_active")
    node.setupParameter("thr_high",+0.1) #2
    force_thr_high = node.getParameter("thr_high")
    node.setupParameter("thr_low",-0.1) #-10
    force_thr_low = node.getParameter("thr_low")
 

    proxy_server = GrasshopperInsCheckProxyServer()
    
    # rospy.wait_for_service('/grasshopper/halt')
    # tact_fb = rospy.ServiceProxy('/grasshopper/halt', Trigger)

    cartesian_pub=node.createPublisher("/cartesian/joint_command", JointState )
    reset_tact=node.createPublisher("/tactile_reset", String )

    #node.createSubscriber("/tactilesim", Float32, tactile_cb) #/insertion_control/estimated_twist

    node.createSubscriber("/insertion_control/pippo", Twist, tactile_cb)
    node.createSubscriber("/cartesian/joint_states", JointState, current_pos_cb)
    

    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ LOOP ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    #▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
    step=JointState()
    reset_msg=String()
    step.name=['X','Y','Z']
    contact_force_estim=None
    notgoal=True
    current_pos = [0,0,0]
    p= [0,0,0]
    flag_insertion=False
    reset_msg.data=""
    


    # while contact_force_estim is None or abs(contact_force_estim) > 0.01:
    #     print contact_force_estim
    #     reset_tact.publish(reset_msg) 
    #     time.sleep(1.0)  
    # print "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"


    while node.isActive(): 
        if is_task_active:
            p = list(current_pos)
            if extract:
                if notgoal:
                    p[1] = 0.044
                    step.position = p
                    cartesian_pub.publish(step)
                    notgoal=False 
                if current_pos[1]>0.043:
                    print"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" 
                    print("finish retrive")
                    setTaskEnd() 
            else:#debug
                p[1] = 0.001
                step.position = p
                cartesian_pub.publish(step)
                print("advanced")
        else:
            if flag_insertion:
                print("wire insertion SUCCEED")
            else: 
                print("wire insertion FAILED")


        node.tick()

    

    #--------------------------------------------

    # while node.isActive():
    #     # contact_force_estim= node.getData("/tactilesim").data
    #     # pos_y = node.getData("/cartesian/joint_states").position[1] 
    #     if is_task_active:
    #         p = current_pos
    #         if insert: 

    #             p[1] = 0.001
    #             step.position = p
    #             cartesian_pub.publish(step)
    #             print("advanced")
    #             # if p[1]<0.001: 
    #             #     setTaskEnd()  
    #             #     print("insertion finished")
    #         else: 
    #             #p[1] += adv
    #             p[1] = 0.044
    #             step.position = p
    #             cartesian_pub.publish(step)
    #             print("retrieve")
    #             # if p[1]>0.043: 
    #             #     print("setup finished")
    #             #     setTaskEnd() 
    #     elif flag==True:
    #         p = current_pos
    #         step.position = p
    #         cartesian_pub.publish(step)
    #         print("stop")
    #     node.tick()

        
