#! /usr/bin/env python
import rospy
import actionlib
from wires_robotic_platform.utils.logger import Logger

#from wires_robotic_platform.msg import DoDishesAction
from wires_robotic_platform.msg import *


class PippoServer:
    def __init__(self):
        self.action_server = actionlib.SimpleActionServer(
            'action_Pippo', PippoAction, self.goal_callback, False)
        self.action_server.start()

        self.action_feedback = PippoFeedback()
        self.action_result = PippoResult()

        # fake_goal = rospy.get_param("goal")
        # Logger.log("Fake Goal: %s" % fake_goal)

        self.varInternalValue = 0

    def goal_callback(self, goal):
        varGoalValue = goal.pippo_value
        Logger.log("Goal Value: %s" % varGoalValue)

        while self.varInternalValue < varGoalValue:
            self.varInternalValue += 1
            self.action_feedback.pippo_feedback = int(
                100 * (self.varInternalValue / varGoalValue))
            self.action_server.publish_feedback(self.action_feedback)
            rospy.sleep(0.05)

        self.action_result.pippo_result = self.varInternalValue
        self.action_server.set_succeeded(self.action_result)


if __name__ == '__main__':

    rospy.init_node('action_Pippo_server')
    server = PippoServer()
    rospy.spin()
