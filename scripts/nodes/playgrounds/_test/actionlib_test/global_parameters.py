#!/usr/bin/env python
# this is just to define file type

# we will import some values from roslaunch when launch files are used
import rospy

import numpy


class Parameters:

    goal = rospy.get_param("goal")


PARAMETERS = Parameters()
