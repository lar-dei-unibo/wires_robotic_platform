#! /usr/bin/env python
import rospy
import actionlib
from wires_robotic_platform.utils.logger import Logger

#from wires_robotic_platform.msg import DoDishesAction, DoDishesGoal
from wires_robotic_platform.msg import *

from global_parameters import *

# param = Parameters()


def feedback_callback(feedback):
    Logger.log("Feedback Value: %s %%" % feedback.pippo_feedback)


if __name__ == '__main__':
    rospy.init_node('action_Pippo_client')
    action_client = actionlib.SimpleActionClient('action_Pippo', PippoAction)
    action_client.wait_for_server()

    # value = 30
    # value = param.goal
    value = PARAMETERS.goal
    goal = PippoGoal(pippo_value=value, pippo_name="super-pippo")
    action_client.send_goal(goal, feedback_cb=feedback_callback)

    action_client.wait_for_result(rospy.Duration.from_sec(180.0))

    Logger.log("Result Value: %s" % action_client.get_result())
    rospy.spin()
