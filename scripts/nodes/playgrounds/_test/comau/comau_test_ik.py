#!/usr/bin/env python
# license removed for brevity

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import math
import pprint
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation

from wires_robotic_platform.robots.controllers import RobotController, GeneralIKService
from wires_robotic_platform.srv import IKService, IKServiceResponse
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray


rospy.init_node('joint_state_publisher')

comau = RobotController("comau_smart_six", [
    'base_to_link1',
    'link1_to_link2',
    'link2_to_link3',
    'link3_to_link4',
    'link4_to_link5',
    'link5_to_link6',
])

comau_ik_service = GeneralIKService(
    '/comau_smart_six/ik_service_node/ik_service')


rate = rospy.Rate(30)  # 10hz
start_time = rospy.get_time()
f = 0.3


target_frame = Frame()
target_frame.p = Vector(0.77, 0, 0.37)
target_frame.M.DoRotY(-math.pi/2)



while not rospy.is_shutdown():
    #comau.jointCommand([0,0,-1.57,0,0,0], rospy.Time.now())

    current_time = rospy.get_time() - start_time
    mag = 1.57 * math.sin(current_time * 2 * f)

    q_in = comau.getQ()

    target_frame.p = Vector(0.77, mag, 0.37)
    response = comau_ik_service.simple_ik(target_frame, q_in)

    if response.status == IKServiceResponse.STATUS_OK:
        q = response.q_out.data
        comau.jointCommand(q, rospy.Time.now())
    
    rate.sleep()
