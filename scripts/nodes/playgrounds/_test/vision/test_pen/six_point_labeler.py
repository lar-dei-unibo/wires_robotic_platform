#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32, PointStamped
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Vector3, Pose
from std_msgs.msg import ColorRGBA
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
import wires_robotic_platform.utils.visualization as visualization
import wires_robotic_platform.utils.conversions as conversions
from scipy.ndimage.measurements import center_of_mass
from wires_robotic_platform.vision.ar import VirtualBoxFromSixPoints, VirtualObject
import wires_robotic_platform.vision.cv as cv

from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import sys
import random


class VisualizationProxy(object):

    def __init__(self, publisher=None):
        if publisher:
            self.publisher = publisher
        else:
            self.publisher = rospy.Publisher(
                "VISUALIZATION_PROXY/MARKERS",
                MarkerArray,
                queue_size=1
            )
        self.marker_array = MarkerArray()

    def clear(self):
        self.marker_array.markers = []

    def addMarker(self, marker, time):
        marker.header.stamp = time
        marker.ns = "marker_{}".format(len(self.marker_array.markers))
        self.marker_array.markers.append(marker)

    def update(self):
        self.publisher.publish(self.marker_array)


class SixPointListener(object):

    def __init__(self):
        self.points = []
        self.plane = None
        self.box_list = []

    def clear(self):
        self.points = []
        self.plane = None

    def addPoint(self, p):
        self.points.append(p)
        if self.size() == 6:
            self.box_list.append(VirtualBoxFromSixPoints(self.points))
            self.clear()

    def size(self):
        return len(self.points)

    def clickedPointCallback(self, msg):
        p = np.array([msg.point.x, msg.point.y, msg.point.z])
        self.addPoint(p)

    def clickedPoseCallback(self, msg):
        p = np.array([msg.position.x, msg.position.y, msg.position.z])
        self.addPoint(p)
        print("ciao")


def markerFromVirtualBox(virtual_box, frame_id):
    marker = Marker()
    marker.header.frame_id = frame_id
    marker.action = Marker.ADD
    marker.type = Marker.CUBE
    marker.pose.orientation.w = 1
    marker.scale.x = virtual_box.size[0]
    marker.scale.y = virtual_box.size[1]
    marker.scale.z = virtual_box.size[2]
    marker.color = ColorRGBA(1, 0.4, 0.4, 0.4)
    return marker


sixPointBox = SixPointListener()

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("six_point_labeler")
node.setupParameter("camera_tf", "comau_smart_six/tool")
node.setupParameter("base_tf", "comau_smart_six/base_link")
node.setupParameter("hz", 60)
node.setHz(node.getParameter("hz"))

#⬢⬢⬢⬢⬢➤ Markerpub
marker_pub = node.createPublisher("~visualization/mesh", Marker)
points_pub = node.createPublisher("~visualization/points", Marker)
points_pub = node.createPublisher("~visualization/array", MarkerArray)
node.createSubscriber("/clicked_point", PointStamped,
                      sixPointBox.clickedPointCallback)
node.createSubscriber("/arp_detector/clicked_pose", Pose,
                      sixPointBox.clickedPoseCallback)

#⬢⬢⬢⬢⬢➤ Camera
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/asus_camera_1_may2017.yml')
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic="/camera/rgb/image_raw"
)
current_frame = None


def cameraCallback(frame):
    global current_frame
    """ Camera callback. produce FrameRGBD object """
    current_frame = frame.rgb_image.copy()


camera.registerUserCallabck(cameraCallback)

#⬢⬢⬢⬢⬢➤ Visualizatoin proxy
visualization_proxy = VisualizationProxy()

#⬢⬢⬢⬢⬢➤ Debug Mesh
mesh_frame = PyKDL.Frame()
mesh_frame.M = PyKDL.Rotation.RPY(0, 0, random.uniform(-np.pi, np.pi))

mesh = Marker()
mesh.action = Marker.ADD
mesh.header.frame_id = 'world'
mesh.type = Marker.MESH_RESOURCE
mesh.mesh_resource = 'file://' + '/home/daniele/Scaricati/mix.stl'
mesh.scale = Vector3(1, 1, 1)
mesh.pose = transformations.KDLToPose(mesh_frame)
mesh.color = ColorRGBA(0.3, 0.3, 0.3, 1)
mesh.mesh_use_embedded_materials = True

while node.isActive():

    camera_frame = node.retrieveTransform(
        node.getParameter("camera_tf"),
        node.getParameter("base_tf"),
        node.getCurrentTime()
    )
    if camera_frame == None:
        Logger.error("Camera frame not present!")
        continue

    visualization_proxy.clear()

    mesh.header.stamp = node.getCurrentTime()
    marker_pub.publish(mesh)

    for i in range(0, len(sixPointBox.box_list)):
        name = "box_{}".format(i)
        vb = sixPointBox.box_list[i]
        time = node.getCurrentTime()
        node.broadcastTransform(
            vb.plane_rf,
            name + "_plane",
            node.getParameter("camera_tf"),
            time
        )
        node.broadcastTransform(
            vb.rf,
            name,
            node.getParameter("camera_tf"),
            time
        )
        cube = markerFromVirtualBox(vb, frame_id=name)
        visualization_proxy.addMarker(
            marker=cube,
            time=node.getCurrentTime()
        )

        base_vb = camera_frame * vb.rf
        print("BASE_RF", base_vb)

        if current_frame != None:
            vobj = vb.buildVirtualObject()
            img_points = vobj.getImagePoints(
                camera=camera
            )
            VirtualObject.drawBox(img_points, current_frame)

    if current_frame != None:
        cv2.imshow("frame", current_frame)
        cv2.waitKey(1)

    visualization_proxy.update()

    # marker_pub.publish(plane)
    node.tick()
