#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from wires_robotic_platform.utils.ros import RosNode
import os
import numpy as np
import cv2
import rosbag
from cv_bridge import CvBridge, CvBridgeError
import cv_bridge
from sensor_msgs.msg import Image

cb = CvBridge()
#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("video_to_files")
node.setHz(30)

img_pub = node.createPublisher("/camera/rgb/image_raw", Image)


cap = cv2.VideoCapture(
    "/home/daniele/Videos/Webcam/indust_live_video1.mp4")

output = '/tmp/gino'

counter = 0

while node.isActive():

    if cap.isOpened():

        ret, frame = cap.read()

        img_name = 'rgb_{}.jpg'.format(str(counter).zfill(5))
        img_name = os.path.join(output, img_name)
        cv2.imwrite(img_name, frame)
        counter = counter + 1

        for i in range(0, 10):
            ret, frame = cap.read()

        #cv2.imshow("frame", gray)
        # cv2.waitKey(1)

    # marker_pub.publish(plane)
    node.tick()


cap.release()
cv2.destroyAllWindows()
