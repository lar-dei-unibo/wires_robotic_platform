#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32, Pose
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.ar import VirtualObject
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
import wires_robotic_platform.utils.visualization as visualization
import wires_robotic_platform.utils.conversions as conversions
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math

#⬢⬢⬢⬢⬢➤ Strange keys!
KEY_SPACE = 1048608

current_arp_pose = PyKDL.Frame()

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("chessboard_detector")

node.setupParameter("hz", 60)
node.setupParameter("MARKERS_MAP", '400=0.045 ; 701=0.05')
node.setupParameter("MARKER_RELATIVE_FRAME", '0 0 0.3 0 0 0')
node.setupParameter("CAMERA_FRAME", "comau_smart_six/tool")
node.setupParameter("BASE_FRAME", "comau_smart_six/base_link")
node.setupParameter("CAMERA_TOPIC", "/usb_cam/image_raw/compressed")
node.setHz(node.getParameter("hz"))

#⬢⬢⬢⬢⬢➤ Markerpub
marker_pub = node.createPublisher("~visualization", MarkerArray)
clicked_pub = node.createPublisher("~clicked_pose", Pose)

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/microsoft_live_camera.yml')
camera_tf_name = node.getParameter("CAMERA_FRAME")
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=node.getParameter("CAMERA_TOPIC"),
    compressed_image=True
)


def draw(img, corners, imgpts):
    corner = tuple(corners[0].ravel())
    img = cv2.line(img, corner, tuple(imgpts[0].ravel()), (255, 0, 0), 5)
    img = cv2.line(img, corner, tuple(imgpts[1].ravel()), (0, 255, 0), 5)
    img = cv2.line(img, corner, tuple(imgpts[2].ravel()), (0, 0, 255), 5)
    return img


criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)


def cameraCallback(frame):
    global current_arp_pose
    """ Camera callback. produce FrameRGBD object """
    img = frame.rgb_image.copy()

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ret, corners = cv2.findChessboardCorners(gray, (6, 5), None)
    if ret == True:
        objp = np.zeros((6 * 5, 3), np.float64)
        objp[:, :2] = np.mgrid[0:6, 0:5].T.reshape(-1, 2)
        objp = objp * 0.0281
        axis = np.float32([[1, 0, 0], [0, 1, 0], [0, 0, -1]]).reshape(-1, 3)
        axis = axis * 0.1

        corners2 = cv2.cornerSubPix(
            gray, corners, (11, 11), (-1, -1), criteria)

        # Find the rotation and translation vectors.
        result = cv2.solvePnPRansac(
            objp,
            corners2,
            camera.camera_matrix,
            camera.distortion_coefficients
        )
        rvecs = result[1]
        tvecs = result[2]

        # project 3D points to image plane
        imgpts, jac = cv2.projectPoints(
            axis, rvecs, tvecs, camera.camera_matrix, camera.distortion_coefficients)

        pose = transformations.cvToKDL(rvecs, tvecs)

        img = draw(img, corners2, imgpts)
        cv2.imshow('img', img)

    cv2.imshow("output", img)
    c = int(cv2.waitKey(1))
    if c == KEY_SPACE:
        print(c)


#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    node.tick()
