#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32, Pose
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.ar import VirtualObject
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.vision.roars import *
import wires_robotic_platform.utils.conversions as conversions
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math
import json
import random
#⬢⬢⬢⬢⬢➤ Strange keys!
KEY_SPACE = 1048608

current_arp_pose = PyKDL.Frame()

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("roars_testing")

node.setupParameter("SCENE_PATH", '')
node.setupParameter("SCENE_MANIFEST", '')
node.setupParameter("ROBOT_POSE_NAME", 'tf#comau_smart_six_link6.txt')


#⬢⬢⬢⬢⬢➤ Markerpub
marker_pub = node.createPublisher("~visualization", MarkerArray)
clicked_pub = node.createPublisher("~clicked_pose", Pose)

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/microsoft_live_camera.yml')
camera_tf_name = node.getParameter("CAMERA_FRAME")
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=node.getParameter("CAMERA_TOPIC"),
    compressed_image=True
)

scene = TrainingScene(
    scene_path='/media/daniele/data/daniele/datasets/cable_terminal_dataset/raw/scan1',
    image_topic_name='usb_cam_image_raw_compressed',
    robot_pose_name='tf#comau_smart_six_link6.txt'
)

for i in range(0, 8):
    cl = scene.createTrainingClass(i, "class_{}".format(i))
    for j in range(0, 9):
        inst = cl.createTrainingInstance(frame=PyKDL.Frame(
            PyKDL.Vector(random.uniform(-1, 1),
                         random.uniform(-1, 1), random.uniform(-1, 1))),
            size=[random.uniform(0.1, 1), random.uniform(
                0.1, 1), random.uniform(0.1, 1)]
        )
        # print("JSON")
        # print(type(inst))
        # js = json.dumps(inst, cls=CustomJSONEncoder)
        # print(js)
        # decoded = json.loads(js, object_hook=CustomJSONDecoder.decode)
        # print("DECODED")
        # print(type(decoded))
        # print(json.dumps(decoded, cls=CustomJSONEncoder))


# scene_js = json.dumps(scene, cls=CustomJSONEncoder, sort_keys=True, indent=4)
# scene_decoded = json.loads(scene_js, object_hook=CustomJSONDecoder.decode)
# scene_js2 = json.dumps(
#     scene_decoded, cls=CustomJSONEncoder, sort_keys=True, indent=4)

scene.initialize()
# print(scene.image_filenames_lists)

scene_js = json.dumps(scene, cls=CustomJSONEncoder, sort_keys=True, indent=4)
print(scene_js)
with open("/tmp/prova.txt", "w") as outfile:
    outfile.write(scene_js)


def cameraCallback(frame):
    global current_arp_pose
    """ Camera callback. produce FrameRGBD object """
    img = frame.rgb_image.copy()

    cv2.imshow("output", img)
    c = int(cv2.waitKey(1))
    if c == KEY_SPACE:
        print(c)


#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    node.tick()
