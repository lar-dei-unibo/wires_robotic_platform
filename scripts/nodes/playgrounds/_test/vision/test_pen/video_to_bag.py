#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from wires_robotic_platform.utils.ros import RosNode

import numpy as np
import cv2
import rosbag
from cv_bridge import CvBridge, CvBridgeError
import cv_bridge
from sensor_msgs.msg import Image

cb = CvBridge()
#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("generic_terminal_detector")
node.setHz(30)

img_pub = node.createPublisher("/camera/rgb/image_raw", Image)

bag = rosbag.Bag('test.bag', 'w')


cap = cv2.VideoCapture(
    "/home/daniele/Desktop/ARP_video_fx554.2563_fy415.6922.mov")

while node.isActive():

    if cap.isOpened():

        ret, frame = cap.read()

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.resize(gray, (640, 480))
        msg = cb.cv2_to_imgmsg(gray)
        msg.header.stamp = node.getCurrentTime()
        img_pub.publish(msg)

        #cv2.imshow("frame", gray)
        # cv2.waitKey(1)

    # marker_pub.publish(plane)
    node.tick()


cap.release()
cv2.destroyAllWindows()
