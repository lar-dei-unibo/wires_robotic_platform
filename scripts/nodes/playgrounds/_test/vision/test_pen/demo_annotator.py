#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32, Pose
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.ar import VirtualObject
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math

#⬢⬢⬢⬢⬢➤ Strange keys!
KEY_SPACE = 1048608

current_arp_pose = PyKDL.Frame()

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("demo_annotator")

node.setupParameter("hz", 60)
node.setupParameter("MARKERS_MAP", '400=0.045 ; 701=0.05')
node.setupParameter("MARKER_RELATIVE_FRAME", '0 0 0.3 0 0 0')
node.setupParameter("CAMERA_FRAME", "comau_smart_six/tool")
node.setupParameter("BASE_FRAME", "comau_smart_six/base_link")
node.setupParameter("CAMERA_TOPIC", "/usb_cam/image_raw/compressed")
node.setHz(node.getParameter("hz"))


#⬢⬢⬢⬢⬢➤ Markerpub
marker_pub = node.createPublisher("~visualization", MarkerArray)
clicked_pub = node.createPublisher("~clicked_pose", Pose)

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/microsoft_live_camera.yml')
camera_tf_name = node.getParameter("CAMERA_FRAME")
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=node.getParameter("CAMERA_TOPIC"),
    compressed_image=True
)


def createRay(point_2d, camera):
    point_2d = np.array([
        point_2d[0],
        point_2d[1],
        1.0
    ]).reshape(3, 1)
    ray = np.matmul(camera.camera_matrix_inv, point_2d)
    ray = ray / np.linalg.norm(ray)
    return ray.reshape(3)


def rayIntersection(ray, plane_coefficients):
    t = -(plane_coefficients[3]) / (
        plane_coefficients[0] * ray[0] +
        plane_coefficients[1] * ray[1] +
        plane_coefficients[2] * ray[2]
    )
    x = ray[0] * t
    y = ray[1] * t
    z = ray[2] * t
    inters = np.array([x, y, z])
    return inters.reshape(3)


clicked_points = []
virtual_objects = []


def mouse_callback(event, x, y, flags, param):
    global global_camera_pose

    if event == cv2.EVENT_LBUTTONDOWN:
        if global_camera_pose != None:
            ray = createRay((x, y), camera)
            # rayp = PyKDL.Vector(ray[0], ray[1], ray[2])
            # rayp = global_camera_pose * rayp
            # ray = [rayp.x(), rayp.y(), rayp.z()]
            # print("A", ray)
            inters = rayIntersection(ray, fake_plane)
            inters = PyKDL.Vector(inters[0], inters[1], inters[2])
            inters = global_camera_pose * inters
            clicked_points.append(inters)
            print((x, y), inters)
            p = inters
            frame = PyKDL.Frame(PyKDL.Vector(p[0], p[1], p[2]))
            vobj = VirtualObject(frame=frame, size=[0.04, 0.04, 0.002])
            virtual_objects.append(vobj)


named = False
global_camera_pose = None
global_img = None


fake_plane = np.array([0, 0, 1, -0.33])


def cameraCallback(frame):
    global named, global_camera_pose, global_img
    """ Camera callback. produce FrameRGBD object """
    global_img = frame.rgb_image.copy()



#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    camera_pose = node.retrieveTransform(
        "comau_smart_six/link6", "comau_smart_six/base_link", -1)
    if camera_pose != None and global_img != None:
        img = global_img
        corr = transformations.KDLFromRPY(0, np.pi, np.pi / 2)
        corr.p.z(-0.2)
        camera_pose = camera_pose * corr
        node.broadcastTransform(
            camera_pose, "camerax", "camera_smart_six/base_link", node.getCurrentTime())

        global_camera_pose = camera_pose

        if not named:
            cv2.namedWindow('output', cv2.WINDOW_NORMAL)
            cv2.setMouseCallback('output', mouse_callback)
            named = True

        counter = 0
        for vobj in virtual_objects:
            img_pts = vobj.getImagePoints(
                camera_frame=camera_pose,
                camera=camera
            )
            VirtualObject.drawBox(img_pts, img)

        cv2.imshow("output", img)
        c = int(cv2.waitKey(1))
        if c < 255:
            if c == 115:
                break
            print(c)

    node.tick()


print("SAVING..")
print(virtual_objects)
