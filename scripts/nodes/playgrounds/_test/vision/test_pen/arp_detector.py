#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32, Pose
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
import wires_robotic_platform.utils.visualization as visualization
import wires_robotic_platform.utils.conversions as conversions
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from keyboard.msg import Key
from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math

#⬢⬢⬢⬢⬢➤ Strange keys!
KEY_SPACE = 1048608

current_arp_pose = PyKDL.Frame()

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("arp_detector")

node.setupParameter("hz", 60)
node.setupParameter("MARKERS_MAP", '400=0.045 ; 701=0.05')
node.setupParameter("MARKER_RELATIVE_FRAME", '0 0 0.3 0 0 0')
node.setupParameter("CAMERA_FRAME", "comau_smart_six/tool")
node.setupParameter("BASE_FRAME", "comau_smart_six/base_link")
node.setHz(node.getParameter("hz"))

#⬢⬢⬢⬢⬢➤ Markerpub
marker_pub = node.createPublisher("~visualization", MarkerArray)
clicked_pub = node.createPublisher("~clicked_pose", Pose)

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/asus_camera_1_may2017.yml')
camera_tf_name = node.getParameter("CAMERA_FRAME")
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic="/camera/rgb/image_raw"
)

#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera.getCameraFile(), z_up=True)
markers_map = {}

for i in range(400, 413):
    markers_map[i] = 0.04


# z 0.1875 - 0.1375 - 0.0875
# x,y 0.025

cube_offset = 0.087
delta_cube_offset = 0.05
component_markers_map = {
    400: [0, 0, 0.2215, 0, 0, 0],
    401: [-0.025, 0, cube_offset + 2 * delta_cube_offset, 0, -np.pi * 0.5, 0],
    402: [-0.025, 0, cube_offset + delta_cube_offset, 0, -np.pi * 0.5, 0],
    403: [-0.025, 0, cube_offset, 0, -np.pi * 0.5, 0],

    410: [0.0, 0.025, cube_offset + 2 * delta_cube_offset, 0, -np.pi * 0.5, -np.pi * 0.5],
    411: [0.0, 0.025, cube_offset + delta_cube_offset, 0, -np.pi * 0.5, -np.pi * 0.5],
    412: [0.0, 0.025, cube_offset, 0, -np.pi * 0.5, -np.pi * 0.5],

    404: [0.0, -0.025, cube_offset + 2 * delta_cube_offset, 0, np.pi * 0.5, -np.pi * 0.5],
    405: [0.0, -0.025, cube_offset + delta_cube_offset, 0, np.pi * 0.5, -np.pi * 0.5],
    406: [0.0, -0.025, cube_offset, 0, np.pi * 0.5, -np.pi * 0.5],

    407: [0.025, 0.0, cube_offset + 2 * delta_cube_offset, 0, np.pi * 0.5, 0],
    408: [0.025, 0.0, cube_offset + delta_cube_offset, 0, np.pi * 0.5, 0],
    409: [0.025, 0.0, cube_offset, 0, np.pi * 0.5, 0]
}


for id, val in component_markers_map.iteritems():
    component_markers_map[id] = transformations.KDLFromArray(val)


marker_relative_transform = transformations.KDLFromString(
    node.getParameter('MARKER_RELATIVE_FRAME'), fmt='RPY')

Logger.log("Marker map:")
Logger.log("{}".format(markers_map))

camera_pose = PyKDL.Frame(PyKDL.Vector(0, 0, 0.5))

camera_pose.M.DoRotY(-2)

tt = PyKDL.Frame()
tt.M.DoRotZ(1.57)

camera_pose = camera_pose * tt


#⬢⬢⬢⬢⬢➤ Model
arp_model = MarkerArray()

cube = visualization.createCube(
    "arp",
    PyKDL.Frame(PyKDL.Vector(0, 0, 0.14)),
    size=[0.05, 0.05, 0.16],
    color=visualization.Color(1, 1, 1),
    name="arp_cube"
)
tip = visualization.createCube(
    "arp",
    PyKDL.Frame(PyKDL.Vector(0, 0, 0.03)),
    size=[0.006, 0.006, 0.06],
    color=visualization.Color(1, 1, 1),
    name="arp_tip"
)


def cameraCallback(frame):
    global current_arp_pose
    """ Camera callback. produce FrameRGBD object """
    output = frame.rgb_image.copy()

    #⬢⬢⬢⬢⬢➤ Detects markers
    markers = marker_detector.detectMarkersMap(
        frame.rgb_image, markers_map=markers_map)

    node.broadcastTransform(camera_pose, "camera",
                            "world", node.getCurrentTime())

    mean_pose = [0, 0, 0, 0, 0, 0, 0]
    counter = 0
    #⬢⬢⬢⬢⬢➤ draw markers
    for id, marker in markers.iteritems():
        if id in component_markers_map:

            marker.draw(output)

            #⬢⬢⬢⬢⬢➤ Publish Marker
            node.broadcastTransform(marker, marker.getName(),
                                    camera_tf_name, node.getCurrentTime())

            contribute = component_markers_map[id].Inverse()
            contribute = marker * contribute

            # node.broadcastTransform(contribute, marker.getName() + "_contrib",
            #                        camera_tf_name, node.getCurrentTime())

            mean_pose[0] += contribute.p.x()
            mean_pose[1] += contribute.p.y()
            mean_pose[2] += contribute.p.z()
            qx, qy, qz, qw = contribute.M.GetQuaternion()
            mean_pose[3] += qx
            mean_pose[4] += qy
            mean_pose[5] += qz
            mean_pose[6] += qw
            counter += 1

    mean_pose = np.array([mean_pose], dtype=np.float32) / float(counter)
    mean_pose = mean_pose.reshape(7)
    mean_pose = transformations.KDLFromArray(mean_pose, fmt='XYZQ')

    if not math.isnan(mean_pose.p.x()):
        current_arp_pose = mean_pose

        node.broadcastTransform(
            mean_pose,
            "arp",
            camera_tf_name,
            node.getCurrentTime()
        )

        obj_points = np.zeros((1, 3, 1), dtype="float32")
        obj_points[0, 0] = current_arp_pose.p.x()
        obj_points[0, 1] = current_arp_pose.p.y()
        obj_points[0, 2] = current_arp_pose.p.z()
        print("Points", obj_points)
        cRvec, cTvec = transformations.KDLToCv(PyKDL.Frame())
        img_points, _ = cv2.projectPoints(
            obj_points,
            cRvec,
            cTvec,
            camera.camera_matrix,
            camera.distortion_coefficients
        )
        img_points = np.reshape(img_points, (2))
        print("Image", img_points)
        cv2.circle(
            output,
            (int(img_points[0]), int(img_points[1])),
            5,
            (0, 0, 255),
            -1
        )

    cv2.imshow("output", output)
    c = int(cv2.waitKey(1))
    if c == KEY_SPACE:
        print("Publishing", current_arp_pose)
        clicked_pub.publish(transformations.KDLToPose(current_arp_pose))



#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    cube.header.stamp = node.getCurrentTime()
    arp_model.markers = []
    arp_model.markers.append(cube)
    arp_model.markers.append(tip)
    marker_pub.publish(arp_model)
    # marker_pub.publish(plane)
    node.tick()
