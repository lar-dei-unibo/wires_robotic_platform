#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
from wires_robotic_platform.robots.controllers import RobotController, GeneralIKService, Robot
from wires_robotic_platform.robots.trajectories import FuzzyTrajectoryGenerator
from wires_robotic_platform.robots.market import RobotMarket
from wires_robotic_platform.partdb.proxy import GearboxProxy
from wires_robotic_platform.srv import IKService, IKServiceResponse
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.sfm.machines as machines
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector


import message_filters
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
import cv2
import aruco
import rospkg
import numpy as np

rospack = rospkg.RosPack()
pack_path = rospack.get_path('wires_robotic_platform')
camera_file = pack_path + "/data/camera_calibration/asus_camera_1.yml"
marker_detector = MarkerDetector(camera_file=camera_file)


# markerdetector = aruco_detector.getMarkerDetector()
# markerdetector.setMinMaxSize(0.025)


orb = cv2.ORB()
bridge = CvBridge()


def drawPoint(image, point, radius=3, thickness=3, color=np.array([255, 255, 255])):
    cv2.circle(
        image, (int(point[0]), int(point[1])), radius=radius, color=color, thickness=thickness)


def getGatesMarkers(markers, image, id1=420, id2=422, guess_distance_multiplier=1.3):
    if id1 in markers and id2 in markers:
        m1 = markers[id1]
        m2 = markers[id2]
        c1 = m1.center.astype(int)
        c2 = m2.center.astype(int)
        middle = (c1 + c2) / 2.0
        middle = middle.astype(int)
        distance = np.linalg.norm(c1 - c2)
        direction = (c1 - c2) / distance

        direction = np.array([-direction[1], direction[0]]) * \
            distance * 0.5 * guess_distance_multiplier

        guess1 = middle + direction
        guess2 = middle - direction

        cv2.arrowedLine(img=image, pt1=(c1[0], c1[1]), pt2=(
            c2[0], c2[1]), color=np.array([255, 255, 255]))

        drawPoint(image, middle)
        #drawPoint(image, guess1)
        #drawPoint(image, guess2)
        return [middle, guess1]
    return None


def buildTerminalArrow(image, middle, kp):
    centroid = np.array([0.0, 0.0])
    farest_dist = 0
    farest = None
    for k in kp:
        centroid += k.pt
        dist = np.linalg.norm(k.pt - middle)
        if dist > farest_dist:
            farest = np.array([k.pt[0], k.pt[1]])
            farest_dist = dist

    centroid = centroid / len(kp)
    direction = (farest - centroid) / np.linalg.norm(centroid - middle)

    tip = centroid + direction * 10
    centroid = centroid.astype(int)
    farest = farest.astype(int)

    cv2.arrowedLine(img=image, pt1=(centroid[0], centroid[1]), pt2=(
        farest[0], farest[1]), color=np.array([0, 0, 255]), thickness=3, tipLength=1)

    drawPoint(image, centroid, radius=5, thickness=-
              1, color=np.array([0, 0, 255]))


def callback(rgb_msg, depth_msg):
    # Solve all of perception here...
    try:
        rgb_image = bridge.imgmsg_to_cv2(rgb_msg, "bgr8")
    except CvBridgeError as e:
        print(e)
        return

    try:
        depth_image = bridge.imgmsg_to_cv2(depth_msg, "16UC1")
    except CvBridgeError as e:
        print(e)
        return

    display_image = rgb_image.copy()

    blank_image = np.zeros((480, 640, 1), np.uint8)

    markers = marker_detector.detectMarkersMap(
        rgb_image, markers_metric_size=0.025)

    for id, marker in markers.iteritems():
        # print marker ID and point positions
        # print("Marker: {:d}".format(marker.getID()))
        # print(marker.corners)
        # print(marker.center)
        marker.draw(display_image, scale=3)

    guesses = getGatesMarkers(markers, display_image)
    if guesses != None:
        blank_image = np.zeros((480, 640, 1), np.uint8)
        drawPoint(blank_image, guesses[1], radius=40, thickness=-1)
        # drawPoint(blank_image, guesses[1], radius=40, thickness=-1)
        drawPoint(display_image, guesses[1], radius=40, thickness=1)
        # drawPoint(display_image, guesses[1], radius=40, thickness=1)
        cv2.imshow('mask', blank_image)
        kp = orb.detect(rgb_image, blank_image)
        print("DEtected:", len(kp))
        # display_image = cv2.drawKeypoints(
        #   display_image, kp, color=(0, 255, 0), flags=0)
        buildTerminalArrow(display_image, guesses[0], kp)

    cv2.imshow('image', display_image)
    cv2.waitKey(1)


rospy.init_node('terminal_detector')

rgb_sub = message_filters.Subscriber('camera/rgb/image_raw', Image)
depth_sub = message_filters.Subscriber('/camera/depth/image_raw', Image)

ts = message_filters.ApproximateTimeSynchronizer([rgb_sub, depth_sub], 10, 0.1)
ts.registerCallback(callback)
rospy.spin()
