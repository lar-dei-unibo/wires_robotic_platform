#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker
from std_msgs.msg import Header
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGBD

import message_filters
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
import cv2
import aruco
import rospkg
import numpy as np

rospack = rospkg.RosPack()
pack_path = rospack.get_path('wires_robotic_platform')
camera_file = pack_path + "/data/camera_calibration/asus_camera_1.yml"

rospy.init_node('camera_calibration_tf_printer')


bridge = CvBridge()
tf_listener = tf.TransformListener()
br = tf.TransformBroadcaster()
target_marker_id = 802
robot_base_tf_name = "comau_smart_six/base_link"
robot_base_ee_name = "comau_smart_six/link6"


def broadcastTransform(br, frame, frame_id, parent_frame, time):
    br.sendTransform((frame.p.x(), frame.p.y(), frame.p.z()),
                     frame.M.GetQuaternion(),
                     time,
                     frame_id,
                     parent_frame)


def cameraCallback(frame):
    """ Camera callback. produce FrameRGBD object """
    output = frame.rgb_image.copy()

    #⬢⬢⬢⬢⬢➤ Detects markers
    markers = marker_detector.detectMarkersMap(
        frame.rgb_image, markers_metric_size=0.15)

    if target_marker_id in markers:

        #⬢⬢⬢⬢⬢➤ draw markers
        for id, marker in markers.iteritems():
            marker.draw(output, scale=3)

            if id == target_marker_id:
                broadcastTransform(br, marker, "marker", "camera", frame.time)
                print("bvroad")

    cv2.imshow("image", output)
    c = cv2.waitKey(1)
    if c == 1048608:
        rospy.signal_shutdown(0)


#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera = CameraRGBD(
    configuration_file=camera_file,
    rgb_topic="/camera/rgb/image_raw",
    depth_topic="camera/depth/image_raw")

#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera.getCameraFile())

#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


rospy.spin()
