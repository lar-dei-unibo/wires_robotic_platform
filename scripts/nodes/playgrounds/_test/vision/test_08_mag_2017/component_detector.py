#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker
from std_msgs.msg import Header
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGBD

import message_filters
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
import cv2
import aruco
import rospkg
import numpy as np

rospack = rospkg.RosPack()
pack_path = rospack.get_path('wires_robotic_platform')
camera_file = pack_path + "/data/camera_calibration/asus_camera_1_may2017.yml"
bridge = CvBridge()
br = tf.TransformBroadcaster()
cv2.namedWindow("mask", cv2.WINDOW_NORMAL)


def broadcastTransform(br, frame, frame_id, parent_frame, time):
    br.sendTransform((frame.p.x(), frame.p.y(), frame.p.z()),
                     frame.M.GetQuaternion(),
                     time,
                     frame_id,
                     parent_frame)


class Circle3D(object):

    def __init__(self, ecircle, meter_radius, name):
        self.cx = ecircle.x
        self.cy = ecircle.y
        self.radius = ecircle.z
        self.meter_radius = meter_radius

        self.points_2d = np.array([])
        self.points_3d = np.array([])

        self.dirs = []
        self.dirs.append(np.array([1, 0]))
        self.dirs.append(np.array([0, 1]))
        self.dirs.append(np.array([-1, 0]))
        self.dirs.append(np.array([0, -1]))

        for dir in self.dirs:
            p2d = np.array([self.cx, self.cy]) + dir * self.radius
            p3d = np.array([dir[0], dir[1], 0]) * meter_radius
            if self.points_2d.size <= 0:
                self.points_2d = p2d
                self.points_3d = p3d
            else:
                self.points_2d = np.vstack([self.points_2d, p2d])
                self.points_3d = np.vstack([self.points_3d, p3d])

        ret, Rvec, Tvec = cv2.solvePnP(
            self.points_3d, self.points_2d, camera.camera_matrix, camera.distortion_coefficients)
        frame = transformations.cvToKDL(Rvec, Tvec)
        broadcastTransform(br, frame, "Circle_{}".format(
            name), "world", rospy.get_rostime())


class ComponentBaseWithMarkers(object):
    MARKER_TL = 700
    MARKER_TR = 701
    MARKER_BL = 702
    MARKER_BR = 703

    def __init__(self, markers):
        self.markers_tl = None
        self.markers_tr = None
        self.markers_bl = None
        self.markers_br = None

        self.marker_d1 = None
        self.marker_d2 = None

        if ComponentBaseWithMarkers.MARKER_TL in markers and ComponentBaseWithMarkers.MARKER_BR in markers:
            self.marker_d1 = markers[ComponentBaseWithMarkers.MARKER_TL]
            self.marker_d2 = markers[ComponentBaseWithMarkers.MARKER_BR]
        if ComponentBaseWithMarkers.MARKER_TR in markers and ComponentBaseWithMarkers.MARKER_BL in markers:
            self.marker_d1 = markers[ComponentBaseWithMarkers.MARKER_TR]
            self.marker_d2 = markers[ComponentBaseWithMarkers.MARKER_BL]

        self.mask_ready = False
        if self.marker_d1 != None and self.marker_d2 != None:
            self.mask_ready = True

    def isMaskReady(self):
        return self.mask_ready

    def getMaskCenter(self):
        if self.isMaskReady():
            center = 0.5 * (self.marker_d1.center + self.marker_d2.center)
            return center

    def generateCenterMask(self, source_image):
        if self.isMaskReady():
            center = self.getMaskCenter()
            mask = np.zeros((source_image.shape[0],
                             source_image.shape[1], 1), np.uint8)

            diag = np.linalg.norm(center - self.marker_d1.center)
            cv2.circle(
                mask,
                (int(center[0]), int(center[1])),
                radius=int(diag),
                color=np.array([255, 255, 255]),
                thickness=-1)
            return mask
        return None


def cameraCallback(frame):
    """ Camera callback. produce FrameRGBD object """
    output = frame.rgb_image.copy()

    #⬢⬢⬢⬢⬢➤ Detects markers
    markers = marker_detector.detectMarkersMap(
        frame.rgb_image, markers_metric_size=0.04)

    #⬢⬢⬢⬢⬢➤ draw markers
    for id, marker in markers.iteritems():
        marker.draw(output, scale=3)

    #⬢⬢⬢⬢⬢➤ Generate Component Base from markers
    component_base = ComponentBaseWithMarkers(markers)

    if component_base.isMaskReady():
        #⬢⬢⬢⬢⬢➤ Compute mask
        center = component_base.getMaskCenter()
        mask = component_base.generateCenterMask(frame.rgb_image)
        masked = cv2.bitwise_or(frame.rgb_image, frame.rgb_image, mask=mask)

        #⬢⬢⬢⬢⬢➤ Call service for circle detection
        #masked = cv2.resize(masked, (320, 240))
        masked = cv2.cvtColor(masked, cv2.COLOR_BGR2GRAY)
        response = polygon_service(
            colored=False,
            image=bridge.cv2_to_imgmsg(masked, "8UC1"),
            circle_max_radius=100)

        #⬢⬢⬢⬢⬢➤ Draw Circles
        index = 0
        radius_map = {}
        for c in response.circles:
            if int(c.z) in radius_map:
                radius_map[int(c.z)].append(c)
            else:
                radius_map[int(c.z)] = [c]

        for c in response.circles:
            if len(radius_map[int(c.z)]) <= 2:
                continue
            cv2.circle(
                masked,
                (int(c.x), int(c.y)),
                radius=int(c.z),
                color=np.array([255, 255, 0]), thickness=2)
            c3d = Circle3D(c, 0.007, index)
            index += 1
        print(radius_map)
        cv2.imshow("mask", masked)

    cv2.imshow("image", output)
    cv2.waitKey(1)


#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera = CameraRGBD(
    configuration_file=camera_file,
    rgb_topic="/camera/rgb/image_raw",
    depth_topic="camera/depth/image_raw")

#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera.getCameraFile())

#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


#⬢⬢⬢⬢⬢➤ Polygon Service proxy
polygon_service_name = "/poly_detection/detection_service"
try:
    rospy.wait_for_service(polygon_service_name, timeout=10)
except rospy.ROSException as e:
    Logger.error("Service timeout:" + str(e))
polygon_service = rospy.ServiceProxy(polygon_service_name, PolyDetectionService)


rospy.init_node('terminal_detector')
marker_pub = rospy.Publisher("prodo", Marker, queue_size=1)
rospy.spin()
