#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
import os
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.utils.ros import RosNode
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.cameras import CameraRGB
import wires_robotic_platform.vision.cv as cv
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import rospkg
import numpy as np
import math
import sys

KEY_SPACE = 32
KEY_Q = 113

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("save_camera_frames")
robot_name = "bonmetc60"  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


node.setupParameter("hz", 60)
node.setupParameter("CAMERA_TOPIC", "/usb_cam_1/image_raw")
node.setupParameter("OUTPUT_PATH", "/tmp/saved_frames")
node.setupParameter("SAVE_WITH_TF", True)
node.setupParameter("TF_BASE",  "/{}/base_link".format(robot_name))
node.setupParameter("TF_TARGET",  "/{}/link6".format(robot_name))
node.setHz(node.getParameter("hz"))

tf_enabled = node.getParameter("SAVE_WITH_TF")

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage('wires_robotic_platform', 'data/camera_calibration/microsoft_live_camera.yml')
camera_tf_name = "camera"
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=node.getParameter("CAMERA_TOPIC")
)

node.setupParameter("start_index", 0)
frame_save_counter = node.getParameter("start_index")


def cameraCallback(frame):
    global frame_save_counter
    """ Camera callback. produce FrameRGBD object """
    output = frame.rgb_image.copy()

    tf_target = None
    if tf_enabled:
        tf_target = node.retrieveTransform(
            node.getParameter("TF_TARGET"),
            node.getParameter("TF_BASE"),
            -1
        )
        if tf_target == None:
            return

    cv2.imshow("output", output)
    c = cv2.waitKey(1)
    if c > 0:
        if c == KEY_SPACE:
            if not os.path.exists(node.getParameter("OUTPUT_PATH")):
                os.makedirs(node.getParameter("OUTPUT_PATH"))

            coutner_str = str(frame_save_counter).zfill(5)
            frame_name = "frame_{}.png".format(coutner_str)
            tffile_name = "pose_{}.txt".format(coutner_str)
            frame_save_counter += 1
            filename = os.path.join(
                node.getParameter("OUTPUT_PATH"), frame_name)
            filenametf = os.path.join(
                node.getParameter("OUTPUT_PATH"), tffile_name)
            cv2.imwrite(filename, output)
            if tf_enabled:
                np.savetxt(
                    filenametf,
                    transformations.KDLtoNumpyVector(tf_target)
                )
            Logger.log("Saved frames:{}".format(coutner_str))
        print(c)
        if c == KEY_Q:
            print("EXIT")
            sys.exit(0)



#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    node.tick()
