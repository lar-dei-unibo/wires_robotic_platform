#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
import tf
import cv2
import math
import time
import message_filters
import rospkg
import math
import numpy as np
import PyKDL
from PyKDL import Frame, Vector, Rotation

from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header

import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode


def frameCameraFromBase():
    tf = None
    try:
        tf = node.retrieveTransform(frame_id="comau_smart_six/tool",
                                    parent_frame_id="comau_smart_six/base_link",
                                    time=-1)
    except Exception as e:
        print "Waiting for tf..."
    return tf


def frameTargetFromCamera():
    tf = None
    try:
        tf = node.retrieveTransform(frame_id="marker_705",
                                    parent_frame_id="comau_smart_six/tool",
                                    time=-1)
    except Exception as e:
        print "Waiting for tf..."
    return tf


if __name__ == '__main__':
    node = RosNode("component_detector_node")
    node.setupParameter("hz", 1)
    node.setHz(node.getParameter("hz"))
    node.setupParameter("FIXED_MARKER_HEIGHT", None)
    z_fixed = node.getParameter('FIXED_MARKER_HEIGHT')
    try:
        while node.isActive():
            T_base_camera = frameCameraFromBase()
            T_camera_component = frameTargetFromCamera()
            if T_base_camera is not None and T_camera_component is not None:
                component_tf = T_base_camera * T_camera_component
                if component_tf is not None:
                    # componentl_tf.p[2] = z_fixed
                    z_rot_angle = component_tf.M.GetRPY()[2]
                    component_tf.M = PyKDL.Rotation()
                    component_tf.M.DoRotZ(z_rot_angle)
                if component_tf:
                    node.broadcastTransform(component_tf, "component_tf",
                                            "comau_smart_six/base_link", node.getCurrentTime())
            node.tick()
    except rospy.ROSInterruptException:
        pass
