#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState, CompressedImage
from geometry_msgs.msg import Point, Point32, Pose
from visualization_msgs.msg import Marker, MarkerArray

from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
import wires_robotic_platform.utils.visualization as visualization
import wires_robotic_platform.utils.conversions as conversions
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math

#⬢⬢⬢⬢⬢➤ Strange keys!
KEY_SPACE = 1048608

current_arp_pose = PyKDL.Frame()

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("cable_measurement_sight")

node.setupParameter("hz", 60)
node.setupParameter("MARKERS_MAP", '400=0.045 ; 701=0.05')
node.setupParameter("MARKER_RELATIVE_FRAME", '0 0 0.3 0 0 0')
node.setupParameter("CAMERA_FRAME", "comau_smart_six/tool")
node.setupParameter("BASE_FRAME", "comau_smart_six/base_link")
node.setHz(node.getParameter("hz"))

#⬢⬢⬢⬢⬢➤ Markerpub
img_pug = node.createPublisher("~sight_image", CompressedImage)

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/microsoft_live_camera.yml')
camera_tf_name = node.getParameter("CAMERA_FRAME")
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic="/usb_cam/image_raw/compressed",
    compressed_image=True
)

#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera.getCameraFile(), z_up=True)
markers_map = {}

w = 640
h = 480
cx = w / 2
cy = h / 2


def cameraCallback(frame):
    global current_arp_pose
    """ Camera callback. produce FrameRGBD object """
    output = frame.rgb_image.copy()

    v_p1 = (cx, 0)
    v_p2 = (cx, h)
    h_p1 = (0, cy)
    h_p2 = (w, cy)

    cv2.line(output, v_p1, v_p2, (255, 0, 255), thickness=1)
    cv2.line(output, h_p1, h_p2, (255, 0, 255), thickness=1)

    cv2.imshow("output", output)
    c = int(cv2.waitKey(1))

    #⬢⬢⬢⬢⬢➤ Publish output
    msg = CompressedImage()
    #msg.header.stamp = rospy.Time.now()
    msg.format = "jpeg"
    msg.data = np.array(cv2.imencode('.jpg', output)[1]).tostring()
    img_pug.publish(msg)

    if c == KEY_SPACE:
        print("Publishing", current_arp_pose)


#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    # marker_pub.publish(plane)
    node.tick()
