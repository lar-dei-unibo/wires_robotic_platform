#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker
from std_msgs.msg import Header
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB

import message_filters
from std_msgs.msg import Float32MultiArray
from sensor_msgs.msg import Image, CameraInfo, JointState
from cv_bridge import CvBridge, CvBridgeError
import cv2
import aruco
import rospkg
import numpy as np
import time
current_qdot = 10000


def jointsCallback(msg):
    global current_qdot
    qdot = np.array(msg.velocity)
    n1 = np.linalg.norm(qdot, ord=1)

    current_qdot = n1

    n2 = np.linalg.norm(qdot, ord=2)
    n3 = np.linalg.norm(qdot, ord=3)

    mag_msg = Float32MultiArray()
    mag_msg.data.append(n1)
    mag_msg.data.append(n2)
    mag_msg.data.append(n3)

    magnitude_pub.publish(mag_msg)


node_name = 'bag_analyzer_camera_calibration'
rospy.init_node(node_name)
marker_pub = rospy.Publisher("prodo", Marker, queue_size=1)
rate = rospy.Rate(60)  # 10hz

joints_sub = rospy.Subscriber(
    "/comau_smart_six/joint_states", JointState, jointsCallback, queue_size=1)
magnitude_pub = rospy.Publisher(
    "/{}/magnitude".format(node_name), Float32MultiArray, queue_size=1)

rospack = rospkg.RosPack()
pack_path = rospack.get_path('wires_robotic_platform')
camera_file = pack_path + "/data/camera_calibration/asus_camera_1_may2017.yml"
bridge = CvBridge()
br = tf.TransformBroadcaster()
listener = tf.TransformListener()
cv2.namedWindow("image", cv2.WINDOW_NORMAL)

target_marker_id = 700
robot_base_tf_name = "comau_smart_six/base_link"
robot_base_ee_name = "comau_smart_six/link6"

ee_transforms = []
marker_transforms = []

saving_path = "/home/daniele/Scrivania/camera_ee_calibration.txt"


def saveFrames():
    thefile = open(saving_path, 'w')
    for i in range(0, len(ee_transforms)):
        ee_tf = ee_transforms[i]
        mk_tf = marker_transforms[i]

        thefile.write("{} {} {} {} {} {} {} {} {} {} {} {} {} {}\n".format(
            ee_tf[0][0], ee_tf[0][1], ee_tf[0][2], ee_tf[1][0], ee_tf[1][1], ee_tf[1][2], ee_tf[1][3],
            mk_tf[0][0], mk_tf[0][1], mk_tf[0][2], mk_tf[1][0], mk_tf[1][1], mk_tf[1][2], mk_tf[1][3]))
    thefile.close()


def cameraCallback(frame):
    """ Camera callback. produce FrameRGBD object """

    ee_frame = transformations.retrieveTransform(
        listener,
        robot_base_tf_name,
        robot_base_ee_name,
        time=rospy.Time(0),
        print_error=True,
        none_error=True
    )
    print(ee_frame)
    if ee_frame == None:
        Logger.log("No frame received!")

    # global current_qdot
    # print current_qdot
    # if current_qdot > 0.5:
    #     return
    output = frame.rgb_image.copy()
    #⬢⬢⬢⬢⬢➤ Detects markers
    markers = marker_detector.detectMarkersMap(
        frame.rgb_image, markers_metric_size=0.1)

    if target_marker_id in markers:

        #⬢⬢⬢⬢⬢➤ draw markers
        for id, marker in markers.iteritems():
            marker.draw(output, scale=3)

            if id == target_marker_id:

                if ee_frame:
                    ee_transforms.append(transformations.KDLtoTf(ee_frame))
                    marker_transforms.append(transformations.KDLtoTf(marker))
                    Logger.log("ok {}".format(len(ee_transforms)))
                    # Logger.log(ee_frame)
                    # Logger.log(marker)
                    pass
                else:
                    Logger.log("TF not ready!")

    cv2.imshow("image", output)
    c = cv2.waitKey(1)
    if c == 1048608:
        saveFrames()
        rospy.signal_shutdown(0)



#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic="/camera/rgb/image_raw")


#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera.getCameraFile())

#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while not rospy.is_shutdown():

    rate.sleep()
