#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker
from std_msgs.msg import Header
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.ar import VirtualObject, VirtualObjectGroundTruth
from wires_robotic_platform.vision.dataset import DatasetStorage

import message_filters
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
import cv2
import aruco
import rospkg
import numpy as np
import os


rospy.init_node('terminal_detector')
marker_pub = rospy.Publisher("prodo", Marker, queue_size=1)
rate = rospy.Rate(60)  # 10hz

# PARAMETERS


class PARAMETERS(object):
    BUILD_DATASET = False
    DATASET_NAME = rospy.get_param("~dataset_name", 'mix1')
    DATASET_MAX_TIME = 300
    DATASET_SAMPLE_TIME = 0.33
    DEBUG_DRAW_3D_BOXES = True
    DEBUG_DRAW_2D_FRAMES = False


Logger.log("INIT WITH DATASET: {}".format(PARAMETERS.DATASET_NAME))

rospack = rospkg.RosPack()
pack_path = rospack.get_path('wires_robotic_platform')
camera_file = pack_path + "/data/camera_calibration/asus_camera_1_may2017.yml"
ee_tf_name = "/comau_smart_six/link6"
camera_tf_name = "camera"
bridge = CvBridge()
br = tf.TransformBroadcaster()
listener = tf.TransformListener()
cv2.namedWindow("image", cv2.WINDOW_NORMAL)

show_frame = False
start_time = -1
elapsed_time = 0


def closeAndSave():
    datasetStorage.save()
    rospy.signal_shutdown(0)


def cameraCallback(frame):
    global start_time, elapsed_time
    """ Camera callback. produce FrameRGBD object """

    # TIme count
    if start_time < 0:
        start_time = frame.time.to_sec()

    elapsed_time = frame.time.to_sec() - start_time

    # Time over check
    if PARAMETERS.BUILD_DATASET:
        print("Dataset size", datasetStorage.size())
        if elapsed_time > PARAMETERS.DATASET_MAX_TIME:
            closeAndSave()

    robot_pose = transformations.retrieveTransform(
        listener,
        "comau_smart_six/base_link",
        ee_tf_name,
        rospy.Time(0),
        none_error=True,
        print_error=True)

    if robot_pose == None:
        return

    camera_pose = robot_pose * virtualObejctGroundTruth.camera_frame

    transformations.broadcastTransform(
        br, virtualObejctGroundTruth.camera_frame, camera_tf_name, "comau_smart_six/link6", rospy.Time.now())

    output = frame.rgb_image.copy()

    #⬢⬢⬢⬢⬢➤ Detects markers
    markers = marker_detector.detectMarkersMap(
        frame.rgb_image, markers_metric_size=0.04)

    #⬢⬢⬢⬢⬢➤ draw markers
    for id, marker in markers.iteritems():
        marker.draw(output, scale=3)
        transformations.broadcastTransform(
            br, marker, "marker_{}".format(marker.getID()), camera_tf_name, frame.time)

    virtualObejctGroundTruth._loadFromFile()
    virtual_objects = virtualObejctGroundTruth.getVirtualObjects()

    for name, data in virtual_objects.iteritems():
        color = data["color"]
        virtual_object = data["vobj"]

        transformations.broadcastTransform(
            br, virtual_object, name, "comau_smart_six/base_link", frame.time)

        img_points = virtual_object.getImagePoints(
            camera_frame=camera_pose, camera=camera)

        img_frame = virtual_object.getImageFrame(
            points=img_points, camera=camera, only_top_face=False)

        img_frame_onlytop = virtual_object.getImageFrame(
            points=img_points, camera=camera, only_top_face=True)

        if PARAMETERS.DEBUG_DRAW_3D_BOXES:
            VirtualObject.drawBox(img_points, output, color=color)

        if PARAMETERS.DEBUG_DRAW_2D_FRAMES:
            VirtualObject.drawFrame(
                img_frame, output, color=color, thickness=5, camera=camera)

        if PARAMETERS.BUILD_DATASET:
            datasetStorage.pushFrame(
                frame,
                {
                    "frame": np.array([img_frame]).reshape(1, 4),
                    "frametop": np.array([img_frame_onlytop]).reshape(1, 4),
                    "camerapose": transformations.KDLtoNumpyVector(camera_pose)
                }
            )

    # counter = 0
    # for f in virtual_object.frames:
    #     transformations.broadcastTransform(
    #         br, f, "p_{}".format(counter), "virtual_object", frame.time)
    #     counter += 1

    cv2.imshow("image", output)



#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic="/camera/rgb/image_raw")


#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera.getCameraFile())

#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)

#⬢⬢⬢⬢⬢➤ Dataset
dataset_output_path = "/home/daniele/Scrivania/yolo_dataset"
datasetStorage = DatasetStorage(
    dataset_path=dataset_output_path,
    dataset_name=PARAMETERS.DATASET_NAME,
    sample_time=PARAMETERS.DATASET_SAMPLE_TIME
)


#⬢⬢⬢⬢⬢➤ Virtual Object loader
virtual_object_path = '/home/daniele/Scrivania/wires_stuffs/datasets/components_dataset/'

virtualObejctGroundTruth = VirtualObjectGroundTruth(
    path=virtual_object_path, dataset_name=PARAMETERS.DATASET_NAME)


#⬢⬢⬢⬢⬢➤ Virtual objects


while not rospy.is_shutdown():
    c = cv2.waitKey(1)
    if c > 0:
        Logger.log("Key: {}".format(c))
    if c == 1048675:
        PARAMETERS.DEBUG_DRAW_2D_FRAMES = not PARAMETERS.DEBUG_DRAW_2D_FRAMES
    if c == 1048608:
        closeAndSave()

    rate.sleep()
