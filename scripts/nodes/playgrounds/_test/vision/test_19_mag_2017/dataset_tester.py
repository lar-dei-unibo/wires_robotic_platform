#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker
from std_msgs.msg import Header
import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.dataset import DatasetLoader, YoloDataset
from wires_robotic_platform.vision.ar import VirtualObject

import message_filters
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
import cv2
import aruco
import rospkg
import numpy as np
import os
import random

rospy.init_node('dataset_tester')
marker_pub = rospy.Publisher("prodo", Marker, queue_size=1)
rate = rospy.Rate(60)  # 10hz

rospack = rospkg.RosPack()
pack_path = rospack.get_path('wires_robotic_platform')
camera_file = pack_path + "/data/camera_calibration/asus_camera_1.ymlx"
ee_tf_name = "/comau_smart_six/link6"
camera_tf_name = "camera"
bridge = CvBridge()
br = tf.TransformBroadcaster()
listener = tf.TransformListener()
cv2.namedWindow("image", cv2.WINDOW_NORMAL)

# PARAMETERS


class PARAMETERS(object):
    DATASET_PATH = rospy.get_param(
        "~dataset_path", '/home/daniele/Scrivania/wires_stuffs/datasets/components_dataset')

    DATASET_NAME = rospy.get_param("~dataset_name", 'mix1')
    CREATE_MANIFEST = rospy.get_param("~create_manifest", False)
    RANDOM_TEST = rospy.get_param("~random_test", True)
    INPUT_FIELDS = rospy.get_param(
        "~random_test", 'rgb camerapose').split(" ")
    COMPUTE_FRAMES = rospy.get_param("~compute_frames", True)
    BUILD_YOLO_DATASET = rospy.get_param("~build_yolo", True)
    YOLO_DATASET_OUTPUT_PATH = rospy.get_param(
        "~yolo_output", '/home/daniele/Scrivania/wires_stuffs/datasets/yolo_dataset')
    YOLO_TEST_PERCENTAGE = rospy.get_param("~yolo_test_percentage", 0.1)
    DATASET_TO_BUILD = rospy.get_param(
        "~dataset_names", "mix1 mix2 mix3 mix4 component1 component2 component3 component4 component5 component6").split(" ")


#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic="/camera/rgb/image_raw")

#⬢⬢⬢⬢⬢➤ Dataset
dataset_base_path = PARAMETERS.DATASET_PATH
dataset_name = PARAMETERS.DATASET_NAME

dataset = DatasetLoader(
    base_path=dataset_base_path,
    dataset_name=dataset_name
)

#⬢⬢⬢⬢⬢➤ YOLO DATASET

Logger.error("CLASS MAP HARDCODED!")
class_map = {
    "component1": 0,
    "component2": 1,
    "component3": 2,
    "component4": 3,
    "component5": 4,
    "component6": 5
}

if PARAMETERS.BUILD_YOLO_DATASET:
    whole_list = []
    for name in PARAMETERS.DATASET_TO_BUILD:
        yoloDataset = YoloDataset(
            base_path=dataset_base_path,
            dataset_name=name,
            output_path=PARAMETERS.YOLO_DATASET_OUTPUT_PATH,
            camera=camera,
            class_map=class_map
        )
        aslist = yoloDataset.asList(center_coordinates=True)
        whole_list.extend(aslist)
        print(name, len(whole_list))
    random.shuffle(whole_list)
    YoloDataset.writeList(whole_list, PARAMETERS.YOLO_DATASET_OUTPUT_PATH, 0.1)

    exit(0)
# yoloDataset.test()

#⬢⬢⬢⬢⬢➤ DELETE FILES
# for f in dataset.files_list:
#     if "frame" in f:
#         os.remove(os.path.join(dataset.full_path, f))

#⬢⬢⬢⬢⬢➤ GENERATE MANIFEST
if PARAMETERS.CREATE_MANIFEST:
    dataset.generateManifest(PARAMETERS.INPUT_FIELDS, save=True)

#⬢⬢⬢⬢⬢➤ COMPUTE FRAMES
if PARAMETERS.COMPUTE_FRAMES:
    pass

#⬢⬢⬢⬢⬢➤ RANDOM TEST
if PARAMETERS.RANDOM_TEST:
    index = random.randint(0, dataset.size() - 1)
    rgb_file = dataset.getPath(index, 'rgb')
    camerapose_file = dataset.getPath(index, 'camerapose')
    camera_pose = transformations.NumpyVectorToKDL(np.loadtxt(camerapose_file))
    image = cv2.imread(rgb_file)

    for name, data in dataset.ground_truth.iteritems():

        vobj = VirtualObject(frame=data["pose"], size=data["size"])
        img_points = vobj.getImagePoints(
            camera_frame=camera_pose,
            camera=camera
        )
        img_frame = vobj.getImageFrame(
            points=img_points,
            camera=camera,
            only_top_face=False
        )
        img_frame = VirtualObject.enlargeFrame(img_frame, 1.2)
        #VirtualObject.drawBox(img_points, image, data["color"])
        VirtualObject.drawFrame(
            img_frame, image, color=data["color"], camera=camera)

    cv2.imshow("image", image)
    cv2.waitKey(0)
    print(rgb_file, frame_file)

while not rospy.is_shutdown():

    rate.sleep()
