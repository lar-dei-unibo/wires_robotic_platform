#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from wires_robotic_platform.vision.roars import *
from wires_robotic_platform.vision.ar import VirtualObject
from wires_robotic_platform.utils.ros import RosNode
import cv2
from scipy.optimize import minimize
import math

#⬢⬢⬢⬢⬢➤ KEY BINDINGS
KEY_NEXT_FRAME = 109
KEY_PREV_FRAME = 110
KEY_PREV_CLASS = 120
KEY_NEXT_CLASS = 99


class Ray3DIntersection(object):

    def __init__(self):
        self.rays = []

    def clear(self):
        self.rays = []

    def commonPointCostFunction(self, x):
        px = x
        closets_e = 0
        for ray in self.rays:

            center = ray[0]
            direct = ray[1]
            a = center + direct * 1000
            b = center + direct * -1000
            clos = Ray3DIntersection.computeClosestPoint(a, b, px)
            dist = clos - px
            closets_e += np.linalg.norm(dist) * np.linalg.norm(dist)
        closets_e = math.sqrt(closets_e)
        return closets_e

    def computeIntersection(self):
        x0 = np.array([0, 0, 0])
        res = minimize(
            self.commonPointCostFunction,
            x0,
            method='nelder-mead',
            options={'xtol': 1e-8, 'disp': True}
        )
        print("Result:", res.x)
        return res.x

    @staticmethod
    def computeClosestPoint(a, b, p):
        #############################
        # ➤ Computes closest point to 'p' onto a line described by 'a' and 'b'
        #############################
        ap = p - a
        ab = b - a
        result = a + np.dot(ap, ab) / np.dot(ab, ab) * ab
        return result


ray_3d_intersection = Ray3DIntersection()


def compute3DRay(point_2d, camera_matrix_inv, camera_pose):
    #############################
    # ➤ Computes 3D Ray from camera
    #############################

    #⬢⬢⬢⬢⬢➤ Computes RAY
    point_2d = np.array([
        point_2d[0],
        point_2d[1],
        1.0
    ]).reshape(3, 1)
    ray = np.matmul(camera_matrix_inv, point_2d)
    ray = ray / np.linalg.norm(ray)
    ray = ray.reshape(3)

    #⬢⬢⬢⬢⬢➤ RAY in global reference frame
    ray_v = PyKDL.Vector(ray[0], ray[1], ray[2])
    ray_v = camera_pose * ray_v
    center = camera_pose.p
    ray_dir = ray_v - center
    ray_dir.Normalize()

    line = (
        np.array([center[0], center[1], center[2]]),
        np.array([ray_dir[0], ray_dir[1], ray_dir[2]])
    )
    return line


def transformPlane(coefficients, camera_pose):

    plane_rf = PyKDL.Frame()
    plane_rf.p = PyKDL.Vector(0, 0, -coefficients[3])
    plane_rf.M = PyKDL.Rotation(
        1, 0, coefficients[0],
        0, 1, coefficients[1],
        0, 0, coefficients[2]
    )

    relative_plane = camera_pose.Inverse() * plane_rf

    a = relative_plane.M[0, 2]
    b = relative_plane.M[1, 2]
    c = relative_plane.M[2, 2]

    x = relative_plane.p.x()
    y = relative_plane.p.y()
    z = relative_plane.p.z()
    d = -(a * x + b * y + c * z)

    new_coefficients = [a, b, c, d]
    return new_coefficients


def clickedPointPlaneIntersection(point_2d, camera_matrix_inv, camera_pose, plane_coefficients):
    #############################
    # ➤ Computes 3D point from a 2d point and a virtual plane in front of the camera
    #############################

    #⬢⬢⬢⬢⬢➤ Computes RAY
    point_2d = np.array([
        point_2d[0],
        point_2d[1],
        1.0
    ]).reshape(3, 1)
    ray = np.matmul(camera_matrix_inv, point_2d)
    ray = ray / np.linalg.norm(ray)
    ray = ray.reshape(3)

    #⬢⬢⬢⬢⬢➤ Plane in Camera coordinates
    plane_coefficients = transformPlane(plane_coefficients, camera_pose)

    #⬢⬢⬢⬢⬢➤ Intersection with plane relative to camera
    t = -(plane_coefficients[3]) / (
        plane_coefficients[0] * ray[0] +
        plane_coefficients[1] * ray[1] +
        plane_coefficients[2] * ray[2]
    )
    x = ray[0] * t
    y = ray[1] * t
    z = ray[2] * t
    inters = np.array([x, y, z])
    inters = inters.reshape(3)

    #⬢⬢⬢⬢⬢➤ Transform intersection point in globa reference frame
    inters = PyKDL.Vector(inters[0], inters[1], inters[2])
    inters = camera_pose * inters
    return np.array([inters.x(), inters.y(), inters.z()])


def computeReferenceFrameFrom2Points(p1, p2):
    #############################
    # ➤ Computes a Reference frame starting from 2 3D points. They must lie on the virtual plane
    #############################

    ax = p1 - p2
    length = np.linalg.norm(ax)
    ax = ax / length
    az = np.array([0, 0, 1])
    ay = np.cross(az, ax)

    frame = PyKDL.Frame()
    frame.M = PyKDL.Rotation(
        ax[0], ay[0], az[0],
        ax[1], ay[1], az[1],
        ax[2], ay[2], az[2]
    )
    frame.p = PyKDL.Vector(
        p2[0], p2[1], p2[2]
    )
    return frame, length


#⬢⬢⬢⬢⬢➤ Buffer of clicked points
clicked_points_buffer = []

#⬢⬢⬢⬢⬢➤ Buffer of clicked rays
clicked_rays = []


def mouse_callback(event, x, y, flags, param):
    #############################
    # ➤ Callback from CV Gui
    #############################

    global image_index, plane_z, scene, camera_relative_plane, class_index, clicked_points_buffer, target_objects_height, ray_3d_intersection

    #⬢⬢⬢⬢⬢➤ MIDDLE CLICK
    if event == cv2.EVENT_MBUTTONDOWN:
        line = compute3DRay(
            (x, y),
            scene.camera_params.camera_matrix_inv,
            scene.getCameraPose(image_index)
        )
        ray_3d_intersection.rays.append(line)

        print("New ray! [{}]".format(len(ray_3d_intersection.rays)))
        if len(ray_3d_intersection.rays) > 2:
            computed_point = ray_3d_intersection.computeIntersection()
            plane_z = computed_point[2]
            camera_relative_plane = np.array([0, 0, 1, -plane_z])

            print("New reference plane:", camera_relative_plane)
            ray_3d_intersection.clear()

    #⬢⬢⬢⬢⬢➤ LEFT CLICK
    if event == cv2.EVENT_LBUTTONDOWN:
        #⬢⬢⬢⬢⬢➤ Computes 2D Point intersection with virtual plane
        p3d = clickedPointPlaneIntersection(
            (x, y),
            scene.camera_params.camera_matrix_inv,
            scene.getCameraPose(image_index),
            camera_relative_plane
        )
        frame = PyKDL.Frame(PyKDL.Vector(p3d[0], p3d[1], p3d[2]))

        print("Added point", p3d)
        clicked_points_buffer.append(p3d)

        #⬢⬢⬢⬢⬢➤ If 2 points are available computes new Instance
        if len(clicked_points_buffer) >= 2:
            p1 = clicked_points_buffer[0]
            p2 = clicked_points_buffer[1]
            clicked_points_buffer = []

            p1 = np.array([p1[0], p1[1], p1[2]])
            p2 = np.array([p2[0], p2[1], p2[2]])
            frame, length = computeReferenceFrameFrom2Points(p1, p2)

            scene.getTrainingClass(class_index).createTrainingInstance(
                frame=frame,
                size=[length * 2, length * 2, target_objects_height]
            )


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("roars_labeler_virtualplane")

scene_manifest_file = node.setupParameter("scene_manifest_file", '')
action = node.setupParameter("action", '')
class_number = node.setupParameter("class_number", 2)
plane_z = node.setupParameter("plane_z", -0.49)
target_objects_height = node.setupParameter("target_objects_height", 0.005)

#⬢⬢⬢⬢⬢➤ Plane coefficient
camera_relative_plane = np.array([0, 0, 1, -plane_z])

#⬢⬢⬢⬢⬢➤ Create Scenes
scene = TrainingScene.loadFromFile(scene_manifest_file)

#⬢⬢⬢⬢⬢➤ Check preliminary validity
if not scene.isValid():
    print("Scene is not valid!")
    sys.exit(0)

if action == 'CLEAR':
    #############################
    # ➤ CLEAR ACTION
    #############################
    scene.clearClasses()
    for l in range(0, class_number):
        scene.createTrainingClass(label=l, name="class_{}".format(l))

elif action == 'LABELING':
    #############################
    # ➤ LABELING ACTION
    #############################

    def loadImage(index):
        image_file = scene.image_filenames_lists[index]
        return cv2.imread(image_file)

    cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    cv2.setMouseCallback('image', mouse_callback)

    class_index = 0
    image_index = 0

    img = loadImage(image_index)

    while node.isActive():

        for _, cl in scene.classes.iteritems():
            for inst in cl.instances:
                vobj = VirtualObject(
                    frame=inst, size=inst.size, label=inst.label)
                img_pts = vobj.getImagePoints(
                    camera_frame=scene.getCameraPose(image_index),
                    camera=scene.camera_params
                )
                VirtualObject.drawBox(
                    img_pts, img, color=TrainingClass.getColorByLabel(cl.label))

        #⬢⬢⬢⬢⬢➤ Print Gui
        output = img.copy()
        frame_str = "Frame: {}".format(image_index)
        class_str = "Current Class: {} [{}]".format(
            class_index, scene.getTrainingClass(class_index).getName())
        cv2.putText(output, frame_str, (10, 30),
                    cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 0, 0), 1)
        cv2.putText(output, class_str, (10, 50),
                    cv2.FONT_HERSHEY_DUPLEX, 0.5, TrainingClass.getColorByLabel(class_index), 1)
        cv2.imshow('image', output)
        c = cv2.waitKey(10)

        #⬢⬢⬢⬢⬢➤ KEY EVENTS
        if c < 255:
            if c == KEY_NEXT_FRAME:
                image_index += 1
                image_index = image_index % scene.size()
                img = loadImage(image_index)
            if c == KEY_PREV_FRAME:
                image_index -= 1
                image_index = image_index % scene.size()
                img = loadImage(image_index)
            if c == KEY_PREV_CLASS:
                class_index -= 1
                class_index = class_index % scene.classesNumber()
            if c == KEY_NEXT_CLASS:
                class_index += 1
                class_index = class_index % scene.classesNumber()
            else:
                pass
                # print(c)

        node.tick()

#⬢⬢⬢⬢⬢➤ Save Scene to file if it is valid
if scene.isValid():
    print("Saving changes")
    scene.save(scene_manifest_file)
