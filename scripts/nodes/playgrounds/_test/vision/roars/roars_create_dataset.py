#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from wires_robotic_platform.vision.roars import *
from wires_robotic_platform.vision.ar import VirtualObject
from wires_robotic_platform.utils.ros import RosNode
import cv2
from scipy.optimize import minimize
import math
import sys
import random

#⬢⬢⬢⬢⬢➤ KEY BINDINGS
KEY_NEXT_FRAME = 109
KEY_PREV_FRAME = 110
KEY_PREV_CLASS = 120
KEY_NEXT_CLASS = 99

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("roars_create_dataset")

manifests_folder = node.setupParameter("manifests_folder", '')
dataset_output_folder = node.setupParameter("dataset_output_folder", '')
class_number = node.setupParameter("class_number", 2)
testing_percentage = node.setupParameter("testing_percentage", 0.04)
validation_percentage = node.setupParameter("validation_percentage", 0.02)
action = node.setupParameter("action", 'TEST')

#⬢⬢⬢⬢⬢➤ Loading Training Dataset
training_dataset = TrainingDataset.buildDatasetFromManifestsFolder(
    manifests_folder)

#⬢⬢⬢⬢⬢➤ Check validity
if training_dataset == None:
    print("Error!")
    sys.exit(0)


if action == 'TEST':
    #############################
    # ➤ TESTING DATASET WITH RANDOM PICKS
    #############################

    while node.isActive():
        scene = random.choice(training_dataset.scenes)
        frame = scene.pickTrainingFrame()

        image_index = frame.internal_index
        img = cv2.imread(frame.image_path)
        for inst in frame.scene.getAllInstances():
            vobj = VirtualObject(
                frame=inst, size=inst.size, label=inst.label)
            img_pts = vobj.getImagePoints(
                camera_frame=scene.getCameraPose(image_index),
                camera=scene.camera_params
            )
            VirtualObject.drawBox(
                img_pts,
                img,
                color=TrainingClass.getColorByLabel(inst.label)
            )

            img_frame = vobj.getImageFrame(
                img_pts,
                camera=scene.camera_params,
                only_top_face=False
            )

            VirtualObject.drawFrame(
                img_frame,
                img,
                camera=scene.camera_params
            )

        cv2.imshow("output", img)
        cv2.waitKey(0)
        print(frame)
        node.tick()

if action == 'CREATE':

    if not os.path.exists(dataset_output_folder):
        print("Output folder '{}' doesn't exist".format(dataset_output_folder))
        sys.exit(0)

    yolo_dataset = YoloDatasetBuilder(training_dataset, dataset_output_folder)
    yolo_dataset.build(options={
        'test_percentage': testing_percentage,
        'validity_percentage': validation_percentage
    })

#⬢⬢⬢⬢⬢➤ Save Scene to file if it is valid
if scene.isValid():
    print("Saving changes")
#    scene.save(scene_manifest_file)
