#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker
from std_msgs.msg import Header

import math
import pprint
import time
pp = pprint.PrettyPrinter(indent=4)
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.ar import VirtualObject, VirtualObjectGroundTruth
from wires_robotic_platform.vision.dataset import DatasetStorage

import message_filters
from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
import cv2
import aruco
import rospkg
import numpy as np
import os


rospy.init_node('test_triangulation')
marker_pub = rospy.Publisher("prodo", Marker, queue_size=1)
rate = rospy.Rate(60)  # 10hz


class KPHistory(object):

    def __init__(self):
        self.kps = []
        self.descs = []
        self.poses = []
        self.last_kp = None
        self.last_desc = None
        self.last_pose = None
        self.last_ready = False

    def push(self, kp, desc, pose):
        self.kps.append(kp)
        self.descs.append(desc)
        self.poses.append(pose)
        self.last_kp = kp
        self.last_desc = desc
        self.last_pose = pose
        self.last_ready = True
        return len(self.kps) - 1


# PARAMETERS


class PARAMETERS(object):
    BUILD_DATASET = True


rospack = rospkg.RosPack()
pack_path = rospack.get_path('wires_robotic_platform')
camera_file = pack_path + "/data/camera_calibration/asus_camera_1_may2017.yml"
ee_tf_name = "/comau_smart_six/link6"
robot_base_tf_name = "comau_smart_six/base_link"
camera_tf_name = "camera"
bridge = CvBridge()
br = tf.TransformBroadcaster()
listener = tf.TransformListener()
cv2.namedWindow("image", cv2.WINDOW_NORMAL)

start_time = 0
elapsed_time = 0

# camera_tool = np.loadtxt(
#    "/media/daniele/8fcd5abb-6f25-424f-b2ff-10229753c6d3/daniele/ros/bags/wires/components/gt/camera.txt")
camera_tool = [0.10062795440783212, 0.025256501161909414, -0.18138356951681653,
               -1.8900927670849523, 1.8953379895592086, 0.05468598243404694, -0.047122428440120934]
camera_frame = transformations.NumpyVectorToKDL(camera_tool)

#⬢⬢⬢⬢⬢➤ ORB
orb = cv2.ORB()

#⬢⬢⬢⬢⬢➤ FLANN
FLANN_INDEX_KDTREE = 1
index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
search_params = dict(checks=50)   # or pass empty dictionary
flann = cv2.FlannBasedMatcher(index_params, search_params)
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)


#⬢⬢⬢⬢⬢➤ KP History
kp_history = KPHistory()

global_projected_points = []

skip_counter = 0


def cameraCallback(frame):
    global start_time, elapsed_time, skip_counter
    """ Camera callback. produce FrameRGBD object """

    skip_counter += 1
    if skip_counter % 30 != 0:
        return

    # TIme count
    if start_time < 0:
        start_time = frame.time.to_sec()
    elapsed_time = frame.time.to_sec() - start_time

    robot_pose = transformations.retrieveTransform(
        listener,
        robot_base_tf_name,
        ee_tf_name,
        rospy.Time(0),
        none_error=True,
        print_error=True)

    if robot_pose == None:
        return

    camera_pose = robot_pose * camera_frame
    transformations.broadcastTransform(
        br, camera_frame, camera_tf_name, ee_tf_name, rospy.Time.now()
    )

    output = frame.rgb_image.copy()

    # find the keypoints with ORB
    kp = orb.detect(output, None)
    kp, des = orb.compute(output, kp)

    if kp_history.last_ready:
        # matches = flann.knnMatch(des, kp_history.last_desc, k=2)
        matches = bf.match(des, kp_history.last_desc)
        matches = sorted(matches, key=lambda x: x.distance)

        matches = matches[0:10]

        size = len(matches)
        kps1 = np.zeros((2, size), dtype=np.float)
        kps2 = np.zeros((2, size), dtype=np.float)

        for i in range(0, len(matches)):
            match = matches[i]
            id1 = match.trainIdx
            id2 = match.queryIdx
            kp1 = kp_history.last_kp[id1]
            kp2 = kp[id2]
            kps1[0, i] = kp1.pt[0]
            kps1[1, i] = kp1.pt[1]
            kps2[0, i] = kp2.pt[0]
            kps2[1, i] = kp2.pt[1]

        last_pose = kp_history.last_pose

        proj1 = transformations.buildProjectionMatrix(
            last_pose, camera.camera_matrix)
        proj2 = transformations.buildProjectionMatrix(
            camera_pose, camera.camera_matrix)

        triangulated_points = cv2.triangulatePoints(
            proj1, proj2, kps1, kps2)

        triangulated_points = triangulated_points.T

        for p in triangulated_points:
            pp = PyKDL.Vector(
                p[0] / p[3], p[1] / p[3], p[2] / p[3]
            )
            #pp = camera_pose * pp
            global_projected_points.append(pp)

    kp_history.push(kp, des, camera_pose)

    img2 = cv2.drawKeypoints(output, kp, None, color=(0, 255, 0), flags=0)

    cv2.imshow("image", img2)


#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic="/camera/rgb/image_raw")

#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while not rospy.is_shutdown():
    c = cv2.waitKey(1)
    if c > 0:
        Logger.log("Key: {}".format(c))

    print("Gloabl points", len(global_projected_points))

    marker = Marker()
    marker.header.frame_id = robot_base_tf_name
    marker.header.stamp = rospy.Time.now()
    marker.type = marker.POINTS
    marker.action = marker.ADD
    marker.scale.x = 0.002
    marker.scale.y = 0.002
    marker.scale.z = 0.002
    marker.color.a = 1
    for p in global_projected_points:
        point = Point()
        point.x = p.x()
        point.y = p.y()
        point.z = p.z()
        marker.points.append(point)

    marker_pub.publish(marker)

    rate.sleep()
