#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.vision.cameras import Camera
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.vision.visionutils as visionutils
import cv2
from cv_bridge import CvBridge, CvBridgeError
import cv_bridge
import numpy as np
import PyKDL
import math
import os


class FerruleInterestAreaPolygons(object):
    @staticmethod
    def getSquare(square_side):
        polygon_points = np.array([
            [0, 0],
            [0, -square_side * 0.5],
            [square_side, -square_side * 0.5],
            [square_side, square_side * 0.5],
            [0, square_side * 0.5]
        ])
        return polygon_points


class FerruleInterestArea(PyKDL.Frame):

    def __init__(self, image, origin_frame,  polygon_points=FerruleInterestAreaPolygons.getSquare(140)):
        super(FerruleInterestArea, self).__init__()
        self.image = image
        self.M = origin_frame.M
        self.p = origin_frame.p
        self.polygon_points = polygon_points

    def getRelativePolygonPoints(self, dtype=int):
        polygon_origin = self
        relative_polygon_points = []
        for p in self.polygon_points:
            pext = PyKDL.Vector(p[0], p[1], 0.0)
            pext = polygon_origin * pext
            relative_polygon_points.append(np.array([
                pext.x(),
                pext.y()
            ],  dtype=dtype))
        return relative_polygon_points

    def buildMask(self, target_image):
        zeros = np.zeros(
            (target_image.shape[0], target_image.shape[1], 1), dtype=np.int8)
        points = self.getRelativePolygonPoints()
        visionutils.drawPolygon(zeros, points, True, (255, 255, 255), -1)
        return zeros

    @staticmethod
    def generateSquaredFerruleArea(image, rf, square_size):
        polygon = FerruleInterestAreaPolygons.getSquare(square_size)
        return FerruleInterestArea(image, rf, polygon)

    @staticmethod
    def crop_image(img, tol=0):
        # img is image data
        # tol  is tolerance
        mask = img > tol
        return img[np.ix_(mask.any(1), mask.any(0))]

    def buildCrop(self, target_image, debug_output=False):
        mask = self.buildMask(target_image)
        masked = cv2.bitwise_and(target_image, target_image, mask=mask)

        polygon_origin = self
        angle = math.atan2(
            polygon_origin.M[1, 0], polygon_origin.M[0, 0]) * 180 / math.pi
        cols = target_image.shape[1]
        rows = target_image.shape[0]
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
        masked_rot = cv2.warpAffine(masked, M, (cols, rows))
        cropped = FerruleInterestArea.crop_image(masked_rot)

        if debug_output:
            cv2.namedWindow("cropped", cv2.WINDOW_NORMAL)
            # cv2.imshow("rotated", masked_rot)
            # cv2.imshow("mask", mask)
            # cv2.imshow("masked", masked)
            cv2.imshow("cropped", cropped)

        return cropped

    @staticmethod
    def extractPoints(image, th=200):
        points = []
        max_x = 0
        max_x_point = None
        for i in range(0, image.shape[0]):
            for j in range(0, image.shape[1]):
                if image[i, j] > th:
                    if j > max_x:
                        max_x = j
                        max_x_point = np.array([j, i])
                    points.append(np.array([j, i]))
        if len(points) <= 0:
            return [], np.array([0, 0])
        else:
            return points, max_x_point

    @staticmethod
    def saltAndPepperThresholdFilter(image, th=200, kerner_size=2):
        kernel = np.ones((kerner_size, kerner_size), np.uint8)
        cropped = cv2.erode(image, kernel)
        image = cv2.dilate(cropped, kernel)
        image[image < th] = [0]
        return image

    @staticmethod
    def grayAndSmooth(image, kernel_size=3, sig2=25):
        output_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        if kernel_size > 0:
            kernel = np.ones((kernel_size, kernel_size), np.float32) / sig2
            output_gray = cv2.filter2D(output_gray, -1, kernel)
        return output_gray

    @staticmethod
    def adaptiveThreshold(image, maxValue=255, block_size=11, C=2, avoid_black_white=True, replaced_value=50):
        thresh = cv2.adaptiveThreshold(image, maxValue, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                       cv2.THRESH_BINARY, block_size, C)
        if avoid_black_white:
            thresh[thresh == maxValue] = [replaced_value]
            thresh[thresh == 0] = [maxValue]
        return thresh

    @staticmethod
    def fitALine(points, second_point_dist=20):
        if len(points) == 0:
            return [0, 0, np.array([0, 0]), np.array([1, 0])]
        points = np.array(points)
        [vx, vy, x, y] = cv2.fitLine(
            points,
            cv2.DIST_L2,
            0,
            0.01,
            0.01
        )

        line_center = np.array([x, y], dtype=int)
        line_arrow = line_center + np.array([vx, vy]) * second_point_dist

        m = (line_arrow[1] - line_center[1]) / (line_arrow[0] - line_center[0])
        q = line_center[1] - m * line_center[0]
        return [m, q, line_center, line_arrow]

    @staticmethod
    def buildExtrema(m, q, points, max_x):
        x_inf = 10000
        y_inf = m * x_inf + q
        y_minf = m * -x_inf + q
        p_inf = np.array([x_inf, y_inf], dtype=int)
        p_minf = np.array([-x_inf, y_minf], dtype=int)
        max_x_point = np.array([max_x[0], 0])
        max_x_point[1] = m * max_x[0] + q
        zero_point = np.array([0, int(q)])
        return zero_point, max_x_point

    @staticmethod
    def buildRealLine(m, q, source_image, pixel_ratio, invert_m=True, invert_q=True):
        h = int(source_image.shape[0] * 0.5)
        q_rel = q - h

        m_sign = -1 if invert_m else 1
        q_sign = -1 if invert_q else 1
        out_m = m_sign * m
        out_q = q_sign * q_rel * pixel_ratio * 1000  # millimeter
        # try:
        # if not isinstance(out_q, float):
        #    out_q = out_q[0]
        return out_m, out_q[0]
        # except:
        #    return out_m, out_q

    @staticmethod
    def cropOnTerminal(source_image, terminal_probable_point, radius=50):
        sphere_mask = np.zeros(
            (source_image.shape[0], source_image.shape[1], 1),
            dtype=np.int8
        )
        cv2.circle(sphere_mask, tuple(
            terminal_probable_point), radius, (255), -1)
        masked_on_terminal = cv2.bitwise_and(
            source_image,
            source_image,
            mask=sphere_mask
        )
        return masked_on_terminal

    @staticmethod
    def estimatesCableRFs(zero_point, max_x_point, pixel_ratio):
        cable_rf_x = max_x_point - zero_point
        cable_rf_x = cable_rf_x / np.linalg.norm(cable_rf_x)
        cable_rf_y = np.array([cable_rf_x[1], -cable_rf_x[0]])

        #⬢⬢⬢⬢⬢➤ Estimate Cable RF on zero axis
        cable_rf = transformations.KDLFrom2DRF(
            cable_rf_x,
            -cable_rf_y,
            zero_point
        )

        #⬢⬢⬢⬢⬢➤ Estimates Terminal RF on terminal tip
        terminal_rf = transformations.KDLFrom2DRF(
            cable_rf_x,
            -cable_rf_y,
            max_x_point
        )

        cable_length = np.linalg.norm(zero_point - max_x_point) * pixel_ratio
        return cable_rf, terminal_rf, cable_length

    def globalRFFromLocal(self, local_rf, source_image):
        h = int(cable_image.shape[0] * 0.5)
        correction_frame = PyKDL.Frame(PyKDL.Vector(0, -h, 0))
        base_rf = PyKDL.Frame()
        base_rf.M = self.M
        base_rf.p = self.p
        base_rf = base_rf * correction_frame
        return base_rf * local_rf

    def drawDebug(self, image):
        visionutils.draw2DReferenceFrame(
            self, image, size=image.shape[0] * 0.05)
        polygon_points = self.getRelativePolygonPoints()
        visionutils.drawPolygon(image, polygon_points)


#############################
# ➤ NODE CODE
#############################


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("ferrule_2d_rf_estimation")

node.setupParameter("hz", 20)
node.setHz(node.getParameter("hz"))


#⬢⬢⬢⬢⬢➤ Camera
camera_file = node.getFileInPackage(
    'wires_robotic_platform',
    'data/camera_calibration/asus_camera_1_may2017.yml'
)
camera = Camera(configuration_file=camera_file)


#⬢⬢⬢⬢⬢➤ Target Image
target_image = node.setupParameter(
    "target_image", "/home/daniele/Desktop/18-RAL/datasets/ferrules_top/003.jpg")
img = cv2.imread(target_image)

#⬢⬢⬢⬢⬢➤ GroundTruth
folder = os.path.dirname(target_image)
base_name = os.path.basename(target_image).split(".")[0]
ground_truth_file = os.path.join(folder, base_name + ".txt")
ground_truth_rf = None
if os.path.exists(ground_truth_file):
    gt_arr = np.loadtxt(ground_truth_file)
    ground_truth_rf = transformations.NumpyVectorToKDL(gt_arr)


#⬢⬢⬢⬢⬢➤ Builds a 2D RF for crop
crop_rf_array = node.setupParameter(
    "crop_rf", "0;-1;380;260", array_type=float)
crop_size = node.setupParameter("crop_size", 170)

crop_rf = PyKDL.Frame()
crop_rf.M = PyKDL.Rotation(
    crop_rf_array[0], -crop_rf_array[1], 0,
    crop_rf_array[1], crop_rf_array[0], 0,
    0, 0, 1
)
crop_rf.p = PyKDL.Vector(crop_rf_array[2], crop_rf_array[3], 0)

#⬢⬢⬢⬢⬢➤ Creates Ferrule Area
ferrule_area = FerruleInterestArea.generateSquaredFerruleArea(
    img,
    crop_rf,
    crop_size
)

#⬢⬢⬢⬢⬢➤ Filters original image
filtered = ferrule_area.grayAndSmooth(img)

#⬢⬢⬢⬢⬢➤ Produces binary image
threshold = ferrule_area.adaptiveThreshold(filtered)

#⬢⬢⬢⬢⬢➤ Crop selected area
cropped = ferrule_area.buildCrop(threshold)

#⬢⬢⬢⬢⬢➤ Cable snapshot
cable_image = ferrule_area.saltAndPepperThresholdFilter(
    cropped,
    kerner_size=3
)

terminal_crop_radius = node.setupParameter("terminal_crop_radius", -1)
crop_on_terminal = 2 if terminal_crop_radius > 0 else 1
terminal_crop_center = None
while crop_on_terminal > 0:

    #⬢⬢⬢⬢⬢➤ Compute pointd
    cable_points, max_x_point = ferrule_area.extractPoints(cable_image)

    #⬢⬢⬢⬢⬢➤ Fit a line on all white points
    m, q, line_center, line_arrow = ferrule_area.fitALine(cable_points)

    #⬢⬢⬢⬢⬢➤ Builds extrema points
    zero_point, max_x_point = ferrule_area.buildExtrema(
        m, q,
        cable_points,
        max_x_point
    )

    #⬢⬢⬢⬢⬢➤ Estimates Cable RFs
    cable_rf, terminal_rf, cable_length = ferrule_area.estimatesCableRFs(
        zero_point,
        max_x_point,
        1
    )

    if crop_on_terminal > 1:
        #⬢⬢⬢⬢⬢➤ Crop On terminal
        cable_image = ferrule_area.cropOnTerminal(
            cable_image,
            max_x_point,
            radius=terminal_crop_radius
        )
        terminal_crop_center = max_x_point

    #⬢⬢⬢⬢⬢➤ Emulates a Do-Until loop
    crop_on_terminal -= 1

#⬢⬢⬢⬢⬢➤ Global RF computation
global_terminal_rf = ferrule_area.globalRFFromLocal(
    terminal_rf,
    cable_image
)


#⬢⬢⬢⬢⬢➤ Enable Visualization
visualization = node.setupParameter("visualization", False)

if visualization:
    #⬢⬢⬢⬢⬢➤ Plot Detetion on cropped image
    cable_image = cv2.bitwise_not(cable_image)
    cable_image = cv2.cvtColor(cable_image, cv2.COLOR_GRAY2BGR)
    cropped = cv2.cvtColor(cropped, cv2.COLOR_GRAY2BGR)
    #visionutils.draw2DReferenceFrame(cable_rf, cable_image)
    visionutils.draw2DReferenceFrame(terminal_rf, cable_image)

    terminal_line_p1 = terminal_rf * PyKDL.Frame(PyKDL.Vector(10000, 0, 0))
    terminal_line_p2 = terminal_rf * PyKDL.Frame(PyKDL.Vector(-10000, 0, 0))
    if not math.isnan(terminal_line_p1.p.x()):
        cv2.line(
            cropped,
            (int(terminal_line_p1.p.x()), int(terminal_line_p1.p.y())),
            (int(terminal_line_p2.p.x()), int(terminal_line_p2.p.y())),
            (0, 255, 255), 2
        )

    if terminal_crop_radius > 0:
        cv2.circle(cable_image, tuple(terminal_crop_center),
                   terminal_crop_radius, (255), 2)

    #⬢⬢⬢⬢⬢➤ Plot GroundTruth
    if ground_truth_rf != None:
        visionutils.draw2DReferenceFrame(
            ground_truth_rf, img, force_color=(255, 255, 0))

    #⬢⬢⬢⬢⬢➤ Plot Results

    visionutils.draw2DReferenceFrame(global_terminal_rf, img)

    ferrule_area.drawDebug(img)
    ferrule_area.drawDebug(threshold)

    width = img.shape[1]
    height = img.shape[0]
    rescale_factor = node.setupParameter("output_image_rescale", 1)

    newimg = visionutils.stackImages(
        [img, threshold, cropped, cable_image], rescale=(int(width * rescale_factor), int(height * rescale_factor)))

    cv2.imshow("img", newimg)
    cv2.waitKey(0)


write_output = node.setupParameter("write_output", True)

if write_output:

    if ground_truth_rf:
        detection_angle = math.atan2(
            global_terminal_rf.M.UnitX().y(),
            global_terminal_rf.M.UnitX().x()
        )
        gt_angle = math.atan2(
            ground_truth_rf.M.UnitX().y(),
            ground_truth_rf.M.UnitX().x()
        )
        detection_pos = np.array(
            [global_terminal_rf.p.x(), global_terminal_rf.p.y()])
        gt_pos = np.array([ground_truth_rf.p.x(), ground_truth_rf.p.y()])

        error_angle = math.fabs(gt_angle - detection_angle)
        error_pos = np.linalg.norm(gt_pos - detection_pos)
        print terminal_crop_radius, gt_angle, gt_pos[0], gt_pos[1], detection_angle, detection_pos[0], detection_pos[1],  error_angle, error_pos
