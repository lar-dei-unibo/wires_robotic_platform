#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from std_msgs.msg import Float64MultiArray
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
from wires_robotic_platform.vision.ar import VirtualObject
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
import wires_robotic_platform.utils.conversions as conversions
import wires_robotic_platform.vision.visionutils as visionutils
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from visualization_msgs.msg import MarkerArray, Marker
from wires_robotic_platform.utils.ros import RosNode
from std_msgs.msg import ColorRGBA

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math


colors_map = {
    "blue": (243, 150, 33),
    "green": (80, 175, 76),
    "red":  (54, 67, 244),
    "orange": (54, 67, 244)
}


LINE_COUNTER = 0


def createMarkerLine(ray, id, color):

    global LINE_COUNTER

    print ("RAY", ray)
    marker = Marker()
    marker.header.frame_id = "/comau_smart_six/base_link"
    LINE_COUNTER = LINE_COUNTER + 1
    marker.id = id
    marker.type = marker.ARROW
    marker.action = marker.ADD
    marker.scale.x = 0.002
    marker.scale.y = 0.005
    marker.scale.z = 0.02

    marker.color.r = color[2] / 255.0
    marker.color.g = color[1] / 255.0
    marker.color.b = color[0] / 255.0
    marker.color.a = 1
    marker.colors = [ColorRGBA(), ColorRGBA()]
    marker.points = [Point(), Point()]

    marker.points[0].x = ray[0][0]
    marker.points[0].y = ray[0][1]
    marker.points[0].z = ray[0][2]

    marker.points[1].x = ray[1][0]
    marker.points[1].y = ray[1][1]
    marker.points[1].z = ray[1][2]

    return marker


def createMarkerSphere(point, id, color):
    global LINE_COUNTER
    marker = Marker()
    marker.header.frame_id = "/comau_smart_six/base_link"
    LINE_COUNTER = LINE_COUNTER + 1
    marker.id = id
    marker.type = marker.CYLINDER
    marker.action = marker.ADD
    marker.scale.x = 0.02
    marker.scale.y = 0.02
    marker.scale.z = 0.001

    marker.color.r = color[2] / 255.0
    marker.color.g = color[1] / 255.0
    marker.color.b = color[0] / 255.0
    marker.color.a = 0.7
    marker.pose.orientation.w = 1
    marker.pose.position.x = point[0]
    marker.pose.position.y = point[1]
    marker.pose.position.z = point[2]

    return marker



#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("terminal_auto_labeler")

node.setupParameter("hz", 160)
node.setupParameter("MARKER_ID", 703)
node.setupParameter("MARKER_SIZE", 0.01)
node.setupParameter("VIZ", True)
node.setupParameter("CAMERA_TOPIC", "/usb_cam/image_raw/compressed")
node.setupParameter("CAMERA_CONFIGURATION",
                    'microsoft_live_camera.yml')
node.setupParameter("CAMERA_TF_NAME", 'camera')
fixed_rf = node.setupParameter("FIXED_RF", True)
fixed_2d_rf = node.setupParameter(
    "FIXEX_2D_RF", "1;0;371;187", array_type=float)
#"FIXEX_2D_RF", "-0.996195;-0.0871557;326.517;248.205", array_type=float)

mpub = node.createPublisher('rays', MarkerArray)
marker_array = MarkerArray()

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/' + node.getParameter("CAMERA_CONFIGURATION"))
camera_tf_name = node.getParameter("CAMERA_TF_NAME")
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=node.getParameter("CAMERA_TOPIC"),
    compressed_image='compressed' in node.getParameter("CAMERA_TOPIC")
)


def mouseCallback(event, x, y, flags, param):
    global click_index, click_points
    pass


window_setup = False
z = -0.49
objects = [
    PyKDL.Frame(PyKDL.Vector(0.65, 0.035, z)),
    PyKDL.Frame(PyKDL.Vector(0.725, 0.06, z)),
    PyKDL.Frame(PyKDL.Vector(0.75, 0.15, z))
]
colors = [
    colors_map["green"],
    colors_map["green"],
    colors_map["red"]
]

img_counter = 0


def cameraCallback(frame):
    """ Camera callback. produce FrameRGBD object """
    global window_setup, marker_array, img_counter
    if not window_setup:
        window_setup = True
        cv2.namedWindow('output', cv2.WINDOW_NORMAL)
        cv2.setMouseCallback('output', mouseCallback)

    output = frame.rgb_image.copy()

    camera_pose = node.retrieveTransform(
        'camera', '/comau_smart_six/base_link', -1)
    camera_pose_arr = transformations.KDLtoNumpyVector(camera_pose).reshape(7)

    marker_array.markers = []
    for index in range(0, len(objects)):
        obj = objects[index]
        color = colors[index]

        vobj = VirtualObject(obj, [0.03, 0.03, 0.001])
        center = vobj.getImageSinglePoint(
            camera_frame=camera_pose, camera=camera)
        cv2.circle(output, center, 50, color, 3)

        #vobj.draw(output, camera_pose, camera=camera, color=color)

        arr = transformations.KDLtoNumpyVector(obj).reshape(7)
        mray = createMarkerLine((camera_pose_arr[0:3], arr[0:3]), index, color)
        marker_array.markers.append(mray)

        mobj = createMarkerSphere(arr[0:3], index + 1000, color)
        marker_array.markers.append(mobj)
    mpub.publish(marker_array)

    wait_steps = 20
    if img_counter > wait_steps:
        counter_str = "Auto-generated labeled images: {:d}".format(
            img_counter - wait_steps)

        cv2.rectangle(output, (0, 420), (5000, 5000), colors_map['blue'], -1)
        cv2.putText(output, counter_str, (10, 460),
                    cv2.FONT_HERSHEY_DUPLEX, 1, (255, 255, 255))

    cv2.imshow("output", output)
    c = cv2.waitKey(1)
    img_counter = img_counter + 1



#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    # marker_pub.publish(plane)
    node.tick()
