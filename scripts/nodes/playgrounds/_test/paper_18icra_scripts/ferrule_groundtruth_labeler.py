#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.vision.cameras import Camera
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.vision.visionutils as visionutils
import cv2
from cv_bridge import CvBridge, CvBridgeError
import cv_bridge
import numpy as np
import PyKDL
import math
import os


#############################
# ➤ NODE CODE
#############################
clicked_points = []
clicked_rf = None
current_cursor = (0, 0)


def mouseCallback(event, x, y, flags, param):
    global clicked_points, clicked_rf, current_cursor
    if event == cv2.EVENT_MOUSEMOVE:
        current_cursor = (x, y)
    if event == cv2.EVENT_LBUTTONDOWN:
        clicked_points.append((x, y))
        if len(clicked_points) >= 2:
            p1 = np.array([clicked_points[0][0], clicked_points[0][1]])
            p2 = np.array([x, y])
            ax = p2 - p1
            ax = ax / np.linalg.norm(ax)

            clicked_rf = transformations.KDLFrom2DRFAsArray(
                [ax[0], ax[1], p1[0], p1[1]])

            clicked_points = []
        else:
            clicked_rf = None



#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("ferrule_2d_rf_estimation")


#⬢⬢⬢⬢⬢➤ Target Image
target_image = node.setupParameter(
    "target_image", "/home/daniele/Desktop/18-RAL/datasets/ferrules_top/001.jpg")
img = cv2.imread(target_image)

#⬢⬢⬢⬢⬢➤ GroundTruth
folder = os.path.dirname(target_image)
base_name = os.path.basename(target_image).split(".")[0]
ground_truth_file = os.path.join(folder, base_name + ".txt")
if os.path.exists(ground_truth_file):
    gt_arr = np.loadtxt(ground_truth_file)
    clicked_rf = transformations.NumpyVectorToKDL(gt_arr)
else:
    np.savetxt(ground_truth_file, np.array(
        [0, 0, 0, 0, 0, 0, 1]).reshape(1, 7))


#⬢⬢⬢⬢⬢➤ Labeling window
cv2.namedWindow('image')
cv2.setMouseCallback('image', mouseCallback)


while node.isActive():

    output_image = img.copy()

    #⬢⬢⬢⬢⬢➤ Draw current RF or points
    if clicked_rf != None:
        visionutils.draw2DReferenceFrame(clicked_rf, output_image)
    if len(clicked_points) > 0:
        cv2.circle(output_image, clicked_points[0], 4, (255, 0, 0), -1)
        cv2.line(output_image, current_cursor,
                 clicked_points[0], (255, 255, 255), 2)

    cv2.imshow("image", output_image)
    c = cv2.waitKey(10)
    if c == 113:
        break

    node.tick()

if clicked_rf != None:
    print(clicked_rf)
    np.savetxt(ground_truth_file, transformations.KDLtoNumpyVector(clicked_rf))
