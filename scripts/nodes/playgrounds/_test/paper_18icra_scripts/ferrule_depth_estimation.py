#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.markers import MarkerDetector
import wires_robotic_platform.utils.transformations as transformations
import os
import glob
import cv2
from cv_bridge import CvBridge, CvBridgeError
import cv_bridge
import numpy as np
import PyKDL
import random

# DA SPOSTARE
import message_filters
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import JointState, Image, CompressedImage
from geometry_msgs.msg import Twist
import datetime
import copy
from collections import OrderedDict

from scipy.optimize import minimize
import math

#############################
# ➤ Ray3DIntersection
#############################


class Ray3DIntersection(object):

    def __init__(self):
        self.rays = []

    def clear(self):
        self.rays = []

    def commonPointCostFunction(self, x):
        px = x
        closets_e = 0
        for ray in self.rays:

            center = ray[0]
            direct = ray[1]
            a = center + direct * 1000
            b = center + direct * -1000
            clos = Ray3DIntersection.computeClosestPoint(a, b, px)
            dist = clos - px
            closets_e += np.linalg.norm(dist) * np.linalg.norm(dist)
        closets_e = math.sqrt(closets_e)
        return closets_e

    def computeIntersection(self):
        x0 = np.array([0, 0, 0])
        res = minimize(
            self.commonPointCostFunction,
            x0,
            method='nelder-mead',
            options={'xtol': 1e-8, 'disp': True}
        )
        return res.x

    @staticmethod
    def computeClosestPoint(a, b, p):
        #############################
        # ➤ Computes closest point to 'p' onto a line described by 'a' and 'b'
        #############################
        ap = p - a
        ab = b - a
        result = a + np.dot(ap, ab) / np.dot(ab, ab) * ab
        return result

    @staticmethod
    def compute3DRay(point_2d, camera_matrix_inv, camera_pose):
        #############################
        # ➤ Computes 3D Ray from camera
        #############################

        #⬢⬢⬢⬢⬢➤ Computes RAY
        point_2d = np.array([
            point_2d[0],
            point_2d[1],
            1.0
        ]).reshape(3, 1)
        ray = np.matmul(camera_matrix_inv, point_2d)
        ray = ray / np.linalg.norm(ray)
        ray = ray.reshape(3)

        #⬢⬢⬢⬢⬢➤ RAY in global reference frame
        ray_v = PyKDL.Vector(ray[0], ray[1], ray[2])
        ray_v = camera_pose * ray_v
        center = camera_pose.p
        ray_dir = ray_v - center
        ray_dir.Normalize()

        line = (
            np.array([center[0], center[1], center[2]]),
            np.array([ray_dir[0], ray_dir[1], ray_dir[2]])
        )
        return line


#############################
# ➤ NODE CODE
#############################


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("ferrule_depth_estimation")

node.setupParameter("hz", 20)
node.setHz(node.getParameter("hz"))

dataset_folder = node.setupParameter("dataset_folder", "")
image_folder = node.setupParameter(
    "image_folder", "camera_rgb_image_raw_compressed")
predictions_file = node.setupParameter(
    "predictions_file", "darknet_rgb_detector_predictions.txt")
poses_file = node.setupParameter("poses_file", "tf#_comau_smart_six_link6.txt")
camera_extrinsics_file = node.setupParameter(
    "camera_extrinsics_file", "camera_extrinsics.txt")
use_marker = node.setupParameter("use_marker", True)

#⬢⬢⬢⬢⬢➤ Creates Camera Proxy
camera_topic = node.setupParameter(
    "camera_topic",
    "/usb_cam/image_raw/compressed"
)
camera_file = node.getFileInPackage(
    'wires_robotic_platform',
    'data/camera_calibration/asus_camera_1_may2017.yml'
)
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=camera_topic,
    compressed_image="compressed" in camera_topic
)

#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera_file, z_up=True)


image_folder = os.path.join(dataset_folder, image_folder)
predictions_file = os.path.join(dataset_folder, predictions_file)
poses_file = os.path.join(dataset_folder, poses_file)
camera_extrinsics_file = os.path.join(dataset_folder, camera_extrinsics_file)
images = sorted(glob.glob(image_folder + "/*.jpg"))

poses = np.loadtxt(poses_file)
predictions = np.loadtxt(predictions_file)


camera_extrinsics = transformations.NumpyVectorToKDL(
    np.loadtxt(camera_extrinsics_file))

camera_poses = []
marker_poses = []
rays = []
for pose in poses:
    p = transformations.NumpyVectorToKDL(pose)
    p = p * camera_extrinsics
    camera_poses.append(p)

valids = []

for index in range(0, len(poses)):
    image = cv2.imread(images[index])
    prediction = predictions[index]
    camera_pose = camera_poses[index]

    #⬢⬢⬢⬢⬢➤ Marker detection
    markers = marker_detector.detectMarkersMap(image, markers_metric_size=0.04)
    marker_id = 700
    marker = None
    if marker_id in markers:
        marker = markers[marker_id]

    if marker:
        marker.draw(image)
        marker = camera_pose * marker

    marker_poses.append(marker)

    #⬢⬢⬢⬢⬢➤ Ferrule detection
    ferrule_position = None
    ferrule_found = False
    for i in range(0, int(prediction[0])):
        x = prediction[1 + i * 5]
        y = prediction[1 + i * 5 + 1]
        w = prediction[1 + i * 5 + 2]
        h = prediction[1 + i * 5 + 3]
        c = prediction[1 + i * 5 + 4]

        width = image.shape[1]
        height = image.shape[0]
        x = x * width - w * width * 0.5
        y = y * height - h * height * 0.5
        w = w * width
        h = h * height

        pos = np.array([x + w * 0.5, y + h * 0.5])

        ratio = min(w / h, h / w)
        if ratio > 0.0:
            if y + h * 0.5 < height * 0.5 and x + w * 0.5 < width * 0.8 and c == 0:
                print ratio
                if ferrule_position != None:
                    if ferrule_position[1] > pos[1]:
                        ferrule_position = pos
                else:
                    ferrule_position = pos
                ferrule_found = True
                cv2.rectangle(image, (int(x), int(y)),
                              (int(x + w), int(y + h)), (0, 255, 0))
                cv2.circle(image, (int(pos[0]), int(
                    pos[1])), 3, (255, 0, 0), -1)
                break

    if ferrule_found and marker != None:

        rays.append(
            Ray3DIntersection.compute3DRay(
                ferrule_position, camera.camera_matrix_inv, camera_pose)
        )
        valids.append(index)
    else:
        rays.append(None)

    cv2.imshow("img", image)
    cv2.waitKey(1)


print(len(poses), len(valids))
ray_intersection = Ray3DIntersection()


iterative_computations = []


for i in range(0, len(valids)):
    counter = 0
    ray_intersection.clear()
    marker_p = np.array([0.0, 0.0, 0.0])
    for index in valids:
        ray_intersection.rays.append(rays[index])
        marker_p = marker_p + np.array([
            marker_poses[index].p.x(),
            marker_poses[index].p.y(),
            marker_poses[index].p.z()
        ])
        # rint(marker_poses[index].p)
        counter = counter + 1
        if counter > i:
            print counter
            break

    marker_p = marker_p / counter
    intersection = ray_intersection.computeIntersection()
    iterative_computations.append(np.hstack((marker_p, intersection)))

iterative_computations = np.array(iterative_computations)
np.savetxt("iterative_computations.txt", iterative_computations)

#

# while node.isActive():
#     print("ok")
#     node.tick()
