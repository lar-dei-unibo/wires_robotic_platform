#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from std_msgs.msg import Float64MultiArray
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.vision.markers import MarkerDetector
from wires_robotic_platform.vision.cameras import CameraRGB
from wires_robotic_platform.vision.terminals import TerminalTray, Terminal, TerminalDetector
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.sci import ClusterBuilder2D
import wires_robotic_platform.utils.conversions as conversions
import wires_robotic_platform.vision.visionutils as visionutils
from scipy.ndimage.measurements import center_of_mass
import wires_robotic_platform.vision.cv as cv
from visualization_msgs.msg import MarkerArray, Marker
from geometry_msgs.msg import Point32, Point
from std_msgs.msg import ColorRGBA
from wires_robotic_platform.utils.ros import RosNode

import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import aruco
import rospkg
import numpy as np
import math


class prediction(object):
    """
    Object that represent a generic detection obtained from an object detection system.
    Each prediction is associated with a bounding box
    """

    def __init__(self, coordinates, class_id, confidence, class_name=None):
        """
        Create a prediction
        Args:
            coordinates: 4 float representing the coordinates of the predicted bounding box in realitve dimension [ymin,xmin,ymax,xmax].
            class_id: id of the predicted class.
            confidence: confidence associated with the prediction
            class_name: optional human readable name of the detected class
        """
        assert len(coordinates) == 4
        self.ymin = coordinates[0]
        self.xmin = coordinates[1]
        self.ymax = coordinates[2]
        self.xmax = coordinates[3]
        self.classId = int(class_id)
        self.confidence = confidence
        self.className = class_name

    def __str__(self):
        if self.className is None:
            return "Class: {}, confidence: {:.2f}, coordinates: {}".format(self.classId, self.confidence, self.box())
        else:
            return "Class: {}-{}, confidence: {:.2f}, coordinates: {}".format(self.classId, self.className, self.confidence, self.box())

    def __repr__(self):
        return self.__str__()

    def getClassName(self):
        if self.className is None:
            return str(self.classId)
        else:
            return self.className

    def box(self, center=False):
        """
        Get coordinates of the bounding box as a 4 float array either with [ymin,xmin,ymax,xmax] or if center=True [x_center,y_center,w,h]
        """
        if not center:
            return [self.ymin, self.xmin, self.ymax, self.xmax]
        else:
            return [(self.xmax - self.xmin) / 2, (self.ymax - self.ymin) / 2, self.xmax - self.xmin, self.ymax - self.ymin]

    def getArea(self):
        w = self.xmax - self.xmin
        h = self.ymax - self.ymin
        return w * h

    def getCenter(self):
        return (int(0.5 * (self.xmax + self.xmin)), int(0.5 * (self.ymax + self.ymin)))

    def intersect(self, other):
        return (self.xmin <= other.xmax and self.xmax >= other.xmin and self.ymin <= other.ymax and self.ymax >= other.ymin)

    def intersectionArea(self, other):
        if not self.intersect(other):
            return 0
        else:
            i_xmin = max(self.xmin, other.xmin)
            i_xmax = min(self.xmax, other.xmax)
            i_ymin = max(self.ymin, other.ymin)
            i_ymax = min(self.ymax, other.ymax)
            i_w = i_xmax - i_xmin
            i_h = i_ymax - i_ymin
            return i_w * i_h

    def toArray(self):
        """
        Encode the detection in 6 floats
        """
        return [self.classId, self.ymin, self.xmin, self.ymax, self.xmax, self.confidence]

    @classmethod
    def fromArray(cls, array, centers=False):
        """
        Construct a prediction from an array of six floats.
        The six floats encode [class_id, ymin, xmin, ymax, xmax, confidence] if centers=False, else [class_id, x_center, y_center, width, height, confidence]
        """
        assert(len(array) == 6)
        if not centers:
            return cls(array[1:5], array[0], array[-1])
        else:
            x_c, y_c, w, h = array[1:5]
            xmin = x_c - (w / 2)
            xmax = x_c + (w / 2)
            ymin = y_c - (h / 2)
            ymax = y_c + (h / 2)
            return cls([ymin, xmin, ymax, xmax], array[0], array[-1])

    @classmethod
    def fromMatrix(cls, matrix):
        """
        Construct a list of predictions from a matrix of float
        """
        result = []
        for i in range(matrix.shape(0)):
            result.append(cls.fromArray(matrix[i]))
        return result

    @staticmethod
    def toMatrix(predictions):
        """
        Serialize a bunch of predictions in a matrix of float
        """
        result = np.ndarray(shape=(len(predictions), 6), dtype=float)
        for i, d in enumerate(predictions):
            result[i] = d.toArray()
        return result

    def draw(self, img):
        cv2.rectangle(img,
                      (int(self.xmin), int(self.ymin)),
                      (int(self.xmax), int(self.ymax)),
                      colors_map['blue'],
                      2
                      )

############################################################
############################################################
############################################################
############################################################
############################################################


LINE_COUNTER = 0


def createMarkerLine(ray):
    global LINE_COUNTER
    marker = Marker()
    marker.header.frame_id = "/comau_smart_six/base_link"
    LINE_COUNTER = LINE_COUNTER + 1
    marker.id = LINE_COUNTER
    marker.type = marker.ARROW
    marker.action = marker.ADD
    marker.scale.x = 0.002
    marker.scale.y = 0.005
    marker.scale.z = 0.02

    marker.color.r = colors_map['orange'][2] / 255.0
    marker.color.g = colors_map['orange'][1] / 255.0
    marker.color.b = colors_map['orange'][0] / 255.0
    marker.color.a = 0.3
    marker.colors = [ColorRGBA(), ColorRGBA()]
    marker.points = [Point(), Point()]

    marker.points[0].x = ray[0][0]
    marker.points[0].y = ray[0][1]
    marker.points[0].z = ray[0][2]

    end_point = ray[0] + ray[1] * 0.4
    marker.points[1].x = end_point[0]
    marker.points[1].y = end_point[1]
    marker.points[1].z = end_point[2]

    return marker


def createMarkerSphere(point):
    global LINE_COUNTER
    marker = Marker()
    marker.header.frame_id = "/comau_smart_six/base_link"
    LINE_COUNTER = LINE_COUNTER + 1
    marker.id = 5000
    marker.type = marker.SPHERE
    marker.action = marker.ADD
    marker.scale.x = 0.02
    marker.scale.y = 0.02
    marker.scale.z = 0.02

    marker.color.r = colors_map['green'][2] / 255.0
    marker.color.g = colors_map['green'][1] / 255.0
    marker.color.b = colors_map['green'][0] / 255.0
    marker.color.a = 0.9
    marker.pose.orientation.w = 1
    marker.pose.position.x = point[0]
    marker.pose.position.y = point[1]
    marker.pose.position.z = point[2]

    return marker


class ImageInterestArea(PyKDL.Frame):

    def __init__(self, image, origin_frame, relative_frame=PyKDL.Frame(), polygon_points=[]):
        super(ImageInterestArea, self).__init__()
        self.image = image
        self.M = origin_frame.M
        self.p = origin_frame.p
        self.relative_frame = relative_frame
        self.polygon_points = polygon_points

    def getRelativePolygonPoints(self, dtype=int):
        polygon_origin = self * self.relative_frame
        relative_polygon_points = []
        for p in self.polygon_points:
            pext = PyKDL.Vector(p[0], p[1], 0.0)
            pext = polygon_origin * pext
            relative_polygon_points.append(np.array([
                pext.x(),
                pext.y()
            ],  dtype=dtype))
        return relative_polygon_points

    def buildMask(self, target_image):
        zeros = np.zeros(
            (target_image.shape[0], target_image.shape[1], 1), dtype=np.int8)
        points = self.getRelativePolygonPoints()
        visionutils.drawPolygon(zeros, points, True, (255, 255, 255), -1)
        return zeros

    @staticmethod
    def crop_image(img, tol=0):
        # img is image data
        # tol  is tolerance
        mask = img > tol
        return img[np.ix_(mask.any(1), mask.any(0))]

    def buildCrop(self, target_image, debug_output=False):
        mask = self.buildMask(target_image)
        masked = cv2.bitwise_and(target_image, target_image, mask=mask)

        polygon_origin = self * self.relative_frame
        angle = math.atan2(
            polygon_origin.M[1, 0], polygon_origin.M[0, 0]) * 180 / math.pi
        cols = target_image.shape[1]
        rows = target_image.shape[0]
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)
        masked_rot = cv2.warpAffine(masked, M, (cols, rows))
        cropped = ImageInterestArea.crop_image(masked_rot)

        if debug_output:
            cv2.namedWindow("cropped", cv2.WINDOW_NORMAL)
            # cv2.imshow("rotated", masked_rot)
            # cv2.imshow("mask", mask)
            # cv2.imshow("masked", masked)
            cv2.imshow("cropped", cropped)

        return cropped

    @staticmethod
    def extractPoints(image, th=200):
        points = []
        max_x = 0
        max_x_point = None
        for i in range(0, image.shape[0]):
            for j in range(0, image.shape[1]):
                if image[i, j] > th:
                    if j > max_x:
                        max_x = j
                        max_x_point = np.array([j, i])
                    points.append(np.array([j, i]))
        if len(points) <= 0:
            return [], np.array([0, 0])
        else:
            return points, max_x_point

    @staticmethod
    def saltAndPepperThresholdFilter(image, th=200, kerner_size=2):
        kernel = np.ones((kerner_size, kerner_size), np.uint8)
        cropped = cv2.erode(image, kernel)
        image = cv2.dilate(cropped, kernel)
        image[image < th] = [0]
        return image

    @staticmethod
    def fitALine(points, second_point_dist=20):
        points = np.array(points)
        [vx, vy, x, y] = cv2.fitLine(
            points,
            cv2.DIST_L2,
            0,
            0.01,
            0.01
        )

        line_center = np.array([x, y], dtype=int)
        line_arrow = line_center + np.array([vx, vy]) * second_point_dist

        m = (line_arrow[1] - line_center[1]) / (line_arrow[0] - line_center[0])
        q = line_center[1] - m * line_center[0]
        return [m, q, line_center, line_arrow]

    @staticmethod
    def buildExtrema(m, q, points, max_x):
        x_inf = 10000
        y_inf = m * x_inf + q
        y_minf = m * -x_inf + q
        p_inf = np.array([x_inf, y_inf], dtype=int)
        p_minf = np.array([-x_inf, y_minf], dtype=int)
        max_x_point = np.array([max_x[0], 0])
        max_x_point[1] = m * max_x[0] + q
        zero_point = np.array([0, int(q)])
        return zero_point, max_x_point

    @staticmethod
    def buildRealLine(m, q, source_image, pixel_ratio, invert_m=True, invert_q=True):
        h = int(source_image.shape[0] * 0.5)
        q_rel = q - h

        m_sign = -1 if invert_m else 1
        q_sign = -1 if invert_q else 1
        out_m = m_sign * m
        out_q = q_sign * q_rel * pixel_ratio * 1000  # millimeter
        return out_m, out_q[0]

    @staticmethod
    def cropOnTerminal(source_image, terminal_probable_point, radius=50):
        sphere_mask = np.zeros(
            (source_image.shape[0], source_image.shape[1], 1),
            dtype=np.int8
        )
        cv2.circle(sphere_mask, tuple(terminal_probable_point), 50, (255), -1)
        masked_on_terminal = cv2.bitwise_and(
            source_image,
            source_image,
            mask=sphere_mask
        )
        return masked_on_terminal

    @staticmethod
    def estimatesCableRFs(zero_point, max_x_point, pixel_ratio):
        cable_rf_x = max_x_point - zero_point
        cable_rf_x = cable_rf_x / np.linalg.norm(cable_rf_x)
        cable_rf_y = np.array([cable_rf_x[1], -cable_rf_x[0]])

        #⬢⬢⬢⬢⬢➤ Estimate Cable RF on zero axis
        cable_rf = transformations.KDLFrom2DRF(
            cable_rf_x,
            -cable_rf_y,
            zero_point
        )

        #⬢⬢⬢⬢⬢➤ Estimates Terminal RF on terminal tip
        terminal_rf = transformations.KDLFrom2DRF(
            cable_rf_x,
            -cable_rf_y,
            max_x_point
        )

        cable_length = np.linalg.norm(zero_point - max_x_point) * pixel_ratio
        return cable_rf, terminal_rf, cable_length

    def drawDebug(self, image):
        polygon_origin = self * self.relative_frame
        visionutils.draw2DReferenceFrame(self, image)
        visionutils.draw2DReferenceFrame(polygon_origin, image)
        polygon_points = self.getRelativePolygonPoints()
        #visionutils.drawPolygon(image, polygon_points)

############################################################
############################################################
############################################################
############################################################
############################################################


rays = []


def lineLineIntersection(rays):

    lines = []
    points = []
    for r in rays:
        l = np.array([
            r[1][0],
            r[1][1],
            r[1][2]
        ]).reshape(3, 1)

        p = np.array([
            r[0][0],
            r[0][1],
            r[0][2]
        ]).reshape(3, 1)
        l = l / np.linalg.norm(l)

        lines.append(l)
        points.append(p)

    v_l = np.zeros((3, 3))
    v_r = np.zeros((3, 1))
    for index in range(0, len(lines)):
        l = lines[index]
        p = points[index]
        v = np.eye(3) - np.matmul(l, l.T)
        v_l = v_l + v
        v_r = v_r + np.matmul(v, p)

    x = np.matmul(np.linalg.pinv(v_l), v_r)
    return x


def compute3DRay(point_2d, camera_matrix_inv, camera_pose):
    '''
    Computes 3D Ray from camera
    '''

    point_2d = np.array([
        point_2d[0],
        point_2d[1],
        1.0
    ]).reshape(3, 1)
    ray = np.matmul(camera_matrix_inv, point_2d)
    ray = ray / np.linalg.norm(ray)
    ray = ray.reshape(3)

    ray_v = PyKDL.Vector(ray[0], ray[1], ray[2])
    ray_v = camera_pose * ray_v
    center = camera_pose.p
    ray_dir = ray_v - center
    ray_dir.Normalize()

    line = (
        np.array([center[0], center[1], center[2]]),
        np.array([ray_dir[0], ray_dir[1], ray_dir[2]])
    )
    return line


colors_map = {
    "blue": (243, 150, 33),
    "green": (80, 175, 76),
    "orange": (0, 152, 255)
}

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("terminal_measurement_forvideo")

node.setupParameter("hz", 60)
node.setupParameter("MARKER_ID", 703)
node.setupParameter("MARKER_SIZE", 0.01)
node.setupParameter("VIZ", True)
node.setupParameter("CAMERA_TOPIC", "/usb_cam/image_raw/compressed")
node.setupParameter("CAMERA_CONFIGURATION",
                    'microsoft_live_camera.yml')
node.setupParameter("CAMERA_TF_NAME", 'camera')
fixed_rf = node.setupParameter("FIXED_RF", True)
fixed_2d_rf = node.setupParameter(
    "FIXEX_2D_RF", "1;0;371;187", array_type=float)
gt = node.setupParameter(
    "GT", "0.78;-0.08;-0.475", array_type=float)
#"FIXEX_2D_RF", "-0.996195;-0.0871557;326.517;248.205", array_type=float)


mpub = node.createPublisher('rays_vis', MarkerArray)
marker_array = MarkerArray()

current_prediction = None


def predictionsCallback(msg):
    global current_prediction
    size = int(msg.data[0])
    predictions = []
    for i in range(0, size):
        x = msg.data[1 + i * 5] * 640
        y = msg.data[1 + i * 5 + 1] * 480

        pred = prediction.fromArray((
            msg.data[1 + i * 5 + 4],
            msg.data[1 + i * 5] * 640,
            msg.data[1 + i * 5 + 1] * 480,
            msg.data[1 + i * 5 + 2] * 640,
            msg.data[1 + i * 5 + 3] * 480,
            1
        ), centers=True)

        if pred.classId < 1:
            if y > 130 and y < 190 and x > 80:
                predictions.append(pred)

    if len(predictions) > 0:
        current_prediction = predictions[0]
    else:
        current_prediction = None
    print("PREDICTIONS",      len(predictions))


node.createSubscriber("/darknet_rgb_detector/predictions",
                      Float64MultiArray, predictionsCallback)

fixed_pixel_ratio = node.setupParameter(
    #"FIXED_PIXEL_RATIO", 0.0002975126876346885)
    "FIXED_PIXEL_RATIO", 0.00029539571324978134)
fixed_ratio_correction = node.setupParameter(
    "FIXED_RATION_CORRECTION", 0.83
)
sensor_offset = node.setupParameter(
    "SENSOR_OFFSET", "0;0", array_type=float)
#"SENSOR_OFFSET", "-0.01;-0.0024", array_type=float)
node.setHz(node.getParameter("hz"))

#⬢⬢⬢⬢⬢➤ Offset publisher
wire_params_pub = node.createPublisher("~wire_params", Float64MultiArray)

#⬢⬢⬢⬢⬢➤ Create sCamera Proxy
camera_file = node.getFileInPackage(
    'wires_robotic_platform', 'data/camera_calibration/' + node.getParameter("CAMERA_CONFIGURATION"))
camera_tf_name = node.getParameter("CAMERA_TF_NAME")
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=node.getParameter("CAMERA_TOPIC"),
    compressed_image='compressed' in node.getParameter("CAMERA_TOPIC")
)

# Opencv Frame

click_index = 0
click_points = []
click_points.append(np.array([0.0, 0.0]))
click_points.append(np.array([0.0, 0.0]))


def mouseCallback(event, x, y, flags, param):
    global click_index, click_points
    if event == cv2.EVENT_RBUTTONDOWN:
        click_points[1] = np.array([x, y])
    if event == cv2.EVENT_LBUTTONDOWN:
        click_points[0] = np.array([x, y])


window_setup = False


#⬢⬢⬢⬢⬢➤ Creates marker detector
marker_detector = MarkerDetector(camera_file=camera.getCameraFile(), z_up=True)

markers_map = {
    node.getParameter("MARKER_ID"): node.getParameter("MARKER_SIZE")
}

Logger.log("Marker map:")
Logger.log("{}".format(markers_map))


reference_size = node.getParameter("MARKER_SIZE")

# Lista dei punti che configurano la maskera da analizzare. Si possono pesacre da file
# O creare un set di poligoni buoni da utilizzare. Il rettangolo
# potrebbe sempre andar bene
polygon_points = np.array([
    [0, 0],
    [0, 50],
    [100, 50],
    [100, -50],
    [0, -50]
])
polygon_points = polygon_points * 1  # fattore di scala del poligono


def cameraCallback(frame):
    """ Camera callback. produce FrameRGBD object """
    global window_setup, current_prediction, LINE_COUNTER
    if not window_setup:
        window_setup = True
        cv2.namedWindow("output", cv2.WINDOW_NORMAL)
        cv2.setMouseCallback('output', mouseCallback)
    # frame.rgb_image = cv2.flip(frame.rgb_image, 0)
    output_gray = cv2.cvtColor(frame.rgb_image, cv2.COLOR_BGR2GRAY)

    camera_pose = node.retrieveTransform(
        'camera', '/comau_smart_six/base_link', -1)

    kernel = np.ones((5, 5), np.float32) / 45
    output_gray = cv2.filter2D(output_gray, -1, kernel)

    # output_gray = cv2.fastNlMeansDenoisingMulti(
    #     output_gray, 2, 5, None, 4, 7, 35)
    output = frame.rgb_image.copy()

    # Threshold adattivo sull'immagine per fare backgorund removal
    thresh1 = cv2.adaptiveThreshold(output_gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                    cv2.THRESH_BINARY, 11, 2)
    thresh1[thresh1 == 255] = [50]
    thresh1[thresh1 == 0] = [255]
    #cv2.imshow("th", thresh1)

    #⬢⬢⬢⬢⬢➤ Detects markers
    markers = marker_detector.detectMarkersMap(
        frame.rgb_image, markers_map=markers_map)

    reference_marker = None
    #⬢⬢⬢⬢⬢➤ draw markers
    for id, marker in markers.iteritems():
        # marker.draw(output)
        #⬢⬢⬢⬢⬢➤ Publish Marker
        node.broadcastTransform(marker, marker.getName(),
                                camera_tf_name, node.getCurrentTime())
        reference_marker = marker
        break

    if fixed_rf and current_prediction != None:
        pred = current_prediction
        print("AREA", pred.getArea())
        pixel_ratio = fixed_pixel_ratio

        rays.append(compute3DRay(
            pred.getCenter(),
            camera.camera_matrix_inv,
            camera_pose
        ))

        marker_2d_rf = PyKDL.Frame()
        marker_2d_rf.M = PyKDL.Rotation(
            0, 1, 0,
            -1, 0, 0,
            0, 0, -1
        )
        marker_2d_rf.p = PyKDL.Vector(
            0.5 * (pred.xmin + pred.xmax), pred.ymax, 0)

        # compute a relative frame, potrebbe essere calcolato dalla dimensione del dito
        # del gripper. L'orientamento dipende dal setup. in questo caso è
        # girato per errore
        relative_frame = PyKDL.Frame()

        # Crea un ImageInterestArea che serve ad analizzare/croppare l'area di
        # interesse
        image_area = ImageInterestArea(
            output, marker_2d_rf, relative_frame, polygon_points)
        image_area.drawDebug(output)
        current_prediction.draw(output)
        cropped = image_area.buildCrop(thresh1, debug_output=False)
        cropped_color = cv2.cvtColor(cropped, cv2.COLOR_GRAY2BGR)

        # Filtraggio dell'otuput croppato, semplice erosion/dilation per
        # rimuovere il sale e pepe
        cable_image = ImageInterestArea.saltAndPepperThresholdFilter(
            cropped,
            kerner_size=3
        )

        circle_to_draw = None
        cropped_on_terminal = 2
        while cropped_on_terminal > 0:
            # Compute all white points, and the max X point
            cable_points, max_x_point = ImageInterestArea.extractPoints(
                cable_image)

            #⬢⬢⬢⬢⬢➤ Fit a line on all white points
            if len(cable_points) > 0:
                m, q, line_center, line_arrow = ImageInterestArea.fitALine(
                    cable_points)
            else:
                m = 0
                q = 0
                line_center = np.array([0, 0])
                line_arrow = np.array([0, 0])

            #⬢⬢⬢⬢⬢➤ Compute cable extrema

            zero_point, max_x_point = ImageInterestArea.buildExtrema(
                m, q,
                cable_points,
                max_x_point
            )

            cable_image = ImageInterestArea.cropOnTerminal(
                cable_image,
                max_x_point,
                radius=150
            )

            if cropped_on_terminal > 1:
                cv2.circle(cropped_color, tuple(
                    max_x_point), 50, colors_map['blue'], 1)
                cv2.circle(cropped_color, tuple(
                    max_x_point), 3, colors_map['blue'], -1)

            #⬢⬢⬢⬢⬢➤ Emulates a Do-Until loop
            cropped_on_terminal -= 1

        #⬢⬢⬢⬢⬢➤ Builds a real line in meter
        if len(cable_points) > 0:
            out_m, out_q = ImageInterestArea.buildRealLine(
                m, q,
                cable_image,
                pixel_ratio,
                invert_m=True, invert_q=True
            )
        else:
            out_m = 0
            out_q = 0

        #⬢⬢⬢⬢⬢➤ Estimates Cable length
        cable_rf, terminal_rf, cable_length = ImageInterestArea.estimatesCableRFs(
            zero_point, max_x_point, pixel_ratio)

        #⬢⬢⬢⬢⬢➤ Angle distance correction
        angle_distance_correction = sensor_offset[0] / math.cos(math.atan(m))

        #⬢⬢⬢⬢⬢➤ Publishes Wire Params
        # - sensor_offset[1] * 1000
        sensor_q = out_m * -sensor_offset[0] * \
            1000 + out_q + sensor_offset[1] * 1000

        out_msg = Float64MultiArray()
        out_msg.data = [out_m, sensor_q, cable_length *
                        1000 / fixed_ratio_correction + angle_distance_correction * 1000]
        wire_params_pub.publish(out_msg)

        #⬢⬢⬢⬢⬢➤ DEBUG DRAW
        angle_str = "{:.2f} deg".format(math.atan(out_m) * 180 / np.pi)
        offset_str = "{:.2f} mm".format(out_q)
        length_str = "{:.2f} mm".format(cable_length *
                                        1000 / fixed_ratio_correction + angle_distance_correction * 1000)

        # L'output viene anche ristrasformato a colori solo per uno scopo di
        # visualizzazione
        cable_image = cv2.cvtColor(cable_image, cv2.COLOR_GRAY2BGR)

        visionutils.draw2DReferenceFrame(cable_rf, cable_image)
        visionutils.draw2DReferenceFrame(terminal_rf, cable_image)

        h = int(cable_image.shape[0] * 0.5)
        correction_frame = PyKDL.Frame(PyKDL.Vector(0, -h, 0))
        rel_frame = marker_2d_rf * relative_frame
        rel_frame = rel_frame * correction_frame
        global_terminal_frame = rel_frame * terminal_rf
        visionutils.draw2DReferenceFrame(global_terminal_frame, output)

        cv2.circle(cable_image, tuple(line_center), 5, (255, 255, 0), -1)
        cv2.arrowedLine(cable_image, tuple(line_center), tuple(
            line_arrow), (255, 255, 0), 2, tipLength=0.3)

        line_dir = max_x_point - zero_point
        p_minf = zero_point - line_dir * 1000
        p_inf = zero_point + line_dir * 1000
        cv2.line(cable_image, tuple(p_minf.astype(int)),
                 tuple(p_inf.astype(int)), (255, 0, 255), 1)

        cv2.circle(cable_image, tuple(max_x_point), 5, (0, 0, 255), -1)
        cv2.circle(cable_image, tuple(zero_point), 5, (255), -1)

        cv2.line(cable_image, (0, h),
                 (cable_image.shape[1], h), (255, 255, 0), 1)

        #cv2.namedWindow("cable", cv2.WINDOW_NORMAL)
        #cv2.imshow("cable", cable_image)

        ##cv2.namedWindow("cropped", cv2.WINDOW_NORMAL)
        #cv2.imshow("cropped", cropped)

        ''' MISURA CON DUE PUNTI FATTA A MANO SOLO A SCOPO DI DEBUG
        p1 = click_points[0]
        p2 = click_points[1]
        cv2.circle(output, (int(p1[0]), int(p1[1])), 5, (255, 255, 0), 2)
        cv2.circle(output, (int(p2[0]), int(p2[1])), 5, (255, 255, 0), 2)
        cv2.line(output, (int(p1[0]), int(p1[1])),
                 (int(p2[0]), int(p2[1])), (255, 255, 0), 2)
        measure_line = np.linalg.norm(p1 - p2)
        measure_line_meter = measure_line * \
            reference_marker.measurePixelRatio(reference_size)
        # print(measure_line, measure_line_meter)
        '''

    if node.getParameter('VIZ'):

        intersection = lineLineIntersection(rays).reshape(3)
        gt_point = np.array(gt).reshape(3)
        dist = np.linalg.norm(gt_point - intersection)
        print("RAYS", len(rays), intersection, gt_point, dist)

        gt_str = "Ground Truth: {:.3f},{:.3f},{:.3f}".format(
            gt[0], gt[1], gt[2])
        intersection_str = "Estimated point: {:.3f},{:.3f},{:.3f}".format(
            intersection[0], intersection[1], intersection[2])
        error_str = "Estimation error: {:.4f} m".format(dist)
        counter_str = "Rays counter: {:d}".format(int(len(rays) * 0.5))

        text_x = 10
        text_y = 340
        tpt1 = np.array([text_x, text_y], dtype=int)
        tpt2 = np.array([text_x, tpt1[1] + 40], dtype=int)
        tpt3 = np.array([text_x, tpt2[1] + 40], dtype=int)
        tpt4 = np.array([text_x, tpt3[1] + 40], dtype=int)

        cv2.rectangle(output, (tpt1[0] - 20, tpt1[1] - 30),
                      (1500, 1500), colors_map['green'], -1)

        fsize = 0.7
        cv2.putText(output, gt_str, tuple(tpt1),
                    cv2.FONT_HERSHEY_DUPLEX, fsize, (255, 255, 255))
        cv2.putText(output, intersection_str, tuple(tpt2),
                    cv2.FONT_HERSHEY_DUPLEX, fsize, (255, 255, 255))
        cv2.putText(output, counter_str, tuple(tpt3),
                    cv2.FONT_HERSHEY_DUPLEX, fsize, (255, 255, 255))
        cv2.putText(output,  error_str, tuple(tpt4),
                    cv2.FONT_HERSHEY_DUPLEX, fsize, (255, 255, 255))

        LINE_COUNTER = 0
        marker_array.markers = []

        for index in range(0, len(rays)):
            if index % 5 == 0:
                r = rays[index]
                rmarker = createMarkerLine(r)
                marker_array.markers.append(rmarker)
        minter = createMarkerSphere(intersection)
        marker_array.markers.append(minter)
        mpub.publish(marker_array)

        cv2.imshow("output", output)

        c = cv2.waitKey(1)



#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)


while node.isActive():

    # marker_pub.publish(plane)
    node.tick()
