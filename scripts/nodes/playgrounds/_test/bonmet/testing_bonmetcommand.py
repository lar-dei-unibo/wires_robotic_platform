#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
import scipy
import time
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode
import numpy as np
from sensor_msgs.msg import JointState
from std_msgs.msg import UInt32


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("test_ad_minchiam")
p = [1.0, 0, 0, 0, 0, 0]
node.setupParameter("hz", 10)
node.setHz(node.getParameter("hz"))
node.setupParameter("p", [0, 0, 0, 0, 0, 0])
p = node.getParameter("p")

pub = node.createPublisher("/bonmetc60/joint_command", JointState, queue_size=1)
msg = JointState()
msg.position = p
msg.velocity = []
msg.effort = []

pub_sync = node.createPublisher("/bonmetc60/external_command", UInt32, queue_size=1)
sync = UInt32()
sync.data = 1010

print("ready")
while node.isActive():
    try:
        print(p)
        # pub_sync.publish(sync)
        pub.publish(msg)
        node.tick()
    except Exception as e:
        print e
