#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import time
from threading import Timer

import numpy as np
import rospy
import scipy
from sensor_msgs.msg import JointState
from std_msgs.msg import UInt32

from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.utils.ros import RosNode

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("test_ad_minchiam")
node.setupParameter("hz", 10)
node.setHz(node.getParameter("hz"))
node.setupParameter("p", [0, 0, 0, 0, 0, 0])
p = node.getParameter("p")

stage = 0
counter = 0
Tr = [[0.6, 0.1, -0.7, 0.1, 0.9, -0.5],
      [0.4, -0.2, -0.4, 0.1, 0.9, -0.7],
      [-0.2, -0.2, -0.4, 0.1, 0.9, -1.3],
      [-0.2, 0, -0.9, 0.1, 0.6, -1.3]]

times = [50, 50, 50, 50]


pub = node.createPublisher("/bonmetc60/joint_command", JointState, queue_size=1)
pub_sync = node.createPublisher("/bonmetc60/external_command", UInt32, queue_size=1)

sync = UInt32()
sync.data = 1010


print("ready")
while node.isActive():
    try:

        counter += 1
        if counter >= times[stage]:
            p = Tr[stage]
            counter = 0

            if stage < len(times) - 1:
                stage += 1
            else:
                stage = 0

        msg = JointState()
        msg.position = p
        msg.velocity = []
        msg.effort = []
        pub.publish(msg)
        print p
        node.tick()
    except Exception as e:
        print e
