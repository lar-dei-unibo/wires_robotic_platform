#!/usr/bin/env python
"""
This nodes is a testing node to show CAD Library tools to Load XML files and show result in RVIZ and as STL files
"""
from wires_robotic_platform.partdb.cad import Scene, Shape, Face, STLUtils, Transform, Part, Terminal, PartCollection, Gearbox, Channel, Component
import wires_robotic_platform.utils.transformations as transformations
import wires_robotic_platform.utils.visualization as visualization
from wires_robotic_platform.utils.ros import RosNode
import PyKDL
import numpy as np
import rospy
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point
import random
import math
import tf
import os
import xml.etree.ElementTree as ET

# MARKER ARRAY TOPIC
topic = 'visualization_marker_array'
publisher = rospy.Publisher(topic, MarkerArray, queue_size=1)

# NODE
node = RosNode('testing_database')
rate = rospy.Rate(10)  # 10hz
br = tf.TransformBroadcaster()
listener = tf.TransformListener()

# PARAMS
output_stl = rospy.get_param('~output_stl', False)
filtered_components = rospy.get_param('~filtered_components', "")
random_pos = rospy.get_param('~random_pos', False)

# Gearbox
# '/home/lar/Desktop/gearbox_zero/'
# files_path = "/home/riccardo/ros/wires_ws/src/wires_robotic_platform/data/gearbox_zero/"
files_path = "/home/riccardo/Desktop/gearbox_zero/"

# files_path = '/home/lar-do/work/workspace_wires/src/wires_robotic_platform/data/gearbox_zero/'
gearbox = Gearbox("gearbox00001", files_path)

# Part
prts = Component()

if random_pos:
    gearbox.base.p = PyKDL.Vector(
        random.uniform(-1, 1), random.uniform(-1, 1), random.uniform(-1, 1))
    gearbox.base.M.DoRotX(random.uniform(-0.7, 0.7))
    gearbox.base.M.DoRotY(random.uniform(-0.7, 0.7))
    gearbox.base.M.DoRotZ(random.uniform(-0.7, 0.7))

# Output STL files
if output_stl:
    output_folder = os.path.join(files_path, "out")
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
    gearbox.scene.exportSTLData(output_folder, base_transform=False)
    gearbox.scene.exportSTLData(output_folder, False,
                                prefix='corrected_', correction=True)
    gearbox.scene.exportSTLData(output_folder, False,
                                prefix='boxed_', correction=True, box_rf=True)


def broadcastTransform(br, frame, frame_id, parent_frame, time=rospy.get_rostime()):
    br.sendTransform((frame.p.x(), frame.p.y(), frame.p.z()),
                     frame.M.GetQuaternion(),
                     time,
                     frame_id,
                     parent_frame)


transforms = []
gearbox.scene.deepSearch(transforms, only_useful=True)
markerArray = MarkerArray()
package_path = 'file://' + files_path + 'out/'


# for trans in gearbox.useful_transforms:
# print str(trans), '->', str(trans.getRoot())

id_counter = 0
for trans in transforms:

    if len(filtered_components) > 0:
        if trans.item_id.id_list[0] not in filtered_components:
            continue

    parent = trans.parent
    path = package_path + trans.getName() + ".stl"
    color = visualization.Color(0.5, 0.5, 0.5, 1)
    if trans.getName() == 'ID000074' or trans.getName() == 'ID000039':
        color = visualization.Color(1, 0, 0, 1)
    marker = visualization.createMesh(parent.getName(), mesh_path=path,
                                      transform=trans, color=color)
    marker.id = len(markerArray.markers)
    markerArray.markers.append(marker)

    if len(trans.item_id.id_list) == 3:
        part = gearbox.parts.getPartById(trans.item_id.id_list[2])
        if part != None:
            for terminal in part.terminals:

                arrow = visualization.createArrow(
                    trans.getName() + "_final", terminal)
                arrow.id = len(markerArray.markers)
                markerArray.markers.append(arrow)

# Display Channels
for channel_id in gearbox.channels.channel_map:
    channel = gearbox.getChannel(channel_id)
    transform = channel.getRelativeFrame()
    arrow = visualization.createArrow(
        "gearbox", transform, length=1.3)
    arrow.id = len(markerArray.markers)
    markerArray.markers.append(arrow)

    points = channel.get2DStartEndPoint()
    marker = visualization.createLineList(
        "gearbox", thickness=0.002, points=points, color=visualization.Color(0, 1, 0, 1))
    marker.id = len(markerArray.markers)
    markerArray.markers.append(marker)

for intersection in gearbox.channels.getIntersections():
    int_trans = intersection.getFrame()
    sphere = visualization.createSphere(
        "gearbox", transform=int_trans, radius=0.1)
    sphere.id = len(markerArray.markers)
    markerArray.markers.append(sphere)


while not rospy.is_shutdown():

    # Move Gearbox on gearbox_rf if it is available
    try:
        tf_gearbox = listener.lookupTransform(
            '/world', '/gearbox_rf', rospy.Time(0))

        correction = gearbox.scene.getCorrection(
            gearbox.base, box_correction=False)

        tf_gearbox_frame = transformations.tfToKDL(tf_gearbox)
        tf_gearbox_frame = tf_gearbox_frame * correction.Inverse()
        gearbox.base.p = tf_gearbox_frame.p
        gearbox.base.M = tf_gearbox_frame.M
    except:  # (tf.ExtrapolationException, tf.LookupException) as e:
        print ('error')

    current_time = rospy.get_rostime()

    for marker in markerArray.markers:
        marker.header.stamp = current_time
    publisher.publish(markerArray)

    # Broadcast Channel tF
    for intersection in gearbox.channels.getIntersections():
        broadcastTransform(br, intersection.getFrame(), intersection.getName(),
                           "gearbox", time=current_time)

    # Broadcast Component tF
    for trans in gearbox.useful_transforms:
        if trans.item_id == None:
            continue

        if trans.isRoot():
            correction = gearbox.scene.getCorrection(
                trans, cad_correction=True)  # , box_correction=True

            broadcastTransform(br, correction, "gearbox",
                               "world", time=current_time)

            broadcastTransform(br, trans, trans.getName(),
                               "gearbox", time=current_time)

            # correction = gearbox.scene.getCorrection(
            #         trans, box_correction=True)
            # broadcastTransform(br, correction, "gearbox",
            #                    trans.getName(), time=current_time)

        else:
            broadcastTransform(br, trans, trans.getName(),
                               trans.parent.getName(), time=current_time)

            boxed_transform = gearbox.scene.getCorrection(trans)
            # broadcastTransform(br, boxed_transform, trans.getName() + "_final",
            #  trans.getName(), time=current_time)
            # add the tf of all part of the component #
            tf_list = []
            tfparent = prts.parenttf(trans.getName(), boxed_transform)
            tf_list = prts.addtf(trans.getName())
            broadcastTransform(br, tfparent, trans.getName() + "_corner",
                               trans.getName(), time=current_time)  # add the corner reference frame,trans.getName()
            for comp_tf in tf_list:
                broadcastTransform(br, comp_tf[1], comp_tf[0],
                                   trans.getName() + "_corner", time=current_time)

    rate.sleep()
