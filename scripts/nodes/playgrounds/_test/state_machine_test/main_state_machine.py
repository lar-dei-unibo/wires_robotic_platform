#!/usr/bin/env python

import rospy
import roslaunch
from wires_robotic_platform.msg import sms_msg

class StateMachine():

    def __init__(self):
        package = 'wires_robotic_platform'      
        executable_s1 = 'state1.py' 
        executable_s2 = 'state2.py'
        self.node_s1 = roslaunch.core.Node(package, executable_s1)      # node object of state 1 
        self.node_s2 = roslaunch.core.Node(package, executable_s2)      # node object of state 2 

        self.launchProcess = roslaunch.scriptapi.ROSLaunch()            # create a scripting interface for roslaunch
        self.process = None                                             

    def state_transition_callback(self, msg):
        if msg.status == msg.STATUS_DONE:
            self.process.stop()         # stop the running node                    

            if msg.id == 1:
                rospy.loginfo("state 1: DONE. ")
                self.process = self.launchProcess.launch(self.node_s2)      # move to the state 2
            elif msg.id == 2:
                rospy.loginfo("state 2: DONE. ")
                self.process = self.launchProcess.launch(self.node_s1)      # move to the state 1
            else:
                rospy.logerr("NO_STATE_TRANSITION_ERROR")

    def start_state_machine(self):

        rospy.init_node('state_machine_main')
        rospy.loginfo("state machine: started")
        
        # initial state 
        node_to_launch = self.node_s1

        # start roslaunch
        self.launchProcess.start()

        # lauch the node (and return an 'handler' of the correspondent process) 
        self.process = self.launchProcess.launch(node_to_launch)

        # subscrive to the topic "sms" ("state machine status")
        rospy.Subscriber("sms", sms_msg, self.state_transition_callback)
        rospy.spin()  


if __name__ == '__main__':
    sm = StateMachine()
    try:
        sm.start_state_machine()
    except rospy.ROSInterruptException:
        pass
