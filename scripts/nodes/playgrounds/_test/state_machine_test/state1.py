#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import numpy
from numpy import *
from wires_robotic_platform.msg import sms_msg

def state1():
    smsPublisher = rospy.Publisher('sms', sms_msg, queue_size=10)
    rospy.init_node('s1', anonymous=True)
    rate = rospy.Rate(10) # 10hz

    sms = sms_msg()
    sms.id = 1
    sms.status = 1              # STATUS_PROGRESS
    smsPublisher.publish(sms)   

    rospy.loginfo("state 1: start")

    var = 1

    while not rospy.is_shutdown():
        
        var += 1
        if var == 10:
            sms.status = 0   #STATUS_DONE
        elif var <10:
            sms.status = 1   #STATUS_PROGRESS
        else:            
            sms.status = 2   #STATUS_ERROR_UNKNOWN

        smsPublisher.publish(sms)
        rate.sleep()

if __name__ == '__main__':
    try:
        state1()
    except rospy.ROSInterruptException:
        pass
