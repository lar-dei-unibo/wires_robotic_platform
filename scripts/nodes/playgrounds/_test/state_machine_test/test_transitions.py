#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import numpy
from numpy import *
from wires_robotic_platform.msg import sms_msg


def state2():
    smsPublisher = rospy.Publisher('sms', sms_msg, queue_size=10)
    rospy.init_node('s2', anonymous=True)
    rate = rospy.Rate(10)  # 10hz

    sms = sms_msg()
    sms.id = 2
    sms.status = 1                  # STATUS_PROGRESS
    smsPublisher.publish(sms)

    var = 10

    while not rospy.is_shutdown():

        var -= 1
        if var == 0:
            sms.status = 0  # STATUS_DONE
        elif var > 0:
            sms.status = 1  # STATUS_PROGRESS
        else:
            sms.status = 2  # STATUS_ERROR_UNKNOWN

        smsPublisher.publish(sms)
        rate.sleep()


if __name__ == '__main__':
    try:
        state2()
    except rospy.ROSInterruptException:
        pass
