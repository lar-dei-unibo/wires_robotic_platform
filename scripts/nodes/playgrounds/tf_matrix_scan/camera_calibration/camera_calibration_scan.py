#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32
from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient
from wires_robotic_platform.proxy.proxy_message import SimpleMessage
import wires_robotic_platform.utils.transformations as transformations
import message_filters
from wires_robotic_platform.vision.cameras import CameraRGB
import wires_robotic_platform.vision.cv as cv
import cv2
import os
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import cv2 
import rospkg
import numpy as np
import math
import json
from wires_robotic_platform.utils.tfmatrix import TfMatrixCube, TfMatrixHyperCube, TfMatrixSphere
from std_msgs.msg import Int32
from geometry_msgs.msg import Pose
import threading

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("matrix_tf_sequencer")
node.setupParameter("hz", 30)
node.setHz(node.getParameter("hz"))
delay_time = 2  # node.setupParameter("delay", 2)

moving_pub = node.createPublisher("~moving_flag", Int32)
pose_pub = node.createPublisher("~tool_pose", Pose)

counter = 0
active_command = None

moving_flag = -1

robot_name = "bonmetc60"  # @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ CAMERA ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

node.setupParameter("CAMERA_TOPIC", "/usb_cam_1/image_raw")
node.setupParameter("OUTPUT_PATH", "/tmp/saved_frames")
node.setupParameter("SAVE_WITH_TF", True)
node.setupParameter("TF_BASE", "/{}/base_link".format(robot_name))
node.setupParameter("TF_TARGET", "/{}/link6".format(robot_name))

tf_enabled = node.getParameter("SAVE_WITH_TF")

#⬢⬢⬢⬢⬢➤ sCamera Proxy
camera_file = node.getFileInPackage('wires_robotic_platform', 'data/camera_calibration/microsoft_live_camera.yml')
camera_tf_name = "camera"
camera = CameraRGB(
    configuration_file=camera_file,
    rgb_topic=node.getParameter("CAMERA_TOPIC")
)

frame_save_counter = 0
frame = None


#⬢⬢⬢⬢⬢➤ Camera Callback
def cameraCallback(frame_msg):
    """ produce FrameRGBD object """
    global frame
    frame = frame_msg


#⬢⬢⬢⬢⬢➤ Frame Saver
def saveFrame():
    global frame_save_counter
    if frame is not None:
        output = frame.rgb_image.copy()

        tf_target = None
        if tf_enabled:
            tf_target = node.retrieveTransform(
                node.getParameter("TF_TARGET"),
                node.getParameter("TF_BASE"),
                -1
            )
            if tf_target == None:
                return

        # cv2.imshow("output", output)

        if not os.path.exists(node.getParameter("OUTPUT_PATH")):
            os.makedirs(node.getParameter("OUTPUT_PATH"))

        coutner_str = str(frame_save_counter).zfill(5)
        frame_name = "frame_{}.png".format(coutner_str)
        tffile_name = "pose_{}.txt".format(coutner_str)
        frame_save_counter += 1
        filename = os.path.join(
            node.getParameter("OUTPUT_PATH"), frame_name)
        filenametf = os.path.join(
            node.getParameter("OUTPUT_PATH"), tffile_name)
        cv2.imwrite(filename, output)
        if tf_enabled:
            np.savetxt(
                filenametf,
                transformations.KDLtoNumpyVector(tf_target)
            )
        Logger.log("\nSaved frames:{}".format(coutner_str))
    else:
        Logger.warning("Frame is None")


#⬢⬢⬢⬢⬢➤ Camera Msgs Callback
camera.registerUserCallabck(cameraCallback)

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ SUPERVISOR COMUNIOCATION ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇


def goToTf(tf_name, tool="none"):
    #⬢⬢⬢⬢⬢➤ Go To Tf Helper Function
    global command_proxy_client, moving_flag
    message = SimpleMessage(
        receiver="{}_supervisor".format(robot_name), command="jumptotf")
    message.setData("tf_name", tf_name)
    message.setData("tool_name", tool)
    # print("Sending", message.toString())
    active_command = command_proxy_client.sendCommand(message.toString())
    moving_flag = 1


def sendNewMessage():
    #⬢⬢⬢⬢⬢➤ Send New Message to Proxy
    global counter, matrix_tf
    frame_name, _ = matrix_tf.pickNextFrame()
    goToTf(frame_name)


def supervisor_done_callback(command):
    #⬢⬢⬢⬢⬢➤ Action Callback
    global moving_flag, matrix_tf, matrix_sphere
    # print("Response:", str(command))
    moving_flag = 0
    if matrix_tf.isEmpty():
        matrix_tf = matrix_sphere

    time.sleep(1.5)
    saveFrame()
    time.sleep(5)
    sendNewMessage()


command_proxy_client = CommandProxyClient("{}_supervisor".format(robot_name))
command_proxy_client.registerDoneCallback(supervisor_done_callback)


# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ TF MATRIX ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

node.setupParameter("scale_cube", 0.75)
node.setupParameter("offset_cube", [0.0, 0, 0])

#⬢⬢⬢⬢⬢➤ Retrive Chessboard TF
base_frame = None
while base_frame is None:
    base_frame = node.retrieveTransform("tf_storage_chessboard", "/{}/base_link".format(robot_name), -1)

print("Chessboard TF FOUND!!!!!!!!")

#⬢⬢⬢⬢⬢➤ Build the TF Matrix
sphere_center_frame = PyKDL.Frame()
sphere_center_frame.M = base_frame.M
sphere_center_frame.p = base_frame.p + PyKDL.Vector(0.00, +0.0, -0.0)  # coords first tf wrt base-tf
matrix_sphere = TfMatrixSphere(
    base=sphere_center_frame,
    size=np.array([1, 1, 1]),
    size_m=np.array([0.8, -0.02, +0.01]),
    indices_offset=np.array([0, 0, 1])
)

matrix_sphere.order(mode="reverse")
scale_factor = node.getParameter("scale_cube")
offset_cube = node.getParameter("offset_cube")
step_size = 0.02 * scale_factor
matrix_shape = [8, 6, 2]
cube_center_frame = PyKDL.Frame()
cube_center_frame.M = base_frame.M
cx = step_size * float(matrix_shape[1] - 1) / 2.0
cy = step_size * float(matrix_shape[0] - 1) / 2.0
cube_center_frame.p = base_frame.p + PyKDL.Vector(cx + offset_cube[0], cy + offset_cube[1], 0.0 + offset_cube[2])    # coords first tf wrt base-tf
matrix_cube = TfMatrixCube(
    base=cube_center_frame,
    size=np.array(matrix_shape),
    size_m=np.array([-step_size, -step_size, +step_size * 0.5])
)

matrix_tf = matrix_cube

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

sendNewMessage()

moving_msg = Int32()
while node.isActive():
    moving_msg.data = moving_flag
    moving_pub.publish(moving_msg)

    tool_pose = Pose()

    tool_frame = node.retrieveTransform("/{}/tool".format(robot_name), "/{}/base_link".format(robot_name), -1)
    if tool_frame is not None:
        tool_pose = transformations.KDLToPose(tool_frame)

    pose_pub.publish(tool_pose)
    node.tick()
