#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.taskmanager.task_manager_state_machine import TaskManagerSM
import json


node = RosNode("task_manager")
node.setupParameter("hz", 50)
node.setHz(node.getParameter("hz"))

task_name = "test_task"

comau_name = Parameters.get("COMAU_NAME")
gripper_name = Parameters.get("SCHUNK_NAME")
robot_list = [comau_name]
# robot_list = [comau_name]
tactile_name = "tactile"
sensor_list = [tactile_name]

insertion_task_name = "insertion_task"
tool_correction_task_name = "tool_correction_task"
wire_tf_filter_task_name = "tf_filter"
subtask_list = []

insertion_control_params = {
    "step_size": 0.0001,
    "force_p_gain": 1000.0,
    "force_threshold": 0.3,
    "threshold": [0.15, 2, 2]
}

######### simple test working ############
# instruction_list = [tactile_name + " reset"]

######### test contorller insertion #########
# instruction_list = ["system sleep 3",
#                     comau_name + " direct active:::" + json.dumps(True),
#                     "system sleep 3",
#                     comau_name + " controllerdisable id:::" + json.dumps(["_all_"]),
#                     "system sleep 3",
#                     comau_name + " controllerselect id:::" + json.dumps(["wire_insertion"]),
#                     "system sleep 3",
#                     comau_name + " controllerparameters parameters:::" + json.dumps({"wire_insertion": insertion_control_params}),
#                     # comau_name + " controllerparameters standard_index:::" + json.dumps({"wire_insertion": "fuzzy"}),
#                     "system sleep 3",
#                     comau_name + " controllerstart input_data:::" + json.dumps({"wire_insertion": insertion_control_inputs}),
#                     "system sleep 3",
#                     insertion_task_name + " waitend condition:::fuzzy_insertion",
#                     "system sleep 3",
#                     comau_name + " direct active:::" + json.dumps(False)]

######### test tool correction #########
# instruction_list = ["system sleep 5",
#                     comau_name + " gototf tf_storage_normal gripper",
#                     "system sleep 5",
#                     tool_correction_task_name + " correct tool_name:::gripper",
#                     "system sleep 5",
#                     comau_name + " gototf tf_storage_normal dynamic"]

######## test salti condizionati #########
# instruction_list = [comau_name + " gototf tf_storage_normal gripper",
#                     gripper_name + " gotoshape shape_open",
#                     "system condition jumpfalse:::___insertion_fail___",
#                     comau_name + " gotoshape shape_tmp_sx",
#                     gripper_name + " gotoshape shape_close",
#                     "___insertion_fail___",
#                     comau_name + " gotoshape shape_tmp_dx"
#                     ]

######## test tf filter #########
# instruction_list = [wire_tf_filter_task_name + " clear tf_name:::terminal_tf",
#                     wire_tf_filter_task_name + " start tf_name:::terminal_tf",
#                     "system sleep 10"
# comau_name + " gototf terminal_tf_filtered gripper"
# ]

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ INSTRUCTION LIST ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

instruction_list = [
    comau_name + " movetotf matrix_cube_3_3_0 none",
    comau_name + " movetotf matrix_cube_3_0_0 none",

    # comau_name + " movetotf tf_storage_pino1 gripper",
    # comau_name + " movetotf tf_storage_pino2 gripper"
]

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

tskm = TaskManagerSM(task_name)

tskm.start(robot_list, sensor_list, instruction_list, subtask_list)
try:
    while node.isActive():
        tskm.stepForward()
        node.tick()
except rospy.ROSInterruptException:
    pass
