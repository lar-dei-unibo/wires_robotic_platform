#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.taskmanager.task_manager_state_machine import TaskManagerSM
import wires_robotic_platform.utils.transformations as transformations
import json


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("wires_task")
    node.setupParameter("hz", 50)
    node.setHz(node.getParameter("hz"))

    task_name = "wires_task"

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ Parameters ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    #⬢⬢⬢⬢⬢➤ Robots
    manip_name = Parameters.get("BONMET_NAME")
    robot_list = [manip_name]

    #⬢⬢⬢⬢⬢➤ Sensors
    tactile_name = "tactile"
    sensor_list = [tactile_name]

    #⬢⬢⬢⬢⬢➤ Subtasks
    connections_scan_task_name = "connections_scan"
    subtask_list = [connections_scan_task_name]

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ INSTRUCTION LIST ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    instruction_list = [

        "___start___",

        connections_scan_task_name + " next",
        "system condition jumpfalse:::___end___",
        "system sleep 1",
        "___approach___",
        manip_name + " jumptotf target_s_1 gripper",
        "system condition jumpfalse:::___approach___",
        "system sleep 1",
        "___contact___",
        manip_name + " jumptotf target_s_0 gripper",
        "system condition jumpfalse:::___contact___",
        "system sleep 1",
        manip_name + " jumptotf target_s_2 gripper",
        "system condition jumpfalse:::___contact___",
        "system sleep 1",
        "___rise___",
        manip_name + " jumptotf target_s_1 gripper",
        "system condition jumpfalse:::___rise___",
        "system sleep 1",

        "system condition jumpalways:::___start___",

        "___end___",
        "system sleep 100"

    ]

    ###############################################################################################
    ###############################################################################################
    ###############################################################################################

    tskm = TaskManagerSM(task_name)

    tskm.start(robot_list, sensor_list, instruction_list, subtask_list)
    try:
        while node.isActive():
            tskm.stepForward()
            node.tick()
    except rospy.ROSInterruptException:
        pass
