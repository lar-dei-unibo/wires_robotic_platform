#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pkgutil
import rospy
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Point, Point32, Pose
from std_msgs.msg import String
from std_msgs.msg import Header
import math
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.srv import PolyDetectionService
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger

from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient, CommandMessage
from wires_robotic_platform.taskmanager.task_manager_state_machine import TaskManagerSM
from wires_robotic_platform.proxy.proxy_message import SimpleMessage
import wires_robotic_platform.sfm.machines as machines
import message_filters
from sensor_msgs.msg import Image, CameraInfo
import cv2
import rospkg
from wires_robotic_platform.utils.ros import RosNode
import numpy as np
import math
import rosnode
import sys
import json
import random


if __name__ == '__main__':
    node = RosNode("task_manager")
    node.setupParameter("hz", 50)
    node.setHz(node.getParameter("hz"))

    task_name = "test_istr"

    robot_name = Parameters.get("BONMET_NAME")
    robot_list = [robot_name]
    sensor_list = []
    subtask_list = []

    instruction_list = [

        robot_name + " gototf tf_storage_p1 camera",
        "system sleep 2",
        robot_name + " gototf tf_storage_p2 camera"
        "system sleep 2"

    ]

    ###############################################################################################
    ###############################################################################################
    ###############################################################################################

    tskm = TaskManagerSM(task_name)

    tskm.start(robot_list, sensor_list, instruction_list, subtask_list)
    try:
        while node.isActive():
            tskm.stepForward()
            node.tick()
    except rospy.ROSInterruptException:
        pass
