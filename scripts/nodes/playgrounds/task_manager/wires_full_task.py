#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.taskmanager.task_manager_state_machine import TaskManagerSM
import wires_robotic_platform.utils.transformations as transformations
import json


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("wires_task")
    node.setupParameter("hz", 50)
    node.setHz(node.getParameter("hz"))

    task_name = "wires_task"

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ Parameters ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    #⬢⬢⬢⬢⬢➤ Robots
    manip_name = Parameters.get("BONMET_NAME")
    # gripper_name = Parameters.get("SCHUNK_NAME")
    robot_list = [manip_name]

    #⬢⬢⬢⬢⬢➤ Sensors
    tactile_name = "tactile"
    sensor_list = [tactile_name]

    #⬢⬢⬢⬢⬢➤ Subtasks
    grasshopper_insertion_tactile_task_name = "grasshopper_insertion_tactile"
    connections_scan_task_name = "connections_scan"
    terminal_side_measure_task_name = "terminal_side_measure"
    gripper_insertion_task_name = "grasshopper_insertion"
    gripper_grasp_task_name = "grasshopper_grasp"
    wires_rack_scan_task_name = "wires_rack_scan"
    grasshopper_move_task_name = "grasshopper_move"
    screwdriver_task_name = "screwdriver" 
    subtask_list = [connections_scan_task_name, 
                    gripper_grasp_task_name,
                    wires_rack_scan_task_name,
                    grasshopper_move_task_name,
                    screwdriver_task_name,
                    terminal_side_measure_task_name, 
                    grasshopper_insertion_tactile_task_name]

    OPEN_DIST = " {}".format(Parameters.get("GRIPPER_APERTURE"))
    CLOSE_DIST = " {}".format(Parameters.get("GRIPPER_CLOSURE"))
    CLOSE_DIST_SOFT = " {}".format(Parameters.get("GRIPPER_CLOSURE_SOFT"))

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ INSTRUCTION LIST ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    instruction_list = [

        "___start___",

        wires_rack_scan_task_name + " next",
        "system sleep 0.5",
        gripper_grasp_task_name + OPEN_DIST,
        "system sleep 0.5",


        # --------------------------------
        # ---------- Wire Grasp ----------
        # --------------------------------

        "___midle_pose_0___",
        manip_name + " jumptotf midle_pose gripper",
        "system condition jumpfalse:::___midle_pose_0___",

        grasshopper_move_task_name + " home",
        "system counter1 set:::3",
        "system sleep 0.5",

        "___wire_grasp_0___",
        manip_name + " jumptotf target_wire_1 gripper",
        "system condition jumpfalse:::___wire_grasp_0___",
        manip_name + " jumptotf target_wire_0 gripper",
        gripper_grasp_task_name + CLOSE_DIST,
        "system sleep 0.5",
        "___wire_grasp_1___",
        manip_name + " jumptotf target_wire_1 gripper",
        "system condition jumpfalse:::___wire_grasp_1___",

        "system condition jumpalways:::___midle_pose_1___",
        
        # repete from here only if the previous insertion has failed
        "___insertion_fail___", 
        "system counter1 step:::1",
        "system condition jumpcounter1:::___continue_task___",
        "system flag raise:::1", 
        gripper_grasp_task_name + CLOSE_DIST,
        "system sleep 0.5",
        screwdriver_task_name + " unscrew",
        "system sleep 0.5",
        "___raise_screwdriver_0___", 
        manip_name + " jumptotf target_screw_1 gripper",
        "system condition jumpfalse:::___raise_screwdriver_0___",

        #Grasshopper Distance from Target= 0.00859362378066
        #era commentata perche non riesce a raggiungere la posizione di home con il cavo nel gripper.
        grasshopper_move_task_name + " home", 
        "system sleep 0.5", 
       

        "___midle_pose_1___",
        manip_name + " jumptotf midle_pose gripper",
        "system condition jumpfalse:::___midle_pose_1___",

        # --------------------------------
        # ------ Terminal Measure --------
        # --------------------------------
 

        "___terminal_side_measure___",
        manip_name + " jumptotf side_vision gripper",
        "system condition jumpfalse:::___terminal_side_measure___",
        "system sleep 2.5",
        terminal_side_measure_task_name + " detect",
        "system sleep 0.5",


        "___midle_pose_2___",
        manip_name + " jumptotf midle_pose gripper",
        "system condition jumpfalse:::___midle_pose_2___",

        # --------------------------------
        # ------ Driver in Screw ----------
        # --------------------------------

        "system condition jumpflag1:::___approach___",
        connections_scan_task_name + " next",
        "system condition jumpfalse:::___end___",
        "system sleep 1",
        
        "___approach___",
        "system flag lower:::1",
        manip_name + " jumptotf target_screw_1 gripper",
        "system condition jumpfalse:::___approach___",  # approach
        "system sleep 0.5",

        grasshopper_move_task_name + " y dist:::_detect_it_",
        "system sleep 0.5",

        "___contact___",
        manip_name + " jumptotf target_screw_0 gripper",
        "system condition jumpfalse:::___contact___",   # contact
        "system sleep 0.5",
        
        screwdriver_task_name + " approach",

        manip_name + " jumptotf target_screw_2 gripper",    # press
        "system sleep 0.5",

        # screwdriver_task_name + " unscrew", # TODO unscrew has no stop
        # "system sleep 0.5",

        # --------------------------------
        # -------- Wire in Hole ----------
        # --------------------------------


        # "___insertion___",
        # screwdriver_task_name + " unscrew",
        # "system sleep 3",
        grasshopper_insertion_tactile_task_name + " push tf_name:::target_hole_1",
        # "system condition jumpfalse:::___insertion___",
        "system sleep 1",
        screwdriver_task_name + " screw",
        "system sleep 1",
        gripper_grasp_task_name + CLOSE_DIST_SOFT,
        "system sleep 0.5",
        grasshopper_insertion_tactile_task_name + " pull",
        "system condition jumpfalse:::___insertion_fail___",

        "___continue_task___",
        # "system counter1 set:::3",
        gripper_grasp_task_name + OPEN_DIST,
        "system sleep 0.5",

        "___raise_screwdriver_1___",
        manip_name + " jumptotf target_screw_1 gripper",
        "system condition jumpfalse:::___raise_screwdriver_1___",
        "system sleep 0.5",

        "system condition jumpalways:::___start___",

        "___end___",
        "system sleep 0.5"

    ]

    ###############################################################################################
    ###############################################################################################
    ###############################################################################################

    tskm = TaskManagerSM(task_name)

    tskm.start(robot_list, sensor_list, instruction_list, subtask_list)
    try:
        while node.isActive():
            tskm.stepForward()
            node.tick()
    except rospy.ROSInterruptException:
        pass
