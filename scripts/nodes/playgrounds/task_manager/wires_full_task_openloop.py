#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
import time
from PyKDL import Frame, Vector, Rotation
import PyKDL
import tf
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.param.global_parameters import Parameters
from wires_robotic_platform.taskmanager.task_manager_state_machine import TaskManagerSM
import wires_robotic_platform.utils.transformations as transformations
import json


if __name__ == '__main__':

    #⬢⬢⬢⬢⬢➤ NODE
    node = RosNode("wires_task_openloop")
    node.setupParameter("hz", 50)
    node.setHz(node.getParameter("hz"))

    task_name = "wires_task"

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ Parameters ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    #⬢⬢⬢⬢⬢➤ Robots
    manip_name = Parameters.get("BONMET_NAME")
    # gripper_name = Parameters.get("SCHUNK_NAME")
    robot_list = [manip_name]

    #⬢⬢⬢⬢⬢➤ Sensors
    tactile_name = "tactile"
    sensor_list = [tactile_name]

    #⬢⬢⬢⬢⬢➤ Subtasks
    grasshopper_insertion_tactile_task_name = "grasshopper_insertion_tactile"
    connections_scan_task_name = "connections_scan"
    terminal_side_measure_task_name = "terminal_side_measure"
    gripper_insertion_task_name = "grasshopper_insertion"
    gripper_grasp_task_name = "grasshopper_grasp"
    wires_rack_scan_task_name = "wires_rack_scan"
    grasshopper_move_task_name = "grasshopper_move"
    screwdriver_task_name = "screwdriver"
    capture_image_task_name = "capture_image"  # TODO only for test
    subtask_list = [connections_scan_task_name,
                    gripper_insertion_task_name,
                    gripper_grasp_task_name,
                    wires_rack_scan_task_name,
                    grasshopper_move_task_name,
                    screwdriver_task_name,
                    terminal_side_measure_task_name,
                    capture_image_task_name,  # TODO only for test
                    grasshopper_insertion_tactile_task_name]

    OPEN_DIST = " {}".format(0.02)
    CLOSE_DIST = " {}".format(0.005)
    CLOSE_DIST_SOFT = " {}".format(0.008)

# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇ INSTRUCTION LIST ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇
# ▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇

    instruction_list = [

        "___start___",

        wires_rack_scan_task_name + " next",
        "system sleep 1",
        gripper_grasp_task_name + OPEN_DIST,
        "system sleep 1",


        # --------------------------------
        # ---------- Wire Grasp ----------
        # --------------------------------

        "___midle_pose_0___",
        manip_name + " jumptotf tf_storage_midle_pose gripper",
        "system condition jumpfalse:::___midle_pose_0___",

        grasshopper_move_task_name + " home",
        "system sleep 1",

        "___wire_grasp_0___",
        manip_name + " jumptotf target_wire_1 gripper",
        "system condition jumpfalse:::___wire_grasp_0___",
        manip_name + " jumptotf target_wire_0 gripper",
        gripper_grasp_task_name + CLOSE_DIST,
        "system sleep 1",
        "___wire_grasp_1___",
        manip_name + " jumptotf target_wire_1 gripper",
        "system condition jumpfalse:::___wire_grasp_1___",

        "___midle_pose_1___",
        manip_name + " jumptotf tf_storage_midle_pose gripper",
        "system condition jumpfalse:::___midle_pose_1___",

        # --------------------------------
        # ------ Driver in Screw ----------
        # --------------------------------

        connections_scan_task_name + " next",
        "system condition jumpfalse:::___end___",
        "system sleep 1",

        "___approach___",
        manip_name + " jumptotf target_screw_1 gripper",
        "system condition jumpfalse:::___approach___",  # approach
        "system sleep 1",

        grasshopper_move_task_name + " y dist:::_detect_it_",
        "system sleep 1",

        "___contact___",
        manip_name + " jumptotf target_screw_0 gripper",
        "system condition jumpfalse:::___contact___",   # contact
        "system sleep 1",
        manip_name + " jumptotf target_screw_2 gripper",    # press
        "system sleep 1",

        # screwdriver_task_name + " unscrew", # TODO unscrew has no stop
        # "system sleep 1",

        # --------------------------------
        # -------- Wire in Hole ----------
        # --------------------------------

        gripper_insertion_task_name + " target_hole_1",
        "system sleep 1",

        "___insertion___",
        gripper_insertion_task_name + " target_hole_0",
        "system condition jumpfalse:::___insertion___",
        "system sleep 1",
        screwdriver_task_name + " screw",
        "system sleep 2",
        gripper_insertion_task_name + " target_hole_1",
        gripper_grasp_task_name + CLOSE_DIST_SOFT,
        "system sleep 1",
        grasshopper_insertion_tactile_task_name + " pull",
        "system sleep 1",

        "___rise___",
        manip_name + " jumptotf target_screw_1 gripper",
        "system condition jumpfalse:::___rise___",
        "system sleep 1",

        "system condition jumpalways:::___start___",

        "___end___",
        "system sleep 1"

    ]

    ###############################################################################################
    ###############################################################################################
    ###############################################################################################

    tskm = TaskManagerSM(task_name)

    tskm.start(robot_list, sensor_list, instruction_list, subtask_list)
    try:
        while node.isActive():
            tskm.stepForward()
            node.tick()
    except rospy.ROSInterruptException:
        pass
