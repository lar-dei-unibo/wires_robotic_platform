#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
import os
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import *
import sys
from wires_robotic_platform.ui.pyqt import PyQtWindow
from wires_robotic_platform.utils.ros import RosNode

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("ui_window_example")

ui_file = node.getFileInPackage("wires_robotic_platform", "data/ui/main.ui")
w = PyQtWindow(uifile=ui_file)


w.run()
