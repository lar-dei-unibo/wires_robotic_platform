#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
import os
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtGui import *
import sys
from wires_robotic_platform.ui.pyqt import PyQtWindow
from wires_robotic_platform.utils.ros import RosNode
from wires_robotic_platform.storage.save_shape import SaveShape
from wires_robotic_platform.storage.save_parameters import SaveParams
from wires_robotic_platform.storage.tf_storage import TfStorage
from wires_robotic_platform.utils.logger import Logger
from wires_robotic_platform.proxy.alarm_proxy import AlarmProxy
from wires_robotic_platform.proxy.proxy_message import SimpleMessage, SimpleMessageProxy
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.transformations import FrameVectorFromKDL, FrameVectorToKDL, ListToKDLVector, KDLVectorToList
from wires_robotic_platform.storage.mongo import MessageStorage
from wires_robotic_platform.proxy.command_proxy import CommandProxyClient
from wires_robotic_platform.stubs.tf_manager_stub import *
from sensor_msgs.msg import JointState
from wires_robotic_platform.param.global_parameters import Parameters

import PyKDL
import rospy
import math
import numpy
import time

import xml.etree.ElementTree as ET

import json
from numpy import *
import tf

from tf.msg import tfMessage
from std_msgs.msg import String, Float64MultiArray

#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("component_gui")
# xml_path = Parameters.get(param="PACKAGE_DATA_FOLDER") + \
#    "/gearbox_zero/Component_tf.xml"
file_path = rospy.get_param('~file_path', "")
xml_path = file_path


class CustomWindow(PyQtWindow):

    def __init__(self, uifile, node):
        super(CustomWindow, self).__init__(uifile=uifile)

        self.node = node

        ############# Variable Initialization #############
        # self.robot_name = "comau_smart_six"
        # self.variable = {}
        # self.listener = tf.TransformListener()
        # self.node.createSubscriber("/tf", tfMessage, self.tf_callback)

        ############# Qt Objects ############
        #self.alarm_set_button.setStyleSheet("background-color: green")
        #self.direct_button.setStyleSheet("background-color: white")
        # #---
        # self.tool_box.currentIndexChanged.connect(self.tool_change)
        self.Insert_type.currentIndexChanged[QtCore.QString].connect(
            self.display_title)
        self.Add_component.released.connect(self.add_component)
        self.Eliminate_tf.released.connect(self.eliminate_component)
        self.Load_file_2.released.connect(self.load_file2)
        # self.shape_filter.textChanged.connect(self.shapeListUpdate)
        # self.tfListUpdate()

    def display_title(self,):

        model1 = QStandardItemModel(self.type_1_display)
        table1 = []
        model2 = QStandardItemModel(self.type_2_display)
        table2 = []

        if str(self.Insert_type.currentText()) == 'hole and/or screw':
            table1.append('insert the coordinate of the hole')
            table2.append('insert the coordinate of the screw')
        if str(self.Insert_type.currentText()) == 'visual':
            table1.append('insert the coordinate of the visual')
            table2.append('the fields below are useless')
        if str(self.Insert_type.currentText()) == 'marker':
            table1.append('insert the coordinate of the marker')
            table2.append('the fields below are useless')
        if str(self.Insert_type.currentText()) == 'correction':
            table1.append('insert the coordinate for the correction')
            table2.append('the fields below are useless')
        for index1 in table1:
            row1 = QStandardItem(index1)
            model1.appendRow(row1)
        for index2 in table2:
            row2 = QStandardItem(index2)
            model2.appendRow(row2)

        self.type_1_display.setModel(model1)
        self.type_1_display.show()
        self.type_2_display.setModel(model2)
        self.type_2_display.show()

    def display_tf(self, tree, root):

        ### Read the file and write it on the display only the tf part ###
        model = QStandardItemModel(self.Display_2)
        table = []

        for code in root:  # analize all the root
            table.append(code.get('name'))
            for couple in code.findall('couple'):
                if couple == code.findall('couple')[0]:
                    table.append('name  \t hole  \t\t\t screw')
                sentence = couple.get('name') + '\t'
                sentence2 = ' \t'
                for subitem in couple.findall('subitem'):
                    if subitem.get('name') == 'hole':
                        for pos in subitem.findall('pos'):
                            sentence = sentence + ' ' + pos.text
                        for rot in subitem.findall('rot'):
                            sentence = sentence + ' ' + rot.text
                    if subitem.get('name') == 'screw':
                        for pos in subitem.findall('pos'):
                            sentence2 = sentence2 + ' ' + pos.text
                        for rot in subitem.findall('rot'):
                            sentence2 = sentence2 + ' ' + rot.text
                sentence = sentence + sentence2
                table.append(sentence)

            for item in code.findall('item'):
                table.append(item.get('name'))
                sentence = ''
                for pos in item.findall('pos'):
                    sentence = sentence + ' ' + pos.text
                for rot in item.findall('rot'):
                    sentence = sentence + ' ' + rot.text
                table.append(sentence)

        for index in table:
            row = QStandardItem(index)
            model.appendRow(row)
        self.Display_2.setModel(model)
        self.Display_2.show()
        return

    def load_file2(self):

        ### open the file ###
        tree = ET.parse(xml_path)
        root = tree.getroot()
        self.display_tf(tree, root)

        return

    def add_component(self):

        ### open the file ###
        tree = ET.parse(xml_path)
        root = tree.getroot()

        ### set the variable ###
        find_code = False
        find_item = False
        find_couple = False
        selector = False

        ###create a model for the error display ###
        model = QStandardItemModel(self.Error_display)
        table = []
        for index in table:
            row = QStandardItem(index)
            model.appendRow(row)
        self.Error_display.setModel(model)
        self.Error_display.show()

        ### check if all text are insert ###
        if not str(self.Insert_code.text()):
            table.append('Insert a code')
            for index in table:
                row = QStandardItem(index)
                model.appendRow(row)
            self.Error_display.setModel(model)
            self.Error_display.show()
            return

        if str(self.Insert_type.currentText()) == 'hole and/or screw':
            selector = True
            table.append(
                'insert all the value for the hole \n and for the corresponding screw if it exist')
            if not str(self.Insert_number.text()):
                table.append('Insert a name')
                for index in table:
                    row = QStandardItem(index)
                    model.appendRow(row)
                self.Error_display.setModel(model)
                self.Error_display.show()
                return

        if not str(self.insert_x1.text()):
            table.append('Insert a x coordinate')
            for index in table:
                row = QStandardItem(index)
                model.appendRow(row)
            self.Error_display.setModel(model)
            self.Error_display.show()
            return

        if not str(self.insert_y1.text()):
            table.append('Insert a y coordinate')
            for index in table:
                row = QStandardItem(index)
                model.appendRow(row)
            self.Error_display.setModel(model)
            self.Error_display.show()
            return

        if not str(self.insert_z1.text()):
            table.append('Insert a z coordinate')
            for index in table:
                row = QStandardItem(index)
                model.appendRow(row)
            self.Error_display.setModel(model)
            self.Error_display.show()
            return

        if not str(self.insert_R1.text()):
            table.append('Insert a R rotation')
            for index in table:
                row = QStandardItem(index)
                model.appendRow(row)
            self.Error_display.setModel(model)
            self.Error_display.show()
            return

        if not str(self.insert_P1.text()):
            table.append('Insert a P rotation')
            for index in table:
                row = QStandardItem(index)
                model.appendRow(row)
            self.Error_display.setModel(model)
            self.Error_display.show()
            return

        if not str(self.insert_Y1.text()):
            table.append('Insert a Y rotation')
            for index in table:
                row = QStandardItem(index)
                model.appendRow(row)
            self.Error_display.setModel(model)
            self.Error_display.show()
            return

        onlyhole = False

        if str(self.Insert_type.currentText()) == 'hole and/or screw':
            if not str(self.insert_x2.text()):
                if not str(self.insert_y2.text()):
                    if not str(self.insert_z2.text()):
                        if not str(self.insert_R2.text()):
                            if not str(self.insert_P2.text()):
                                if not str(self.insert_Y2.text()):
                                    onlyhole = True
            if onlyhole == False:
                if not str(self.insert_x2.text()):
                    table.append('Insert a x coordinate for the screw')
                    for index in table:
                        row = QStandardItem(index)
                        model.appendRow(row)
                    self.Error_display.setModel(model)
                    self.Error_display.show()
                    return

                if not str(self.insert_y2.text()):
                    table.append('Insert a y coordinate for the screw')
                    for index in table:
                        row = QStandardItem(index)
                        model.appendRow(row)
                    self.Error_display.setModel(model)
                    self.Error_display.show()
                    return

                if not str(self.insert_z2.text()):
                    table.append('Insert a z coordinate for the screw')
                    for index in table:
                        row = QStandardItem(index)
                        model.appendRow(row)
                    self.Error_display.setModel(model)
                    self.Error_display.show()
                    return

                if not str(self.insert_R2.text()):
                    table.append('Insert a R rotation for the screw')
                    for index in table:
                        row = QStandardItem(index)
                        model.appendRow(row)
                    self.Error_display.setModel(model)
                    self.Error_display.show()
                    return

                if not str(self.insert_P2.text()):
                    table.append('Insert a P rotation for the screw')
                    for index in table:
                        row = QStandardItem(index)
                        model.appendRow(row)
                    self.Error_display.setModel(model)
                    self.Error_display.show()
                    return

                if not str(self.insert_Y2.text()):
                    table.append('Insert a Y rotation for the screw')
                    for index in table:
                        row = QStandardItem(index)
                        model.appendRow(row)
                    self.Error_display.setModel(model)
                    self.Error_display.show()
                    return

        ### start to add the element ###
        for code in root.findall('code'):  # analize all the code elements
            # if the code is already in the file
            if code.get('name') == str(self.Insert_code.text()):
                find_code = True
                if selector == True:
                    for couple in code.findall('couple'):
                        # if the item is already in the file
                        if couple.get('name') == str(self.Insert_number.text()):
                            find_couple = True
                            for subitem in couple.findall('subitem'):
                                if subitem.get('name') == 'hole':
                                    # analize all the pos
                                    for pos in subitem.findall('pos'):
                                        if pos.get('name') == 'x':
                                            # put the new value on the pos x
                                            pos.text = str(
                                                self.insert_x1.text())
                                        if pos.get('name') == 'y':
                                            # put the new value on the pos y
                                            pos.text = str(
                                                self.insert_y1.text())
                                        if pos.get('name') == 'z':
                                            # put the new value on the pos z
                                            pos.text = str(
                                                self.insert_z1.text())
                                        tree.write(xml_path)
                                    # analize all the rot
                                    for rot in subitem.findall('rot'):
                                        if rot.get('name') == 'R':
                                            # put the new value on the rot a
                                            rot.text = str(
                                                self.insert_R1.text())
                                        if rot.get('name') == 'P':
                                            # put the new value on the rot b
                                            rot.text = str(
                                                self.insert_P1.text())
                                        if rot.get('name') == 'Y':
                                            # put the new value on the rot g
                                            rot.text = str(
                                                self.insert_Y1.text())
                                        tree.write(xml_path)
                                if onlyhole == False:
                                    if subitem.get('name') == 'screw':
                                        # analize all the pos
                                        for pos in subitem.findall('pos'):
                                            if pos.get('name') == 'x':
                                                # put the new value on the pos x
                                                pos.text = str(
                                                    self.insert_x2.text())
                                            if pos.get('name') == 'y':
                                                # put the new value on the pos y
                                                pos.text = str(
                                                    self.insert_y2.text())
                                            if pos.get('name') == 'z':
                                                # put the new value on the pos z
                                                pos.text = str(
                                                    self.insert_z2.text())
                                            tree.write(xml_path)
                                        # analize all the rot
                                        for rot in subitem.findall('rot'):
                                            if rot.get('name') == 'R':
                                                # put the new value on the rot a
                                                rot.text = str(
                                                    self.insert_R2.text())
                                            if rot.get('name') == 'P':
                                                # put the new value on the rot b
                                                rot.text = str(
                                                    self.insert_P2.text())
                                            if rot.get('name') == 'Y':
                                                # put the new value on the rot g
                                                rot.text = str(
                                                    self.insert_Y2.text())
                                            tree.write(xml_path)

                    if find_code == True and find_couple == False:
                        attrib = {'name': str(self.Insert_number.text())}
                        couple = code.makeelement('couple', attrib)
                        code.append(couple)
                        attrib = {'name': 'hole'}
                        subitem1 = code.makeelement('subitem', attrib)
                        couple.append(subitem1)
                        attrib = {'name': 'x'}
                        pos = subitem1.makeelement('pos', attrib)
                        subitem1.append(pos)
                        pos.text = str(self.insert_x1.text())
                        attrib = {'name': 'y'}
                        pos = subitem1.makeelement('pos', attrib)
                        subitem1.append(pos)
                        pos.text = str(self.insert_y1.text())
                        attrib = {'name': 'z'}
                        pos = subitem1.makeelement('pos', attrib)
                        subitem1.append(pos)
                        pos.text = str(self.insert_z1.text())
                        attrib = {'name': 'R'}
                        rot = subitem1.makeelement('rot', attrib)
                        subitem1.append(rot)
                        rot.text = str(self.insert_R1.text())
                        attrib = {'name': 'P'}
                        rot = subitem1.makeelement('rot', attrib)
                        subitem1.append(rot)
                        rot.text = str(self.insert_P1.text())
                        attrib = {'name': 'Y'}
                        rot = subitem1.makeelement('rot', attrib)
                        subitem1.append(rot)
                        rot.text = str(self.insert_Y1.text())
                        tree.write(xml_path)
                        if onlyhole == False:
                            attrib = {'name': 'screw'}
                            subitem2 = code.makeelement('subitem', attrib)
                            couple.append(subitem2)
                            attrib = {'name': 'x'}
                            pos = subitem2.makeelement('pos', attrib)
                            subitem2.append(pos)
                            pos.text = str(self.insert_x2.text())
                            attrib = {'name': 'y'}
                            pos = subitem2.makeelement('pos', attrib)
                            subitem2.append(pos)
                            pos.text = str(self.insert_y2.text())
                            attrib = {'name': 'z'}
                            pos = subitem2.makeelement('pos', attrib)
                            subitem2.append(pos)
                            pos.text = str(self.insert_z2.text())
                            attrib = {'name': 'R'}
                            rot = subitem2.makeelement('rot', attrib)
                            subitem2.append(rot)
                            rot.text = str(self.insert_R2.text())
                            attrib = {'name': 'P'}
                            rot = subitem2.makeelement('rot', attrib)
                            subitem2.append(rot)
                            rot.text = str(self.insert_P2.text())
                            attrib = {'name': 'Y'}
                            rot = subitem2.makeelement('rot', attrib)
                            subitem2.append(rot)
                            rot.text = str(self.insert_Y2.text())
                            tree.write(xml_path)

                else:
                    for item in code.findall('item'):
                        if item.get('name') == str(self.Insert_type.currentText()):
                            find_item = True
                            for pos in item.findall('pos'):  # analize all the pos
                                if pos.get('name') == 'x':
                                    # put the new value on the pos x
                                    pos.text = str(self.insert_x1.text())
                                if pos.get('name') == 'y':
                                    # put the new value on the pos y
                                    pos.text = str(self.insert_y1.text())
                                if pos.get('name') == 'z':
                                    # put the new value on the pos z
                                    pos.text = str(self.insert_z1.text())
                                tree.write(xml_path)
                            for rot in item.findall('rot'):  # analize all the rot
                                if rot.get('name') == 'R':
                                    # put the new value on the rot a
                                    rot.text = str(self.insert_R1.text())
                                if rot.get('name') == 'P':
                                    # put the new value on the rot b
                                    rot.text = str(self.insert_P1.text())
                                if rot.get('name') == 'Y':
                                    # put the new value on the rot g
                                    rot.text = str(self.insert_Y1.text())
                                tree.write(xml_path)

                    ### if exist the same code add only the new item,the new name and the new coordinate ###
                    if find_code == True and find_item == False:
                        attrib = {'name': str(self.Insert_type.currentText())}
                        item = code.makeelement('item', attrib)
                        code.append(item)
                        attrib = {'name': 'x'}
                        pos = item.makeelement('pos', attrib)
                        item.append(pos)
                        pos.text = str(self.insert_x1.text())
                        attrib = {'name': 'y'}
                        pos = item.makeelement('pos', attrib)
                        item.append(pos)
                        pos.text = str(self.insert_y1.text())
                        attrib = {'name': 'z'}
                        pos = item.makeelement('pos', attrib)
                        item.append(pos)
                        pos.text = str(self.insert_z1.text())
                        attrib = {'name': 'R'}
                        rot = item.makeelement('rot', attrib)
                        item.append(rot)
                        rot.text = str(self.insert_R1.text())
                        attrib = {'name': 'P'}
                        rot = item.makeelement('rot', attrib)
                        item.append(rot)
                        rot.text = str(self.insert_P1.text())
                        attrib = {'name': 'Y'}
                        rot = item.makeelement('rot', attrib)
                        item.append(rot)
                        rot.text = str(self.insert_Y1.text())
                        tree.write(xml_path)

        ### if not exist nothing add the new code.the new item,the new name and the new coordinate ###
        if find_code == False:
            attrib = {'name': str(self.Insert_code.text())}
            code = root.makeelement('code', attrib)
            root.append(code)
            if selector == True:
                attrib = {'name': str(self.Insert_number.text())}
                couple = code.makeelement('couple', attrib)
                code.append(couple)
                attrib = {'name': 'hole'}
                subitem1 = code.makeelement('subitem', attrib)
                couple.append(subitem1)
                attrib = {'name': 'x'}
                pos = subitem1.makeelement('pos', attrib)
                subitem1.append(pos)
                pos.text = str(self.insert_x1.text())
                attrib = {'name': 'y'}
                pos = subitem1.makeelement('pos', attrib)
                subitem1.append(pos)
                pos.text = str(self.insert_y1.text())
                attrib = {'name': 'z'}
                pos = subitem1.makeelement('pos', attrib)
                subitem1.append(pos)
                pos.text = str(self.insert_z1.text())
                attrib = {'name': 'R'}
                rot = subitem1.makeelement('rot', attrib)
                subitem1.append(rot)
                rot.text = str(self.insert_R1.text())
                attrib = {'name': 'P'}
                rot = subitem1.makeelement('rot', attrib)
                subitem1.append(rot)
                rot.text = str(self.insert_P1.text())
                attrib = {'name': 'Y'}
                rot = subitem1.makeelement('rot', attrib)
                subitem1.append(rot)
                rot.text = str(self.insert_Y1.text())
                tree.write(xml_path)
                if onlyhole == False:
                    attrib = {'name': 'screw'}
                    subitem2 = code.makeelement('subitem', attrib)
                    couple.append(subitem2)
                    attrib = {'name': 'x'}
                    pos = subitem2.makeelement('pos', attrib)
                    subitem2.append(pos)
                    pos.text = str(self.insert_x2.text())
                    attrib = {'name': 'y'}
                    pos = subitem2.makeelement('pos', attrib)
                    subitem2.append(pos)
                    pos.text = str(self.insert_y2.text())
                    attrib = {'name': 'z'}
                    pos = subitem2.makeelement('pos', attrib)
                    subitem2.append(pos)
                    pos.text = str(self.insert_z2.text())
                    attrib = {'name': 'R'}
                    rot = subitem2.makeelement('rot', attrib)
                    subitem2.append(rot)
                    rot.text = str(self.insert_R2.text())
                    attrib = {'name': 'P'}
                    rot = subitem2.makeelement('rot', attrib)
                    subitem2.append(rot)
                    rot.text = str(self.insert_P2.text())
                    attrib = {'name': 'Y'}
                    rot = subitem2.makeelement('rot', attrib)
                    subitem2.append(rot)
                    rot.text = str(self.insert_Y2.text())
                    tree.write(xml_path)
            else:
                attrib = {'name': str(self.Insert_type.currentText())}
                item = code.makeelement('item', attrib)
                code.append(item)
                attrib = {'name': 'x'}
                pos = item.makeelement('pos', attrib)
                item.append(pos)
                pos.text = str(self.insert_x1.text())
                attrib = {'name': 'y'}
                pos = item.makeelement('pos', attrib)
                item.append(pos)
                pos.text = str(self.insert_y1.text())
                attrib = {'name': 'z'}
                pos = item.makeelement('pos', attrib)
                item.append(pos)
                pos.text = str(self.insert_z1.text())
                attrib = {'name': 'R'}
                rot = item.makeelement('rot', attrib)
                item.append(rot)
                rot.text = str(self.insert_R1.text())
                attrib = {'name': 'P'}
                rot = item.makeelement('rot', attrib)
                item.append(rot)
                rot.text = str(self.insert_P1.text())
                attrib = {'name': 'Y'}
                rot = item.makeelement('rot', attrib)
                item.append(rot)
                rot.text = str(self.insert_Y1.text())
                tree.write(xml_path)

        # open the upload file & display it
        tree = ET.parse(xml_path)
        root = tree.getroot()
        self.display_tf(tree, root)
        return

    def eliminate_component(self):

        ### open the file ###
        tree = ET.parse(xml_path)
        root = tree.getroot()

        ### error handing ###
        model = QStandardItemModel(self.Error_display)
        table = []

        if str(self.Insert_type.currentText()) == 'hole and/or screw' and not str(self.Insert_number.text()):
            table.append('insert the name')

        for index in table:
            row = QStandardItem(index)
            model.appendRow(row)
        self.Error_display.setModel(model)
        self.Error_display.show()

        ### Find & eliminate the specific component ###
        for code in root:  # analize all the root
            if code.get('name') == str(self.Insert_code.text()):
                if str(self.Insert_type.currentText()) == 'hole and/or screw':
                    for couple in code.findall('couple'):
                        if couple.get('name') == str(self.Insert_number.text()):
                            code.remove(couple)
                            tree.write(xml_path)
                else:
                    for item in code.findall('item'):
                        if item.get('name') == str(self.Insert_type.currentText()):
                            code.remove(item)
                            tree.write(xml_path)

            if (not code.findall('item')) and (not code.findall('couple')):
                root.remove(code)
                tree.write(xml_path)

        # open the upload file & display it
        tree = ET.parse(xml_path)
        root = tree.getroot()
        self.display_tf(tree, root)
        return


ui_file = node.getFileInPackage(
    "wires_robotic_platform", "data/ui/component_gui.ui")
w = CustomWindow(uifile=ui_file, node=node)


# w.updateList(commands)

w.run()
