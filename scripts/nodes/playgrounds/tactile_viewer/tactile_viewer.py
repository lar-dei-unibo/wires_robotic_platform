#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import rospy
import math
import random
import tf
import numpy as np
import wires_robotic_platform.utils.transformations as transformations
from wires_robotic_platform.utils.logger import Logger
from cv_bridge import CvBridge, CvBridgeError

from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Image
from std_msgs.msg import String, Float64MultiArray
import json
import cv2

from wires_robotic_platform.utils.ros import RosNode

import matplotlib.pyplot as plt
from scipy import interpolate

side_dim = 4


def tactile_callback(msg):
    global tactile_matrix
    tactile_data = np.array([msg.data])
    # tactile_data = agumentTactileData(tactile_data, side_dim=16)
    tactile_matrix = np.reshape(tactile_data, (side_dim, side_dim))
    tactile_matrix = np.float32(tactile_matrix)
    # tactile_matrix = interpolateTactileMap(tactile_matrix)


def agumentTactileData(tactile_data, side_dim=16):
    ag_data = np.zeros((side_dim * side_dim))
    j = 0
    for i in range(0, len(ag_data), 4):
        ag_data = tactile_data[j]
        j += 1
    return ag_data


def interpolateTactileMap(matrix):
    x = np.arange(0, matrix.shape[1])
    y = np.arange(0, matrix.shape[0])
    # mask invalid values
    matrix = np.ma.masked_invalid(matrix)
    xx, yy = np.meshgrid(x, y)
    # get only the valid values
    x1 = xx[~matrix.mask]
    y1 = yy[~matrix.mask]
    newarr = matrix[~matrix.mask]

    GD1 = interpolate.griddata((x1, y1), newarr.ravel(),
                               (xx, yy),
                               method='cubic')
    return GD1


#⬢⬢⬢⬢⬢➤ NODE
node = RosNode("tf_export_test")

node.setupParameter("hz", 100)
node.setHz(node.getParameter("hz"))

node.createSubscriber("/tactile", Float64MultiArray, tactile_callback)
tactile_matrix = np.zeros((side_dim, side_dim))

plt.ion()

while node.isActive():

    plt.imshow(tactile_matrix, cmap='hot', interpolation='sinc')
    plt.draw()
    plt.pause(0.00001)
    plt.clf()

    node.tick()
