/*------------------------------------------------------------------------------

Copyright (c) 2007-2011 rafael grompone von gioi (grompone@gmail.com) 
Copyright (c) 2012-2014 viorica patraucean (vpatrauc@gmail.com)
  
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.


main.c - This file belongs to ELSDc project (Ellipse and Line Segment 
         Detector with continuous validation)
       - It contains the main function, which reads a pgm image, calls the 
         detector, and writes the result in ASCII and SVG form.

------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
extern "C" {
#include "misc.h"
#include "pgm.h"
#include "svg.h"
#include "polygon.h"
#include "ring.h"
#include "elsdc.h"
}
/*----------------------------------------------------------------------------*/
void drawEllipse(cv::Mat &img, Ring *ering);

/** Main function; takes as argument the name of a pgm image, runs the detection
    on it, and writes the result in ASCII and SVG form.  
 */
int main(int argc, char **argv)
{
    /* check arguments */
    if (argc < 2)
        error("usage: ./elsdc image_name.pgm");

    cv::Mat test = cv::imread(argv[1]);

    cv::Mat grey;
    cv::cvtColor(test, grey, CV_BGR2GRAY);

    int rows = test.rows;
    int cols = test.cols;
    int size = rows * cols;
    PImageDouble in = new_PImageDouble(cols, rows);

    printf("Allocation\n");
    in->data = new double[size];
    in->xsize = cols;
    in->ysize = rows;

    for (int i = 0; i < size; i++)
    {
        int v = grey.at<unsigned char>(i);
        in->data[i] = double(v);
    }

    printf("Created\n");

    PImageInt out; /* output image having the same size as 'in'; the pixels
                          supporting a certain geometric primitive are marked 
                          with the same label */

    int ell_count = 0;      /* number of detected ellipses */
    int *ell_labels = NULL; /* the pixels supporting a certain ellipse are marked 
                          with the same unique label */
    Ring *ell_out = NULL;   /* array containing the parameters of the detected 
                          ellipses; correlated with ell_labels, i.e. the i-th
                          element of ell_labels is the label of the pixels 
                          supporting the ellipse defined by the parameters 
                          ell[i] */

    int poly_count = 0;       /* number of detected polygons */
    int *poly_labels = NULL;  /* the pixels supporting a certain polygon are marked 
                          with the same unique label */
    Polygon *poly_out = NULL; /* array containing the parameters of the detected 
                          polygons; correlated with poly_labels, i.e. the i-th
                          element of ell_labels is the label of the pixels 
                          supporting the polygon defined by the parameters
                          poly[i] */

    FILE *ell_ascii;  /* output file with the parameters of the detected 
                          ellipses -- ASCII format */
    FILE *poly_ascii; /* output file with the parameters of the detected
                          polygons -- ASCII format */
    FILE *fsvg;       /* output file with the detected ellipses and polygons 
                          in vectorial form */
    int i, j;

    /* read input image; must be PGM form */
    //    in = read_pgm_image_double(argv[1]);
    int xsize = in->xsize, ysize = in->ysize;

    /* create and initialize with 0 output label image */
    out = new_PImageInt_ini(in->xsize, in->ysize, 0);

    /* call detection procedure */
    ELSDc(in, &ell_count, &ell_out, &ell_labels, &poly_count, &poly_out,
          &poly_labels, out);

    cv::Mat output = test.clone();

    cv::namedWindow("out", CV_WINDOW_FREERATIO);
    //    cv::flip(output,output,0);
    for (int i = 0; i < ell_count; i++)
    {
        Ring r = ell_out[i];

        drawEllipse(output, &r);
        cv::circle(output, cv::Point2f(r.x1, r.y1), 2, cv::Scalar(255), 2);
        cv::circle(output, cv::Point2f(r.x2, r.y2), 2, cv::Scalar(255), 2);

        //        printf("Angles: %f, %f, %f = %f,%f\n", r.theta * 180.0 / M_PI, r.ang_start * 180.0 / M_PI, r.ang_end * 180.0 / M_PI, r.ax, r.bx);
        //        cv::imshow("out", output);
        //        cv::waitKey(0);
        //                output
        //
        //
        //                );
    }

    for (int i = 0; i < poly_count; i++)
    {
        Polygon p = poly_out[i];
        cv::Point p1(p.pts[0].x, p.pts[0].y);
        cv::Point p2(p.pts[1].x, p.pts[1].y);
        //        cv::line(output,p1,p2,cv::Scalar(255,0,0),1);
    }

    printf("E Found %d\n", ell_count);
    printf("P Found %d\n", poly_count);

    cv::imshow("out", output);
    cv::waitKey(0);

    /* write labels image in pgm form */
    //    write_pgm_image_int(out->data, out->xsize, out->ysize, "labels.pgm");
    //    free_PImageInt(out);
    //    if (ell_out != NULL) {
    //        free(ell_out);
    //        free(ell_labels);
    //    }
    //    if (poly_out != NULL) {
    //        for (i = 0; i < poly_count; i++)
    //            free(poly_out[i].pts);
    //        free(poly_out);
    //        free(poly_labels);
    //    }

    return 0;
}

void drawEllipse(cv::Mat &img, Ring *ering)
{
    int fa, fs;
    double ang_start, ang_end;
    double x1, y1, x2, y2;

    /* consider small arc, in trigonometric sense */
    fa = 0;
    fs = 1;

    if (ering->full) /* if complete ellipse, write full ellipse, not an arc */
    {
        cv::ellipse(img,
                    cv::Point2f(ering->cx, ering->cy),
                    cv::Size(ering->ax, ering->bx),
                    ering->theta * 180 / M_PI,
                    0, 360,
                    cv::Scalar(0, 255, 255));
    }
    else /* compute limits of the arc and write it */
    {
        /* compute starting point */
        rosin_point(ering, ering->x1, ering->y1, &x1, &y1);
        /* compute ending point */
        rosin_point(ering, ering->x2, ering->y2, &x2, &y2);
        if ((double_equal(x1, x2) && double_equal(y1, y2)) || dist(x1, y1, x2, y2) < 2.0)
        {
            cv::ellipse(img,
                        cv::Point2f(ering->cx, ering->cy),
                        cv::Size(ering->ax, ering->bx),
                        ering->theta * 180 / M_PI,
                        0, 360,
                        cv::Scalar(0, 255, 255));

            return;
        }

        /* compute angles delimiting the arc */

        double ax = ering->ax;
        double bx = ering->bx;
        if (ax > bx)
        {
            std::swap(ax, bx);
        }
        double ratio = ax / bx;
        if (ratio > 0.85)
        {
            cv::ellipse(img,
                        cv::Point2f(ering->cx, ering->cy),
                        cv::Size(ering->ax, ering->bx),
                        ering->theta * 180 / M_PI,
                        0,
                        360,
                        cv::Scalar(0, 0, 255));
            return;
        }
        return;

        /* make sure delimiting angles are positive and ordered, to be able to 
           choose between small/big arc */
        //        if (ang_start < 0) ang_start += M_2__PI;
        //        if (ang_end < 0) ang_end += M_2__PI;
        //
        //        if (ang_end < ang_start) ang_end += M_2__PI;
        //
        //                if ((ang_end - ang_start) > M_PI) {
        //        //            std::swap(ang_end, ang_start);
        //                }
        ang_start = ering->ang_start;
        ang_end = ering->ang_end;
        if ((ang_start - ang_end) > M_PI)
        {
            std::swap(ang_end, ang_start);
        }

        cv::ellipse(img,
                    cv::Point2f(ering->cx, ering->cy),
                    cv::Size(ering->ax, ering->bx),
                    ering->theta * 180 / M_PI,
                    ang_start * 180 / M_PI,
                    ang_end * 180 / M_PI,
                    cv::Scalar(255, 0, 255));
    }
}