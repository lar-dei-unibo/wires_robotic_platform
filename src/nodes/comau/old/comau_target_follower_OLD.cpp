#include "ros/ros.h"
#include "kdl_conversions/kdl_msg.h"
#include "wires_robotic_platform/IKService.h"
#include "wires_robotic_platform/ComauFollow.h"
#include "comau_robot/ComauSmartSix.h"
#include "sensor_msgs/JointState.h"
#include <tf/transform_listener.h>
#include <tf_conversions/tf_kdl.h>
#include <geometry_msgs/Pose.h>

ros::NodeHandle *nh;
tf::TransformListener *tf_listener;
comau_robot::ComauSmartSix *robot;
typedef comau_robot::ComauSmartSix::IKSolutionState IkState;
//

enum FollowMachineState
{
    IDLE,
    INVALID,
    RESET,
    FOLLOWING,
    ALARM,
    ALARM_RESET
};

struct FollowMachine
{
    FollowMachineState state;
    KDL::Frame current_pose;
    KDL::Frame target;
    KDL::Frame tool;
    bool joint_state_ready;
    bool current_pose_ready;
    sensor_msgs::JointState current_joint_state;
    sensor_msgs::JointState next_joint_state;
    double max_q_dot;

    FollowMachine(double max_q_dot = 0.1)
    {
        this->max_q_dot = max_q_dot;
        this->reset();
    }

    bool checkMovement(std::vector<double> &q_out)
    {
        double q_dot = qMagnitude(current_joint_state.position, q_out);
        ROS_INFO("Check MAG %f", q_dot);
        return q_dot <= this->max_q_dot;
    }

    void reset()
    {
        state = FollowMachineState::INVALID;
        joint_state_ready = false;
        current_pose_ready = false;
    }

    double qMagnitude(std::vector<double> &q_in, std::vector<double> &q_out)
    {
        double mag = 0.0;
        for (int i = 0; i < q_in.size(); i++)
        {
            mag += pow(q_in[i] - q_out[i], 2.0);
        }
        mag = sqrt(mag);
        return mag;
    }
};
FollowMachine followMachine(0.2);

/**
 *
 */
void followCallback(const wires_robotic_platform::ComauFollow::ConstPtr &msg)
{

    if (msg->action == wires_robotic_platform::ComauFollow::ACTION_SETTARGET)
    {
        if (followMachine.state == FollowMachineState::ALARM)
            return;
        tf::Transform temp_tf;
        tf::poseMsgToTF(msg->target_pose, temp_tf);
        tf::poseTFToKDL(temp_tf, followMachine.target);
        followMachine.state = FollowMachineState::FOLLOWING;
    }
    else if (msg->action == wires_robotic_platform::ComauFollow::ACTION_RESET)
    {
        if (followMachine.state == FollowMachineState::ALARM)
            return;
        followMachine.state = FollowMachineState::RESET;
    }
    else if (msg->action == wires_robotic_platform::ComauFollow::ACTION_ALARM)
    {
        followMachine.state = FollowMachineState::ALARM;
    }
    else if (msg->action == wires_robotic_platform::ComauFollow::ACTION_ALARMRESET)
    {
        followMachine.state = FollowMachineState::ALARM_RESET;
    }
}

/**
 *
 */
void jointStatesCallback(const sensor_msgs::JointState::ConstPtr &msg)
{
    followMachine.current_joint_state = *msg;
    followMachine.joint_state_ready = true;
}

/**
 *
 */
int main(int argc, char **argv)
{
    ros::init(argc, argv, "comau_target_follower");
    nh = new ros::NodeHandle("~");
    tf_listener = new tf::TransformListener();

    /**
     * Robot description load
     */
    std::string robot_name = nh->param<std::string>("robot_name", "comau_smart_six");
    std::string robot_description;
    robot_description = nh->param("/" + robot_name + "/robot_description", std::string());
    if (robot_description.size() == 0)
    {
        ROS_ERROR("Robot description is void!");
        return 0;
    }

    /**
     * Robot Model
     */
    std::string base_link = robot_name + "/base_link";
    std::string ee_link = robot_name + "/link6";
    robot = new comau_robot::ComauSmartSix(robot_description, base_link, ee_link);

    /**
     * Joint topic subscription
     */
    std::string joint_topic = "/" + robot_name + "/joint_states";
    ros::Subscriber joint_sub = nh->subscribe("/" + robot_name + "/joint_states", 1, jointStatesCallback);
    ros::Subscriber follow_sub = nh->subscribe("/" + robot_name + "/target_to_follow", 1, followCallback);
    ros::Publisher joint_pub = nh->advertise<sensor_msgs::JointState>("/" + robot_name + "/joint_command", 1);

    /**
     * Spin
     */
    ROS_INFO_STREAM(robot_name << " target follower ready.");

    ros::Rate r(100);
    while (nh->ok())
    {

        //CHECK TF READY
        try
        {
            tf::StampedTransform transform;
            tf_listener->lookupTransform(base_link, ee_link,
                                         ros::Time(0), transform);

            tf::poseTFToKDL(transform, followMachine.current_pose);
            if (!followMachine.current_pose_ready)
            {
                followMachine.target = followMachine.current_pose;
            }
            followMachine.current_pose_ready = true;

            if (followMachine.state == FollowMachineState::INVALID)
                followMachine.state = FollowMachineState::IDLE;
        }
        catch (tf::TransformException ex)
        {
            ROS_ERROR("%s", ex.what());
            followMachine.current_pose_ready = false;
            followMachine.state = FollowMachineState::INVALID;
        }

        //INVALID IF NOT JOINT STATES
        if (!followMachine.joint_state_ready)
        {
            followMachine.state = FollowMachineState::INVALID;
        }

        //MACHINE STEATES CONTROLS
        if (followMachine.state == FollowMachineState::INVALID)
        {
            ROS_INFO("FollowMachine state INVALID");
        }
        else if (followMachine.state == FollowMachineState::IDLE)
        {
            // ROS_INFO("FollowMachine state IDLE");
            //std::cout << followMachine.current_pose << "\n";
        }
        else if (followMachine.state == FollowMachineState::RESET)
        {
            ROS_INFO("FollowMachine state RESET");
            followMachine.reset();
        }
        else if (followMachine.state == FollowMachineState::ALARM)
        {
            ROS_INFO("FollowMachine state ALARM");
        }
        else if (followMachine.state == FollowMachineState::ALARM_RESET)
        {
            followMachine.state = FollowMachineState::RESET;
        }
        else if (followMachine.state == FollowMachineState::FOLLOWING)
        {
            ROS_INFO("FollowMachine state FOLLOWING");
            std::vector<double> q_out;
            IkState state = robot->ik(followMachine.target, followMachine.current_joint_state.position, q_out);

            if (state == IkState::OK)
            {
                if (followMachine.checkMovement(q_out))
                {
                    followMachine.next_joint_state.header.stamp = ros::Time::now();
                    followMachine.next_joint_state.name = followMachine.current_joint_state.name;
                    followMachine.next_joint_state.position = q_out;
                    joint_pub.publish(followMachine.next_joint_state);
                }
            }

            followMachine.state = FollowMachineState::IDLE;
        }

        r.sleep();
        ros::spinOnce();
    }

    return 0;
}