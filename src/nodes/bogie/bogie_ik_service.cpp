#include "ros/ros.h"
#include "kdl_conversions/kdl_msg.h"
#include "wires_robotic_platform/IKService.h"

ros::NodeHandle *nh;
// bogie_robot::ComauSmartSix *robot;

// typedef bogie_robot::ComauSmartSix::IKSolutionState IkState;

bool ik(wires_robotic_platform::IKService::Request &req, wires_robotic_platform::IKService::Response &res)
{

    KDL::Frame target_frame;
    tf::poseMsgToKDL(req.target_pose, target_frame);

    res.q_out.data.resize(2);

    // IkState state = robot->ik(target_frame, req.q_in.data, res.q_out.data);
    res.q_out.data[0] = target_frame.p.x() - 0.503;
    res.q_out.data[1] = target_frame.p.y() - 0.806; //TODO: Cambiare assolutamente questa frociata

    res.status = wires_robotic_platform::IKService::Response::STATUS_OK;

    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "bogie_ik_service");
    nh = new ros::NodeHandle("~");

    std::string robot_name = nh->param<std::string>("robot_name", "bogie");

    // std::string robot_description;
    // // robot_description = nh->param("/" + robot_name + "/robot_description", std::string());

    // if (robot_description.size() == 0) {
    //     ROS_ERROR("Robot description is void!");
    //     return 0;
    // }

    // robot = new bogie_robot::ComauSmartSix(robot_description, robot_name + "/base_link", robot_name + "/link6");

    ros::ServiceServer service = nh->advertiseService("ik_service", ik);
    ROS_INFO_STREAM(robot_name << " ik service ready.");
    ros::spin();

    return 0;
}