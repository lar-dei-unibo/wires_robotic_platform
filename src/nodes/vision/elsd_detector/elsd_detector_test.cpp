#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iomanip>
#include <cstdint>
#include <boost/thread/thread.hpp>
#include <chrono>

//ROS
#include <ros/ros.h>
#include <geometry_msgs/Point.h>

//OPENCV
#include <opencv2/opencv.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/features2d.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

//Boost
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

//Elsd
#include <elsd/Elsd.hpp>

//Wires robotic platform
#include <wires_robotic_platform/PolyDetectionService.h>

ros::NodeHandle *nh;

Elsd elsd;
double max_radius = 30;

bool integration_service_callback(wires_robotic_platform::PolyDetectionService::Request &req, wires_robotic_platform::PolyDetectionService::Response &res)
{

    cv::Mat img;
    if (req.colored)
    {
        img = cv_bridge::toCvCopy(req.image, "bgr8")->image;
    }
    else
    {
        img = cv_bridge::toCvCopy(req.image, "8UC1")->image;
    }

    std::vector<Ring> rings;
    std::vector<Polygon> polygons;
    elsd.computeRings(img, rings, polygons, req.colored);

    for (int i = 0; i < rings.size(); i++)
    {
        Ring &r = rings[i];
        if (req.circle_max_radius > 0)
            if (r.ax > req.circle_max_radius || r.bx > req.circle_max_radius)
                continue;

        geometry_msgs::Point32 circle;
        circle.x = r.cx;
        circle.y = r.cy;
        circle.z = (r.ax + r.bx) / 2.0;
        res.circles.push_back(circle);
    }

    return true;
}

/**
 * 
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char **argv)
{

    // Initialize ROS
    ros::init(argc, argv, "poly_detection");
    nh = new ros::NodeHandle("~");

    std::string source_file = nh->param<std::string>("source_file", "");
    std::string output_file = nh->param<std::string>("output_file", "");
    int debug = nh->param<int>("debug", 0);

    ROS_INFO_STREAM(" Source file: " << source_file);
    ROS_INFO_STREAM(" Output File: " << output_file);
    ROS_INFO_STREAM(" Debug: " << debug);

    cv::Mat source_image = cv::imread(source_file);
    ROS_INFO_STREAM("Loaded image: " << source_image.rows << "x" << source_image.cols);

    cv::Mat zeros = cv::Mat::zeros(source_image.size(), CV_8UC3);
    if (debug > 0)
    {
        cv::imshow("img", source_image);
        cv::waitKey(0);
    }

    std::vector<Ring> rings;
    std::vector<Polygon> polygons;
    elsd.computeRings(source_image, rings, polygons);

    cv::Scalar mask_color = cv::Scalar(255, 255, 255);
    int mask_thickness = 5;

    for (int i = 0; i < rings.size(); i++)
    {
        Ring &r = rings[i];
        r.full = 0;
        //if (r.width > 10)
        //  continue;

        //this->drawEllipse(output, &r);
        // cv::circle(source_image, cv::Point2f(r.x1, r.y1), 2, cv::Scalar(255), 2);
        // cv::circle(source_image, cv::Point2f(r.x2, r.y2), 2, cv::Scalar(255), 2);
        elsd.drawEllipse(zeros, &r, mask_color, mask_thickness);

        // cv::circle(source_image, cv::Point(r.cx, r.cy), (r.ax + r.bx) / 2.0, cv::Scalar(255, 0, 255), 2);
    }

    for (int i = 0; i < polygons.size(); i++)
    {
        Polygon &p = polygons[i];

        if (p.dim == 2)
        {

            for (int j = 0; j < p.dim; j += 2)
            {

                cv::Point2f p1(p.pts[j].x, p.pts[j].y);
                cv::Point2f p2(p.pts[j + 1].x, p.pts[j + 1].y);
                double dist = cv::norm(p1 - p2);
                ROS_INFO_STREAM("DIST: " << dist);
                if (dist > 0)
                    cv::line(
                        zeros,
                        cv::Point(p.pts[j].x, p.pts[j].y),
                        cv::Point(p.pts[j + 1].x, p.pts[j + 1].y),
                        mask_color, mask_thickness);
            }
        }
    }
    // for (int j = 0; j < this->polygon.dim; j += 2)
    // {
    //     cv::line(img, cv::Point(this->polygon.pts[j].x, this->polygon.pts[j].y), cv::Point(this->polygon.pts[j + 1].x, this->polygon.pts[j + 1].y), cv::Scalar(47, 47, 211), 2);
    // }
    if (debug > 0)
    {
        cv::imshow("img", zeros);
        cv::waitKey(0);
    }

    cv::imwrite(output_file, zeros);
    //Services
    //ros::ServiceServer service_integration = nh->advertiseService("detection_service", integration_service_callback);

    // int hz;
    // nh->param<int>("hz", hz, 30);

    // ros::spin();
}