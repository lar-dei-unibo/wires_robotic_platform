

#include "ros/ros.h"
#include <tf/transform_broadcaster.h>
#include "kdl_conversions/kdl_msg.h"
#include "wires_robotic_platform/IKService.h"
#include "wires_robotic_platform/RobotFollowSetting.h"
#include "wires_robotic_platform/RobotFollow.h"
#include "wires_robotic_platform/RobotFollowStatus.h"
#include "filters/FIROrder2.hpp"
#include "comau_robot/ComauSmartSix.h"
#include "sensor_msgs/JointState.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64MultiArray.h"
#include <tf/transform_listener.h>
#include <tf_conversions/tf_kdl.h>
#include <geometry_msgs/Pose.h>
#include <boost/thread.hpp>
#include <kdl/frames.hpp>
#include <vector>
#include <iostream>

ros::NodeHandle *nh;
FIROrder2<2> *filter;

geometry_msgs::Twist sensor_data;
std::vector<double> sensor_data_vec(3);

geometry_msgs::Twist data_filtered;
std::vector<double> data_filtered_vec(3);

double hz = 100;
double cutoff = 10;
double quality = 20;
double sample_time = 1 / hz;

void toVector(const geometry_msgs::Twist &m, std::vector<double> &k)
{
    k[0] = m.linear.x;
    k[1] = m.linear.y;
    k[2] = m.linear.z;
}

void toTwist(const std::vector<double> &k, geometry_msgs::Twist &m)
{
    m.linear.x = k[0];
    m.linear.y = k[1];
    m.linear.z = k[2];
}

void sersor_cb(const geometry_msgs::Twist::ConstPtr &msg)
{
    sensor_data = *msg;
}

void parameters_cb(const std_msgs::Float64MultiArray::ConstPtr &msg)
{
    sample_time = msg->data[0];
    quality = msg->data[1];
    cutoff = msg->data[2];
    filter->updateParameters(sample_time, quality, cutoff);
}

/**
 *
 */
int main(int argc, char **argv)
{
    ros::init(argc, argv, "force_sensor_filter");
    nh = new ros::NodeHandle("~");

    // Subscribers
    ros::Subscriber force_sub = nh->subscribe("/atift", 1, sersor_cb);
    ros::Subscriber param_sub = nh->subscribe("/filter_parameter", 1, parameters_cb);

    //  Publishers
    ros::Publisher force_pub = nh->advertise<geometry_msgs::Twist>("/atift_filter", 1);

    filter = new FIROrder2<2>(cutoff, quality, sample_time);

    ros::Rate r(hz);
    while (nh->ok())
    {

        toVector(sensor_data, sensor_data_vec);

        filter->setInput(sensor_data_vec);
        data_filtered_vec = filter->output;
        toTwist(data_filtered_vec, data_filtered);

        force_pub.publish(data_filtered);

        r.sleep();
        ros::spinOnce();
    }

    return 0;
}
