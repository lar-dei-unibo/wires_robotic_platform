/*
   comau_cartesian_state_publisher.cpp

   Developer: Daniele De Gregorio
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <sys/uio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <iostream>
#include <math.h>
#include <pthread.h>
#include <signal.h>

#include "ros/ros.h"
#include <kdl/frames_io.hpp>
#include <tf/transform_broadcaster.h>
#include "comau_robot/ComauSmartSix.h"

#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

#include <ctime>
#include <elsd/Elsd.hpp>

Elsd elsd;
double max_radius = 30;

struct MyPolygon
{
    Polygon polygon;

    MyPolygon(Polygon p)
    {
        this->polygon = p;
    }

    double maxEdge()
    {
        double max_l = -1.0;
        for (int j = 0; j < this->polygon.dim; j += 2)
        {
            cv::Point2f p1 = cv::Point2f(this->polygon.pts[j].x, this->polygon.pts[j].y);
            cv::Point2f p2 = cv::Point2f(this->polygon.pts[j + 1].x, this->polygon.pts[j + 1].y);
            cv::Point2f d = p1 - p2;
            double l = cv::norm(d);
            if (l > max_l)
                max_l = l;
        }
        return max_l;
    }

    void draw(cv::Mat &img)
    {
        for (int j = 0; j < this->polygon.dim; j += 2)
        {
            cv::line(img, cv::Point(this->polygon.pts[j].x, this->polygon.pts[j].y), cv::Point(this->polygon.pts[j + 1].x, this->polygon.pts[j + 1].y), cv::Scalar(47, 47, 211), 2);
        }
    }

    int size()
    {
        return this->polygon.dim;
    }
};

void imageCallback(const sensor_msgs::ImageConstPtr &msg)
{
    try
    {
        cv::Mat img = cv_bridge::toCvShare(msg, "bgr8")->image;
        cv::Mat gray;
        cv::cvtColor(img, gray, CV_BGR2GRAY);

        std::vector<Ring> rings;
        std::vector<Polygon> polygons;
        elsd.computeRings(img, rings, polygons);
        ROS_INFO("Rings: %d", int(rings.size()));
        ROS_INFO("Polygons: %d", int(polygons.size()));
        for (int i = 0; i < rings.size(); i++)
        {
            Ring &r = rings[i];
            //if (r.width > 10)
            //  continue;
            if (r.ax > max_radius || r.bx > max_radius)
                continue;
            //this->drawEllipse(output, &r);
            //cv::circle(img, cv::Point2f(r.x1, r.y1), 2, cv::Scalar(255), 2);
            //cv::circle(img, cv::Point2f(r.x2, r.y2), 2, cv::Scalar(255), 2);
            //elsd.drawEllipse(img, &r);
            //cv::circle(img, cv::Point(r.cx, r.cy), (r.ax + r.bx) / 2.0, cv::Scalar(255, 0, 255), 2);
        }

        for (int i = 0; i < polygons.size(); i++)
        {

            MyPolygon p(polygons[i]);

            if (p.size() != 16)
                continue;
            if (p.maxEdge() > 50)
                continue;
            p.draw(img);
        }

        cv::imshow("view", img);
        cv::waitKey(1);
    }
    catch (cv_bridge::Exception &e)
    {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }
}

int main(int argc, char *argv[])
{

    ros::init(argc, argv, "image_listener");
    ros::NodeHandle nh;
    cv::namedWindow("view");
    cv::startWindowThread();
    image_transport::ImageTransport it(nh);
    image_transport::Subscriber sub = it.subscribe("/camera/rgb/image_raw", 1, imageCallback);
    ros::spin();
    cv::destroyWindow("view");
}