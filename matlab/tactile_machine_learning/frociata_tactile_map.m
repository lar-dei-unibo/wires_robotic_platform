
close all
clear all
load('grasping/tactiledatagrasp4animation.mat')

 
data = tactile(120:end,:); 
% smooth(data)
maxt = 0.8;  
mint = min(min(data)); 
f1 = figure; 
hold on 
f2 = figure; 
hold on 
plot(data,'w')
for i=1:length(data)
    pause(0.1)
    x = data(i,:); 
    figure(f1)
    [C,h] =contourf(reshape(x,[4,4])',20); %   ,'LevelList', mint:(maxt-mint)/20:maxt
    set(h,'LineColor','none')  
    caxis([mint,maxt])  
    figure(f2)
    plot(1:i,data(1:i,:),'k')  
end


%  
% figure
% nR = 5; nC = 5;
% for i=1:25
%         
%         name_tactile = strcat('tactile_5_dat_',num2str(i));
%         data_tactile = reshape(mean(eval(name_tactile)),[4,4])';
%         subplot(nR,nC,i); [C,h]=contourf(data_tactile,20); title(strcat(name_tactile," y=",num2str(grasp_lables_test(i))));
%         
% end
% 
%  
% figure
% for i=26:50
%         
%         name_tactile = strcat('tactile_5_dat_',num2str(i));
%         data_tactile = reshape(mean(eval(name_tactile)),[4,4])';
%         subplot(nR,nC,i-25); [C,h]=contourf(data_tactile,20); title(strcat(name_tactile," y=",num2str(grasp_lables_test(i))));
%         
% end
% 
% 
%  