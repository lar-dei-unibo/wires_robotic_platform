close all
clear all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  SETTING VARAIBLES:   %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RangeModeList = {'manual', 'saved', 'auto'};  
RangeMode = RangeModeList{2};
topics = {'atift','tactile','tf#comau_smart_six_link6'};
 

testFolderName = 'wire_moving/';
testName = 'test_movimento_cavo_';
numberOfTest = 4; numberOfDataSet = 4;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%  EXTRACTION LOOP:   %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName);
pathName = strcat(testFolderPath, testName); 
dataPathName = strcat(testFolderPath,'/allDataMat/');
for i=1:numberOfTest    
    datafolder = strcat(pathName,num2str(i))
    extractTopic(datafolder,topics);
    assignin('base',strcat('atift_',num2str(i)),atift)
    assignin('base',strcat('tactile_',num2str(i)),tactile)
    assignin('base',strcat('eef_',num2str(i)),tf_comau_smart_six_link6)
    
    plot(tactile);
    
    if RangeMode == 'manual'
        splitList = zeros(numberOfDataSet,2);
        for j=1:numberOfDataSet
            display(strcat('SELECT RANGE  n°',num2str(j)))
            [x,~] = ginput(1)
            splitList(j,1) = x; % start
            [x,~] = ginput(1)
            splitList(j,2) = x; % end 
        end
        filename = input('save new range as...','s'); 
        save(strcat(testFolderPath,filename,'.mat'),'splitList');
    else
        filename = input('load range from...','s');   
        load(strcat(testFolderPath,filename,'.mat'));
    end
    
    
    for j=1:numberOfDataSet
        min = round(splitList(j,1));
        max = round(splitList(j,2));
        
        dat_atift = atift(min:max,:);
        name_atift = strcat('atift_',num2str(i),'_dat_',num2str(j))
        assignin('base',name_atift,dat_atift) 
        save(strcat(dataPathName,name_atift,'.mat'),name_atift);
        
        dat_tactile = tactile(min:max,:);
        name_tactile = strcat('tactile_',num2str(i),'_dat_',num2str(j))
        assignin('base',name_tactile,dat_tactile) 
        save(strcat(dataPathName,name_tactile,'.mat'),name_tactile);
        
        dat_eef = tf_comau_smart_six_link6(min:max,:);
        name_eef = strcat('eef_',num2str(i),'_dat_',num2str(j))
        assignin('base',name_eef,dat_eef);
        save(strcat(dataPathName,name_eef,'.mat'), name_eef);
    end
    
end
save(strcat(dataPathName,'allData.mat'),'*_dat_*')

close all

 