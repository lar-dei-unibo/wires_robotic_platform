close all
clear all
clc 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
percentageDataTest = 20; % 

% testFolderName = 'wire_bend_yz/test_5';  
% numberOfFirstTest = 5; numberOfLastTest = 5; numberOfDataSet = 8;
% compute_noise = false;
% elaborate(testFolderName,numberOfFirstTest,numberOfLastTest,numberOfDataSet,percentageDataTest, compute_noise)
% 
% testFolderName = 'wire_bend_yz/test_4'; 
% numberOfFirstTest = 4; numberOfLastTest = 4; numberOfDataSet = 40;
% compute_noise = true;
% elaborate(testFolderName,numberOfFirstTest,numberOfLastTest,numberOfDataSet,percentageDataTest, compute_noise)

testFolderName = 'wire_bend_yz/test_6'; 
numberOfFirstTest = 6; numberOfLastTest = 6; numberOfDataSet = 12;
compute_noise = true;
elaborate(testFolderName,numberOfFirstTest,numberOfLastTest,numberOfDataSet,percentageDataTest, compute_noise)

testFolderName = 'wire_bend_yz/test_7'; 
numberOfFirstTest = 7; numberOfLastTest = 7; numberOfDataSet = 1;
compute_noise = false;
elaborate(testFolderName,numberOfFirstTest,numberOfLastTest,numberOfDataSet,percentageDataTest, compute_noise)

testFolderName = 'wire_bend_yz/test_8'; 
numberOfFirstTest = 8; numberOfLastTest = 8; numberOfDataSet = 1;
compute_noise = false;
elaborate(testFolderName,numberOfFirstTest,numberOfLastTest,numberOfDataSet,percentageDataTest, compute_noise)



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function elaborate(testFolderName,numberOfFirstTest,numberOfLastTest,numberOfDataSet,percentageDataTest, compute_force_noise)

    numberOfTest = numberOfLastTest- numberOfFirstTest +1;
    dataIndices = 1:numberOfTest*numberOfDataSet;
    
    
    WIRES_DATA_FOLDER = "/media/riccardo/87060f92-ea5c-42c7-bb3e-5662219d0bd0/projects/wires"
    testFolderPath = strcat(WIRES_DATA_FOLDER,'/machine_learning_data/',testFolderName); 
    dataFileName = strcat(testFolderPath,'/allDataMat/allData.mat');
    load(dataFileName) 

    %%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    compute_mean = false;
    compute_norm = false;
    compute_bias = true;   %    corrisponde a resettare il sensore tattile dopo il grasp (e non solo prima)
    tactileCell = cell(numberOfDataSet,numberOfTest); 
    for i=numberOfFirstTest:numberOfLastTest
        for j=1:numberOfDataSet  
            tactile_temp = eval(strcat('tactile_',num2str(i),'_dat_',num2str(j))); 
            if compute_bias
                bias = eval('base',strcat('bias_tactile_avg_',num2str(i),'_',num2str(j)));
                tactile_temp = bsxfun(@minus, tactile_temp, bias);
            end
            [tactile_temp,mu,sigma] = featureNormalize(tactile_temp, compute_mean, compute_norm); 
            tactileCell{j,i-numberOfFirstTest+1} = tactile_temp; 
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    compute_mean = false;
    compute_norm = true;
    compute_bias = true; 
    forceCell = cell(numberOfDataSet,numberOfTest); 
    for i=numberOfFirstTest:numberOfLastTest
        for j=1:numberOfDataSet  
            force_temp = eval(strcat('atift_',num2str(i),'_dat_',num2str(j))); 
            force_temp =  force_temp(:,1:3);   
            if compute_bias
                bias = eval('base',strcat('bias_force_avg_',num2str(i),'_',num2str(j)));
                force_temp = bsxfun(@minus, force_temp, bias(1:3)); 
            end
            if compute_force_noise
                
                fx = force_temp(:,1); 
                fx = smooth(fx,50);
                ind_noise = find( fx<0.5 & fx>-0.5);
                fx(ind_noise) = fx(ind_noise).*0.0; 
                force_temp(:,1)=fx; 
                
                fy = force_temp(:,2); 
                fy = smooth(fy,50);
                ind_noise = find(fy<0.26 & fy>-0.52);
                fy(ind_noise) = fy(ind_noise).*0.01; 
                force_temp(:,2)=fy;
                
                fz = force_temp(:,3);   
                fz = smooth(fz,50);
                ind_noise = find( fz<0.253 & fz>-0.54);
                fz(ind_noise) =fz(ind_noise).*0.01; 
                force_temp(:,3)=fz;
            end
            [force_temp,mu,sigma] = featureNormalize(force_temp, compute_mean, compute_norm); 

            forceCell{j,i-numberOfFirstTest+1} = force_temp; 
        end
    end  

    %%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    compute_mean = false;
    compute_norm = true;
    compute_bias = true;
    eefCell = cell(numberOfDataSet,numberOfTest); 
    for i=numberOfFirstTest:numberOfLastTest
        for j=1:numberOfDataSet  
            eef_temp = eval(strcat('eef_',num2str(i),'_dat_',num2str(j))); 
            eef_temp =  eef_temp(:,1:3);     
            if compute_bias
                bias = eval('base',strcat('bias_eef_avg_',num2str(i),'_',num2str(j)));
                eef_temp = bsxfun(@minus, eef_temp, bias(1:3)); 
            end
            [eef_temp,mu,sigma] = featureNormalize(eef_temp, compute_mean, compute_norm);  
            eefCell{j,i-numberOfFirstTest+1} = eef_temp; 
        end
    end  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%


    tactile = [];
    for k=dataIndices
        tactile = [tactile; tactileCell{k}];
    end
    figure;
    hold on
    plot(tactile)  
    title("tactile")

    force = [];
    for k=dataIndices 
        data = forceCell{k};
        force = [force; data];
    end  
    figure;
    hold on
    plot(force(:,[2,3])) 
    title("force")
    
    eef = [];
    for k=dataIndices 
        data = eefCell{k};
        eef = [eef; data];
    end  
    figure;
    hold on
    plot(eef(:,[1,3])) 
    title("eef")


    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    length(tactile)
    length(force)
    numSamplesTestData = round(length(tactile)*percentageDataTest/100);
    testIndices = (length(tactile) - numSamplesTestData+1):length(tactile);
    trainIndices = [1:(length(tactile)-numSamplesTestData)];

    xTrain = tactile(trainIndices,:); 
    yTrain = force(trainIndices,:); 

%     figure; plot(yTrain); title("yTrain")
%     figure; plot(xTrain); title("xTrain")

    xTest = tactile(testIndices,:);
    yTest = force(testIndices,:);

%     figure; plot(xTest); title("xTest")
%     figure; plot(yTest); title("yTest")

    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


    testFolderPath = strcat(WIRES_DATA_FOLDER,'/machine_learning_data/',testFolderName); 
    dataPathName = strcat(testFolderPath,'/allDataMat/');
    save(strcat(dataPathName,'elabData.mat'),'tactile','force', 'xTrain', 'yTrain','xTest', 'yTest')

end





