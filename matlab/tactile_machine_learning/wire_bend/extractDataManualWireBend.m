close all
clear all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  SETTING VARAIBLES:   %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RangeModeList = {'manual', 'saved', 'auto'};  
RangeMode = RangeModeList{1};
topics = {'atift','tactile','tf#_comau_smart_six_link6'};
 

testFolderName = 'wire_bend_yz/';
testName = 'test_';
% numberOfFirstTest = 4; numberOfLastTest = numberOfFirstTest; numberOfDataSet = 40; 
% numberOfFirstTest = 5; numberOfLastTest = numberOfFirstTest; numberOfDataSet = 8; 
% numberOfFirstTest = 6; numberOfLastTest = numberOfFirstTest; numberOfDataSet = 12; 
% numberOfFirstTest = 7; numberOfLastTest = numberOfFirstTest; numberOfDataSet = 1;  
numberOfFirstTest = 8; numberOfLastTest = numberOfFirstTest; numberOfDataSet = 1; 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%  EXTRACTION LOOP:   %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
WIRES_DATA_FOLDER = "/media/riccardo/87060f92-ea5c-42c7-bb3e-5662219d0bd0/projects/wires"
testFolderPath = strcat(WIRES_DATA_FOLDER,'/machine_learning_data/',testFolderName);
pathName = strcat(testFolderPath, testName); 
for i=numberOfFirstTest:numberOfLastTest    
    datafolder = strcat(pathName,num2str(i));
    dataPathName = strcat(datafolder,'/allDataMat/');
    extractTopic(datafolder,topics);
    assignin('base',strcat('atift_',num2str(i)),atift)
    assignin('base',strcat('tactile_',num2str(i)),tactile)
%     assignin('base',strcat('schunk_',num2str(i)),schunk_pg70_joint_setpoint)
    assignin('base',strcat('eef_',num2str(i)),tf__comau_smart_six_link6)
    
    plot((tactile));
%     figure
%     plot(atift); 
    
    if RangeMode == 'manual'
        splitList = zeros(numberOfDataSet,2);
        for j=1:numberOfDataSet
            display(strcat('SELECT RANGE  n°',num2str(j)))
            [x,~] = ginput(1)
            splitList(j,1) = x; % start
            [x,~] = ginput(1)
            splitList(j,2) = x; % end 
        end
        filename = input('save new range as...','s'); 
        save(strcat(testFolderPath,filename,'.mat'),'splitList');
    else
        filename = input('load range from...','s');   
        load(strcat(testFolderPath,filename,'.mat'));
    end
    
    
    for j=1:numberOfDataSet
        min = round(splitList(j,1));
        max = round(splitList(j,2));
        
        dat_atift = atift(min:max,:);
        name_atift = strcat('atift_',num2str(i),'_dat_',num2str(j))
        assignin('base',name_atift,dat_atift) 
        save(strcat(dataPathName,name_atift,'.mat'),name_atift);
        
        dat_tactile = tactile(min:max,:);
        name_tactile = strcat('tactile_',num2str(i),'_dat_',num2str(j))
        assignin('base',name_tactile,dat_tactile) 
        save(strcat(dataPathName,name_tactile,'.mat'),name_tactile);
        
%         dat_schunk = schunk_pg70_joint_setpoint(min:max,:);
%         name_schunk = strcat('schunk_',num2str(i),'_dat_',num2str(j))
%         assignin('base',name_schunk,dat_schunk);
%         save(strcat(dataPathName,name_schunk,'.mat'), name_schunk);
        
        dat_eef = tf__comau_smart_six_link6(min:max,:);
        name_eef = strcat('eef_',num2str(i),'_dat_',num2str(j))
        assignin('base',name_eef,dat_eef);
        save(strcat(dataPathName,name_eef,'.mat'), name_eef);
    end
    
end

a = [];
for i=numberOfFirstTest:numberOfLastTest    
    for j=1:numberOfDataSet
        atift_data = eval(strcat('atift_',num2str(i),'_dat_',num2str(j)));
        tactile_data = eval(strcat('tactile_',num2str(i),'_dat_',num2str(j)));
        eef_data = eval(strcat('eef_',num2str(i),'_dat_',num2str(j)));
        
        plot(atift_data);
        [x1,~] = ginput(1)
        assignin('base',strcat('biasIndex1_',num2str(i),'_',num2str(j)),round(x1));
        [x2,~] = ginput(1)
        assignin('base',strcat('biasIndex2_',num2str(i),'_',num2str(j)),round(x2));
        
        atift_bias =  atift_data(x1:x2,:);
        tactile_bias =  tactile_data(x1:x2,:);
        eef_bias =  eef_data(x1:x2,:);
        
        assignin('base',strcat('bias_force_avg_',num2str(i),'_',num2str(j)),mean(atift_bias)); 
        assignin('base',strcat('bias_tactile_avg_',num2str(i),'_',num2str(j)),mean(tactile_bias)); 
        assignin('base',strcat('bias_eef_avg_',num2str(i),'_',num2str(j)),mean(eef_bias)); 
        
        a = [a; atift_bias];
        a = [a; zeros(1,6)];
    end 
end

save(strcat(dataPathName,'allData.mat'),'*_dat_*','bias_*')
  

close all
 