close all
clear all
clc 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderName = 'wire_push_x/dataset3/';  
numberOfTest = 3; numberOfDataSet = 18;
dataIndices = 1:numberOfTest*numberOfDataSet; 
indexDataTest = randperm(max(dataIndices),3)
dataIndices(indexDataTest) = [];


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName); 
dataFileName = strcat(testFolderPath,'/allDataMat/allData.mat');
load(dataFileName) 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
compute_mean = true;
compute_normalization = false;
tactileCell = cell(numberOfDataSet,numberOfTest); 
for i=1:numberOfTest
    for j=1:numberOfDataSet 
        tactile_temp = eval(strcat('tactile_',num2str(i),'_dat_',num2str(j)));
        if compute_mean
            tactile_temp = tactile_temp - repmat(mean(tactile_temp),size(tactile_temp,1),1);
        end
        if compute_normalization
            tactile_temp = tactile_temp -min(tactile_temp(:));
            tactile_temp = 2* tactile_temp / max(tactile_temp(:)) -1;
            %tactile_temp = tactile_temp - mean(tactile_temp);
        end
        %plot(tactile_temp);
        tactileCell{j,i} = tactile_temp;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
offset1 = 0.2045;  
offset2 = 0.1089;  
offset3 = 0.2681;  
eefCell = cell(numberOfDataSet,numberOfTest);
for i=1:numberOfTest
    for j=1:numberOfDataSet 
        data1 = eval(strcat('eef_',num2str(i),'_dat_',num2str(j)));
        data = data1(:,2);
        offset = eval(strcat('offset',num2str(i)));
        data = data - offset;
        data = data/max(data);
        data(data<0)=0;
        eefCell{j,i} = data;
    end
end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
windowSize = 30; 
b = (1/windowSize)*ones(1,windowSize); 
a = 1;
forceCell = cell(numberOfDataSet,numberOfTest);
for i=1:numberOfTest
    for j=1:numberOfDataSet 
        data = eval(strcat('atift_',num2str(i),'_dat_',num2str(j)));
        fx = data(:,1);
        fy = data(:,2);
        fz = data(:,3);
        
        force_temp = fx - max(fx); 
        force_temp = abs(force_temp);
        force_temp(force_temp<1.2) = 0;
        
        force_temp = smooth(force_temp,100);
        force_temp(force_temp<0) = 0;
        force_temp = force_temp/max(force_temp); 
        forceCell{j,i} = force_temp; 
    end
end  
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 

tactile = [];
for k=dataIndices
    tactile = [tactile; tactileCell{k}];
end
% figure;
% hold on
% plot(tactile) 

eef = [];
for k=dataIndices 
    data = eefCell{k};
    eef = [eef; data];
end 
% figure;
% hold on
% plot(eef)

force = [];
for k=dataIndices 
    data = forceCell{k};
    force = [force; data];
end 
% figure;
% hold on
% plot(force) 
 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
 
xTrain = tactile; 
yTrain = force; 
figure;
hold on
plot(yTrain.*0.1)
plot(xTrain)
 
xTest = [];
yTest = [];
for i=indexDataTest
    x = tactileCell{i}; 
    xTest = [xTest; x];
    y = forceCell{i};
    yTest = [yTest; y];
end
figure;
hold on
plot(yTest.*0.1)
plot(xTest)
  

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName); 
dataPathName = strcat(testFolderPath,'/allDataMat/');
save(strcat(dataPathName,'elabData.mat'),'tactile','eef','force', 'xTrain', 'yTrain','xTest', 'yTest')







