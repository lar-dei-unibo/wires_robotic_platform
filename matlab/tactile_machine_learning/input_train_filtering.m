%%%%%%%%%%%%%%%%%%%%%%%%%%%%
numberOfDataSet=18
numberOfTest=3
compute_mean = true;
compute_normalization = true;
tactileCell = cell(numberOfDataSet,numberOfTest);
forceCell = cell(numberOfDataSet,numberOfTest);
for i=1:numberOfTest
    for j=1:numberOfDataSet 
        tactile_temp = eval(strcat('tactile_',num2str(i),'_dat_',num2str(j)));
        if compute_mean
            tactile_temp = tactile_temp - mean(tactile_temp);
        end
        if compute_normalization
            tactile_temp = tactile_temp -min(tactile_temp(:));
            tactile_temp = 2* tactile_temp / max(tactile_temp(:)) -1;
            %tactile_temp = tactile_temp - mean(tactile_temp);
        end
        %plot(tactile_temp);
        tactileCell{j,i} = tactile_temp;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
normalize_force = true
windowSize = 30; 
b = (1/windowSize)*ones(1,windowSize); 
a = 1;
forceCell = cell(numberOfDataSet,numberOfTest);
for i=1:numberOfTest
    for j=1:numberOfDataSet 
        data = eval(strcat('atift_',num2str(i),'_dat_',num2str(j)));
        fx = data(:,1);
        fy = data(:,2);
        fz = data(:,3);
        
        force_temp = fx - max(fx); 
        force_temp = abs(force_temp);
        force_temp(force_temp<1.2) = 0;
        
        force_temp = smooth(force_temp,100);
        force_temp(force_temp<0) = 0;
        force_temp = force_temp/max(force_temp); 
        forceCell{j,i} = force_temp; 
    end
end  


indexDataTest = 2
dataIndices = 1:numberOfTest*numberOfDataSet;
if indexDataTest>0
    dataIndices(indexDataTest) = [];
end

tactile = [];
force = [];
for k=dataIndices
    tactile = [tactile; tactileCell{k}];
    force = [force; forceCell{k}];
end


%plot(force)

% classifier
xTrain = tactile; 
yTrain = force;
% reg
xTest = tactileCell{indexDataTest}; 
yTest = forceCell{indexDataTest};



Mdl = fitensemble(xTrain,yTrain,'LSBoost',300,'Tree');

yPrediction = predict(Mdl,xTest);

hold on;
plot(yPrediction);
plot(yTest);

