function [X_norm, mu, sigma] = featureNormalize(X)
     X_norm = bsxfun(@minus, X, mean(X)); 
%      X_norm = bsxfun(@rdivide, X_norm, std(X_norm));
     mu = mean(X);
     sigma = std(X_norm);
end
