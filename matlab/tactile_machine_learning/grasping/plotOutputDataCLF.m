
close all
clear all
testFolderName = 'wire_grasping/test2';  
testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName); 
dataPathName = strcat(testFolderPath,'/allDataMat/');
load(strcat(dataPathName,'outputData.mat') )

keys_MLP = regexprep(string(keys_MLP),' +',''); 
keys_RF = regexprep(string(keys_RF),' +',''); 
keys_SVC = regexprep(string(keys_SVC),' +','');
keys_best = ["","",""];

clf_names = ["MLP","RF","SVC"];
for i=1:length(clf_names)
    clf_name = clf_names(i);
    keys = eval(strcat("keys_",clf_name));
     
    
    %%%%%%%%%%%%%%%%%%%%
    % Precision-Recall %    
    %%%%%%%%%%%%%%%%%%%%
    figure; hold on;
    for j=1:length(keys)
        precision  = eval(strcat("precision_",clf_name,'_',keys(j)));
        recall  = eval(strcat("recall_",clf_name,'_',keys(j)));
        plot(recall ,precision)
    end
    axis([0 1 0 1])
    title(strcat(clf_name, " Precision-Recall"))
    legend(keys)
    
    
    %%%%%%%%%%%%%%
    % Prediction %    
    %%%%%%%%%%%%%%
    k_best = eval(strcat("best_",clf_name));
    yTest = eval("yTest");
    figure; hold on; 
    yPred = eval(strcat("yPred_",clf_name, "_", k_best));
    plot(yPred,'o')
    plot(yTest )  
    title(strcat(clf_name, " Test"))
%     legend(keys)

    %%%%%%%%%
    % Times %    
    %%%%%%%%%
    figure; hold on;
    c = categorical(keys); 
    tArray = [];
    for j=1:length(keys)
        tArray(j,1)  = eval(strcat("tPred_",clf_name,'_',keys(j)));
        tArray(j,2)  = eval(strcat("tTrain_",clf_name,'_',keys(j)));
    end 
    bar(c,tArray)
    title(strcat(clf_name, " Time"))  
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    k_best = eval(strcat("best_",clf_name))
    keys_best(i) =  strcat(clf_name,"_",k_best);
    precision  = eval(strcat("precision_",clf_name,'_',keys(j)));
    assignin('base',strcat('precision_',clf_name),precision);
    recall  = eval(strcat("recall_",clf_name,'_',keys(j)));
    assignin('base',strcat('recall_',clf_name),recall);
    tPred  = eval(strcat("tPred_",clf_name,'_',keys(j)));
    assignin('base',strcat('tPred_',clf_name),tPred);
    tTrain  = eval(strcat("tTrain_",clf_name,'_',keys(j)));
    assignin('base',strcat('tTrain_',clf_name),tTrain);
    
end

dataForPaperPath = '/home/riccardo/Wires/machine_learning_data/dataForPaper/clf/';   
% save(strcat(dataForPaperPath,'precision_recall.mat'),'precision_MLP','precision_RF','precision_SVC','recall_MLP','recall_RF','recall_SVC', 'keys_best')
% save(strcat(dataForPaperPath,'time.mat'),'tPred_MLP','tPred_RF','tPred_SVC','tTrain_SVC','tTrain_MLP','tTrain_RF', 'keys_best')
 


figure; hold on;
for j=1:length(keys_best)
    precision  = eval(strcat("precision_", keys_best(j)));
    recall  = eval(strcat("recall_", keys_best(j)));
    plot(recall ,precision)
end
axis([0 1 0 1])
title(strcat(" Precision-Recall"))
legend(regexprep(string(keys_best),'_',' '))
