close all
clear all
clc 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderName = 'wire_grasping/test2';  
numberOfTest = 5; numberOfDataSet = 50; sampleN = 5;
dataIndices = 1:numberOfTest*numberOfDataSet; 
% dataIndices(151:200)=[];
indexDataTest = (numberOfTest-1)*50+randperm(max(50),50);
dataIndices(indexDataTest) = [];
% dataIndices = dataIndices(randperm(length(dataIndices)));


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName); 
dataFileName = strcat(testFolderPath,'/allDataMat/allData.mat');
load(dataFileName) 
% grasp_lables = [1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1]';
grasp_lables = [ones(1,50), zeros(1,50), zeros(1,50), zeros(1,50)];
grasp_lables(8) = 0;
grasp_lables(50+34) = 1;
grasp_lables = [grasp_lables, ones(1,14), zeros(1,12), zeros(1,9),ones(1,8), zeros(1,7)];


%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
compute_mean = false;
compute_normalization = false;
tactileCell = cell(numberOfDataSet,numberOfTest); 
for i=1:numberOfTest
    for j=1:numberOfDataSet 
        tactile_temp = eval(strcat('tactile_',num2str(i),'_dat_',num2str(j)));
        if compute_mean
            tactile_temp = tactile_temp - mean(tactile_temp);
        end
        if compute_normalization
            tactile_temp = tactile_temp -min(tactile_temp(:));
            tactile_temp = 2* tactile_temp / max(tactile_temp(:)) -1; 
        end 
        tactileCell{j,i} = tactile_temp; 
    end
end 

k = 1;
successCell = cell(numberOfDataSet,numberOfTest); 
for i=1:numberOfTest
    for j=1:numberOfDataSet 
        successCell{j,i} =  ones(sampleN,1)*grasp_lables(k); 
        k = k+1;
    end
end  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% close all
% i=1;j=40;
% grasp_lables(j+50*(i-1))
% temp = eval(strcat('tactile_',num2str(i),'_dat_',num2str(j)));
% figure;contourf(reshape(temp,[4,4])');


tactile = [];
grasp_lables = []; 
for k=dataIndices
    tactile = [tactile; tactileCell{k}]; 
    grasp_lables = [grasp_lables; successCell{k}];
end
figure;
hold on
plot(tactile)
plot(grasp_lables)
  

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
 
xTrain = tactile; 
yTrain = grasp_lables; 

xTest = [];
yTest = [];
for i=indexDataTest
    x = tactileCell{i}; 
    xTest = [xTest; x];
    y = successCell{i};
    yTest = [yTest; y];
end

data = [xTrain; xTest];
[data,mu,sigma] = featureNormalize(data);  
xTrain = data(1:length(xTrain),:);
xTest = data(length(xTrain)+1:end,:);
 
%   

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName); 
dataPathName = strcat(testFolderPath,'/allDataMat/');
% save(strcat(dataPathName,'elabData.mat'),'tactile', 'xTrain', 'yTrain','xTest', 'yTest')


%  
% figure
% nR = 5; nC = 5;
% for i=1:1
%         
%         name_tactile = strcat('tactile_5_dat_',num2str(i));
%         data_tactile = reshape(mean(eval(name_tactile)),[4,4])';
%         subplot(nR,nC,i); [C,h]=contourf(data_tactile,20); title(name_tactile);
%         
% end
% 
%  
% figure
% for i=26:50
%         
%         name_tactile = strcat('tactile_5_dat_',num2str(i));
%         data_tactile = reshape(mean(eval(name_tactile)),[4,4])';
%         subplot(nR,nC,i-25); [C,h]=contourf(data_tactile,20); title(name_tactile);
%         
% end







