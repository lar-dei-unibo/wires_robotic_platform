close all
clear all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  SETTING VARAIBLES:   %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RangeModeList = {'manual', 'saved', 'auto'};  
RangeMode = RangeModeList{1};
topics = {'schunk_pg70_joint_setpoint','tactile' };

testFolderName = 'wire_grasping/test2/';
testName = 'wire_grasping_';
numberOfTest = 2; 
numberOfDataSet = 50;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%  EXTRACTION LOOP:   %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName);
pathName = strcat(testFolderPath, testName); 
dataPathName = strcat(testFolderPath,'/allDataMat/');

for i=1:numberOfTest    
    datafolder = strcat(pathName,num2str(i))
    extractTopic(datafolder,topics);
    
    figure; 
    hold on;
    plot(schunk_pg70_joint_setpoint(:,1)./20);
    plot(tactile);
    hold off;
    
    [t1,~] = ginput(1);
    [t2,~] = ginput(1);
    time = round(t1):round(t2);
    gripper_i = schunk_pg70_joint_setpoint(time,1);
    tactile_i = tactile(time,:);
    assignin('base',strcat('gripper_',num2str(i)),gripper_i)
    assignin('base',strcat('tactile_',num2str(i)),tactile_i)
    
    r_edges = find(diff(gripper_i)>0);
    f_edges = find(diff(gripper_i)<0);
    m = min(size(r_edges,1),size(f_edges,1));
    splitList = [f_edges(1:m),r_edges(1:m)];
     
    plot(tactile_i); 
    hold on; 
    plot( gripper_i./20)
     
        
    
    for j=1:m
        min_index = round(splitList(j,1));
        max_index = round(splitList(j,2)); 
        w = 0.25;
        sample_index = round(mean([min_index*(1-w),max_index*(1+w)]));
        sample_range = sample_index-2:sample_index+2;
        dat_tactile = tactile_i(sample_range,:);
        name_tactile = strcat('tactile_',num2str(i),'_dat_',num2str(j))
        assignin('base',name_tactile,dat_tactile) 
        save(strcat(dataPathName,name_tactile,'.mat'),name_tactile);
        plot(sample_range,dat_tactile, 'o')
    end
    
end
% save(strcat(dataPathName,'allData.mat'),'*_dat_*')

% close all

 