
close all
clear all
dataForPaperPath = '/home/riccardo/Wires/machine_learning_data/dataForPaper/fulltest/';    
load(strcat(dataForPaperPath,'newot4.mat') )

nframes = 5;
v_map_obj = VideoWriter(strcat(dataForPaperPath,'tacmap.avi'),'Uncompressed AVI');
open(v_map_obj)
v_grf_obj = VideoWriter(strcat(dataForPaperPath,'tacgraph.avi'),'Uncompressed AVI');
open(v_grf_obj)

data = tactile(180:end,:); 
data = data +0.001*rand(length(data),1);
windowSize = 2;  a = 1;  b = (1/windowSize)*ones(1,windowSize);
for i=1:16
    data(:,i) = filter(b,a,data(:,i));
end
data = data +0.001*rand(length(data),1);
maxt = max(max(data));   
mint = min(min(data)); 
f1 = figure;  
f2 = figure; hold on   
plot(data,'k'); vl = plot(1,data(1,:),'r');
writeVideo(v_grf_obj,getframe(f2)); drawnow;
for i=1:length(data)
    pause(0.01)
    x = data(i,:); 
    dataGrid = reshape(x,[4,4]);
    %%%
    figure(f1)
    [C,h] =contourf(dataGrid',8);  
    set(h,'LineColor','none')  
    caxis([mint,maxt])   
    for f=1:nframes
        writeVideo(v_map_obj,getframe(f1)); drawnow;
    end
    %%%
    figure(f2)  
    set(vl,'Visible','off'); vl = plot(i,data(i,:),'ro', 'MarkerFaceColor','r');  
    
    for f=1:nframes
        writeVideo(v_grf_obj,getframe(f2)); drawnow;
    end
    
end

close(v_map_obj)
close(v_grf_obj)