close all
clear all


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  SETTING VARAIBLES:   %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
newRangeFlg = false;  
topics = {'atift','tactile','tf#comau_smart_six_link6'};

testFolderName = 'wire_inserting/dataset3/'; 
numberOfTest = 3; numberOfDataSet = 18;
nC = 6; nR = 9;

% testFolderName = 'wire_inserting/dataset2/'; 
% numberOfTest = 1; numberOfDataSet = 6;
% nC = 3; nR = 2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%  PLOT LOOP:   %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName); 
dataFileName = strcat(testFolderPath,'/allDataMat/allData.mat');
load(dataFileName) 

figure
k=0;
for j=1:numberOfTest
    for i=1:numberOfDataSet
        
        name_atift = strcat('atift_',num2str(j),'_dat_',num2str(i));
        data_atift = eval(name_atift);
        k = k + 1;
        subplot(nR,nC,k); plot(data_atift); title(name_atift);
    end
end

figure
k=0;
for j=1:numberOfTest
    for i=1:numberOfDataSet
        
        name_tactile = strcat('tactile_',num2str(j),'_dat_',num2str(i));
        data_tactile = eval(name_tactile);
        k = k + 1;
        subplot(nR,nC,k); plot(data_tactile); title(name_tactile);
        
    end
end

figure
k=0;
for j=1:numberOfTest
    for i=1:numberOfDataSet 
        name_eef = strcat('eef_',num2str(j),'_dat_',num2str(i));
        data_eef = eval(name_eef); 
        k = k + 1;
        subplot(nR,nC,k); plot(data_eef(:,2)); title(name_eef);
    end
end

