close all
clear all
clc 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderName = 'wire_push_x/dataset3/';  
numberOfTest = 3; numberOfDataSet = 18;
dataIndices = 1:numberOfTest*numberOfDataSet; 
indexDataTest = randperm(max(dataIndices),5)
dataIndices(indexDataTest) = [];


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName); 
dataFileName = strcat(testFolderPath,'/allDataMat/allData.mat');
load(dataFileName) 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% compute_mean = false;
% compute_normalization = false;
tactileCell = cell(numberOfDataSet,numberOfTest); 
for i=1:numberOfTest
    for j=1:numberOfDataSet 
        tactile_temp = eval(strcat('tactile_',num2str(i),'_dat_',num2str(j))); 
        [tactile_temp,mu,sigma] = featureNormalize(tactile_temp);  
         
        tactileCell{j,i} = tactile_temp;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
offset1 = 0.2045;  
offset2 = 0.1089;  
offset3 = 0.2681;  
eefCell = cell(numberOfDataSet,numberOfTest);
tf_y = [];
for i=1:numberOfTest
    for j=1:numberOfDataSet 
        data1 = eval(strcat('eef_',num2str(i),'_dat_',num2str(j)));
        data = data1(:,2);
        tf_y = [tf_y; data]; 
        
        offset = eval(strcat('offset',num2str(i)));
        data = data - offset;
        data = data/max(data);
        data(data<0)=0;
        eefCell{j,i} = data;
    end
end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
windowSize = 30; 
b = (1/windowSize)*ones(1,windowSize); 
a = 1;
forceCell = cell(numberOfDataSet,numberOfTest);
f_xyz = [];
for i=1:numberOfTest
    for j=1:numberOfDataSet 
        data = eval(strcat('atift_',num2str(i),'_dat_',num2str(j)));
        fx = data(:,1);
        fy = data(:,2);
        fz = data(:,3);
        f = [fx, fy, fz];
        f_xyz = [f_xyz; f];
        
        force_temp = fx - max(fx); 
        force_temp = abs(force_temp);
        force_temp(force_temp<1.2) = 0;
        
        force_temp = smooth(force_temp,100);
        force_temp(force_temp<0) = 0;
        force_temp = force_temp/max(force_temp); 
        forceCell{j,i} = force_temp; 
    end
end  
 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 

tactile = [];
for k=dataIndices
    tactile = [tactile; tactileCell{k}];
end
% figure;
% hold on
% plot(tactile) 

eef = [];
for k=dataIndices 
    data = eefCell{k};
    eef = [eef; data];
end 
% figure;
% hold on
% plot(eef)

force = [];
for k=dataIndices 
    data = forceCell{k};
    force = [force; data];
end 
% figure;
% hold on
% plot(force) 
 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
 
xTrain = tactile; 
yTrain = force; 
 
xTest = [];
yTest = [];
for i=indexDataTest
    x = tactileCell{i}; 
    xTest = [xTest; x];
    y = forceCell{i};
    yTest = [yTest; y];
end


% data = [xTrain; xTest];
% [data,mu,sigma] = featureNormalize(data);  
% xTrain = data(1:length(xTrain),:);
% xTest = data(length(xTrain)+1:end,:);

figure;
hold on
plot(yTrain)
plot(xTrain)

figure;
hold on
plot(yTest)
plot(xTest)
  

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName); 
dataPathName = strcat(testFolderPath,'/allDataMat/');
% save(strcat(dataPathName,'elabData.mat'),'tactile','eef','force', 'xTrain', 'yTrain','xTest', 'yTest')


 
range_t = 56000;
f_xyz = f_xyz(1:range_t,:);
tf_y = tf_y(1:range_t,:);
figure; hold on;  
subplot(2,1,1); plot(f_xyz);
subplot(2,1,2); plot(tf_y);
% dataForPaperPath = '/home/riccardo/Wires/machine_learning_data/dataForPaper/reg/';  
% save(strcat(dataForPaperPath,'force.mat'),'f_xyz','tf_y')
