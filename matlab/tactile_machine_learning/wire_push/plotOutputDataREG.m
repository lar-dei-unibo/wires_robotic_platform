
close all
clear all
testFolderName = 'wire_push_x/dataset3/';  
testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName); 
dataPathName = strcat(testFolderPath,'/allDataMat/');
load(strcat(dataPathName,'outputData.mat') )

keys_MLP = regexprep(string(keys_MLP),' +',''); 
keys_RF = regexprep(string(keys_RF),' +','');  

clf_names = ["MLP","RF"];
k = 1;
all_keys = [];
for i=1:length(clf_names)
    reg_name = clf_names(i);
    keys = eval(strcat("keys_",reg_name));
    all_keys = [all_keys; keys];
      
    
    %%%%%%%%%%%%%%
    % MSE &  DTW %    
    %%%%%%%%%%%%%%    
    for j=1:length(keys) 
        cl(k) = strcat(reg_name, "_", keys(j));
        yPred = eval(strcat("yPred_",reg_name, "_", keys(j)));
        DTW(k)  = dtw(yTest,yPred);    
        dtw_baseline = dtw(ones(1,length(yPred)), zeros(1,(length(yPred))));
        DTW(k) = DTW(k) / dtw_baseline;
        MSE(k)  = eval(strcat("mse_",reg_name,'_',keys(j)));
        k = k+1;
    end  

    %%%%%%%%%%%%%%
    % Prediction %    
    %%%%%%%%%%%%%%
    k_best = eval(strcat("best_",reg_name));
    yTest = eval("yTest");
    figure; hold on; 
    yPred = eval(strcat("yPred_",reg_name, "_", k_best));
    assignin('base',strcat('yPred_',reg_name),yPred)
    plot(yPred)
    plot(yTest )  
    title(strcat(reg_name, " Test")) 
    

    %%%%%%%%%
    % Times %    
    %%%%%%%%%
%     figure; hold on;
%     c = categorical(keys); 
%     tArray = [];
%     for j=1:length(keys)
%         tArray(j,1)  = eval(strcat("tPred_",clf_name,'_',keys(j)));
%         tArray(j,2)  = eval(strcat("tTrain_",clf_name,'_',keys(j)));
%     end 
%     bar(c,tArray)
%     title(strcat(clf_name, " Time"))  

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    k_best = eval(strcat("best_",reg_name))
    keys_best(i) =  strcat(reg_name,"_",k_best);   
    yPred  = eval(strcat("yPred_",reg_name,'_',keys(j)));
    assignin('base',strcat('yPred_',reg_name),yPred);
    tPred  = eval(strcat("tPred_",reg_name,'_',keys(j)));
    assignin('base',strcat('tPred_',reg_name),tPred);
    tTrain  = eval(strcat("tTrain_",reg_name,'_',keys(j)));
    assignin('base',strcat('tTrain_',reg_name),tTrain);
    
end

figure; hold on;
c = categorical(cl);   
bar(c,[MSE; DTW]')
legend("mse","dtw")
dataForPaperPath = '/home/riccardo/Wires/machine_learning_data/dataForPaper/reg/';  
save(strcat(dataForPaperPath,'mse_dtw.mat'),'MSE','DTW','cl') 
save(strcat(dataForPaperPath,'yPred_yTest.mat'),'yPred_MLP','yPred_RF','yTest', 'keys_best')
save(strcat(dataForPaperPath,'time.mat'),'tPred_MLP','tPred_RF' ,'tTrain_MLP','tTrain_RF', 'keys_best')

% 
% figure; hold on;
% c = categorical(cl);   
% bar(c,DTW')
% 
% figure; hold on;
% c = categorical(cl);   
% bar(c,MSE')


 