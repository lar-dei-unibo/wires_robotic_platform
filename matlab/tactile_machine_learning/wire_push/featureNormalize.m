function [X_norm, mu, sigma] = featureNormalize(X)
     X_norm = bsxfun(@minus, X, mean(X)); 
     X_norm = bsxfun(@rdivide, X_norm, max(X_norm)-min(X_norm)+0.001);
     mu = mean(X);
     sigma = std(X_norm);
end
