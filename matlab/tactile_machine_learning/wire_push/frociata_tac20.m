close all
clear all
clc 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderName = 'wire_push_x/';  

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
testFolderPath = strcat('/home/riccardo/Wires/machine_learning_data/',testFolderName); 
dataFileName = strcat(testFolderPath,'/tac20.mat');
load(dataFileName) 
plot(tactile_data')
[tactile_temp,mu,sigma] = featureNormalize(tactile_data(:,9000:end)); 
save(strcat(testFolderPath,'/tac20elab.mat'),'tactile_temp')

tactile_signal = tactile_border(:,1) - tactile_border(:,2);
 
plot(tactile_time, tactile_signal);