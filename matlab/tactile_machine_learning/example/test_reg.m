
 
close all
clc
tSplit = 16600;
x = tactile;
xTrain = x(1:tSplit,:);
xTest = x(tSplit+1:end,:);
 
tf6 = tfcomausmartsixlink6(:,2) - tfcomausmartsixlink6(1,2);
tf6(tf6<0)=0;
y = tf6/max(tf6);
 
yTrain = y(1:tSplit);
yTest = y(tSplit+1:end);

figure;
hold on;
plot(yTrain,'r'); 


Mdl = fitensemble(xTrain,yTrain,'LSBoost',1000,'Tree');

yPrediction = predict(Mdl,xTrain);


figure;
hold on;
plot(yPrediction,'r');
plot(yTrain,'b');

