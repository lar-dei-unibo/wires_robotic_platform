close all
clear all
clc 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

testFolderName = 'wire_contact_xyz'; 
testID = 1;  numberOfDataSet = 10*5; 
compute_noise = true;
elaborate(testFolderName,testID,numberOfDataSet, compute_noise)

testID = 2;  numberOfDataSet = 8*5; 
compute_noise = true;
elaborate(testFolderName,testID,numberOfDataSet, compute_noise)

testID = 3;  numberOfDataSet = 5; 
compute_noise = false;
elaborate(testFolderName,testID,numberOfDataSet, compute_noise)

testID = 5;  numberOfDataSet = 9; 
compute_noise = false;
elaborate(testFolderName,testID,numberOfDataSet, compute_noise)
 


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

function elaborate(testFolderName,testID,numberOfDataSet, compute_force_noise)
 
    dataIndices = 1:numberOfDataSet;
    
    
    WIRES_DATA_FOLDER = "/media/riccardo/87060f92-ea5c-42c7-bb3e-5662219d0bd0/projects/wires"
    testFolderPath = strcat(WIRES_DATA_FOLDER,'/machine_learning_data/',testFolderName); 
     
    dataFileName = strcat(testFolderPath,'/test_',num2str(testID),'/allDataMat/allData.mat');
    load(dataFileName)  
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    compute_mean = false;
    compute_norm = false;
    compute_bias = true;   %    corrisponde a resettare il sensore tattile dopo il grasp (e non solo prima)
    tactileCell = cell(numberOfDataSet);  
    for j=1:numberOfDataSet  
        tactile_temp = eval(strcat('tactile_',num2str(testID),'_dat_',num2str(j))); 
        if compute_bias
            bias = eval('base',strcat('bias_tactile_avg_',num2str(testID),'_',num2str(j)));
            tactile_temp = bsxfun(@minus, tactile_temp, bias);
        end
        [tactile_temp,mu,sigma] = featureNormalize(tactile_temp, compute_mean, compute_norm); 
        tactileCell{j} = tactile_temp; 
    end 

    %%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    compute_mean = false;
    compute_norm = false;
    compute_bias = true; 
    forceCell = cell(numberOfDataSet);  
    for j=1:numberOfDataSet  
        force_temp = eval(strcat('atift_',num2str(testID),'_dat_',num2str(j))); 
        force_temp =  force_temp(:,1:3);   
        if compute_bias
            bias = eval('base',strcat('bias_force_avg_',num2str(testID),'_',num2str(j)));
            force_temp = bsxfun(@minus, force_temp, bias(1:3)); 
        end
        if compute_force_noise

            fx = force_temp(:,1); 
            fx = smooth(fx,5);
            ind_noise = find( fx<1& fx>-0.6);
            fx(ind_noise) = fx(ind_noise).*0.01; 
            force_temp(:,1)=fx; 

            fy = force_temp(:,2); 
            fy = smooth(fy,5);
            ind_noise = find(fy<0.34 & fy>-0.52);
            fy(ind_noise) = fy(ind_noise).*0.01; 
            force_temp(:,2)=fy;

            fz = force_temp(:,3);   
            fz = smooth(fz,5);
            ind_noise = find( fz<0.34 & fz>-0.54);
            fz(ind_noise) =fz(ind_noise).*0.01; 
            force_temp(:,3)=fz;
        end
        [force_temp,mu,sigma] = featureNormalize(force_temp, compute_mean, compute_norm); 

        forceCell{j } = force_temp; 
    end 
 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%


    tactile = [];
    for k=dataIndices 
        tactile = [tactile; tactileCell{k}];
    end
    figure;
    hold on
    plot(tactile)  
    title("tactile")

    force = [];
    for k=dataIndices 
        data = forceCell{k};
        force = [force; data];
    end  
    fx = force(:,1); fy = force(:,2); fz = force(:,3); 
    fx = smooth(fx,50); fy = smooth(fy,50); fz = smooth(fz,50);
    force(:,1) = fx; force(:,2) = fy; force(:,3) = fz; 
    figure;
    hold on
    plot(force(:,[1:3])) 
    title("force")
     

    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%     length(tactile)
%     length(force)
%     numSamplesTestData = round(length(tactile)*percentageDataTest/100);
%     testIndices = (length(tactile) - numSamplesTestData+1):length(tactile);
%     trainIndices = [1:(length(tactile)-numSamplesTestData)];
% 
%     xTrain = tactile(trainIndices,:); 
%     yTrain = force(trainIndices,:); 
%  
% 
%     xTest = tactile(testIndices,:);
%     yTest = force(testIndices,:);
%  

    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


    testFolderPath = strcat(WIRES_DATA_FOLDER,'/machine_learning_data/',testFolderName); 
    dataPathName = strcat(testFolderPath,'/test_',num2str(testID),'/allDataMat/');
    save(strcat(dataPathName,'elabData.mat'),'tactile','force')

end





