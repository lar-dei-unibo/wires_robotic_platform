close all
clear all
clc 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% testFolderName = 'wire_bend_yz/test_1';  
% numberOfFirstTest = 1; numberOfLastTest = 1; numberOfDataSet = 1;
testFolderName = 'wire_bend_yz/test_2'; 
numberOfFirstTest = 2; numberOfLastTest = 2; numberOfDataSet = 8;
numberOfTest = numberOfLastTest- numberOfFirstTest +1;
dataIndices = 1:numberOfTest*numberOfDataSet;

percentageDataTest = 20; % 

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
WIRES_DATA_FOLDER = "/media/riccardo/87060f92-ea5c-42c7-bb3e-5662219d0bd0/projects/wires"
testFolderPath = strcat(WIRES_DATA_FOLDER,'/machine_learning_data/',testFolderName); 
dataFileName = strcat(testFolderPath,'/allDataMat/allData.mat');
load(dataFileName) 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
window_data = tactile_2_dat_5;
bias = eval('base',strcat('bias_tactile_avg_2_5'));
normal(window_data,bias)
window_data = tactile_2_dat_2;
bias = eval('base',strcat('bias_tactile_avg_2_2'));
normal(window_data,bias)

function x_n = normal(window_data,bias) 
    x_n = bsxfun(@minus, window_data, bias);  
    figure; plot(window_data);
    figure; plot(x_n);
end