function [X, mu, sigma] = featureNormalize(X, compute_mean, compute_norm)
    mu = mean(X);
    if compute_mean
        X  = bsxfun(@minus, X, mean(X));  
    end
    if compute_norm 
        X = bsxfun(@rdivide, X, max(X)-min(X)); 
    end
    sigma = std(X);
end
