close all
clear all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  SETTING VARAIBLES:   %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RangeModeList = {'manual', 'saved', 'auto'};  
RangeMode = RangeModeList{1};
topics = {'atift','tactile','schunk_pg70_joint_setpoint'};
testFolderName = 'wire_contact_xyz';
testName = 'test_'; 

% testID = 1;  numberOfDataSet = 10; numberOfDataSubSet = 5; 


% testID = 2;  numberOfDataSet = 8; numberOfDataSubSet = 5; 
% 

% testID = 3;  numberOfDataSet = 1; numberOfDataSubSet = 5; 


% testID = 4;  numberOfDataSet = 1; numberOfDataSubSet = 4; 


% testID = 5;  numberOfDataSet = 1; numberOfDataSubSet = 9; 


testID = 6;  numberOfDataSet = 1; numberOfDataSubSet = 1; 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%  EXTRACTION LOOP:   %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


WIRES_DATA_FOLDER = "/media/riccardo/87060f92-ea5c-42c7-bb3e-5662219d0bd0/projects/wires"
testFolderPath = strcat(WIRES_DATA_FOLDER,'/machine_learning_data/',testFolderName);
pathName = strcat(testFolderPath, '/',testName);  
datafolder = strcat(pathName,num2str(testID));
dataPathName = strcat(datafolder,'/allDataMat/');
extractTopic(datafolder,topics);
assignin('base',strcat('atift_',num2str(testID)),atift)
assignin('base',strcat('tactile_',num2str(testID)),tactile)
assignin('base',strcat('schunk_',num2str(testID)),schunk_pg70_joint_setpoint) 
 
     
if strcmp(RangeMode,'manual') 
    splitList = zeros(numberOfDataSet,2);
    index = 1; x1_last = 0; x2_last = 1;
    for j=1:numberOfDataSet
        plot(tactile(x2_last:end,:));
        display(strcat('SELECT RANGE  n°',num2str(j)))
        [x1,~] = ginput(1) 
        [x2,~] = ginput(1) 
        x1_last = x1 + x2_last-1;
        x2_last = x2 + x2_last-1;
        plot(tactile(x1_last:x2_last,:));
        for k=1:numberOfDataSubSet 
            [x,~] = ginput(1)
            splitList(index,1) = x1_last+x; % start
            [x,~] = ginput(1)
            splitList(index,2) = x1_last+x; % end  
            index = index+1;
        end
    end
    filename = input('save new range as...','s'); 
    save(strcat(testFolderPath,'/',filename,'.mat'),'splitList');
else
    filename = input('load range from...','s');   
    load(strcat(testFolderPath,'/',filename,'.mat'));
end
 

for j=1:numberOfDataSet*numberOfDataSubSet
    min = round(splitList(j,1));
    max = round(splitList(j,2));

    dat_atift = atift(min:max,:);
    name_atift = strcat('atift_',num2str(testID),'_dat_',num2str(j))
    assignin('base',name_atift,dat_atift) 
    save(strcat(dataPathName,name_atift,'.mat'),name_atift);

    dat_tactile = tactile(min:max,:);
    name_tactile = strcat('tactile_',num2str(testID),'_dat_',num2str(j))
    assignin('base',name_tactile,dat_tactile) 
    save(strcat(dataPathName,name_tactile,'.mat'),name_tactile);

    dat_schunk = schunk_pg70_joint_setpoint(min:max,:);
    name_schunk = strcat('schunk_',num2str(testID),'_dat_',num2str(j))
    assignin('base',name_schunk,dat_schunk);
    save(strcat(dataPathName,name_schunk,'.mat'), name_schunk);

end


for j=1:numberOfDataSet*numberOfDataSubSet
    atift_data = eval(strcat('atift_',num2str(testID),'_dat_',num2str(j)));
    tactile_data = eval(strcat('tactile_',num2str(testID),'_dat_',num2str(j))); 
    
    subplot(1,2,1);   plot(tactile_data);
    subplot(1,2,2);   plot(atift_data);
    [x1,~] = ginput(1)
    assignin('base',strcat('biasIndex1_',num2str(testID),'_',num2str(j)),round(x1));
    [x2,~] = ginput(1)
    assignin('base',strcat('biasIndex2_',num2str(testID),'_',num2str(j)),round(x2));

    atift_bias =  atift_data(x1:x2,:);
    tactile_bias =  tactile_data(x1:x2,:); 

    assignin('base',strcat('bias_force_avg_',num2str(testID),'_',num2str(j)),mean(atift_bias)); 
    assignin('base',strcat('bias_tactile_avg_',num2str(testID),'_',num2str(j)),mean(tactile_bias));  

end  

   
datafolder = strcat(pathName,num2str(testID));
dataPathName = strcat(datafolder,'/allDataMat/');
save(strcat(dataPathName,'allData.mat'),'*_dat_*','bias_*') 

close all
 