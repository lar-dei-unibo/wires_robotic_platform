function [ output_args ] = extractTopic( folderName, topicsCellName) 

    for i = 1:length(topicsCellName) 
        topic = cell2mat(topicsCellName(i)); 
        try
            file_path = fullfile(folderName,strcat(topic,'.txt')); 
        catch e
            display(e)
            file_path = strcat(folderName,'/',strcat(topic,'.txt'));
        end
        data = dlmread(file_path);
        dataName = strrep(topic,'#','_');
        assignin('base',dataName,data)
    end

end

