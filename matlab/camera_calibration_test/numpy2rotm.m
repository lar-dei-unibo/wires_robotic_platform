function [ rotm ] = numpy2rotm( na )
    p = na(1:3);
    q = na(4:end);
    q = q/norm(q);
    q = circshift(q,1);
    rotm = eye(4);
    rotm(1:3,1:3) = quat2rotm(q);
    rotm(1:3,4) = p';
end

