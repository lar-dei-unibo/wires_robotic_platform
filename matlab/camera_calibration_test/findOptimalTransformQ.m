function [ e ] = findOptimalTransformQ(x, robot_poses,pattern_poses )
%FINDOPTIMALTRANSFORM Summary of this function goes here
%   Detailed explanation goes here
    e = 0;
    step = 1;
    
    % Current Transformation Matrix from x point 
    p_find = [x(1) x(2) x(3)]';
    q_find = [x(7) x(4) x(5) x(6)];
    q_find = quatnormalize(q_find);
    rot_find = quat2rotm(q_find);
    T_find = [rot_find p_find; 0 0 0 1];
    
    T_k = [1,0,0,0.03;0,1,0,0;0,0,1,0.0;0,0,0,1];
    %T_k= eye(4);
    matches = length(robot_poses);
    for i=1:step:matches
        for j=1:step:matches
            if i ~= j
                T1_i = robot_poses{i};
                T2_i = pattern_poses{i};
                T1_j = robot_poses{j};
                T2_j = pattern_poses{j};
                Tr_i = ((T1_i * T_find )*T2_i)* T_k;
                Tr_j = ((T1_j * T_find )*T2_j)* T_k;
                pr_i = Tr_i(1:3,4);
                pr_j = Tr_j(1:3,4);
                rot_i = Tr_i(1:3,1:3);
                rot_j = Tr_j(1:3,1:3);
                qa = rotm2quat(rot_i)';
                qd = rotm2quat(rot_j)';
                
                e = e + norm( (pr_j - pr_i).^2);
                e_o=qa(1)*qd(2:4)-qd(1)*qa(2:4)-skew(qd(2:4))*qa(2:4);
                %e = e+ norm(e_o);
            end
        end
    end
end

