images = imageSet('/home/daniele/Desktop/temp/robot_stereo_output_frames/calibration')

squareSizeInMM = 28;
imageSize = [640,480];

camera_params = cameraParamsGood;
imageFileNames = images.ImageLocation;

images = uint8(zeros(480,640,3,length(imageFileNames)));

for i=1:length(imageFileNames)
    img = imread(imageFileNames{i});
    undist= undistortImage(img,camera_params);
    images(:,:,:,i) = undist(:,:,:);
end
 
[imagePoints, boardSize,imageUsed] = detectCheckerboardPoints(images);

poseFileNames = {};
ee_poses = {};
for k=1:length(imageFileNames)
    if imageUsed(k)==0
       display(strcat('Unused Image:',num2str(k)))
    else
        [pathstr,name,ext] = fileparts(imageFileNames{k}) ;
        chunks = strsplit(name,'_')
        number = chunks(2);
        posefile = fullfile(pathstr,strcat('pose_',number,'.txt'));
        poseFileNames{end+1} = posefile;
        pose_7 = dlmread(posefile{1});
        p = pose_7(1:3)';
        q = pose_7(4:7);
        q = [q(4) q(1) q(2) q(3)];
        q = q / norm(q);
        rotm = quat2rotm(q);
        T = [rotm p; 0 0 0 1];
        ee_poses{end+1} = T;
    end
end

for k=1:length(imageFileNames)
   if imageUsed(k)==0
       display(strcat('Unused Image:',num2str(k)))
   end
end


worldPoints = generateCheckerboardPoints(boardSize,squareSizeInMM);


cameraParams = estimateCameraParameters(imagePoints,worldPoints,'WorldUnits','mm',...
    'EstimateSkew', false, 'EstimateTangentialDistortion', true, ...
    'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'millimeters', ...
    'InitialIntrinsicMatrix', [], 'InitialRadialDistortion', [], ...
    'ImageSize', [imageSize(2), imageSize(1)]);

scale = 1000
pattern_poses = {};
for k=1:length(cameraParams.RotationMatrices)
   p = cameraParams.TranslationVectors(k,:)';
   p = p / 1000;
   rot = cameraParams.RotationMatrices(:,:,k);
   T = [inv(rot) p; 0 0 0 1];
   pattern_poses{end+1} = T;
end

clearvars scale rot T boardSize chunks ext imageFileNames imagePoints images imageSize imageUsed
clearvars k name number p pathstr pose_7 posefile 
clearvars q rotm squareSizeInMM worldPoints img undist i