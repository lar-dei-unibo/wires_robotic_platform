images = imageSet('/tmp/saved_frames')

output_path = '/tmp/gino'

imageFileNames = images.ImageLocation;
for k=1:length(imageFileNames)
    
   img = imread(    imageFileNames{k});
   undistort = undistortImage(img,cameraParams);
   imshowpair(img,undistort,'montage');
   [pathstr,name,ext] = fileparts(imageFileNames{k}) ;
   
   output_file = fullfile(output_path,strcat(name,'.png'));
   imwrite(undistort,output_file);
   %pause(1);
   
end