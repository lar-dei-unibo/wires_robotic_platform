res = [0.14891968599592767	0.006959500332911029	-0.18617755312177892	-8.426717517186548	8.577468706518122	0.1412713086747381	-0.1439638828702631]
q = res(4:end);
q = q / norm(q);

res(4:end) = q;

res

p = res(1:3);

rot = quat2rotm(circshift(res(4:end),1))

rot = rot * rotz(-0,'deg')

q = rotm2quat(rot)
q = circshift(q,-1)
res(4:end) = q;
res