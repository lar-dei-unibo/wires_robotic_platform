
%x0 = [0.0 0.0 0.0 -1.8844968413939551	1.904904419745577	0.025082143021258493	-0.061556676364796385];
prev_results = [0.14873234303461205	0.006798595776228452	-0.1868389196100798	-6.453807146259653	6.569907206807896	0.10474438852587295	-0.10858469949179621]

estimated_camera = numpy2rotm(prev_results)


x0 = [0 0 0 0 0 0 1];
trans_opt = 0.03;
min_x = [-trans_opt, -trans_opt,-trans_opt, -Inf, -Inf,-Inf,0];
max_x = [+trans_opt, +trans_opt, +trans_opt, Inf,Inf,Inf,1];
%ee_poses

f = @(x)findOptimalTransformQRefinement(x,ee_poses,pattern_poses,estimated_camera);

%optimizeCameraPose(x0,min_x,max_x);

%x2_0 = [0 0 0 3.1416 0 0]
%f2 = @(x)findOptimalTransformV(x,ee_poses,pattern_poses);
%min_x2 = [-0.9, -0.9, -0.9, -Inf, -Inf,-Inf];
%max_x2 = [0.9, 0.9, 0.9, Inf,Inf,Inf];

%result = [0.09131591676464931	0.023330268359173824	-0.19437327402734972	-0.7408449656427065	0.7505081899194715	0.01462135745716728	-0.01655531561119271]
