'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/view2', {
        templateUrl: 'view2/view2.html',
        controller: 'View2Ctrl'
    });
}])

.controller('View2Ctrl', ["$scope", "$interval", "$q", 'RosProxy', function($scope, $interval, $q, RosProxy) {

    console.log("VIEW2 CREATED!");

    $scope.current_sfm = RosProxy.getSelectedSFM();

    $scope.getCurrentState = function() {
        RosProxy.getCurrentState($scope.current_sfm).then(function(v) {
            $scope.$broadcast('GRAPH_SET_ACTIVE', { id: 2, name: v.name });
        })
    };

    $scope.updateTimer = $interval($scope.getCurrentState, 250);

    RosProxy.getSFMData($scope.current_sfm).then(function(v) {
        $scope.$broadcast('GRAPH_CREATE', { id: 2, data: v });
    })

    /**Messages */
    $scope.$on(
        "$destroy",
        function(event) {
            $interval.cancel($scope.updateTimer);
        }
    );
}]);