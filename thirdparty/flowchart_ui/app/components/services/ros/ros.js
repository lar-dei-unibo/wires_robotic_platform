angular.module('ros', ['ros'])

.factory('Ros', ['$q', 'Logger', function($q, Logger) {
    var self = this;

    self.ros = null;

    /**
     * Creates Ros Bridge
     */
    self.initializeRosBridge = function(host = 'localhost', port = 9090) {
        if (self.ros != null) self.ros.close();


        var deferred = $q.defer();

        self.ros = new ROSLIB.Ros({
            url: 'ws://' + host + ':' + port
        });

        self.ros.on('connection', function() {
            Logger.log('Connected to websocket server.');
            deferred.resolve(true);
        });

        self.ros.on('error', function(error) {
            Logger.log('Error connecting to websocket server: ', error);
            deferred.reject(true);
        });

        self.ros.on('close', function() {
            Logger.log('Connection to websocket server closed.');
        });

        return deferred.promise;
    };

    /**
     * Creates a Ros generic Topic
     */
    self.createTopic = function(topic_name, message_type) {
        if (self.ros == null) Logger.error("No Ros Connection!");
        var topic = new ROSLIB.Topic({
            ros: self.ros,
            name: topic_name,
            messageType: message_type
        });

        return topic;
    };

    /**
     * Wrap Object in a Ros Message
     */
    self.createMessageFromObject = function(obj) {
        return new ROSLIB.Message(obj);
    };



    return self;
}]);