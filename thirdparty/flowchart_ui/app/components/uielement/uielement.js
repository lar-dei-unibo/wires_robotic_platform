'use strict';

angular.module('myApp.uielement-directive', [])

.directive('uielement', ['$compile', '$rootScope', function($compile, $rootScope) {
    return {
        restrict: 'A',
        scope: true,

        link: function($scope, element, attrs) {
            /**
             * Events
             */
            $scope.events = [];
            $scope.events.push("click");
            $scope.events.push("dblclick");
            $scope.events.push("mousedown");
            $scope.events.push("mouseup");
            $scope.events.push("mousemove");
            $scope.events.push("change");
            $scope.events.push("toggle");
            $scope.events.push("keypress");
            $scope.events.push("keydown");

            /**
             * 
             */
            $scope.manageEvent = function(event, data) {
                var value, value_string;
                if (typeof data === 'string') {
                    value = 0.0;
                    value_string = data;
                } else {
                    value = +data;
                    value_string = "";
                }
                $rootScope.$broadcast("uievent", { name: $scope.uiname, type: $scope.uitype, event: event, value: value, value_string: value_string });
            };

            /**
             * Attributes
             */
            $scope.dragging = false;

            var handle = attrs.uielement;
            $scope.uiname = handle.split("#")[1];
            $scope.uitype = handle.split("#")[0];


            $scope.change = function() {
                $(element).trigger("change");
            };

            element.addClass("uielement");
            // $compile(element)($scope);
            console.log("Name", $scope.uiname);
            $(element).bind($scope.events.join(" "),
                function(e) {
                    if (e.type == "mousedown") {
                        $scope.dragging = true;
                    } else if (e.type == "mouseup") {
                        $scope.dragging = false;
                    }
                    //console.log($scope.uiname, e.type);
                    if ($scope.uitype == "BUTTON") {
                        if (e.type == 'mouseup') {
                            $scope.repeating_command = false;
                            clearTimeout($scope.repeating_command_timer);
                        }
                        if (e.type == 'click' || e.type == 'dblclick' || e.type == 'mousedown') {
                            $scope.manageEvent(e.type, null);
                            if ($scope.dragging) {
                                $scope.draggin_repeat_time = $scope.repeating_command ? 50 : 500;
                                $scope.repeating_command_timer = setTimeout(function() {
                                    if ($scope.dragging) {
                                        $scope.repeating_command = true;
                                        $(element).trigger('click');
                                    }
                                }, $scope.draggin_repeat_time);
                            }
                        }

                    }
                    if ($scope.uitype == "CHECK") {
                        if (e.type == 'click') {
                            $scope.manageEvent("change", $scope.uidata);
                        }
                    }
                    if ($scope.uitype == "SLIDER") {
                        if (e.type == 'keydown') {
                            console.log(e);
                        }
                        if ((e.type == 'mousemove' && $scope.dragging) || e.type == 'click' || e.type == 'keypress') {
                            $scope.manageEvent("change", $scope.uidata);
                        }
                    }
                    if ($scope.uitype == "SELECT") {
                        if (e.type == 'change') {
                            $scope.manageEvent("change", $scope.uidata);
                        }
                    }
                    if ($scope.uitype == "SWITCH") {
                        if (e.type == 'change') {
                            $scope.manageEvent("change", $scope.uidata);
                        }
                    }
                    if ($scope.uitype == "RADIO") {
                        if (e.type == 'change') {
                            $scope.manageEvent("change", $scope.uidata);
                        }
                    }

                });
        }
    };
}]);