DEBUG_TEST = null;

'use strict';
angular.module('graphview-directive', [])

.directive('graphView', [function() {
    return {
        templateUrl: 'components/graphview/graphview.html',
        // CONTROLLER
        controller: function($scope, $element) {
            $scope.customer = {
                name: 'Naomi',
                address: '1600 Amphitheatre'
            };

            DEBUG_TEST = $scope;
            /**
             * Attributes
             */
            $scope.container = jQuery(".graph-container");
            $scope.container_element = $scope.container[0];
            $scope.graph_model = { vertices: {}, edges: {} };
            $scope.style = {
                background: "white",
                color_normal: "#039BE5",
                color_active: "#4CAF50",
                color_warning: "#FBC02D",
                color_leaf: "#212121",
                textcolor_dark: "#212121",
                textcolor_light: "#FAFAFA",
                stroke_color: "#0277BD",
                shape_state: mxConstants.SHAPE_RECTANGLE,
                shape_source: mxConstants.SHAPE_ELLIPSE,
                shape_sink: mxConstants.SHAPE_ELLIPSE,
                shape_orphan: mxConstants.SHAPE_HEXAGON
            };
            $scope.default_styles = {
                vertex: "simple",
                edge: "simple"
            };

            /**
             * Set Size of canvas
             */
            $scope.setSize = function(w, h) {
                $scope.container.width(w);
                $scope.container.height(h);
                $scope.container.css("background", "lightskyblue");
            };

            /**
             * Check mxGraph Library
             */
            $scope.checkLibrary = function() {
                if (!mxClient.isBrowserSupported()) {
                    mxUtils.error('Browser is not supported!', 200, false);
                    return false;
                } else {
                    return true;
                }
            };

            /**
             * Initialize MxGraph
             */
            $scope.initialize = function() {
                if ($scope.checkLibrary()) {
                    mxEvent.disableContextMenu($scope.container_element);
                    $scope.graph = new mxGraph($scope.container_element);
                    $scope.graph.setTolerance(20);
                    $scope.graph.setConnectable(true);
                    $scope.graph.setPanning(true);
                    $scope.graph.panningHandler.useLeftButtonForPanning = true;
                    $scope.graph.setAllowDanglingEdges(true);
                    $scope.resetStyle();
                }
            };

            $scope.setStyleForItem = function(item, style) {
                $scope.graph.getModel().beginUpdate();
                for (var s in style) {

                    if (s == "perimeter") continue;
                    console.log(s, style[s], item);
                    $scope.graph.setCellStyles(s, style[s], [item.data]);
                }
                $scope.graph.getModel().endUpdate();
            };

            $scope.resetStyle = function() {

                var style = $scope.graph.getStylesheet().getDefaultVertexStyle();
                style[mxConstants.STYLE_FONTSIZE] = 11;
                style[mxConstants.STYLE_FONTCOLOR] = 'white';
                style[mxConstants.STYLE_STROKE_OPACITY] = 0;
                style[mxConstants.STYLE_FILLCOLOR] = $scope.style.color_normal;
                style[mxConstants.STYLE_ROUNDED] = true;
                style[mxConstants.STYLE_SHADOW] = false;
                style[mxConstants.STYLE_FONTSTYLE] = 0;

                var style = $scope.graph.getStylesheet().getDefaultEdgeStyle();
                style[mxConstants.STYLE_EDGE] = mxEdgeStyle.EntityRelation;
                style[mxConstants.STYLE_STARTARROW] = mxConstants.ARROW_OVAL
                style[mxConstants.STYLE_ENDARROW] = mxConstants.ARROW_BLOCK;
                style[mxConstants.STYLE_STROKECOLOR] = $scope.style.stroke_color;
                style[mxConstants.STYLE_ROUNDED] = true;
                style[mxConstants.STYLE_SHADOW] = false;
                style[mxConstants.STYLE_STROKEWIDTH] = 2;
                style[mxConstants.STYLE_VERTICAL_LABEL_POSITION] = mxConstants.ALIGN_BOTTOM;
                style[mxConstants.STYLE_LABEL_BACKGROUNDCOLOR] = $scope.style.background;
                style[mxConstants.STYLE_NOLABEL] = 0;
                style[mxConstants.STYLE_OPACITY] = 100;
                style[mxConstants.STYLE_FONTCOLOR] = "#009688";
                style[mxConstants.STYLE_FONTFAMILY] = "Roboto-thin";

                //Canvas
                $scope.container.css("background", $scope.style.background);
            };

            $scope.oragnizeGraph = function(type_name, subgraph) {
                subgraph = subgraph || $scope.graph.getDefaultParent();
                type_name = type_name || "hierarchy";
                if (type_name == "fast") {
                    var layout = new mxFastOrganicLayout($scope.graph, true);
                    layout.resizeParent = true;
                    layout.forceConstant = 100;
                    layout.levelDistance = 1180;
                    layout.resetEdges = false;
                    layout.disableEdgeStyle = false;
                    layout.maxIterations = 1000;
                    layout.radius = 100;
                }
                if (type_name == "hierarchy") {
                    layout = new mxHierarchicalLayout($scope.graph, mxConstants.DIRECTION_WEST)
                }
                layout.execute(subgraph);
            };

            /**
             * Creates a Vertex in the Model and in the View
             */
            $scope.addVertex = function(options) {
                var parent = options.parent || $scope.graph.getDefaultParent();
                var id = options.id || null;
                var value = options.name || "default_name";
                var x = options.x || 0;
                var y = options.y || 0;
                var w = options.w || 50;
                var h = options.h || 50;
                var cell = $scope.graph.insertVertex(parent, id, value, x, y, w, h);
                var item = {
                    name: value,
                    data: cell,
                    in_edges: [],
                    out_edges: []
                };
                $scope.graph_model.vertices[value] = item;
                return cell;
            };

            /**
             * Creates an Edge in the Model and in the View
             */
            $scope.addEdge = function(options) {
                var parent = options.parent || $scope.graph.getDefaultParent();
                var id = options.id || null;
                var value = options.name || "default_name";
                var start = options.start;
                var end = options.end;
                if (typeof start === 'string') {
                    start = $scope.getVertexByName(start).data;
                    end = $scope.getVertexByName(end).data;
                }
                var cell = $scope.graph.insertEdge(parent, id, value, start, end);
                var item = {
                    name: value,
                    data: cell,
                    start: start.value,
                    end: end.value,
                    start_data: start,
                    end_data: end
                };
                $scope.graph_model.edges[value] = item;

                var start_vertex = $scope.getVertexByName(start.value);
                var end_vertex = $scope.getVertexByName(end.value);
                start_vertex.out_edges.push(item);
                end_vertex.in_edges.push(item);
                //$scope.setStyleForItem(item, $scope.getStyleForEdgeByName("simple"));
                return cell;
            };


            $scope.restyleByType = function() {
                var vertices = $scope.getAllVertices();
                for (var v in vertices) {
                    var vertex = vertices[v];
                    if (vertex.in_edges == 0 && vertex.out_edges == 0) {
                        $scope.graph.setCellStyles(mxConstants.STYLE_SHAPE, $scope.style.shape_orphan, [vertex.data]);
                        $scope.graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, $scope.style.color_warning, [vertex.data]);
                        $scope.graph.setCellStyles(mxConstants.STYLE_FONTCOLOR, $scope.style.textcolor_dark, [vertex.data]);
                        continue;
                    }
                    if (vertex.in_edges == 0) {
                        $scope.graph.setCellStyles(mxConstants.STYLE_SHAPE, $scope.style.shape_sink, [vertex.data]);
                        $scope.graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, $scope.style.color_leaf, [vertex.data]);
                        continue;
                    }
                    if (vertex.out_edges == 0) {
                        $scope.graph.setCellStyles(mxConstants.STYLE_SHAPE, $scope.style.shape_source, [vertex.data]);
                        $scope.graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, $scope.style.color_leaf, [vertex.data]);
                        continue;
                    }
                    $scope.graph.setCellStyles(mxConstants.STYLE_SHAPE, $scope.style.shape_state, [vertex.data]);
                    $scope.graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, $scope.style.color_normal, [vertex.data]);
                }
            }

            /**
             * Sets status of a Vertex
             */
            $scope.setVertexStatus = function(vertex_name, status) {
                var vertex = $scope.getVertexByName(vertex_name);
                if (vertex) {
                    if (status == "active") {
                        $scope.graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, $scope.style.color_active, [vertex.data]);
                    }
                }
            };

            /**
             * Retrieves Vertex by name
             */
            $scope.getVertexByName = function(name) {
                var item = $scope.graph_model.vertices[name];
                return item;
            };

            /**
             * Retrieves Edge by name
             */
            $scope.getEdgeByName = function(name) {
                var item = $scope.graph_model.edges[name];
                return item;
            };

            /**
             * 
             */
            $scope.getAllVertices = function() {
                return $scope.graph_model.vertices;
            }

            /**
             * 
             */
            $scope.getAllEdges = function() {
                return $scope.graph_model.edges;
            }

            $scope.isLeaf = function(vertex_name) {
                var vertex = $scope.getVertexByName(vertex_name);

            };

            /**
             * Clear graph
             */
            $scope.clear = function() {
                var cells = $scope.graph.getModel().cells;
                var toRemove = [];
                for (var index in cells) {
                    if (index > 1) {
                        var cell = cells[index]
                        toRemove.push(cell)
                    }
                }
                $scope.graph.removeCells(toRemove);
                $scope.graph_model = { vertices: {}, edges: {} };
            };

            /**
             * Test Function
             */
            $scope.test = function() {
                $scope.graph_parent = $scope.graph.getDefaultParent();
                $scope.graph.getModel().beginUpdate();
                try {
                    var states = []
                    for (var i = 0; i < 10; i++) {
                        var x = Math.floor((Math.random() * 200));
                        var y = Math.floor((Math.random() * 200));
                        var s = $scope.addVertex({
                            x: x,
                            y: y,
                            w: 60,
                            h: 60,
                            name: "state_" + i
                        });
                        states.push(s);
                    }
                    for (var i = 0; i < 10; i++) {
                        var i1 = Math.floor((Math.random() * 9));
                        var i2 = Math.floor((Math.random() * 9));
                        $scope.addEdge({
                            name: "edge_" + i1 + "_" + i2,
                            start: states[i1],
                            end: states[i2]
                        })
                    }
                } finally {
                    // Updates the display
                    $scope.graph.getModel().endUpdate();
                }
            }

            /// *******************
            /// MESSAGES
            /// *******************

            $scope.$on('GRAPH_CREATE', function(event, args) {
                console.log("GRAPH CREATE", args);
                $scope.clear();
                if (args && args.data) {
                    $scope.graph.getModel().beginUpdate();
                    for (var s in args.data.states) {
                        var state = args.data.states[s];
                        console.log(state.name);
                        $scope.addVertex(state);
                    }
                    for (var e in args.data.edges) {
                        var edge = args.data.edges[e];
                        $scope.addEdge(edge);
                    }
                    $scope.graph.getModel().endUpdate();

                }
                $scope.oragnizeGraph();
                $scope.restyleByType();
            });

            $scope.$on('GRAPH_SET_ACTIVE', function(event, args) {
                console.log("SET ACTIVE", args);
                $scope.restyleByType();
                $scope.setVertexStatus(args.name, "active");
            });

            $scope.$on('GRAPH_CLEAR', function(event, args) {
                console.log(args);
                $scope.clear();
            });

            $scope.initialize();
            //$scope.test();
            $scope.oragnizeGraph();
            $scope.restyleByType();
        }
    };
}]);