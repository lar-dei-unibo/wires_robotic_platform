'use strict';

angular.module('myApp.testpage', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/testpage', {
        templateUrl: 'views/testpage/testpage.html',
        controller: 'TestPageCtrl'
    });
}])

.controller('TestPageCtrl', ["$scope", "$interval", "$timeout", "$q", 'Ros', 'Logger', function($scope, $interval, $timeout, $q, Ros, Logger) {

    console.log("TestPageCtrl CREATED!");

    $scope.uitopic = null;
    Ros.initializeRosBridge('192.168.7.90').then(function() {
        $scope.init();
    })
    $scope.checks = ['x', 'y', 'z', 'roll'];
    $scope.init = function() {

        $scope.uitopic = Ros.createTopic("/ui/events", "wires_robotic_platform/UiEvent");
    };


    /**Messages */
    $scope.$on(
        "$destroy",
        function(event) {

        }
    );

    $scope.$on("uievent", function(e, d) {
        if ($scope.uitopic != null) {
            console.log("UI EVENT", d);
            $scope.uitopic.publish(Ros.createMessageFromObject(d));
        }
    })
}]);