'use strict';

angular.module('myApp.views.robotjog', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/robotjog', {
        templateUrl: 'views/robotjog/robotjog.html',
        controller: 'RobotJogCtrl'
    });
}])

.controller('RobotJogCtrl', ["$scope", "$interval", "$timeout", "$q", 'Ros', 'Logger', '$mdColorPalette', function($scope, $interval, $timeout, $q, Ros, Logger, $mdColorPalette) {

    console.log("RobotJogCtrl CREATED!");
    $scope.colors = Object.keys($mdColorPalette);

    var jogTIconPrefix = "fa-chevron-circle-";
    var jogQIconPrefix = "fa-rotate-";
    $scope.jogcommands = [
        { name: "jog_x_up", icon: jogTIconPrefix + "up", color: "red" },
        { name: "jog_y_up", icon: jogTIconPrefix + "up", color: "green" },
        { name: "jog_z_up", icon: jogTIconPrefix + "up", color: "blue" },
        { name: "--" },
        { name: "jog_roll_up", icon: jogQIconPrefix + "left", color: "red-800" },
        { name: "jog_pith_up", icon: jogQIconPrefix + "left", color: "green-800" },
        { name: "jog_yaw_up", icon: jogQIconPrefix + "left", color: "blue-800" },

        { name: "jog_x_down", icon: jogTIconPrefix + "down", color: "red" },
        { name: "jog_y_down", icon: jogTIconPrefix + "down", color: "green" },
        { name: "jog_z_down", icon: jogTIconPrefix + "down", color: "blue" },
        { name: "--" },
        { name: "jog_roll_up", icon: jogQIconPrefix + "right", color: "red-800" },
        { name: "jog_pith_up", icon: jogQIconPrefix + "right", color: "green-800" },
        { name: "jog_yaw_up", icon: jogQIconPrefix + "right", color: "blue-800" }
    ];
    $scope.uitopic = null;
    Ros.initializeRosBridge('192.168.7.90').then(function() {
        $scope.init();
    })
    $scope.checks = ['x', 'y', 'z', 'roll'];
    $scope.init = function() {

        $scope.uitopic = Ros.createTopic("/ui/events", "wires_robotic_platform/UiEvent");

        $scope.uifeedback = Ros.createTopic("/ui/feedback", "wires_robotic_platform/UiFeedback");
        $scope.uifeedback.subscribe($scope.uifeedback_callback);
    };

    $scope.uifeedback_callback = function(msg) {
        $scope.uifeedback = msg;
        console.log(msg);
    };

    /**Messages */
    $scope.$on(
        "$destroy",
        function(event) {

        }
    );

    $scope.$on("uievent", function(e, d) {
        if ($scope.uitopic != null) {
            console.log("UI EVENT", d);
            $scope.uitopic.publish(Ros.createMessageFromObject(d));
        }
    })
}]);