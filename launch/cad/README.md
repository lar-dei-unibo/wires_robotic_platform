# Usage

Set the folder path and the switchgear name in 'component_scanner.launch'

Run the command 'roslaunch wires_robotic_platform component_scanner.launch' to launch the node

Call 'rosservice call /part_tf_service "{}" ' to switch to the next component TF

The service will return 'resp.success = True' and 'resp.message = PartCadID' in case a new part is available, 'resp.success = False' and 'resp.message = "Part scan completed"' in case the scan is completed.

Call 'rosservice call /connection_tf_service "{}" ' to switch to the net connection TF (both hole and screw)

The service will return 'resp.success = True' and 'resp.message = PartCadID' in case a new connection is available, 'resp.success = False' and 'resp.message = "connection scan completed"' in case the connection list is completed.
